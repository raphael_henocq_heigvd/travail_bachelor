QMAKE_PROJECT_NAME = TFA_simulator

QT += widgets
QT += core
qtHaveModule(printsupport): QT += printsupport

# CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT
# CONFIG += release
CONFIG += debug
CONFIG += c++11

RESOURCES += src/images.qrc

HEADERS += src/Simulator.hpp
HEADERS += src/Component.hpp
HEADERS += src/Constants.hpp
HEADERS += src/DataTypes.hpp
HEADERS += src/Cell.hpp
HEADERS += src/IOPin.hpp
HEADERS += src/View.hpp

SOURCES += src/Simulator.cpp
SOURCES += src/Cell.cpp
SOURCES += src/IOPin.cpp
SOURCES += src/View.cpp
SOURCES += src/main.cpp
