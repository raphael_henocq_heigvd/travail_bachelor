#ifndef CELL_HPP_
#define CELL_HPP_

#include <QColor>
#include <QGraphicsItem>
#include <QDebug>
#include <QQueue>

#include "Component.hpp"
#include "DataTypes.hpp"
#include "Constants.hpp"

#define SELECTION_MASK        3 /* Ignore the first bit (enable), take the other 2 */
#define MASK_SIZE             3
#define MUX_ENABLE            4
#define IN_NO                 5 /* Mux list: N S W E FU */
#define MUX_NO                7 /* IN list : N S W E FU1 FU2 FUSEL */

#define N_IN                  13
#define N_OUT                 14
#define S_IN                  15
#define S_OUT                 16
#define W_IN                  17
#define W_OUT                 18
#define E_IN                  19
#define E_OUT                 20

class Cell : public Component {
    Q_OBJECT
public:
    Cell(const unsigned int row, const unsigned int col);

    void setConfig(const CELL_CONF_TYPE config);
    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *item,
               QWidget *widget);
    QRectF boundingRect() const;

    void setData(const int data, const unsigned int D)
    {
        // qDebug() << "CELL in" << row << "," << col
        //   << ", set data" << data << "SOURCE DIRECTION =" << decodeDir(D);

        // for (unsigned int m = 0; m < MUX_NO; ++m) {
        //  for (unsigned int i = 0; i < IN_NO; ++i) {
        //      fprintf(stderr, "%d\t", MUXConfig[m].in[i]);
        //  }
        //  fprintf(stderr, "\n");
        // }

        /* Sanity check */
        unsigned int count = 0;
        for (unsigned int m = 0; m < MUX_NO; ++m) {
            if (MUXConfig[m].in[D] != 0)
                count++;
        }
        Q_ASSERT(count > 0);

        /* Direct propagation */
        for (unsigned int m = 0; m <= S; ++m) {
            if (MUXConfig[m].in[D] != 0) {
                unsigned int ddd;
                switch (m) {
                case N:
                    ddd = S;
                    break;
                case S:
                    ddd = N;
                    break;
                case W:
                    ddd = E;
                    break;
                case E:
                    ddd = W;
                    break;
                default:
                    qDebug() << "ddd panic!! ?????????????????";
                    break;
                }

                // qDebug() << "DIRECT PROPAGATION of data" << data
                //   << "from direction" << decodeDir(ddd);
                emit OutProduced(row, col, ddd, data);
            }
        }

        /* Setting data for op */
        for (unsigned int m = 0; m < 2; ++m) {
            if (MUXConfig[m+FU1].in[D] != 0) {
                // qDebug() << "## SETTING DATA" << data
                //   << "FOR OP," << m+FU1 << "(" << m << ")";
                OPfifos[m].enqueue(data);
            }
        }

        /* FU sel */
        if (cell_sel >= 0 && D == (unsigned int)cell_sel) {
            FUSELfifo.enqueue(data);
        }

        // qDebug() << "FIFO[0]:" << OPfifos[0].size();
        // qDebug() << "FIFO[1]:" << OPfifos[1].size();

        bool cond;
        if (cell_sel >= 0) {
            cond = !OPfifos[0].isEmpty() && !OPfifos[1].isEmpty() &&
                !FUSELfifo.isEmpty();
        } else {
            cond = !OPfifos[0].isEmpty() && !OPfifos[1].isEmpty();
        }
        while (cond) {
            qDebug() << "EXECUTING ONE OP!";
            int produced;

            qDebug() << "Cell " << row << "," << col << ", done OP ";
            int op1 = OPfifos[0].dequeue();
            int op2 = OPfifos[1].dequeue();
            unsigned int sel_pop;
            switch (cell_op) {
            case OP_OR:
                qDebug() << "-- > OR";
                produced = op1 | op2;
                break;
            case OP_AND:
                qDebug() << "-- > AND";
                produced = op1 & op2;
                break;
            case OP_SUM:
                qDebug() << "-- > SUM";
                produced = op1 + op2;
                break;
            case OP_SUB:
                qDebug() << "-- > SUB";
                produced = op1 - op2;
                break;
            case OP_MUL:
                qDebug() << "-- > MUL";
                produced = op1 * op2;
                break;
            case OP_DIV:
                qDebug() << "-- > DIV";
                Q_ASSERT(op2 != 0);
                if (op2 < op1) {
                    qDebug() << "Error! Division by zero!";
                    qDebug() << "Op in: (1) = "
                             << qPrintable("0x"+QString::number(op1, 16))
                             << ", (2) = "
                             << qPrintable("0x"+QString::number(op2, 16));
                    return;
                }
                produced = op1 / op2;
                break;
            case OP_MOD:
                qDebug() << "-- > MOD";
                Q_ASSERT(op2 != 0);
                if (op2 < op1) {
                    qDebug() << "Error! Division by zero (mod)!";
                    qDebug() << "Op in: (1) = "
                             << qPrintable("0x"+QString::number(op1, 16))
                             << ", (2) = "
                             << qPrintable("0x"+QString::number(op2, 16));
                    return;
                }
                produced = op1 % op2;
                break;
            case OP_EQU:
                qDebug() << "-- > EQU";
                produced = op1 == op2;
                break;
            case OP_NEQ:
                qDebug() << "-- > NEQ";
                produced = op1 != op2;
                break;
            case OP_GEQ:
                qDebug() << "-- > GEQ";
                produced = op1 >= op2;
                break;
            case OP_GRE:
                qDebug() << "-- > GRE";
                produced = op1 > op2;
                break;
            case OP_SEL:
                sel_pop = FUSELfifo.dequeue();
                qDebug() << "-- > SEL ( sel_pop = "
                         << sel_pop << ")";
                if (sel_pop == 0) {
                    produced = op2;
                } else {
                    produced = op1;
                }
                break;
            default:
                qDebug() << "Invalid operation selected! [ "
                         << cell_op << " ]";
                produced = 0;
            }

            qDebug() << "Op in: (1) = "
                     << qPrintable("0x"+QString::number(op1, 16))
                     << ", (2) = "
                     << qPrintable("0x"+QString::number(op2, 16))
                     << ", res = "
                     << qPrintable("0x"+QString::number(produced, 16));

            if (FU1_const) {
                qDebug() << "FU1 in const, repushing it";
                OPfifos[0].enqueue(op1);
            }

            if (FU2_const) {
                qDebug() << "FU2 in const, repushing it";
                OPfifos[1].enqueue(op2);
            }

            qDebug() << "Result:"
                     << qPrintable("0x"+QString::number(produced, 16));
            // /* Send data to anyone listening */
            for (unsigned int m = 0; m <= S; ++m) {
                // qDebug() << m << ":" << MUXConfig[m].in[FU];
                if (MUXConfig[m].in[FU] != 0) {
                    unsigned int ddd;
                    switch (m) {
                    case N:
                        ddd = S;
                        break;
                    case S:
                        ddd = N;
                        break;
                    case W:
                        ddd = E;
                        break;
                    case E:
                        ddd = W;
                        break;
                    default:
                        qDebug() << "ddd panic!! ?????????????????";
                        break;
                    }
                    // qDebug() << "Producing from direction"
                    //   << decodeDir(ddd);
                    emit OutProduced(row, col, ddd, produced);
                }
            }
            if (cell_sel >= 0) {
                cond = !OPfifos[0].isEmpty() &&
                    !OPfifos[1].isEmpty() &&
                    !FUSELfifo.isEmpty();
            } else {
                cond = !OPfifos[0].isEmpty() &&
                    !OPfifos[1].isEmpty();
            }
        }
    };

signals:
    void OutProduced(unsigned int r, unsigned int c, unsigned int d,
                     int data);
    void PinEnabled(unsigned int row, unsigned int col, unsigned int dir);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    QString decodeDir(unsigned int dir)
    {
        if (dir == N)
            return QString::number(dir)+QString(" = N");
        if (dir == S)
            return QString::number(dir)+QString(" = S");
        if (dir == W)
            return QString::number(dir)+QString(" = W");
        if (dir == E)
            return QString::number(dir)+QString(" = E");
        return QString::number(dir)+QString(" = ???");
    }

    QVector< QQueue< int> > OPfifos;
    QQueue< unsigned int> FUSELfifo;
    unsigned int cell_op;
    int cell_sel;
    unsigned char FU1_const;
    unsigned char FU2_const;
    bool active_cell;

    /**
     * struct out_conf - Configuration of the mux of an output pin
     */
    typedef struct MUXConf {
        unsigned char in[IN_NO];

        MUXConf() {
            for (unsigned int i = 0; i < IN_NO; ++i) {
                in[i] = 0;
            }
        }

        /**
         * active() - Returns true if at least one active line
         */
        bool active() {
            int count = 0;
            for (unsigned int i = 0; i < IN_NO; ++i) {
                count += in[i];
            }
            return count != 0;
        }
    } MUXConf;

    QVector< MUXConf > MUXConfig;

    int x;
    int y;
    const unsigned int row;
    const unsigned int col;
};

#define N_IN_COLOR QColor(255,   0,   0, 200)
#define S_IN_COLOR QColor(  0, 255,   0, 200)
#define W_IN_COLOR QColor(  0,   0, 255, 200)
#define E_IN_COLOR QColor(  0, 255, 255, 200)
#define FU_OUT_COLOR QColor(255, 125, 0, 200)

#define DRAW_OP_BOX(PAINTER)                                \
    do {                                                    \
        PAINTER->drawRect(QRect(x+(CELL_W/2)-(OP_BOX_W/2),  \
                                y+(CELL_H/2)-(OP_BOX_H/2),  \
                                OP_BOX_W, OP_BOX_H));       \
    } while (0);

#define ACTIVE_OP_BOX(PAINTER)                  \
    do {                                        \
        QPen pen = PAINTER->pen();              \
        QBrush brush = PAINTER->brush();        \
        brush.setStyle(Qt::SolidPattern);       \
        pen.setColor(Qt::darkYellow);           \
        pen.setWidth(2);                        \
        brush.setColor(Qt::yellow);             \
        PAINTER->setPen(pen);                   \
        PAINTER->setBrush(brush);               \
        DRAW_OP_BOX(PAINTER);                   \
        brush.setColor(Qt::darkYellow);         \
        PAINTER->setBrush(brush);               \
    } while (0);

#define CONST_OP_BOX(PAINTER)                   \
    do {                                        \
        QPen pen = PAINTER->pen();              \
        QBrush brush = PAINTER->brush();        \
        brush.setStyle(Qt::SolidPattern);       \
        pen.setColor(Qt::red);                  \
        pen.setWidth(2);                        \
        brush.setColor(Qt::red);                \
        PAINTER->setPen(pen);                   \
        PAINTER->setBrush(brush);               \
        DRAW_OP_BOX(PAINTER);                   \
        brush.setColor(Qt::red);                \
        PAINTER->setBrush(brush);               \
    } while (0);

#define INACTIVE_OP_BOX(PAINTER)                \
    do {                                        \
        QPen pen = PAINTER->pen();              \
        QBrush brush = PAINTER->brush();        \
        brush.setStyle(Qt::SolidPattern);       \
        pen.setColor(Qt::darkGray);             \
        pen.setWidth(2);                        \
        brush.setColor(Qt::lightGray);          \
        PAINTER->setPen(pen);                   \
        PAINTER->setBrush(brush);               \
        DRAW_OP_BOX(PAINTER);                   \
        brush.setColor(Qt::darkGray);           \
        PAINTER->setBrush(brush);               \
    } while (0);

#define ACTIVE_COLOR_BOX(PAINTER, COLOR)        \
    QPen pen = PAINTER->pen();                  \
    QBrush brush = PAINTER->brush();            \
    brush.setStyle(Qt::SolidPattern);           \
    pen.setColor(QColor(255, 120, 0));          \
    pen.setWidth(2);                            \
    brush.setColor(COLOR);                      \
    PAINTER->setPen(pen);                       \
    PAINTER->setBrush(brush);

#define INACTIVE_COLOR_BOX(PAINTER)             \
    QPen pen = PAINTER->pen();                  \
    QBrush brush = PAINTER->brush();            \
    brush.setStyle(Qt::SolidPattern);           \
    pen.setColor(Qt::darkGray);                 \
    pen.setWidth(2);                            \
    brush.setColor(Qt::lightGray);              \
    PAINTER->setPen(pen);                       \
    PAINTER->setBrush(brush);

/* FUx boxes are called out even if they are in... */
#define ACTIVE_FU1_OUT_BOX(PAINTER)                                     \
    do {                                                                \
        QPen pen = PAINTER->pen();                                      \
        QBrush brush = PAINTER->brush();                                \
        brush.setStyle(Qt::SolidPattern);                               \
        pen.setColor(Qt::darkBlue);                                     \
        pen.setWidth(2);                                                \
        PAINTER->setPen(pen);                                           \
        brush.setColor(Qt::darkBlue);                                   \
        PAINTER->setBrush(brush);                                       \
        PAINTER->drawRect(QRect(x+(CELL_W/2)-(OP_BOX_W/4)-OP_PIN_W/2,   \
                                y+(CELL_H/2)-(OP_BOX_H/2),              \
                                OP_PIN_W, OP_PIN_H));                   \
                                } while(0);

#define ACTIVE_FU2_OUT_BOX(PAINTER)                                     \
    do {                                                                \
        QPen pen = PAINTER->pen();                                      \
        QBrush brush = PAINTER->brush();                                \
        brush.setStyle(Qt::SolidPattern);                               \
        pen.setColor(Qt::darkBlue);                                     \
        pen.setWidth(2);                                                \
        PAINTER->setPen(pen);                                           \
        brush.setColor(Qt::darkBlue);                                   \
        PAINTER->setBrush(brush);                                       \
        PAINTER->drawRect(QRect(x+(CELL_W/2)+(OP_BOX_W/4)-OP_PIN_W/2,   \
                                y+(CELL_H/2)-(OP_BOX_H/2),              \
                                OP_PIN_W, OP_PIN_H));                   \
                                } while(0);

#define ACTIVE_FUSEL_OUT_BOX(PAINTER)                       \
    do {                                                    \
        QPen pen = PAINTER->pen();                          \
        QBrush brush = PAINTER->brush();                    \
        brush.setStyle(Qt::SolidPattern);                   \
        pen.setColor(Qt::darkGreen);                        \
        pen.setWidth(2);                                    \
        PAINTER->setPen(pen);                               \
        brush.setColor(Qt::darkGreen);                      \
        PAINTER->setBrush(brush);                           \
        PAINTER->drawRect(QRect(x+(CELL_W/2)-(OP_BOX_W/2),  \
                                y+(CELL_H/2)-OP_PIN_W/2,    \
                                OP_PIN_H, OP_PIN_W));       \
                                } while(0);

#define ACTIVE_FU_OUT_BOX(PAINTER)                                  \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QBrush brush = PAINTER->brush();                            \
        brush.setStyle(Qt::SolidPattern);                           \
        pen.setColor(Qt::darkBlue);                                 \
        pen.setWidth(2);                                            \
        PAINTER->setPen(pen);                                       \
        brush.setColor(Qt::darkBlue);                               \
        PAINTER->setBrush(brush);                                   \
        PAINTER->drawRect(QRect(x+(CELL_W/2)-OP_PIN_W/2,            \
                                y+(CELL_H/2)+(OP_BOX_H/2)-OP_PIN_H, \
                                OP_PIN_W, OP_PIN_H));               \
                                } while(0);

#define INACTIVE_FU1_OUT_BOX(PAINTER)                                   \
    do {                                                                \
        QPen pen = PAINTER->pen();                                      \
        QBrush brush = PAINTER->brush();                                \
        brush.setStyle(Qt::SolidPattern);                               \
        pen.setColor(Qt::darkGray);                                     \
        pen.setWidth(2);                                                \
        PAINTER->setPen(pen);                                           \
        brush.setColor(Qt::darkGray);                                   \
        PAINTER->setBrush(brush);                                       \
        PAINTER->drawRect(QRect(x+(CELL_W/2)-(OP_BOX_W/4)-OP_PIN_W/2,   \
                                y+(CELL_H/2)-(OP_BOX_H/2),              \
                                OP_PIN_W, OP_PIN_H));                   \
                                } while(0);

#define INACTIVE_FU2_OUT_BOX(PAINTER)                                   \
    do {                                                                \
        QPen pen = PAINTER->pen();                                      \
        QBrush brush = PAINTER->brush();                                \
        brush.setStyle(Qt::SolidPattern);                               \
        pen.setColor(Qt::darkGray);                                     \
        pen.setWidth(2);                                                \
        PAINTER->setPen(pen);                                           \
        brush.setColor(Qt::darkGray);                                   \
        PAINTER->setBrush(brush);                                       \
        PAINTER->drawRect(QRect(x+(CELL_W/2)+(OP_BOX_W/4)-OP_PIN_W/2,   \
                                y+(CELL_H/2)-(OP_BOX_H/2),              \
                                OP_PIN_W, OP_PIN_H));                   \
                                } while(0);

#define INACTIVE_FUSEL_OUT_BOX(PAINTER)                     \
    do {                                                    \
        QPen pen = PAINTER->pen();                          \
        QBrush brush = PAINTER->brush();                    \
        brush.setStyle(Qt::SolidPattern);                   \
        pen.setColor(Qt::darkGray);                         \
        pen.setWidth(2);                                    \
        PAINTER->setPen(pen);                               \
        brush.setColor(Qt::darkGray);                       \
        PAINTER->setBrush(brush);                           \
        PAINTER->drawRect(QRect(x+(CELL_W/2)-(OP_BOX_W/2),  \
                                y+(CELL_H/2)-OP_PIN_W/2,    \
                                OP_PIN_H, OP_PIN_W));       \
                                } while(0);

#define INACTIVE_FU_OUT_BOX(PAINTER)                                \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QBrush brush = PAINTER->brush();                            \
        brush.setStyle(Qt::SolidPattern);                           \
        pen.setColor(Qt::darkGray);                                 \
        pen.setWidth(2);                                            \
        PAINTER->setPen(pen);                                       \
        brush.setColor(Qt::darkGray);                               \
        PAINTER->setBrush(brush);                                   \
        PAINTER->drawRect(QRect(x+(CELL_W/2)-OP_PIN_W/2,            \
                                y+(CELL_H/2)+(OP_BOX_H/2)-OP_PIN_H, \
                                OP_PIN_W, OP_PIN_H));               \
                                } while(0);

#define ACTIVE_N_IN_BOX(PAINTER, COLOR)                 \
    do {                                                \
        ACTIVE_COLOR_BOX(PAINTER, COLOR);               \
        PAINTER->drawRect(QRect(x+CELL_W-(1.75*PIN_W),  \
                                y,                      \
                                PIN_W, PIN_H));         \
    } while(0);

#define ACTIVE_N_OUT_BOX(PAINTER)                       \
    do {                                                \
        ACTIVE_COLOR_BOX(PAINTER, QColor(255, 200, 0)); \
        PAINTER->drawRect(QRect(x+(0.75*PIN_W),         \
                                y,                      \
                                PIN_W, PIN_H));         \
    } while(0);

#define ACTIVE_S_IN_BOX(PAINTER, COLOR)         \
    do {                                        \
        ACTIVE_COLOR_BOX(PAINTER, COLOR);       \
        PAINTER->drawRect(QRect(x+(0.75*PIN_W), \
                                y+CELL_H-PIN_H, \
                                PIN_W, PIN_H)); \
    } while(0);

#define ACTIVE_S_OUT_BOX(PAINTER)                       \
    do {                                                \
        ACTIVE_COLOR_BOX(PAINTER, QColor(255, 200, 0)); \
        PAINTER->drawRect(QRect(x+CELL_W-(1.75*PIN_W),  \
                                y+CELL_H-PIN_H,         \
                                PIN_W, PIN_H));         \
    } while(0);

#define ACTIVE_W_IN_BOX(PAINTER, COLOR)         \
    do {                                        \
        ACTIVE_COLOR_BOX(PAINTER, COLOR);       \
        PAINTER->drawRect(QRect(x,              \
                                y+(0.75*PIN_W), \
                                PIN_H, PIN_W)); \
                                } while(0);

#define ACTIVE_W_OUT_BOX(PAINTER)                       \
    do {                                                \
        ACTIVE_COLOR_BOX(PAINTER, QColor(255, 200, 0)); \
        PAINTER->drawRect(QRect(x,                      \
                                y+CELL_H-(1.75*PIN_W),  \
                                PIN_H, PIN_W));         \
                                } while(0);

#define ACTIVE_E_IN_BOX(PAINTER, COLOR)                 \
    do {                                                \
        ACTIVE_COLOR_BOX(PAINTER, COLOR);               \
        PAINTER->drawRect(QRect(x+CELL_W-PIN_H,         \
                                y+CELL_H-(1.75*PIN_W),  \
                                PIN_H, PIN_W));         \
                                } while(0);

#define ACTIVE_E_OUT_BOX(PAINTER)                       \
    do {                                                \
        ACTIVE_COLOR_BOX(PAINTER, QColor(255, 200, 0)); \
        PAINTER->drawRect(QRect(x+CELL_W-PIN_H,         \
                                y+(0.75*PIN_W),         \
                                PIN_H, PIN_W));         \
                                } while(0);

#define INACTIVE_N_IN_BOX(PAINTER)                      \
    do {                                                \
        INACTIVE_COLOR_BOX(PAINTER);                    \
        PAINTER->drawRect(QRect(x+CELL_W-(1.75*PIN_W),  \
                                y,                      \
                                PIN_W, PIN_H));         \
    } while(0);

#define INACTIVE_N_OUT_BOX(PAINTER)             \
    do {                                        \
        INACTIVE_COLOR_BOX(PAINTER);            \
        PAINTER->drawRect(QRect(x+(0.75*PIN_W), \
                                y,              \
                                PIN_W, PIN_H)); \
    } while(0);

#define INACTIVE_S_IN_BOX(PAINTER)              \
    do {                                        \
        INACTIVE_COLOR_BOX(PAINTER);            \
        PAINTER->drawRect(QRect(x+(0.75*PIN_W), \
                                y+CELL_H-PIN_H, \
                                PIN_W, PIN_H)); \
    } while(0);

#define INACTIVE_S_OUT_BOX(PAINTER)                     \
    do {                                                \
        INACTIVE_COLOR_BOX(PAINTER);                    \
        PAINTER->drawRect(QRect(x+CELL_W-(1.75*PIN_W),  \
                                y+CELL_H-PIN_H,         \
                                PIN_W, PIN_H));         \
    } while(0);

#define INACTIVE_W_IN_BOX(PAINTER)              \
    do {                                        \
        INACTIVE_COLOR_BOX(PAINTER);            \
        PAINTER->drawRect(QRect(x,              \
                                y+(0.75*PIN_W), \
                                PIN_H, PIN_W)); \
                                } while(0);

#define INACTIVE_W_OUT_BOX(PAINTER)                     \
    do {                                                \
        INACTIVE_COLOR_BOX(PAINTER);                    \
        PAINTER->drawRect(QRect(x,                      \
                                y+CELL_H-(1.75*PIN_W),  \
                                PIN_H, PIN_W));         \
                                } while(0);

#define INACTIVE_E_IN_BOX(PAINTER)                      \
    do {                                                \
        INACTIVE_COLOR_BOX(PAINTER);                    \
        PAINTER->drawRect(QRect(x+CELL_W-PIN_H,         \
                                y+CELL_H-(1.75*PIN_W),  \
                                PIN_H, PIN_W));         \
                                } while(0);

#define INACTIVE_E_OUT_BOX(PAINTER)             \
    do {                                        \
        INACTIVE_COLOR_BOX(PAINTER);            \
        PAINTER->drawRect(QRect(x+CELL_W-PIN_H, \
                                y+(0.75*PIN_W), \
                                PIN_H, PIN_W)); \
                                } while(0);

#define ACTIVE_IN_OUT_LINK(PAINTER, OUT, IN)    \
    do {                                        \
        QPen pen = PAINTER->pen();              \
        QVector< QPoint > inOutPoints;          \
        pen.setColor(IN ## _IN_COLOR);          \
        pen.setWidth(IN_W);                     \
        PAINTER->setPen(pen);                   \
        ACTIVE_ ## OUT ## _ ## IN;              \
        PAINTER->drawPolyline(inOutPoints);     \
        inOutPoints.clear();                    \
        ACTIVE_ ## OUT ## _OUT_BOX(PAINTER);    \
        ACTIVE_ ## OUT ## _PIN_OUT(PAINTER);    \
    } while (0);

#define ACTIVE_FU_OUT_LINK(PAINTER, OUT)        \
    do {                                        \
        QPen pen = PAINTER->pen();              \
        QVector< QPoint > opOutPoints;          \
        pen.setColor(FU_OUT_COLOR);             \
        pen.setWidth(IN_W);                     \
        PAINTER->setPen(pen);                   \
        ACTIVE_ ## OUT ## _FU;                  \
        PAINTER->drawPolyline(opOutPoints);     \
        opOutPoints.clear();                    \
        ACTIVE_ ## OUT ## _OUT_BOX(PAINTER);    \
        ACTIVE_ ## OUT ## _PIN_OUT(PAINTER);    \
    } while (0);

#define ACTIVE_S_N                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+PIN_H+(IN_W/2));                    \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+CELL_H-PIN_H-(IN_W/2));             \
    } while(0);

#define ACTIVE_W_N                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+PIN_H+(IN_W/2));                    \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+CELL_H-(1.75*PIN_W)+(0.5*PIN_W));   \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),                     \
                              y+CELL_H-(1.75*PIN_W)+(0.5*PIN_W));   \
    } while(0);

#define ACTIVE_E_N                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+PIN_H+(IN_W/2));                    \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),              \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
    } while(0);

#define ACTIVE_FU1_N                                                \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+PIN_H+(IN_W/2));                    \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+PIN_H+IN1_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+PIN_H+IN1_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FU2_N                                                \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+PIN_H+(IN_W/2));                    \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),    \
                              y+PIN_H+IN2_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+PIN_H+IN2_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FUSEL_N                                                  \
    do {                                                                \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),        \
                              y+PIN_H+(IN_W/2));                        \
        inOutPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),        \
                              y+PIN_H+(IN2_DST/2));                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(2*OP_PIN_H),   \
                              y+PIN_H+(IN2_DST/2));                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(2*OP_PIN_H),   \
                              y+(CELL_H/2));                            \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(IN_W/2),       \
                              y+(CELL_H/2));                            \
    } while(0);

#define ACTIVE_N_S                                          \
    do {                                                    \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),   \
                              y+CELL_H-PIN_H-(IN_W/2));     \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),   \
                              y+PIN_H+(IN_W/2));            \
    } while(0);

#define ACTIVE_W_S                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+CELL_H-PIN_H-(IN_W/2));             \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+CELL_H-(1.75*PIN_W)+(0.5*PIN_W));   \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),                     \
                              y+CELL_H-(1.75*PIN_W)+(0.5*PIN_W));   \
    } while(0);

#define ACTIVE_E_S                                          \
    do {                                                    \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),   \
                              y+CELL_H-PIN_H-(IN_W/2));     \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),   \
                              y+(0.75*PIN_W)+(0.5*PIN_W));  \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),      \
                              y+(0.75*PIN_W)+(0.5*PIN_W));  \
    } while(0);

#define ACTIVE_FU1_S                                                \
    do {                                                            \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+CELL_H-PIN_H-(IN_W/2));             \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+PIN_H+IN1_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+PIN_H+IN1_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FU2_S                                                \
    do {                                                            \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+CELL_H-PIN_H-(IN_W/2));             \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+PIN_H+IN2_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+PIN_H+IN2_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FUSEL_S                                              \
    do {                                                            \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+CELL_H-PIN_H-(IN_W/2));             \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+(CELL_H/2));                        \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(IN_W/2),   \
                              y+(CELL_H/2));                        \
    } while(0);

#define ACTIVE_N_W                                          \
    do {                                                    \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),             \
                              y+(0.75*PIN_W)+(0.5*PIN_W));  \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),   \
                              y+(0.75*PIN_W)+(0.5*PIN_W));  \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),   \
                              y+PIN_H+(IN_W/2));            \
    } while(0);

#define ACTIVE_S_W                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),                     \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
        inOutPoints << QPoint(x+CELL_W-(1.75*PIN_W)+(0.5*PIN_W),    \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
        inOutPoints << QPoint(x+CELL_W-(1.75*PIN_W)+(0.5*PIN_W),    \
                              y+CELL_H-PIN_H-(IN_W/2));             \
    } while(0);

#define ACTIVE_E_W                                          \
    do {                                                    \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),             \
                              y+(0.75*PIN_W)+(0.5*PIN_W));  \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),      \
                              y+(0.75*PIN_W)+(0.5*PIN_W));  \
    } while(0);

#define ACTIVE_FU1_W                                                \
    do {                                                            \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),                     \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FU2_W                                                \
    do {                                                            \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),                     \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+(0.75*PIN_W)+(0.5*PIN_W));          \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FUSEL_W                                                  \
    do {                                                                \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),                         \
                              y+(0.75*PIN_W)+(0.5*PIN_W));              \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(2*OP_PIN_H),   \
                              y+(0.75*PIN_W)+(0.5*PIN_W));              \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(2*OP_PIN_H),   \
                              y+(CELL_H/2));                            \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(IN_W/2),       \
                              y+(CELL_H/2));                            \
    } while(0);

#define ACTIVE_N_E                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),              \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+(0.75*PIN_W)+(0.5*PIN_W),           \
                              y+PIN_H+(IN_W/2));                    \
    } while(0);

#define ACTIVE_S_E                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),              \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+CELL_W-(1.75*PIN_W)+(0.5*PIN_W),    \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+CELL_W-(1.75*PIN_W)+(0.5*PIN_W),    \
                              y+CELL_H-PIN_H-(IN_W/2));             \
    } while(0);

#define ACTIVE_W_E                                                  \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),              \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+PIN_H+(IN_W/2),                     \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
    } while(0);

#define ACTIVE_FU1_E                                                \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),              \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+(0.9*PIN_W),                        \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+(0.9*PIN_W),                        \
                              y+PIN_H+IN1_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+PIN_H+IN1_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FU2_E                                                \
    do {                                                            \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),              \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+(0.9*PIN_W),                        \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));   \
        inOutPoints << QPoint(x+(0.9*PIN_W),                        \
                              y+PIN_H+IN2_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+PIN_H+IN2_DST);                     \
        inOutPoints << QPoint(x+(CELL_W/2)+(OP_BOX_W/4),            \
                              y+(CELL_H/2)-(OP_BOX_H/2)-(IN_W/2));  \
    } while(0);

#define ACTIVE_FUSEL_E                                                  \
    do {                                                                \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),                  \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));       \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(6*IN_W),                  \
                              y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W));       \
        inOutPoints << QPoint(x+CELL_W-PIN_H-(6*IN_W),                  \
                              y+(CELL_H/2)+(OP_BOX_W/2)+(2*OP_PIN_H));  \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(2*OP_PIN_H),   \
                              y+(CELL_H/2)+(OP_BOX_W/2)+(2*OP_PIN_H));  \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(2*OP_PIN_H),   \
                              y+(CELL_H/2));                            \
        inOutPoints << QPoint(x+(CELL_W/2)-(OP_BOX_W/2)-(IN_W/2),       \
                              y+(CELL_H/2));                            \
    } while(0);

#define ACTIVE_N_FU                                                     \
    do {                                                                \
        opOutPoints << QPoint(x+CELL_W/2,                               \
                              y+CELL_H/2+OP_BOX_H/2+(IN_W/2));          \
        opOutPoints << QPoint(x+CELL_W/2,                               \
                              y+W_LNK_LOW_DST);                         \
        opOutPoints << QPoint(x+CELL_W/2-OP_BOX_W/2-OUT_BOX_LNK_DST,    \
                              y+W_LNK_LOW_DST);                         \
        opOutPoints << QPoint(x+CELL_W/2-OP_BOX_W/2-OUT_BOX_LNK_DST,    \
                              y+PIN_H+(0.75*OUT_LNK_DST));              \
        opOutPoints << QPoint(x+(1.25*PIN_W),                           \
                              y+PIN_H+(0.75*OUT_LNK_DST));              \
        opOutPoints << QPoint(x+(1.25*PIN_W),                           \
                              y+PIN_H+(IN_W/2));                        \
    } while(0);

#define ACTIVE_S_FU                                                     \
    do {                                                                \
        opOutPoints << QPoint(x+CELL_W/2,                               \
                              y+CELL_H/2+OP_BOX_H/2+(IN_W/2));          \
        opOutPoints << QPoint(x+CELL_W/2,                               \
                              y+W_LNK_LOW_DST);                         \
        opOutPoints << QPoint(x+CELL_W/2+OP_BOX_W/2+OUT_BOX_LNK_DST,    \
                              y+W_LNK_LOW_DST);                         \
        opOutPoints << QPoint(x+CELL_W/2+OP_BOX_W/2+OUT_BOX_LNK_DST,    \
                              y+CELL_H-PIN_H-(0.75*OUT_LNK_DST));       \
        opOutPoints << QPoint(x+CELL_W-(1.25*PIN_W),                    \
                              y+CELL_H-PIN_H-(0.75*OUT_LNK_DST));       \
        opOutPoints << QPoint(x+CELL_W-(1.75*PIN_W)+(PIN_W/2),          \
                              y+CELL_H-PIN_H-(IN_W/2));                 \
    } while(0);

#define ACTIVE_W_FU                                             \
    do {                                                        \
        opOutPoints << QPoint(x+CELL_W/2,                       \
                              y+CELL_H/2+OP_BOX_H/2+(IN_W/2));  \
        opOutPoints << QPoint(x+CELL_W/2,                       \
                              y+W_LNK_LOW_DST);                 \
        opOutPoints << QPoint(x+PIN_H+(0.75*OUT_LNK_DST),       \
                              y+W_LNK_LOW_DST);                 \
        opOutPoints << QPoint(x+PIN_H+(0.75*OUT_LNK_DST),       \
                              y+CELL_H-(1.25*PIN_W));           \
        opOutPoints << QPoint(x+PIN_H+(IN_W/2),                 \
                              y+CELL_H-(1.25*PIN_W));           \
    } while(0);

#define ACTIVE_E_FU                                                     \
    do {                                                                \
        opOutPoints << QPoint(x+CELL_W/2,                               \
                              y+CELL_H/2+OP_BOX_H/2+(IN_W/2));          \
        opOutPoints << QPoint(x+CELL_W/2,                               \
                              y+W_LNK_LOW_DST);                         \
        opOutPoints << QPoint(x+CELL_W/2+OP_BOX_W/2+OUT_BOX_LNK_DST,    \
                              y+W_LNK_LOW_DST);                         \
        opOutPoints << QPoint(x+CELL_W/2+OP_BOX_W/2+OUT_BOX_LNK_DST,    \
                              y+CELL_H/2-OP_BOX_H/2-OUT_BOX_LNK_DST);   \
        opOutPoints << QPoint(x+CELL_W-PIN_H-(0.75*OUT_LNK_DST),        \
                              y+CELL_H/2-OP_BOX_H/2-OUT_BOX_LNK_DST);   \
        opOutPoints << QPoint(x+CELL_W-PIN_H-(0.75*OUT_LNK_DST),        \
                              y+(0.75*PIN_W)+(PIN_W/2));                \
        opOutPoints << QPoint(x+CELL_W-PIN_H-(IN_W/2),                  \
                              y+(0.75*PIN_W)+(PIN_W/2));                \
    } while(0);

#define ACTIVE_N_PIN_IN(PAINTER)                                    \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::darkBlue);                                 \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y-IN_W);                            \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y-IN_W-P2C_V_DST);                  \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
        emit PinEnabled(row, col, N_IN);                            \
    } while(0);

#define ACTIVE_N_PIN_OUT(PAINTER)                   \
    do {                                            \
        QPen pen = PAINTER->pen();                  \
        QVector< QPoint > pinConnPoints;            \
        pen.setColor(Qt::darkBlue);                 \
        pen.setWidth(IN_W);                         \
        PAINTER->setPen(pen);                       \
        pinConnPoints << QPoint(x+(1.25*PIN_W),     \
                                y-IN_W);            \
        pinConnPoints << QPoint(x+(1.25*PIN_W),     \
                                y-IN_W-P2C_V_DST);  \
        PAINTER->drawPolyline(pinConnPoints);       \
        pinConnPoints.clear();                      \
        emit PinEnabled(row, col, N_OUT);           \
    } while(0);

#define ACTIVE_S_PIN_IN(PAINTER)                            \
    do {                                                    \
        QPen pen = PAINTER->pen();                          \
        QVector< QPoint > pinConnPoints;                    \
        pen.setColor(Qt::darkBlue);                         \
        pen.setWidth(IN_W);                                 \
        PAINTER->setPen(pen);                               \
        pinConnPoints << QPoint(x+(1.25*PIN_W),             \
                                y+CELL_H+IN_W);             \
        pinConnPoints << QPoint(x+(1.25*PIN_W),             \
                                y+CELL_H+IN_W+P2C_V_DST);   \
        PAINTER->drawPolyline(pinConnPoints);               \
        pinConnPoints.clear();                              \
        emit PinEnabled(row, col, S_IN);                    \
    } while(0);

#define ACTIVE_S_PIN_OUT(PAINTER)                                   \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::darkBlue);                                 \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y+CELL_H+IN_W);                     \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y+CELL_H+IN_W+P2C_V_DST);           \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
        emit PinEnabled(row, col, S_OUT);                           \
    } while(0);

#define ACTIVE_W_PIN_IN(PAINTER)                    \
    do {                                            \
        QPen pen = PAINTER->pen();                  \
        QVector< QPoint > pinConnPoints;            \
        pen.setColor(Qt::darkBlue);                 \
        pen.setWidth(IN_W);                         \
        PAINTER->setPen(pen);                       \
        pinConnPoints << QPoint(x-IN_W,             \
                                y+(1.25*PIN_W));    \
        pinConnPoints << QPoint(x-P2C_H_DST,        \
                                y+(1.25*PIN_W));    \
        PAINTER->drawPolyline(pinConnPoints);       \
        pinConnPoints.clear();                      \
        emit PinEnabled(row, col, W_IN);            \
    } while(0);

#define ACTIVE_W_PIN_OUT(PAINTER)                                   \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::darkBlue);                                 \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x-IN_W,                             \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        pinConnPoints << QPoint(x-P2C_H_DST,                        \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
        emit PinEnabled(row, col, W_OUT);                           \
    } while(0);

#define ACTIVE_E_PIN_IN(PAINTER)                                    \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::darkBlue);                                 \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x+CELL_W+IN_W,                      \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        pinConnPoints << QPoint(x+CELL_W+IN_W+P2C_H_DST,            \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
        emit PinEnabled(row, col, E_IN);                            \
    } while(0);

#define ACTIVE_E_PIN_OUT(PAINTER)                           \
    do {                                                    \
        QPen pen = PAINTER->pen();                          \
        QVector< QPoint > pinConnPoints;                    \
        pen.setColor(Qt::darkBlue);                         \
        pen.setWidth(IN_W);                                 \
        PAINTER->setPen(pen);                               \
        pinConnPoints << QPoint(x+CELL_W+IN_W,              \
                                y+(1.25*PIN_W));            \
        pinConnPoints << QPoint(x+CELL_W+IN_W+P2C_H_DST,    \
                                y+(1.25*PIN_W));            \
        PAINTER->drawPolyline(pinConnPoints);               \
        pinConnPoints.clear();                              \
        emit PinEnabled(row, col, E_OUT);                   \
    } while(0);

#define INACTIVE_N_PIN_IN(PAINTER)                                  \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::lightGray);                                \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y-IN_W);                            \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y-IN_W-P2C_V_DST);                  \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
    } while(0);

#define INACTIVE_N_PIN_OUT(PAINTER)                 \
    do {                                            \
        QPen pen = PAINTER->pen();                  \
        QVector< QPoint > pinConnPoints;            \
        pen.setColor(Qt::lightGray);                \
        pen.setWidth(IN_W);                         \
        PAINTER->setPen(pen);                       \
        pinConnPoints << QPoint(x+(1.25*PIN_W),     \
                                y-IN_W);            \
        pinConnPoints << QPoint(x+(1.25*PIN_W),     \
                                y-IN_W-P2C_V_DST);  \
        PAINTER->drawPolyline(pinConnPoints);       \
        pinConnPoints.clear();                      \
    } while(0);

#define INACTIVE_S_PIN_IN(PAINTER)                          \
    do {                                                    \
        QPen pen = PAINTER->pen();                          \
        QVector< QPoint > pinConnPoints;                    \
        pen.setColor(Qt::lightGray);                        \
        pen.setWidth(IN_W);                                 \
        PAINTER->setPen(pen);                               \
        pinConnPoints << QPoint(x+(1.25*PIN_W),             \
                                y+CELL_H+IN_W);             \
        pinConnPoints << QPoint(x+(1.25*PIN_W),             \
                                y+CELL_H+IN_W+P2C_V_DST);   \
        PAINTER->drawPolyline(pinConnPoints);               \
        pinConnPoints.clear();                              \
    } while(0);

#define INACTIVE_S_PIN_OUT(PAINTER)                                 \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::lightGray);                                \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y+CELL_H+IN_W);                     \
        pinConnPoints << QPoint(x+CELL_W-(0.75*PIN_W)-(0.5*PIN_W),  \
                                y+CELL_H+IN_W+P2C_V_DST);           \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
    } while(0);

#define INACTIVE_W_PIN_IN(PAINTER)                  \
    do {                                            \
        QPen pen = PAINTER->pen();                  \
        QVector< QPoint > pinConnPoints;            \
        pen.setColor(Qt::lightGray);                \
        pen.setWidth(IN_W);                         \
        PAINTER->setPen(pen);                       \
        pinConnPoints << QPoint(x-IN_W,             \
                                y+(1.25*PIN_W));    \
        pinConnPoints << QPoint(x-P2C_H_DST,        \
                                y+(1.25*PIN_W));    \
        PAINTER->drawPolyline(pinConnPoints);       \
        pinConnPoints.clear();                      \
    } while(0);

#define INACTIVE_W_PIN_OUT(PAINTER)                                 \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::lightGray);                                \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x-IN_W,                             \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        pinConnPoints << QPoint(x-P2C_H_DST,                        \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
    } while(0);

#define INACTIVE_E_PIN_IN(PAINTER)                                  \
    do {                                                            \
        QPen pen = PAINTER->pen();                                  \
        QVector< QPoint > pinConnPoints;                            \
        pen.setColor(Qt::lightGray);                                \
        pen.setWidth(IN_W);                                         \
        PAINTER->setPen(pen);                                       \
        pinConnPoints << QPoint(x+CELL_W+IN_W,                      \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        pinConnPoints << QPoint(x+CELL_W+IN_W+P2C_H_DST,            \
                                y+CELL_H-(0.75*PIN_W)-(0.5*PIN_W)); \
        PAINTER->drawPolyline(pinConnPoints);                       \
        pinConnPoints.clear();                                      \
    } while(0);

#define INACTIVE_E_PIN_OUT(PAINTER)                         \
    do {                                                    \
        QPen pen = PAINTER->pen();                          \
        QVector< QPoint > pinConnPoints;                    \
        pen.setColor(Qt::lightGray);                        \
        pen.setWidth(IN_W);                                 \
        PAINTER->setPen(pen);                               \
        pinConnPoints << QPoint(x+CELL_W+IN_W,              \
                                y+(1.25*PIN_W));            \
        pinConnPoints << QPoint(x+CELL_W+IN_W+P2C_H_DST,    \
                                y+(1.25*PIN_W));            \
        PAINTER->drawPolyline(pinConnPoints);               \
        pinConnPoints.clear();                              \
    } while(0);

#define ACTIVE_FU1_PIN_OUT(PAINTER)
#define ACTIVE_FU2_PIN_OUT(PAINTER)
#define ACTIVE_FUSEL_PIN_OUT(PAINTER)

#endif /* CELL_HPP_ */
