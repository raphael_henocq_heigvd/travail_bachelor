#ifndef COMPONENT_HPP_
#define COMPONENT_HPP_

#include <QGraphicsItem>

#include "DataTypes.hpp"

#define N                     0
#define W                     1
#define E                     2
#define S                     3
#define FU                    4
#define FU1                   4
#define FU2                   5
#define FUSEL                 6

class Component : public QGraphicsObject
{
    Q_OBJECT

public:
    virtual ~Component() { };

    virtual void setData(const int data, const unsigned int D) = 0;
    virtual void paint(QPainter *painter,
                       const QStyleOptionGraphicsItem *item,
                       QWidget *widget) = 0;
    virtual void setConfig(const CELL_CONF_TYPE config) = 0;
signals:
    void OutProduced(unsigned int r, unsigned int c, unsigned int d,
                     int data);
    void PinEnabled(unsigned int row, unsigned int col, unsigned int dir);
};

#endif /* COMPONENT_HPP_ */
