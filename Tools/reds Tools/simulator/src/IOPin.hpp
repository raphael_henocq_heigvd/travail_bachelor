#ifndef IO_PIN_HPP_
#define IO_PIN_HPP_

#include <QColor>
#include <QGraphicsItem>
#include <QDebug>

#include "Component.hpp"
#include "DataTypes.hpp"
#include "Constants.hpp"

class IOPin : public Component {
    Q_OBJECT

public:
    IOPin(const unsigned int row, const unsigned int col,
	  const unsigned int dir);
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *item,
	       QWidget *widget);
    QRectF boundingRect() const;

    void setConfig(const CELL_CONF_TYPE config)
	{
	    Q_UNUSED(config);
	}

    void setInNumber(const unsigned int number) { inNumber = number; };
    void setOutNumber(const unsigned int number) { outNumber = number; };

    unsigned int getInNumber() { return inNumber; };
    unsigned int getOutNumber() { return outNumber; };

    void setInputData(const int data)
	{
	    emit OutProduced(row, col, dir, data);
	};

    void setData(const int data, const unsigned int D)
	{
	    Q_UNUSED(D);

	    emit ResultProduced(outNumber, data);
	};

    void setInActive() { in_active = true; };
    void setOutActive() { out_active = true; };

signals:
    void OutProduced(unsigned int r, unsigned int c, unsigned int d,
		     int data);
    void ResultProduced(unsigned int outNumber, int value);
    void PinEnabled(unsigned int r, unsigned int c, unsigned int d);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    int x_in;
    int y_in;
    int x_out;
    int y_out;
    const unsigned int row;
    const unsigned int col;
    unsigned int inNumber;
    unsigned int outNumber;
    const unsigned int dir;

    bool in_active;
    bool out_active;
};

#endif /* IO_PIN_HPP_ */
