#include <QApplication>
#include <QtGui>
#include <QDebug>
#include <QFile>
#include <QMessageBox>
#include <QDesktopWidget>
#include <cstdint>

#include "Simulator.hpp"

#include <unistd.h>

/**
 * File descriptor for logs
 */
FILE *fp;

/**
 * Redirect logs to a local file
 */
void logHandler (QtMsgType type, const QMessageLogContext &context,
                 const QString &msg)
{
    Q_UNUSED(type);
    QByteArray localMsg = msg.toLocal8Bit();
    fprintf(fp, "%s (%s:%u, %s)\n", localMsg.constData(), context.file,
            context.line, context.function);
}

bool readData (QFile& dataFile,
               QVector< int32_t >& dataV)
{
    if (!dataFile.open(QIODevice::ReadOnly)) {
        fprintf(stderr, "dataFile file error [%s]: %s\n",
                dataFile.fileName().toStdString().c_str(),
                dataFile.errorString().toStdString().c_str());
        return false;
    }

    QTextStream in(&dataFile);
    bool ok;

    qDebug() << "Reading data file" << dataFile.fileName();

    while (!in.atEnd()) {
        int inputData = in.readLine().toLong(&ok, 16);
        if (!ok) {
            fprintf(stderr,
                    "Data file parsing error (value #%d)!\n", dataV.size()+1);
            dataFile.close();
            return false;
        }
        qDebug() << "\tRead value:" << inputData;
        dataV.push_back(inputData);
    }

    qDebug() << "Data file" << dataFile.fileName() << "read!";

    dataFile.close();
    return true;
}

bool parseConfigFile (QFile& cfgFile,
                      QVector< int32_t >& configV,
                      QVector< int32_t >& patternV)
{
    if (!cfgFile.open(QIODevice::ReadOnly)) {
        fprintf(stderr, "cfgFile file error [%s]: %s\n",
                cfgFile.fileName().toStdString().c_str(),
                cfgFile.errorString().toStdString().c_str());
        return false;
    }

    qDebug() << "Parsing configuration file" << cfgFile.fileName() << "...";

    QTextStream in(&cfgFile);
    bool ok;
    unsigned int unused;
    Q_UNUSED(unused);

    /* Drop first line (start command)*/
    unused = in.readLine().toInt(&ok, 16);
    /* Second line contains size+offset of the configuration */
    unsigned int cfg = in.readLine().toInt(&ok, 16);
    if (!ok) {
        fprintf(stderr,
                "Error decoding overlay config from file %s\n",
                cfgFile.fileName().toStdString().c_str());
        cfgFile.close();
        return false;
    }

    /* Get config size and offset */
    unsigned int off_cfg = cfg & 0xFFFF;
    cfg >>= 16 & 0xFFFF;

    qDebug() << "\tCFG size:" << cfg << ", offset:" << off_cfg;
    if (cfg == 0) {
        fprintf(stderr,
                "Empty configuration file, skipping it...\n");
        return false;
    }

    /* Discard the third value */
    unused = in.readLine().toInt(&ok, 16);

    /* Get pattern size and offset */
    unsigned int prn = in.readLine().toInt(&ok, 16);
    if (!ok) {
        fprintf(stderr,
                "Error decoding pattern config from file %s\n",
                cfgFile.fileName().toStdString().c_str());
        cfgFile.close();
        return false;
    }
    unsigned int off_prn = 0;
    if (prn != 0) {
        off_prn = prn & 0xFFFF;
        off_prn -= (off_cfg+cfg); /* Subtract the part we surely read */
        prn >>= 16 & 0xFFFF;
    }

    qDebug() << "\tPRN size:" << prn << ", offset:" << off_prn;

    /* Vectors holding the configuration */
    configV.resize(cfg);
    patternV.resize(prn);

    off_cfg -= 4; /* Skip header (we already read it) */
    /* Skip unused lines before config */
    for (unsigned int i = 0; i < off_cfg; ++i) {
        // qDebug() << "\tskipping before CFG" << i << "/" << off_cfg;
        unused = in.readLine().toInt(&ok, 16);
    }

    /* Read configuration elements */
    for (unsigned int i = 0; i < cfg; ++i) {
        configV[i] = in.readLine().toInt(&ok, 16);
        if (!ok) {
            fprintf(stderr,
                    "Error encountered while decoding configuration line %d/%d",
                    i+1, cfg);
            cfgFile.close();
            return false;
        }
        // qDebug() << "[" << cfgFile.fileName() << "] "
        //   << "config " << i+1 << "/" << cfg
        //   << " = " << configV[i];
    }

    /* Skip unused lines before pattern */
    for (unsigned int i = 0; i < off_prn; ++i) {
        // qDebug() << "\tskipping before PRN" << i << "/" << off_prn;
        unused = in.readLine().toInt(&ok, 16);
    }

    /* Read pattern */
    for (unsigned int i = 0; i < prn; ++i) {
        patternV[i] = in.readLine().toInt(&ok, 16);
        if (!ok) {
            fprintf(stderr,
                    "Error encountered while decoding pattern line %d/%d",
                    i+1, prn);
            cfgFile.close();
            return false;
        }
        // qDebug() << "[" << cfgFile.fileName() << "] "
        //   << "pattern " << i+1 << "/" << prn
        //   << " = " << patternV[i];
    }

    cfgFile.close();
    return true;
}

bool loadSimulation(const char * const* argv,
                    Simulator& sim,
                    Simulator& const_gui_sim,
                    Simulator& main_gui_sim)
{
    QFile constConfigFile(argv[1]);
    QFile mainConfigFile(argv[2]);
    QFile constDataFile(argv[3]);
    QFile mainDataFile(argv[4]);
    QVector< int32_t > const_configV;
    QVector< int32_t > const_patternV;
    QVector< int32_t > const_dataV;
    QVector< int32_t > main_configV;
    QVector< int32_t > main_patternV;
    QVector< int32_t > main_dataV;

    qDebug() << "Loading simulation...";

    /*
     * Start by reading the main configuration file, since we might have no
     * constants in our algorithm (but we still have to initialize the overlay).
     * If constants are present, write them at first, then reconfigure and
     * start writing variables and gathering results.
     */
    if (!parseConfigFile(mainConfigFile, main_configV, main_patternV)) {
        fprintf(stderr, "Failed to parse main configuration file\n");
        return false;
    }
    /* Create simulation */
    unsigned int overlaySize = sqrt(main_configV.size());

    qDebug() << "Overlay size:" << overlaySize;

    /*
     * Now read the constants, and if they are present write them to the overlay
     */
    if (!parseConfigFile(constConfigFile, const_configV, const_patternV)) {
        fprintf(stderr,
                "Failed to parse constants configuration file (maybe the system"
                " has no constants!) -- skipping them!\n");
    } else {
        qDebug() << "Configuration of constants in progress...";

        /* Set configuration for constants */
        for (unsigned int r = 0; r < overlaySize; ++r) {
            for (unsigned int c = 0; c < overlaySize; ++c) {
                sim.setConfig(r, c, const_configV[r*overlaySize+c]);
                const_gui_sim.setConfig(r, c, const_configV[r*overlaySize+c]);
            }
        }
        sim.setPattern(const_patternV);

        /* Now set the constants in the overlay */
        if (!readData(constDataFile, const_dataV)) {
            fprintf(stderr, "Failed to parse constant input data!\n");
            return false;
        }

        /*
         * Finally, write the constants in the overlay to the inputs given by
         * the chosen pattern.
         */
        for (unsigned int i = 0; (int)i < const_dataV.size(); ++i) {
            int inNo = sim.getNextInput();
            qDebug() << "Input" << inNo
                     << "set to CONSTANT value" << const_dataV[i];
            sim.setInput(inNo, const_dataV[i]);
        }

        /* Sleep a bit to let constants propagate */
        usleep(10000);
    }

    qDebug() << "Configuring variables (without reset)";

    /* Set configuration for variables WITHOUT reset */
    for (unsigned int r = 0; r < overlaySize; ++r) {
        for (unsigned int c = 0; c < overlaySize; ++c) {
            sim.setConfig(r, c, main_configV[r*overlaySize+c]);
            main_gui_sim.setConfig(r, c, main_configV[r*overlaySize+c]);
        }
    }
    /* Set the new pattern */
    sim.setPattern(main_patternV);

    /* Set data values in the overlay */
    if (!readData(mainDataFile, main_dataV)) {
        fprintf(stderr, "Failed to parse main input data!\n");
        return false;
    }
    for (unsigned int i = 0; (int)i < main_dataV.size(); ++i) {
        int inNo = sim.getNextInput();
        qDebug() << "Input" << inNo << "set to value" << main_dataV[i];
        sim.setInput(inNo, main_dataV[i]);
    }

    qDebug() << "SIMULATION LOADED!";
    return true;
}

int main (int argc, char **argv)
{
    Q_INIT_RESOURCE(images);
    /*
     * If the output is written into a file (no visual feedback of results),
     * store the simulation logs in a logfile.
     */
    if (argc == 6) {
        qInstallMessageHandler(logHandler);
        fp = fopen("log_sim.log", "wt");
        if (fp == NULL) {
            fprintf(stderr, "Error encountered while opening simulator.log!\n");
            return -138;
        }
    }

    qDebug() << "Starting simulator...";

    QApplication app(argc, argv);

    if (argc < 5 || argc > 6) {
        qDebug() << "Usage:" << argv[0]
                 << "[constConfigFile] [mainConfigFile] [constDataFile]"
                 << "[mainDataFile] [outputFile]";
        return -1;
    }

    /*
     * Read the main configuration to get the size of the overlay
     */
    QVector< int32_t > main_configV;
    QVector< int32_t > main_patternV;
    QFile mainConfigFile(argv[2]);
    if (!parseConfigFile(mainConfigFile, main_configV, main_patternV)) {
        fprintf(stderr, "Failed to parse main configuration file\n");
        return false;
    }
    unsigned int overlaySize = sqrt(main_configV.size());

    QString outFName("");
    if (argc == 6) {
        outFName = QString(argv[5]);
    }
    Simulator sim(overlaySize, overlaySize,
                  "simMain", outFName);
    Simulator const_gui_sim(overlaySize, overlaySize,
                            "Overlay - CONSTANTS", "");
    Simulator main_gui_sim(overlaySize, overlaySize,
                           "Overlay - Main simulation", "");

    if (!loadSimulation(argv, sim, const_gui_sim, main_gui_sim)) {
        fprintf(stderr, "Simulation aborted due to previous errors\n");
        return -1;
    }

    if (argc < 6) {
        const_gui_sim.showMaximized();
        main_gui_sim.showMaximized();

        int screen_number = 0;

        if (QApplication::desktop()->isVirtualDesktop()) {
            QPoint bottom_left =
                QApplication::desktop()->screenGeometry(screen_number).bottomLeft();
            const_gui_sim.move(bottom_left);
            main_gui_sim.move(bottom_left);
        } else {
            QApplication::desktop()->screen(screen_number);
        }

        return app.exec();
    } else {
        fclose(fp);
    }

    return 0;
}
