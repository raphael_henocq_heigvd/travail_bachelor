#ifndef SIMULATOR_HPP_
#define SIMULATOR_HPP_

#include <QWidget>
#include <QVector>
#include <QDebug>
#include <QFile>
#include <QApplication>
#include <QKeyEvent>

#include "Component.hpp"
#include "Cell.hpp"
#include "IOPin.hpp"
#include "View.hpp"
#include "Constants.hpp"

QT_BEGIN_NAMESPACE
class QGraphicsScene;
QT_END_NAMESPACE

#define IS_JMP(X)         (X & (1<<29))
#define IS_LOOP(X)        (X & (1<<28))
#define IS_INF(X)         (X & (1<<26))
#define GET_INPUT_NO(X)   (X & 0xFF)
#define GET_JMP_ADDR(X)   (X & 0xFF)
#define GET_IT_CNT(X)     ((X>>16) & 0xFF)

class Simulator : public QWidget
{
    Q_OBJECT

public:
    explicit Simulator(const unsigned int nRows, const unsigned int nCols,
                       const QString& title, const QString& outFName);

    void setConfig(const unsigned int row, const unsigned int col,
                   const CELL_CONF_TYPE config)
    {
        // qDebug() << "Setting new config for CELL" << row
        //   << "," << col;
        components[row+1][col+1]->setConfig(config);
    }

    void setInput(const unsigned int inputNumber,
                  const int inputValue) {
        ((IOPin *)components[inputs[inputNumber].first][inputs[inputNumber].second])->setInputData(inputValue);
    }

    int32_t getNextInput (void);
    void setPattern (const QVector< int32_t >& pattern);

public slots:
    void propagateData(unsigned int row, unsigned int col,
                       unsigned int dir, int data)
    {
        // qDebug() << "Received data" << data
        //   << "from" << row << "," << col
        //   << "SOURCE DIRECTION = " << decodeDir(dir);
        switch (dir) {
        case N:
            // qDebug() << "SENDING this data to "
            //   << row+1 << "," << col;
            components[row+1][col]->setData(data, dir);
            break;
        case S:
            // qDebug() << "SENDING this data to "
            //   << row-1 << "," << col;
            components[row-1][col]->setData(data, dir);
            break;
        case W:
            // qDebug() << "SENDING this data to "
            //   << row << "," << col+1;
            components[row][col+1]->setData(data, dir);
            break;
        case E:
            // qDebug() << "SENDING this data to "
            //   << row << "," << col-1;
            components[row][col-1]->setData(data, dir);
            break;
        default:
            qDebug() << "Invalid direction for propagation: " << dir;
        }
    }

    void PinEnabled(unsigned int row, unsigned int col, unsigned int dir);

    void ResultProduced(unsigned int outNumber, int value)
    {
        QFile outFile;

        if (fname != "") {
            outFile.setFileName(fname.c_str());
            outFile.open(QIODevice::Text | QIODevice::Append);
        } else {
            outFile.open(stdout, QIODevice::WriteOnly);
        }

        unsigned long outData = outNumber;
        char dataStr[256];
        outData <<= 32;
        outData |= (value & 0xFFFFFFFF);
        sprintf(dataStr, "0x%lX\n", outData);
        outFile.write(dataStr, qstrlen(dataStr));
        outFile.close();
    }

signals:
    void setupMatrix();

protected:
    void keyPressEvent(QKeyEvent *event) {
        if (event->key() == Qt::Key_Escape) {
            qApp->quit();
        }
    }

private:

    QString decodeDir(unsigned int dir)
    {
        if (dir == N)
            return QString::number(dir)+QString(" = N");
        if (dir == S)
            return QString::number(dir)+QString(" = S");
        if (dir == W)
            return QString::number(dir)+QString(" = W");
        if (dir == E)
            return QString::number(dir)+QString(" = E");
        return QString::number(dir)+QString(" = ???");
    }

    unsigned int rowsNo;
    unsigned int colsNo;
    void populateScene(const unsigned int nRows, const unsigned int nCols);
    QGraphicsScene *scene;
    std::string fname;
    QVector< QVector< Component* > > components;
    QMap< int, std::pair< int, int > > inputs;
    QVector< std::pair< int, int > > outputs;

    QVector< int32_t > inputPattern;
    unsigned int nextInputIdx;
    unsigned int iterCnt;
};

#endif /* SIMULATOR_HPP_ */
