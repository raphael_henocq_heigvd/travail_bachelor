#include <QtWidgets>
#include <QDebug>
#include <QVarLengthArray>
#include <QLabel>

#include <stdexcept>

#include "Cell.hpp"

Cell::Cell(const unsigned int row, const unsigned int col)
    : row(row), col(col)
{
    x = CELL_H_SPACING(col);
    y = CELL_V_SPACING(row);

    setZValue((x + y) % 2);

    setAcceptHoverEvents(true);

    MUXConfig.resize(MUX_NO);
    OPfifos.resize(2);
}

QRectF
Cell::boundingRect() const
{
    return QRectF(x-100, y-100, CELL_W+200, CELL_H+200);
}

/**
 * IN/CONF| 11 | 10 | 01 | 00 |
 * -------+-------------------+
 *  N     | FU |  S |  E |  W |
 *  W     | FU |  S |  E |  N |
 *  E     | FU |  S |  W |  N |
 *  S     | FU |  E |  W |  N |
 *  FU1   |  S |  E |  W |  N |
 *  FU2   |  S |  E |  W |  N |
 * -------+-------------------+
 */
void
Cell::setConfig(const CELL_CONF_TYPE config)
{
    // qDebug() << "Cell " << row
    //   << ", " << col << ", setting new config ("
    //   << config << ")";
    /* Reset previous configuration */
    for (unsigned int m = 0; m < MUX_NO; ++m) {
        for (unsigned int i = 0; i < IN_NO; ++i) {
            MUXConfig[m].in[i] = 0;
        }
    }

    for (unsigned int m = 0; m < MUX_NO; ++m) {
        unsigned int sel = (config >> (m*MASK_SIZE));
        if (sel & MUX_ENABLE) {
            sel &= SELECTION_MASK;
            switch (sel) {
            case 0:
                if (m == N) {
                    /* set W */
                    MUXConfig[N].in[W] = 1;
                } else {
                    /* set N */
                    MUXConfig[m].in[N] = 1;
                }
                break;
            case 1:
                if (m == N || m == W) {
                    /* set E */
                    MUXConfig[m].in[E] = 1;
                } else {
                    /* set W */
                    MUXConfig[m].in[W] = 1;
                }
                break;
            case 2:
                if (m == N || m == W || m == E) {
                    /* set S */
                    MUXConfig[m].in[S] = 1;
                } else {
                    /* set E */
                    MUXConfig[m].in[E] = 1;
                }
                break;
            case 3:
                if (m == FU1 || m == FU2 || m == FUSEL) {
                    /* set S */
                    MUXConfig[m].in[S] = 1;
                } else {
                    /* set FU */
                    MUXConfig[m].in[FU] = 1;
                }
                break;
            default:
                qDebug() << "Invalid configuration requested: "
                         << sel;
                throw std::runtime_error("invalidConfiguration");
            }
        }
    }

    cell_sel = -1;
    for (unsigned int i = 0; i < IN_NO; ++i) {
        if (MUXConfig[FUSEL].in[i] != 0) {
            qDebug() << "FUSEL set for in " << i;
            cell_sel = i;
        }
    }

    FU1_const = (config >> (MUX_NO*MASK_SIZE)) & 0x1;
    FU2_const = (config >> (MUX_NO*MASK_SIZE+1)) & 0x1;
    cell_op = (config >> ((MUX_NO+1)*MASK_SIZE));
}

void
Cell::paint(QPainter *painter,
            const QStyleOptionGraphicsItem *option,
            QWidget *widget)
{
    Q_UNUSED(widget);

    const qreal lvlDetail =
        option->levelOfDetailFromTransform(painter->worldTransform());

    QPen pen = painter->pen();
    QBrush brush = painter->brush();
    brush.setStyle(Qt::SolidPattern);

    /* Cell's body */
    pen.setColor(Qt::darkGray);
    pen.setWidth(4);
    brush.setColor(Qt::white);
    painter->setPen(pen);
    painter->setBrush(brush);
    painter->drawRect(QRect(x, y, CELL_W, CELL_H));

    bool partial_cell = false;

    if (MUXConfig[FU1].active() && MUXConfig[FU2].active()) {
        ACTIVE_OP_BOX(painter);
        ACTIVE_FU_OUT_BOX(painter);
        active_cell = true;
    } else if ((MUXConfig[FU1].active() && !MUXConfig[FU2].active()) ||
               (!MUXConfig[FU1].active() && MUXConfig[FU2].active())) {
        CONST_OP_BOX(painter);
        ACTIVE_FU_OUT_BOX(painter);
        partial_cell = true;
    } else {
        INACTIVE_OP_BOX(painter);
        INACTIVE_FU_OUT_BOX(painter);
        active_cell = false;
    }

    /* Draw output boxes */
    if (!MUXConfig[N].active()) {
        INACTIVE_N_OUT_BOX(painter);
        INACTIVE_N_PIN_OUT(painter);
    }
    if (!MUXConfig[S].active()) {
        INACTIVE_S_OUT_BOX(painter);
        INACTIVE_S_PIN_OUT(painter);
    }
    if (!MUXConfig[E].active()) {
        INACTIVE_E_OUT_BOX(painter);
        INACTIVE_E_PIN_OUT(painter);
    }
    if (!MUXConfig[W].active()) {
        INACTIVE_W_OUT_BOX(painter);
        INACTIVE_W_PIN_OUT(painter);
    }

    /* Draw input boxes*/
    bool in_active[MUX_NO];
    const QColor ACTIVE_VAR_COLOR(255, 200, 0);
    const QColor ACTIVE_CONST_COLOR(110, 255, 0);
    for (unsigned int i = 0; i < IN_NO; ++i) {
        in_active[i] = false;
    }
    for (unsigned int m = 0; m < MUX_NO; ++m) {
        for (unsigned int i = 0; i < IN_NO; ++i) {
            in_active[i] |= MUXConfig[m].in[i];
        }
    }
    if (in_active[N]) {
        if ((MUXConfig[FU1].in[N] && FU1_const) ||
            (MUXConfig[FU2].in[N] && FU2_const)) {
            ACTIVE_N_IN_BOX(painter, ACTIVE_CONST_COLOR);
        } else {
            ACTIVE_N_IN_BOX(painter, ACTIVE_VAR_COLOR);
        }
        ACTIVE_N_PIN_IN(painter);
    } else {
        INACTIVE_N_IN_BOX(painter);
        INACTIVE_N_PIN_IN(painter);
    }
    if (in_active[S]) {
        if ((MUXConfig[FU1].in[S] && FU1_const) ||
            (MUXConfig[FU2].in[S] && FU2_const)) {
            ACTIVE_S_IN_BOX(painter, ACTIVE_CONST_COLOR);
        } else {
            ACTIVE_S_IN_BOX(painter, ACTIVE_VAR_COLOR);
        }
        ACTIVE_S_PIN_IN(painter);
    } else {
        INACTIVE_S_IN_BOX(painter);
        INACTIVE_S_PIN_IN(painter);
    }
    if (in_active[W]) {
        if ((MUXConfig[FU1].in[W] && FU1_const) ||
            (MUXConfig[FU2].in[W] && FU2_const)) {
            ACTIVE_W_IN_BOX(painter, ACTIVE_CONST_COLOR);
        } else {
            ACTIVE_W_IN_BOX(painter, ACTIVE_VAR_COLOR);
        }
        ACTIVE_W_PIN_IN(painter);
    } else {
        INACTIVE_W_IN_BOX(painter);
        INACTIVE_W_PIN_IN(painter);
    }
    if (in_active[E]) {
        if ((MUXConfig[FU1].in[E] && FU1_const) ||
            (MUXConfig[FU2].in[E] && FU2_const)) {
            ACTIVE_E_IN_BOX(painter, ACTIVE_CONST_COLOR);
        } else {
            ACTIVE_E_IN_BOX(painter, ACTIVE_VAR_COLOR);
        }
        ACTIVE_E_PIN_IN(painter);
    } else {
        INACTIVE_E_IN_BOX(painter);
        INACTIVE_E_PIN_IN(painter);
    }
    if (!in_active[FU1]) {
        INACTIVE_FU1_OUT_BOX(painter);
    }
    if (!in_active[FU2]) {
        INACTIVE_FU2_OUT_BOX(painter);
    }
    if (!in_active[FUSEL]) {
        INACTIVE_FUSEL_OUT_BOX(painter);
    }

    int activated_out;
    /* N MUX */
    activated_out = 0;
    if (MUXConfig[N].in[W]) {
        ACTIVE_IN_OUT_LINK(painter, N, W);
        ++activated_out;
    }
    if (MUXConfig[N].in[E]) {
        ACTIVE_IN_OUT_LINK(painter, N, E);
        ++activated_out;
    }
    if (MUXConfig[N].in[S]) {
        ACTIVE_IN_OUT_LINK(painter, N, S);
        ++activated_out;
    }
    if (MUXConfig[N].in[FU]) {
        ACTIVE_FU_OUT_LINK(painter, N);
        ++activated_out;
    }
    if (activated_out > 1) {
        qDebug() << "Multiple outputs forced on output N ("
                 << activated_out
                 << "), configuration invalid!";
        throw std::runtime_error("invalidConfiguration");
    }

    /* W MUX */
    activated_out = 0;
    if (MUXConfig[W].in[N]) {
        ACTIVE_IN_OUT_LINK(painter, W, N);
        ++activated_out;
    }
    if (MUXConfig[W].in[E]) {
        ACTIVE_IN_OUT_LINK(painter, W, E);
        ++activated_out;
    }
    if (MUXConfig[W].in[S]) {
        ACTIVE_IN_OUT_LINK(painter, W, S);
        ++activated_out;
    }
    if (MUXConfig[W].in[FU]) {
        ACTIVE_FU_OUT_LINK(painter, W);
        ++activated_out;
    }
    if (activated_out > 1) {
        qDebug() << "Multiple outputs forced on output W ("
                 << activated_out
                 << "), configuration invalid!";
        throw std::runtime_error("invalidConfiguration");
    }

    /* E MUX */
    activated_out = 0;
    if (MUXConfig[E].in[N]) {
        ACTIVE_IN_OUT_LINK(painter, E, N);
        ++activated_out;
    }
    if (MUXConfig[E].in[W]) {
        ACTIVE_IN_OUT_LINK(painter, E, W);
        ++activated_out;
    }
    if (MUXConfig[E].in[S]) {
        ACTIVE_IN_OUT_LINK(painter, E, S);
        ++activated_out;
    }
    if (MUXConfig[E].in[FU]) {
        ACTIVE_FU_OUT_LINK(painter, E);
        ++activated_out;
    }
    if (activated_out > 1) {
        qDebug() << "Multiple outputs forced on output E ("
                 << activated_out
                 << "), configuration invalid!";
        throw std::runtime_error("invalidConfiguration");
    }

    /* S MUX */
    activated_out = 0;
    if (MUXConfig[S].in[N]) {
        ACTIVE_IN_OUT_LINK(painter, S, N);
        ++activated_out;
    }
    if (MUXConfig[S].in[W]) {
        ACTIVE_IN_OUT_LINK(painter, S, W);
        ++activated_out;
    }
    if (MUXConfig[S].in[E]) {
        ACTIVE_IN_OUT_LINK(painter, S, E);
        ++activated_out;
    }
    if (MUXConfig[S].in[FU]) {
        ACTIVE_FU_OUT_LINK(painter, S);
        ++activated_out;
    }
    if (activated_out > 1) {
        qDebug() << "Multiple outputs forced on output S ("
                 << activated_out
                 << "), configuration invalid!";
        throw std::runtime_error("invalidConfiguration");
    }

    /* FU1 MUX */
    activated_out = 0;
    if (MUXConfig[FU1].in[N]) {
        ACTIVE_IN_OUT_LINK(painter, FU1, N);
        ++activated_out;
    }
    if (MUXConfig[FU1].in[W]) {
        ACTIVE_IN_OUT_LINK(painter, FU1, W);
        ++activated_out;
    }
    if (MUXConfig[FU1].in[E]) {
        ACTIVE_IN_OUT_LINK(painter, FU1, E);
        ++activated_out;
    }
    if (MUXConfig[FU1].in[S]) {
        ACTIVE_IN_OUT_LINK(painter, FU1, S);
        ++activated_out;
    }
    if (activated_out > 1) {
        qDebug() << "Multiple inputs forced on input FU1 ("
                 << activated_out
                 << "), configuration invalid!";
        throw std::runtime_error("invalidConfiguration");
    }

    /* FU2 MUX */
    activated_out = 0;
    if (MUXConfig[FU2].in[N]) {
        ACTIVE_IN_OUT_LINK(painter, FU2, N);
        ++activated_out;
    }
    if (MUXConfig[FU2].in[W]) {
        ACTIVE_IN_OUT_LINK(painter, FU2, W);
        ++activated_out;
    }
    if (MUXConfig[FU2].in[E]) {
        ACTIVE_IN_OUT_LINK(painter, FU2, E);
        ++activated_out;
    }
    if (MUXConfig[FU2].in[S]) {
        ACTIVE_IN_OUT_LINK(painter, FU2, S);
        ++activated_out;
    }
    if (activated_out > 1) {
        qDebug() << "Multiple inputs forced on input FU2 ("
                 << activated_out
                 << "), configuration invalid!";
        throw std::runtime_error("invalidConfiguration");
    }

    /* FUSEL MUX */
    activated_out = 0;
    if (MUXConfig[FUSEL].in[N]) {
        ACTIVE_IN_OUT_LINK(painter, FUSEL, N);
        ++activated_out;
    }
    if (MUXConfig[FUSEL].in[W]) {
        ACTIVE_IN_OUT_LINK(painter, FUSEL, W);
        ++activated_out;
    }
    if (MUXConfig[FUSEL].in[E]) {
        ACTIVE_IN_OUT_LINK(painter, FUSEL, E);
        ++activated_out;
    }
    if (MUXConfig[FUSEL].in[S]) {
        ACTIVE_IN_OUT_LINK(painter, FUSEL, S);
        ++activated_out;
    }
    if (activated_out > 1) {
        qDebug() << "Multiple inputs forced on input FUSEL ("
                 << activated_out
                 << "), configuration invalid!";
        throw std::runtime_error("invalidConfiguration");
    }

    /* Labels */
    if (lvlDetail >= 0.3) {
        pen.setColor(Qt::black);
        painter->setPen(pen);

        QFont font("Times", 12);
        font.setStyleStrategy(QFont::ForceOutline);
        painter->setFont(font);
        painter->save();
        painter->drawText(x+(1.25*PIN_W)-6, y+(0.75*PIN_H),
                          QString("O"));
        painter->drawText(x+CELL_W-(1.25*PIN_W)-6, y+CELL_H-(0.25*PIN_H),
                          QString("O"));
        painter->drawText(x+4, y+CELL_H-(1.25*PIN_W)+3,
                          QString("O"));
        painter->drawText(x+CELL_W-PIN_H+4, y+(1.25*PIN_W)+3,
                          QString("O"));
        painter->drawText(x+(1.25*PIN_W)-3, y+CELL_H-(0.25*PIN_H),
                          QString("I"));
        painter->drawText(x+CELL_W-(1.25*PIN_W)-3, y+(0.75*PIN_H),
                          QString("I"));
        painter->drawText(x+7, y+(1.25*PIN_W)+3,
                          QString("I"));
        painter->drawText(x+CELL_W-PIN_H+7, y+CELL_H-(1.25*PIN_W)+3,
                          QString("I"));
        if (active_cell || partial_cell) {
            QFont font("Times", 24);
            font.setStyleStrategy(QFont::ForceOutline);
            painter->setFont(font);

            QString OpTxt;
            switch (cell_op) {
            case OP_SUM:
                OpTxt = "SUM";
                break;
            case OP_SUB:
                OpTxt = "SUB";
                break;
            case OP_MUL:
                OpTxt = "MUL";
                break;
            case OP_DIV:
                OpTxt = "DIV";
                break;
            case OP_MOD:
                OpTxt = "MOD";
                break;
            case OP_EQU:
                OpTxt = "EQU";
                break;
            case OP_NEQ:
                OpTxt = "NEQ";
                break;
            case OP_GEQ:
                OpTxt = "GEQ";
                break;
            case OP_GRE:
                OpTxt = "GRE";
                break;
            case OP_SEL:
                OpTxt = "SEL";
                break;
            case OP_AND:
                OpTxt = "AND";
                break;
            case OP_OR:
                OpTxt = "OR";
                break;
            default:
                OpTxt = "???";
                break;
            }
            painter->drawText(x+CELL_W/2-30, y+CELL_H/2+10,
                              OpTxt);
        }
        painter->restore();
    }
}

void Cell::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    update();
}

void Cell::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
}

void Cell::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}
