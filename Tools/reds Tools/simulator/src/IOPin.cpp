#include <QtWidgets>
#include <QDebug>
#include <QVarLengthArray>
#include <QLabel>

#include <stdexcept>

#include "IOPin.hpp"

/**
 * WARNING: This below is so ugly it could be used to scare kids...
 */
IOPin::IOPin(const unsigned int row, const unsigned int col,
	     const unsigned int dir)
	: row(row), col(col), dir(dir)
{
	in_active = false;
	out_active = false;
	/* Set coordinates for the boxes */
	switch (dir) {
	case N:
		x_in = N_IPIN_H_SPACING(col);
		y_in = N_IPIN_V_SPACING(row);
		x_out = N_OPIN_H_SPACING(col);
		y_out = N_OPIN_V_SPACING(row);
		break;
	case S:
		x_in = S_IPIN_H_SPACING(col);
		y_in = S_IPIN_V_SPACING(row);
		x_out = S_OPIN_H_SPACING(col);
		y_out = S_OPIN_V_SPACING(row);
		break;
	case W:
		x_in = W_IPIN_H_SPACING(col);
		y_in = W_IPIN_V_SPACING(row);
		x_out = W_OPIN_H_SPACING(col);
		y_out = W_OPIN_V_SPACING(row);
		break;
	case E:
		x_in = E_IPIN_H_SPACING(col);
		y_in = E_IPIN_V_SPACING(row);
		x_out = E_OPIN_H_SPACING(col);
		y_out = E_OPIN_V_SPACING(row);
		break;
	default:
		throw std::runtime_error("invalidPinPosition");
		break;
	}

	setAcceptHoverEvents(true);
}

QRectF
IOPin::boundingRect() const
{
	qreal x_min = x_in < x_out ? x_in : x_out;
	qreal x_max = x_in > x_out ? x_in : x_out;
	qreal y_min = y_in < y_out ? y_in : y_out;
	qreal y_max = y_in > y_out ? y_in : y_out;

	return QRectF(x_min-50, y_min-50, x_max-x_min+150, y_max-y_min+150);
}

void
IOPin::paint(QPainter *painter,
	     const QStyleOptionGraphicsItem *option,
	     QWidget *widget)
{
	Q_UNUSED(widget);
	Q_UNUSED(option);

	QPen pen = painter->pen();
	QBrush brush = painter->brush();
	brush.setStyle(Qt::SolidPattern);

	pen.setWidth(4);
	if (!in_active) {
		pen.setColor(Qt::lightGray);
		brush.setColor(Qt::lightGray);
	} else {
		pen.setColor(Qt::darkBlue);
		brush.setColor(Qt::darkBlue);
	}
	painter->setPen(pen);
	painter->setBrush(brush);
	painter->drawRect(QRect(x_in, y_in, IO_PIN_W, IO_PIN_H));

	if (!out_active) {
		pen.setColor(Qt::lightGray);
		brush.setColor(Qt::lightGray);
	} else {
		pen.setColor(Qt::darkBlue);
		brush.setColor(Qt::darkBlue);
	}
	painter->setPen(pen);
	painter->setBrush(brush);
	painter->drawRect(QRect(x_out, y_out, IO_PIN_W, IO_PIN_H));
}

void IOPin::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mousePressEvent(event);
	update();
}

void IOPin::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mouseMoveEvent(event);
}

void IOPin::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mouseReleaseEvent(event);
	update();
}
