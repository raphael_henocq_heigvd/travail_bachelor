#include <QtWidgets>

#include "Simulator.hpp"

Simulator::Simulator(const unsigned int nRows, const unsigned int nCols,
                     const QString& title, const QString& outFName)
    : QWidget(0)
{
    rowsNo = nRows;
    colsNo = nCols;

    populateScene(nRows, nCols);
    View *view = new View("Overlay architecture");
    view->view()->setScene(scene);

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(view);
    setLayout(layout);

    setWindowTitle(title);

    fname = outFName.toStdString();

    if (outFName != "") {
        /* Print message only once */
        qDebug() << "Creating a simulation with"
                 << nRows << "x" << nCols << "cells";
        /* Remove the output file */
        QFile file(outFName);
        file.remove();
    }

    resize(QDesktopWidget().availableGeometry(this).size() * 0.95);
}

void
Simulator::populateScene(const unsigned int nR, const unsigned int nC)
{
    Q_ASSERT(nR > 0);
    Q_ASSERT(nC > 0);

    scene = new QGraphicsScene;

    /* +2 because we have to consider the I/O pins on each side! */
    const unsigned int MatRowsNo = nR+2;
    const unsigned int MatColsNo = nC+2;

    components.resize(MatRowsNo);
    for (unsigned int r = 0; r < MatRowsNo; ++r) {
        components[r].resize(MatColsNo);
        for (unsigned int c = 0; c < MatColsNo; ++c) {
            components[r][c] = NULL;
        }
    }

    for (unsigned int r = 1; r < MatRowsNo-1; ++r) {
        /* First column */
        components[r][0] = new IOPin(r, 0, W);
        scene->addItem(components[r][0]);
        QObject::connect(components[r][0],
                         SIGNAL(OutProduced(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)),
                         this,
                         SLOT(propagateData(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)));
        QObject::connect(((IOPin *)components[r][0]),
                         SIGNAL(ResultProduced(unsigned int,
                                               int)),
                         this,
                         SLOT(ResultProduced(unsigned int,
                                             int)));

        /* Last column */
        components[r][MatColsNo-1] = new IOPin(r, MatColsNo-1, E);
        scene->addItem(components[r][MatColsNo-1]);
        QObject::connect(components[r][MatColsNo-1],
                         SIGNAL(OutProduced(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)),
                         this,
                         SLOT(propagateData(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)));
        QObject::connect(((IOPin *)components[r][MatColsNo-1]),
                         SIGNAL(ResultProduced(unsigned int,
                                               int)),
                         this,
                         SLOT(ResultProduced(unsigned int,
                                             int)));
    }

    for (unsigned int c = 1; c < MatColsNo-1; ++c) {
        /* First row */
        components[0][c] = new IOPin(0, c, N);
        scene->addItem(components[0][c]);
        QObject::connect(components[0][c],
                         SIGNAL(OutProduced(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)),
                         this,
                         SLOT(propagateData(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)));
        QObject::connect(((IOPin *)components[0][c]),
                         SIGNAL(ResultProduced(unsigned int,
                                               int)),
                         this,
                         SLOT(ResultProduced(unsigned int,
                                             int)));

        /* Last row */
        components[MatRowsNo-1][c] = new IOPin(MatRowsNo-1, c, S);
        scene->addItem(components[MatRowsNo-1][c]);
        QObject::connect(components[MatRowsNo-1][c],
                         SIGNAL(OutProduced(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)),
                         this,
                         SLOT(propagateData(unsigned int,
                                            unsigned int,
                                            unsigned int,
                                            int)));
        QObject::connect(((IOPin *)components[MatRowsNo-1][c]),
                         SIGNAL(ResultProduced(unsigned int,
                                               int)),
                         this,
                         SLOT(ResultProduced(unsigned int,
                                             int)));
    }

    /* Fill in with the cells */
    for (unsigned int r = 1; r < MatRowsNo-1; ++r) {
        for (unsigned int c = 1; c < MatColsNo-1; ++c) {
            components[r][c] = new Cell(r, c);
            scene->addItem(components[r][c]);
            QObject::connect(components[r][c],
                             SIGNAL(PinEnabled(unsigned int,
                                               unsigned int,
                                               unsigned int)),
                             this,
                             SLOT(PinEnabled(unsigned int,
                                             unsigned int,
                                             unsigned int)));
            QObject::connect(components[r][c],
                             SIGNAL(OutProduced(unsigned int,
                                                unsigned int,
                                                unsigned int,
                                                int)),
                             this,
                             SLOT(propagateData(unsigned int,
                                                unsigned int,
                                                unsigned int,
                                                int)));
        }
    }

    Q_ASSERT(components[0][0] == NULL);
    Q_ASSERT(components[MatRowsNo-1][MatColsNo-1] == NULL);
    Q_ASSERT(components[MatRowsNo-1][0] == NULL);
    Q_ASSERT(components[0][MatColsNo-1] == NULL);

    /* Properly enumerate I/O pins */

    /*
     * Change 15.03.2017: introduced separate input FIFOs on the four sides in
     *                    the hardware, this resulted in a change in the
     *                    numbering scheme.
     *
     * -- BEFORE --
     * + Inputs : numbers from 0 to (perimeter-1), starting from bottom-left
     *            corner
     * + Outputs: numbers from 0 to (perimeter-1), starting from top-right
     *            corner
     *
     * -- AFTER --
     * + Inputs : numbers from 0 to (side_length-1) in the lower 6 bits, the two
     *            MSB encode the side (00 = N, 01 = W, 10 = E, 11 = S)
     * + Outputs: numbers from 0 to (perimeter-1), starting from top-right
     *            corner (SAME as BEFORE)
     */

    /* Input */
    int cnt = 0;
    for (unsigned int r = MatRowsNo-2; r >= 1; --r) {
        /* Left column */
        // qDebug() << "Setting for" << r << ", 0 value" << in_count;
        ((IOPin*)components[r][0])->setInNumber(0x40 | cnt);
        ++cnt;
    }
    cnt = 0;
    for (unsigned int c = 1; c < MatColsNo-1; ++c) {
        /* Top row */
        // qDebug() << "Setting for 0, " << c << "value" << in_count;
        ((IOPin*)components[0][c])->setInNumber(0x00 | cnt);
        ++cnt;
    }
    cnt = 0;
    for (unsigned int r = 1; r < MatRowsNo-1; ++r) {
        /* Right column */
        // qDebug() << "Setting for" << r << "," << MatColsNo-1 << "value" << in_count;
        ((IOPin*)components[r][MatColsNo-1])->setInNumber(0x80 | cnt);
        ++cnt;
    }
    cnt = 0;
    for (unsigned int c = MatColsNo-2; c >= 1; --c) {
        /* Bottom row */
        // qDebug() << "Setting for" << MatRowsNo-1 << "," << c << "value" << in_count;
        ((IOPin*)components[MatRowsNo-1][c])->setInNumber(0xC0 | cnt);
        ++cnt;
    }

    /* Output */
    unsigned int out_count = 0;
    for (unsigned int r = 1; r < MatRowsNo-1; ++r) {
        /* Right column */
        // qDebug() << "Setting for" << r << "," << MatColsNo-1 << "value" << out_count;
        ((IOPin*)components[r][MatColsNo-1])->setOutNumber(out_count++);
    }
    for (unsigned int c = MatColsNo-2; c >= 1; --c) {
        /* Bottom row */
        // qDebug() << "Setting for" << MatRowsNo-1 << "," << c << "value" << out_count;
        ((IOPin*)components[MatRowsNo-1][c])->setOutNumber(out_count++);
    }
    for (unsigned int r = MatRowsNo-2; r >= 1; --r) {
        /* Left column */
        // qDebug() << "Setting for" << r << ", 0 value" << out_count;
        ((IOPin*)components[r][0])->setOutNumber(out_count++);
    }
    for (unsigned int c = 1; c < MatColsNo-1; ++c) {
        /* Top row */
        // qDebug() << "Setting for 0," << c << "value" << out_count;
        ((IOPin*)components[0][c])->setOutNumber(out_count++);
    }
    outputs.resize(out_count);

    // qDebug() << "Inputs:" << inputs.size() << ", outputs:" << outputs.size();

    for (unsigned int r = 0; r < MatRowsNo; ++r) {
        for (unsigned int c = 0; c < MatColsNo; ++c) {
            IOPin *test = dynamic_cast<IOPin*>(components[r][c]);
            if (test != NULL) {
                // qDebug() << "r,c = " << r << "," << c
                //   << "inNumber:" << test->getInNumber()
                //   << "outNumber:" << test->getOutNumber();
                inputs[test->getInNumber()].first = r;
                inputs[test->getInNumber()].second = c;
                outputs[test->getOutNumber()].first = r;
                outputs[test->getOutNumber()].second = c;
                // qDebug() << r << ", " << c
                //   << " :  in = " << test->getInNumber()
                //   << ", out = " << test->getOutNumber();
            }
        }
    }
}

void
Simulator::PinEnabled(unsigned int row, unsigned int col, unsigned int dir)
{
    // qDebug() << "Received PinEnabled from " << row << ", " << col
    //   << ", dir " << dir << "\n";
    if (row == 1) {
        if (dir == N_IN) {
            ((IOPin*)components[row-1][col])->setInActive();
        }
        if (dir == N_OUT) {
            ((IOPin*)components[row-1][col])->setOutActive();
        }
    }
    if (row == rowsNo) {
        if (dir == S_IN) {
            ((IOPin*)components[row+1][col])->setInActive();
        }
        if (dir == S_OUT) {
            ((IOPin*)components[row+1][col])->setOutActive();
        }
    }
    if (col == 1) {
        if (dir == W_IN) {
            ((IOPin*)components[row][col-1])->setInActive();
        }
        if (dir == W_OUT) {
            ((IOPin*)components[row][col-1])->setOutActive();
        }
    }
    if (col == colsNo) {
        if (dir == E_IN) {
            ((IOPin*)components[row][col+1])->setInActive();
        }
        if (dir == E_OUT) {
            ((IOPin*)components[row][col+1])->setOutActive();
        }
    }
}

void Simulator::setPattern (const QVector< int32_t >& pattern)
{
    inputPattern.clear();
    inputPattern = pattern;
    nextInputIdx = 0; /* Next input that will be taken from pattern */
    iterCnt = 0;      /* Counter used in loops/jumps */
}

int32_t Simulator::getNextInput (void)
{

    qDebug() << "-----------------\nnextInputIdx:" << nextInputIdx;
    qDebug() << "inputPattern.size() =" << inputPattern.size();

    if ((int)nextInputIdx >= inputPattern.size()) {
        nextInputIdx = 0;
    }
    int32_t nextInput = inputPattern[nextInputIdx];

    qDebug() << "nextInput:" << nextInput;

    while (1) {
        if (IS_JMP(nextInput)) {

            qDebug() << "\tIS_JMP!";

            if (IS_INF(nextInput)) {
                qDebug() << "\t\tIS_INF! -> jump on" << GET_INPUT_NO(nextInput);
                return GET_INPUT_NO(nextInput);
            } else {
                if (iterCnt == GET_IT_CNT(nextInput)) {
                    /* Done all iterations -> get to next value */
                    ++nextInputIdx;
                    nextInputIdx = nextInputIdx % inputPattern.size();
                    iterCnt = 0;
                } else {
                    /* Loop back */
                    nextInputIdx = GET_JMP_ADDR(nextInput);
                    ++iterCnt;
                }
                nextInput = inputPattern[nextInputIdx];
            }
        } else if (IS_LOOP(nextInput)) {
            qDebug() << "\tIS_LOOP!";

            if (IS_INF(nextInput)) {

                qDebug() << "\t\tIS_INF! -> loop on" << GET_INPUT_NO(nextInput);

                return GET_INPUT_NO(nextInput);
            } else {
                qDebug() << "\titerCnt =" << iterCnt;

                if (iterCnt == GET_IT_CNT(nextInput)) {

                    qDebug() << "\t\titerCnt == GET_IT_CNT(nextInput)!";

                    /* Done all iterations -> get to next value */
                    ++nextInputIdx;
                    nextInputIdx = nextInputIdx % inputPattern.size();
                    iterCnt = 0;
                    nextInput = inputPattern[nextInputIdx];

                    qDebug() << "\t\tnextInputIdx =" << nextInputIdx;
                    qDebug() << "\t\tnextInput =" << nextInput;

                } else {
                    /* Loop back */
                    ++iterCnt;
                    qDebug() << "\t\titerCnt =" << iterCnt;
                    qDebug() << "\t\tloop on =" << GET_INPUT_NO(nextInput);
                    return GET_INPUT_NO(nextInput);
                }
            }
        } else {
            ++nextInputIdx;
            return GET_INPUT_NO(nextInput);
        }
    }
}
