#ifndef VIEW_HPP_
#define VIEW_HPP_

#include <QFrame>
#include <QGraphicsView>

QT_BEGIN_NAMESPACE
class QLabel;
class QSlider;
class QToolButton;
QT_END_NAMESPACE

class View;

class GraphicsView : public QGraphicsView
{
	Q_OBJECT
public:
	GraphicsView(View *v) : QGraphicsView(), view(v) { }

protected:
#ifndef QT_NO_WHEELEVENT
	void wheelEvent(QWheelEvent *);
#endif

private:
	View *view;
};

class View : public QFrame
{
	Q_OBJECT
public:
	explicit View(const QString &name, QWidget *parent = 0);

	QGraphicsView *view() const;

public slots:
	void zoomIn(int level = 1);
	void zoomOut(int level = 1);

private slots:
	void print();
	void setupMatrix();

private:
	GraphicsView *graphicsView;
	QToolButton *printButton;
	QSlider *zoomSlider;
};

#endif /* VIEW_HPP_ */
