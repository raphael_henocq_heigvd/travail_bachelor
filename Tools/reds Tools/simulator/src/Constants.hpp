#ifndef CONSTANTS_HPP_
#define CONSTANTS_HPP_

const unsigned int ROWS_NO = 5;
const unsigned int COLS_NO = 7;

const unsigned int CELL_H = 400;
const unsigned int CELL_W = 400;

const unsigned int CELL_H_DIST = 100;
const unsigned int CELL_V_DIST = 100;

const unsigned int OP_BOX_H = 100;
const unsigned int OP_BOX_W = 100;

const unsigned int OP_PIN_W = 16;
const unsigned int OP_PIN_H = 5;

const unsigned int PIN_H = 20;
const unsigned int PIN_W = 100;

const unsigned int IO_PIN_H = 60;
const unsigned int IO_PIN_W = 60;

const unsigned int SELF_LNK_DST = 25;
const unsigned int OUT_LNK_DST = 50;
const unsigned int OUT_BOX_LNK_DST = 10;
const unsigned int W_LNK_LOW_DST =
    CELL_H-(1.25*PIN_W)+(CELL_W/2-OP_BOX_W/2-OUT_BOX_LNK_DST-
                         (1.25*PIN_W));

const unsigned int IN1_DST = 60;
const unsigned int IN2_DST = 80;
const unsigned int IN_W = 6;

const unsigned int OP_SUM = 0x01;
const unsigned int OP_SUB = 0x02;
const unsigned int OP_MUL = 0x03;
const unsigned int OP_DIV = 0x04;
const unsigned int OP_MOD = 0x05;
const unsigned int OP_EQU = 0x10;
const unsigned int OP_NEQ = 0x11;
const unsigned int OP_GEQ = 0x12;
const unsigned int OP_GRE = 0x13;
const unsigned int OP_SEL = 0x14;
const unsigned int OP_AND = 0x15;
const unsigned int OP_OR  = 0x16;

#define NORTH_PIN 0
#define SOUTH_PIN 1
#define EAST_PIN 2
#define WEST_PIN 3

const unsigned int P2C_H_DST = 0.5*IO_PIN_W;
const unsigned int P2C_V_DST = 0.5*IO_PIN_H;

#define CELL_H_SPACING(c) ((1.5*IO_PIN_W)+(c-1)*(CELL_W+IO_PIN_W))
#define CELL_V_SPACING(r) ((1.5*IO_PIN_H)+(r-1)*(CELL_H+IO_PIN_H))

#define N_OPIN_H_SPACING(c) (CELL_H_SPACING(c)+(1.25*PIN_W)-(0.5*IO_PIN_W))
#define N_OPIN_V_SPACING(r) 0

#define S_OPIN_H_SPACING(c) (CELL_H_SPACING(c)+CELL_W-(1.25*PIN_W)-(0.5*IO_PIN_W))
#define S_OPIN_V_SPACING(r) (CELL_V_SPACING(r-1)+CELL_H+(0.5*IO_PIN_H))

#define W_OPIN_H_SPACING(c) 0
#define W_OPIN_V_SPACING(r) (CELL_V_SPACING(r)+CELL_H-(1.25*PIN_W)-(0.5*IO_PIN_H))

#define E_OPIN_H_SPACING(c) ((c-1)*(CELL_W+IO_PIN_W)+IO_PIN_W)
#define E_OPIN_V_SPACING(r) (CELL_V_SPACING(r)+(1.25*PIN_W)-(0.5*IO_PIN_H))

#define N_IPIN_H_SPACING(c) (CELL_H_SPACING(c)+CELL_W-(1.25*PIN_W)-(0.5*IO_PIN_W))
#define N_IPIN_V_SPACING(r) 0

#define S_IPIN_H_SPACING(c) (CELL_H_SPACING(c)+(1.25*PIN_W)-(0.5*IO_PIN_W))
#define S_IPIN_V_SPACING(r) (CELL_V_SPACING(r-1)+CELL_H+(0.5*IO_PIN_H))

#define W_IPIN_H_SPACING(c) 0
#define W_IPIN_V_SPACING(r) (CELL_V_SPACING(r)+(1.25*PIN_W)-(0.5*IO_PIN_H))

#define E_IPIN_H_SPACING(c) ((c-1)*(CELL_W+IO_PIN_W)+IO_PIN_W)
#define E_IPIN_V_SPACING(r) (CELL_V_SPACING(r)+CELL_H-(1.25*PIN_W)-(0.5*IO_PIN_H))

#endif /* CONSTANTS_HPP_ */
