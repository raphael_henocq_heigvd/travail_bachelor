#include "Model.hpp"

Model::Model(const char * const modelFName)
{
    std::ifstream fin(modelFName);
    if (!fin.good()) {
        log_err("Cannot open model file %s",
                modelFName);
        throw std::runtime_error("Model file error");
    }

    std::vector< std::string > modelVec;
    while (!fin.eof()) {
        std::string line;
        std::getline(fin, line);

        if (line.length() > 0) {
            /* Push a new description line in the vector */
            modelVec.push_back(line);
        }
    }
    fin.close();

    /* Build the system's model from node descriptions */
    init(modelVec);
}

Model::Model(const std::vector< std::string > &modelVec)
{
    init(modelVec);
}

void Model::init(const std::vector< std::string > &modelVec)
{
    typename graph_traits< ModelGraph >::vertex_descriptor a;
    typename graph_traits< ModelGraph >::vertex_descriptor b;
    typename graph_traits< ModelGraph >::edge_descriptor e;

    property_map< ModelGraph, edge_name_t >::type edge_map =
        get(edge_name, g);
    v_idx_map_t v_idx = get(vertex_index, g);

    /* Read nodes from vector and create the node list */
    for (std::vector< std::string >::const_iterator line = modelVec.begin();
         line != modelVec.end(); ++line) {
        nodes.push_back(*line);
        a = add_vertex(g);
    }

    std::pair< v_it_t, v_it_t > vp1;
    std::pair< v_it_t, v_it_t > vp2;
    for (vp1 = vertices(g); vp1.first != vp1.second; ++vp1.first) {
        for (vp2 = vertices(g); vp2.first != vp2.second; ++vp2.first) {
            if (*vp1.first != *vp2.first) {
                SET_ARC_INPUT(*vp1.first, *vp2.first, e, 1);
                SET_ARC_INPUT(*vp1.first, *vp2.first, e, 2);
                SET_ARC_INPUT(*vp1.first, *vp2.first, e, Sel);
            }
        }
    }

#if OUTPUT_MODEL_GRAPH
    std::pair< e_it_t, e_it_t > e_it;
    for (e_it = edges(g); e_it.first != e_it.second; ++e_it.first) {
     std::cout << "source: " << source(*e_it.first, g)
           << " dest  : " << target(*e_it.first, g)
           << " name: " << edge_map[*e_it.first]
           << "\n";
    }

    boost::write_graphviz(std::cout, g);
    }
#endif /* OUTPUT_MODEL_GRAPH */
}

Model::Model(const Model &obj)
{
    g = obj.g;
    nodes = obj.nodes;
}

Model& Model::operator=(const Model &rhs)
{
    if (this != &rhs) {
        g = rhs.g;
        nodes = rhs.nodes;
    }

    return *this;
}

int Model::getRandomStartNode() const
{
    v_idx_map_t v_idx = get(vertex_index, g);

    std::vector< double > w(nodes.size(), 0.0);

    /* Give equal sampling probability to all top-level nodes */
    int cnt = 0;
    for (unsigned int n = 0; n < nodes.size(); ++n) {
        // log_info("Node %d, in_degree: %ld, isPlaced: %d",
        //   n, in_degree(v_idx[n], g), nodes[v_idx[n]].isPlaced());
        if ((in_degree(v_idx[n], g) == 0) &&
            !nodes[v_idx[n]].isPlaced()) {
            w[n] = 1;
            ++cnt;
        }
    }
    if (cnt == 0) {
        /* Returns -1 at the end of the sampling -> no more nodes
           to take */
        log_info("No more nodes left, PR ended!");
        return -1;
    }

    return v_idx[randomWeightedSamplingWithReplacement(w)];
}

bool Model::isExtVar(const int nodeNo, const std::string var) const
{
    std::pair< in_e_t, in_e_t > in_it = in_edges(nodeNo, g);
    v_idx_map_t v_idx = get(vertex_index, g);

    for ( ; in_it.first != in_it.second; ++in_it.first) {
        if (nodes[v_idx[source(*in_it.first, g)]].getOut() == var) {
            return false;
        }
    }

    return true;
}

int Model::getExtInNo() const {
    int cnt = 0;
    for (unsigned int n = 0; n < nodes.size(); ++n) {
        if (isExtVar(n, nodes[n].getIn1())) {
            ++cnt;
        }
        if (isExtVar(n, nodes[n].getIn2())) {
            ++cnt;
        }
        if (nodes[n].getInSel() != "" &&
            isExtVar(n, nodes[n].getInSel())) {
            ++cnt;
        }
    }
    return cnt;
}

int Model::getExtOutNo() const {
    int cnt = 0;
    for (unsigned int n = 0; n < nodes.size(); ++n) {
        if (nodes[n].hasExtOut()) {
            ++cnt;
        }
    }
    return cnt;
}

int Model::addNeighborsInSampling(std::vector< int >& w,
                                  const int startNode) const
{
    std::pair< in_e_t, in_e_t > in_it = in_edges(startNode, g);
    std::pair< out_e_t, out_e_t > out_it = out_edges(startNode, g);
    v_idx_map_t v_idx = get(vertex_index, g);
    int cnt = 0;

    /* If a node is neighbor of multiple, already assigned nodes,
       then it scores higher */
    for ( ; in_it.first != in_it.second; ++in_it.first) {
        if (!nodes[v_idx[source(*in_it.first, g)]].isPlaced()) {
            w[v_idx[source(*in_it.first, g)]]++;
            ++cnt;
        }
    }
    for ( ; out_it.first != out_it.second; ++out_it.first) {
        if (!nodes[v_idx[target(*out_it.first, g)]].isPlaced()) {
            w[v_idx[target(*out_it.first, g)]]++;
            ++cnt;
        }
    }

    return cnt;
}

int Model::getNextFreeModelNode(const int prevNode) const
{
    int foundNo = 0;

    if (nodes.size() == 0) {
        log_err("Empty model node set");
        throw std::runtime_error("Model set error");
    }

    if (prevNode == -1) {
        log_info("prevNode == -1 -> getting a new random start node!");
        return getRandomStartNode();
    }

    std::vector< int > w(nodes.size(), 0);

    foundNo = addNeighborsInSampling(w, prevNode);
    log_info("Found %d neighboring candidate nodes...", foundNo);

    if (foundNo == 0) {
        /* No neighboring node available, check if somewhere in the
           already-explored trees some nodes are still unassigned */
        for (unsigned int n = 0; n < nodes.size(); ++n) {
            foundNo += addNeighborsInSampling(w, n);
        }
        log_info("Found %d dispersed candidate nodes...", foundNo);
    }

    if (foundNo == 0) {
        /* No dispersed nodes available, there must be some new roots
           or we have no more free nodes left! */
        log_info("Graph ended -> getting a new random start node "
                 "(if possible!)");
        return getRandomStartNode();
    }

    return randomWeightedSamplingWithReplacement(w);
}

int Model::getNodesNo() const {
    return nodes.size();
}

const ModelNode& Model::getModelNode(const unsigned int n) const
{
    if (nodes.size() == 0) {
        log_err("Empty model node set");
        throw std::runtime_error("Model set error");
    }
    if (n >= nodes.size()) {
        log_err("The requested model node (%d) does not exist (max node"
                " no: %ld)", n, nodes.size());
        throw std::runtime_error("Wrong node number");
    }

    return nodes[n];
}

void Model::setNodePlaced(const unsigned int n)
{
    if (n >= nodes.size()) {
        log_err("The requested model node does not exist");
        throw std::runtime_error("Wrong node number");
    }

    if (nodes[n].isPlaced() == true) {
        log_err("Node #%d was already placed!", n);
        throw std::logic_error("Duplicate placement error");
    }

    nodes[n].setPlaced();
}

std::vector< int > Model::getNodeList() const
{
    std::vector< int > nlist;
    for (int n = 0; n < (int)nodes.size(); ++n) {
        nlist.push_back(n);
    }
    return nlist;
}
