#include "PR.hpp"

PR::PR(const Model &mdl, const int rowsNo, const int colsNo)
    : model(mdl)
{
    currentState = std::shared_ptr<DFE>(new DFE(model, rowsNo, colsNo));
}

void PR::run(thread_params *tp)
{
    int node = -1;
    bool PROk = true;

    while ((node = model.getNextFreeModelNode(node)) != -1 && PROk == true) {
        log_info("+++++ Randomly sampled node %d +++++", node);

        if (!pthread_mutex_trylock(tp->killer_mutex_ptr)) {
            // We were able to lock, therefore the timer must have gone off
            pthread_mutex_unlock(tp->killer_mutex_ptr);
            log_info("Timer OFF, killing thread %d", tp->n);
            return;
        }

        if (*(tp->PRDone)) {
            log_info("Another thread (%d) succeeded in PR, %d quitting!",
                     *(tp->winner), tp->n);
            return;
        }

        PositionList pList;
        Operation op(*currentState, model, node);

        opStack.push_back(op);

        bool BTOk = true;
        while (((PROk=currentState->PRNode(node, pList)) == false) &&
               BTOk && !(*(tp->PRDone))) {
            BTOk = backtrack(node, pList);
        }

        if (PROk) {
            log_info("+++ P&R succeeded for node %d (placed in "
                     "%d, %d)", node,
                     pList.back().first, pList.back().second);
            model.setNodePlaced(node);

            opStack.back().attemptedPositions = pList;
            log_info("------------ [ OP #%lu OK ! ] ------------",
                     opStack.size());

#if IMPROVE_PR
            /*
             * Pick a node at random and see if one could optimize
             * its placement.
             */
            if (opStack.size() > 2) {
             log_info("Seeking for improvement opportunities "
                  "in the already-routed nodes");

             int nodeToMove = currentState->selectNodeToMove();
             log_info("Trying to move node %d", nodeToMove);

             Operation opImpro(*currentState, model,
                       nodeToMove);
             if (currentState->improvePR(nodeToMove)) {
                 log_info("------------ [ IMPRO-OP #%lu "
                      "OK ! ] ------------",
                      opStack.size());
             } else {
                 log_info("Improvement attempt failed!");
                 *currentState = opImpro.stateBefore;
                 model = opImpro.modelBefore;
             }
            }
#endif /* IMPROVE_PR */
        } else {
            log_info("*** P&R thread %d FAILED! ***", tp->n);
            return;
        }
    }

    assert (currentState->getWaitingVarCount() == 0);
    assert ((int)opStack.size() == model.getNodesNo());

    /* Now verify that all the nodes are there and just once */
    std::vector< int > initialList = model.getNodeList();
    std::vector< int > placed = currentState->getPRNodeList();
    std::sort(initialList.begin(), initialList.end());
    std::sort(placed.begin(), placed.end());
    assert (initialList == placed);

    /* Route the constants */
    if (!currentState->routeConstants()) {
        log_info("*** P&R thread %d FAILURE (constants)!!! ***", tp->n);
        return;
    }

    pthread_mutex_lock(tp->done_mutex_ptr);
    if (*(tp->PRDone) == false) {
        *(tp->PRDone) = true;
        *(tp->winner) = tp->n;
    } else {
        log_info("PR of thread %d successful but another thread (%d) beat"
                 " it on the finish line!", tp->n, *(tp->winner));
        pthread_mutex_unlock(tp->done_mutex_ptr);
        return;
    }
    pthread_mutex_unlock(tp->done_mutex_ptr);

    log_info("\n### P&R thread %d SUCCESS! ###", tp->n);
}

void PR::dumpConstants(const char * const configFName,
                       const char * const pinListFName) const
{
    log_info("Saving PR constants configuration in %s, pinlist in %s",
             configFName, pinListFName);
    const std::string plName = string(pinListFName);
    const std::string cfName = string(configFName);
    currentState->dumpConstants(cfName, plName);
}

void PR::dumpVariables(const char * const configFName,
                       const char * const pinListFName) const
{
    log_info("Saving PR main configuration in %s, pinlist in %s",
             configFName, pinListFName);
    const std::string plName = string(pinListFName);
    const std::string cfName = string(configFName);
    currentState->dumpVariables(cfName, plName);
}

bool PR::backtrack(int &node, PositionList& pList)
{
    if (opStack.size() == 1) {
        log_info("Backtrack from initial node requested!\n"
                 "This means that the algorithm was unable to find a "
                 "valid P&R configuration!");
        return false;
    }

    vector< double > w;
    for (int i = 0; i < (int)opStack.size()-1; ++i) {
        w.push_back(exp(i));
    }
    int btPos = randomWeightedSamplingWithReplacement(w);

    log_info("!!! Backtracking from #%lu to before P&R decision #%d has "
             "been finalized! !!!",
             opStack.size(), btPos+1);

    /* MUST take current state from the subsequent one!!*/
    *currentState = opStack[btPos].stateBefore;
    model = opStack[btPos].modelBefore;
    pList = opStack[btPos].attemptedPositions;
    node = opStack[btPos].modelNode;

    /* Drop deque elements */
    opStack.erase(next(opStack.begin(), btPos+1), opStack.end());

    return true;
}
