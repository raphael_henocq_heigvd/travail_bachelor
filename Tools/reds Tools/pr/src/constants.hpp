#ifndef CONSTANTS_HPP_
#define CONSTANTS_HPP_

#include <iostream>
#include <vector>
#include <string>

/* Delimiter used when parsing the model's description */
const char* const token_delimiters = " \t";

/* Operation's names */
const std::string OP_SUM = "SUM";
const std::string OP_SUB = "SUB";
const std::string OP_MUL = "MUL";
const std::string OP_DIV = "DIV";
const std::string OP_MOD = "MOD";
const std::string OP_EQU = "EQU";
const std::string OP_NEQ = "NEQ";
const std::string OP_GEQ = "GEQ";
const std::string OP_GRE = "GRE";
const std::string OP_SEL = "SEL";
const std::string OP_AND = "AND";
const std::string OP_OR  = "OR";

/* Operation's opcodes */
const int OPCODE_SUM = 0x1000000;
const int OPCODE_SUB = 0x2000000;
const int OPCODE_MUL = 0x3000000;
const int OPCODE_DIV = 0x4000000;
const int OPCODE_MOD = 0x5000000;
const int OPCODE_EQU = 0x10000000;
const int OPCODE_NEQ = 0x11000000;
const int OPCODE_GEQ = 0x12000000;
const int OPCODE_GRE = 0x13000000;
const int OPCODE_SEL = 0x14000000;
const int OPCODE_AND = 0x15000000;
const int OPCODE_OR  = 0x16000000;

/* Constant inputs */
const int FU1_CONST = 0x200000;
const int FU2_CONST = 0x400000;

const std::string CONSTANT_PREFIX = "__C__";
const std::string OUTPUT_PREFIX = "__O__";

const int CELL_SIDE_COUNT = 4;
const int FU_IO_COUNT = 4;

enum { NORTH, EAST, SOUTH, WEST };

/* Outputs on the rows, inputs on the cols */
const int INTERNAL_CONNECTIONS[CELL_SIDE_COUNT][CELL_SIDE_COUNT] =
    {
        { 0x0,   0x5,   0x6,   0x4 },
        { 0x100, 0x0,   0x180, 0x140 },
        { 0x800, 0xC00, 0x0,   0xA00 },
        { 0x20,  0x28,  0x30,  0x0 }
    };

/* in1, in2, inSel, out */
enum { V_IN1, V_IN2, V_INSEL, V_OUT };
const int IO_CONNECTIONS[FU_IO_COUNT][CELL_SIDE_COUNT] =
    {
        { 0x4000,   0x6000,   0x7000,   0x5000 },
        { 0x20000,  0x30000,  0x38000,  0x28000 },
        { 0x100000, 0x180000, 0x1C0000, 0x140000 },
        { 0x7,      0x1C0,    0xE00,    0x38 }
    };

const int FU_IN1_CONSTANT = 0x200000;
const int FU_IN2_CONSTANT = 0x400000;

#endif /* CONSTANTS_HPP_ */
