#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <random>
#include <numeric>
#include <ctime>

#include "constants.hpp"

using namespace std;

#ifdef INFO_MSG
#define log_info(M, ...)                        \
    do {                                        \
        fprintf(stderr, M "\n", ##__VA_ARGS__); \
    } while (0);
#else
#define log_info(M, ...)
#endif /* INFO_MSG */

#ifdef ERR_MSG
#define log_err(M, ...)                             \
    do {                                            \
        fprintf(stderr, "[ERROR] (%s:%d) " M "\n",  \
                __FILE__, __LINE__, ##__VA_ARGS__); \
    } while (0);
#else
#define log_err(M, ...)
#endif /* ERR_MSG */

/**
 * randomWeightedSamplingWithReplacement() - Return a random index corresponding
 *                                           to a value in a weighted set
 *
 * @weights: weights of the samples
 *
 * Return: index of the chosen value
 */
template <typename T>
int
randomWeightedSamplingWithReplacement(const std::vector< T > &weights)
{
#ifdef DEBUG_RUN
    std::mt19937 gen(666);
#else
    std::mt19937 gen(std::time(0));
#endif

    std::vector< T > w = weights;
    w.push_back(0);
    std::vector< unsigned int > sampleList(w.size());
    std::iota(sampleList.begin(), sampleList.end(), 0);

    std::piecewise_constant_distribution< > dist(std::begin(sampleList),
                                                 std::end(sampleList),
                                                 std::begin(w));

    return static_cast< int >(dist(gen));
}

/**
 * encodeOp() - Encode the specified operation
 *
 * @opName: name of the operation to encode
 *
 * Return: integer code corresponding to the operation
 */
int encodeOp(const std::string &opName);

/**
 * showPath() - Create a schematic, textual representation of a path
 *
 * @src   : index of the source node
 * @dst   : index of the destination node
 * @rowsNo: number of rows in the overlay
 * @colsNo: number of cols in the overlay
 * @path  : path to depict
 */
void showPath(const int src, const int dst,
              const int rowsNo, const int colsNo,
              const vector< pair< int, int > >& path);

#endif /* UTILS_HPP_ */
