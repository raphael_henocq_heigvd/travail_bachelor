#ifndef MODEL_NODE_HPP_
#define MODEL_NODE_HPP_

#include <iostream>
#include <vector>
#include <cstdio>
#include <string>
#include <cstring>
#include <stdexcept>

#include <boost/algorithm/string/predicate.hpp>

#include "constants.hpp"
#include "utils.hpp"

/**
 * class ModelNode - Node of the computational model
 *
 * @in1       : name of the variable corresponding to the first FU input
 * @in2       : name of the variable corresponding to the second FU input
 * @inSel     : name of the variable corresponding to the selection input
 * @out       : name of the output variable
 * @op        : operand
 * @placed    : flag marking the node as already place in the overlay
 * @extOut    : mark the node as possessing an external output
 *
 * @note: Node position in the graph will correspond to node position in the
 *        vector
 */
class ModelNode
{
public:
    /**
     * ModelNode() - Initialize a model node starting from a configuration line
     *
     * @cfgLine: node description used to initialize the node
     */
    ModelNode(const std::string &cfgLine)
    {
        /* As std::strtok() is destructive, we should work on a copy of
           the input string */
        char *work_cstr = (char *)malloc(cfgLine.size() + 1);
        strcpy(work_cstr, cfgLine.c_str());

        char *token = std::strtok(work_cstr,
                                  token_delimiters);
        std::vector< std::string > cfgTokens;
        while (token != NULL) {
            cfgTokens.push_back(std::string(token));
            token = std::strtok(NULL, token_delimiters);
        }

        free(work_cstr);

        if (cfgTokens.size() != 4) {
            log_err("Invalid model format (received as input %s)",
                    cfgLine.c_str());
            throw std::logic_error("Model error");
        }

        init(cfgTokens[0], cfgTokens[1], cfgTokens[2], cfgTokens[3]);
    }

    /**
     * ModelNode() - Build a model node from its components
     *
     * @in1: name of the first variable used by the FU
     * @in2: name of the second variable used by the FU
     * @op : name of the operation performed by the FU
     * @out: name of the variable produced by the FU
     */
    ModelNode(const std::string &in1,
              const std::string &in2,
              const std::string &op,
              const std::string &out)
    {
        init(in1, in2, op, out);
    }

    /**
     * ModelNode() - Create a new model node from an existing one
     *
     * @obj: source model node used in the construction
     */
    ModelNode(const ModelNode &obj)
    {
        in1 = obj.in1;
        in2 = obj.in2;
        inSel = obj.inSel;
        out = obj.out;
        op = obj.op;
        placed = obj.placed;
        extOut = obj.extOut;
    }

    /**
     * operator=() - Assign an existing model node to the current one
     *
     * @obj: source model of the assignment
     *
     * Return: reference back to the assigned node
     */
    ModelNode& operator=(const ModelNode &rhs)
    {
        if (this != &rhs) {
            in1 = rhs.in1;
            in2 = rhs.in2;
            inSel = rhs.inSel;
            out = rhs.out;
            op = rhs.op;
            placed = rhs.placed;
            extOut = rhs.extOut;
        }

        return *this;
    }

    /**
     * operator==() - Compare two nodes for equality
     *
     * @m1: first node in the comparison
     * @m2: second node in the comparison
     *
     * Return: true if the two nodes are equal, false otherwise
     */
    friend bool operator==(const ModelNode &m1, const ModelNode &m2)
    {
        if (m1.in1 == m2.in1 &&
            m1.in2 == m2.in2 &&
            m1.inSel == m2.inSel &&
            m1.out == m2.out &&
            m1.op == m2.op &&
            m1.placed == m2.placed &&
            m1.extOut == m2.extOut) {
            return true;
        }
        return false;
    }

    /**
     * operator==() - Compare two nodes for inequality
     *
     * @m1: first node in the comparison
     * @m2: second node in the comparison
     *
     * Return: true if the two nodes are different, false otherwise
     */
    friend bool operator!=(const ModelNode &m1, const ModelNode &m2)
    {
        return !(m1 == m2);
    }

    /**
     * getIn1() - Get the name of the variable on first FU input
     *
     * Return: variable name on first FU input
     */
    std::string getIn1() const
    {
        return in1;
    }

    /**
     * getIn2() - Get the name of the variable on second FU input
     *
     * Return: variable name on second FU input
     */
    std::string getIn2() const
    {
        return in2;
    }

    /**
     * getOut() - Get the name of the variable produced by the FU
     *
     * Return: variable name of FU output
     */
    std::string getOut() const
    {
        return out;
    }

    /**
     * getOp() - Get the name of the FU operator
     *
     * Return: FU's operator name
     */
    std::string getOp() const
    {
        return op;
    }

    /**
     * getOpEncoded() - Get the opcode of the operation performed by the node
     *
     * Return: opcode of the operation performed by the node
     */
    int getOpEncoded() const
    {
        if (op == OP_SUM) {
            return OPCODE_SUM;
        }
        if (op == OP_OR) {
            return OPCODE_OR;
        }
        if (op == OP_AND) {
            return OPCODE_AND;
        }
        if (op == OP_SUB) {
            return OPCODE_SUB;
        }
        if (op == OP_MUL) {
            return OPCODE_MUL;
        }
        if (op == OP_DIV) {
            return OPCODE_DIV;
        }
        if (op == OP_MOD) {
            return OPCODE_MOD;
        }
        if (op == OP_EQU) {
            return OPCODE_EQU;
        }
        if (op == OP_NEQ) {
            return OPCODE_NEQ;
        }
        if (op == OP_GEQ) {
            return OPCODE_GEQ;
        }
        if (op == OP_GRE) {
            return OPCODE_GRE;
        }
        if (op == OP_SEL) {
            return OPCODE_SEL;
        }
        /* Invalid op! */
        return 0x0;
    }

    /**
     * getInSel() - Get the name of the variable on selection FU input
     *
     * Return: variable name on selection FU input
     */
    std::string getInSel() const
    {
        return inSel;
    }

    bool hasExtOut() const
    {
        return extOut;
    }

    /**
     * isPlaced() - Check whether this node has already been placed
     *
     * Return: true if the node has already been placed, false otherwise
     */
    bool isPlaced() const
    {
        return placed;
    }

    /**
     * setPlaced() - Mark the node as placed
     */
    void setPlaced()
    {
        placed = true;
    }

    /**
     * getName() - Get a string useful to visually identify the operation
     *             performed in the node
     *
     * Return: string identifying the node
     */
    std::string getName() const
    {
        if (inSel == "") {
            return in1 + "_" + op + "_" + in2 + "_=_" + out;
        }
        return in1 + "_" + op + "_" + in2 + "_SEL_" + inSel + "=_" + out;
    }

private:
    /**
     * init() - Build a new model node from its exploded inputs and
     *          output variable names
     *
     * @in1       : name of the variable used as first FU input
     * @in2       : name of the variable used as second FU input
     * @op        : node's operation
     * @out       : name of the output variable
     */
    void init(const std::string &in1,
              const std::string &in2,
              const std::string &op,
              const std::string &out)
    {
        this->in1 = in1;
        this->in2 = in2;
        this->out = out;

        /* The variable used in SEL selection is appended after the
           operation name, therefore to retrieve it we just have to
           ignore the operation name */
        if (op.substr(0, OP_SEL.length()) == OP_SEL) {
            this->op = OP_SEL;
            inSel = op.substr(OP_SEL.length(),
                              op.length()-OP_SEL.length());
            if (inSel.length() == 0) {
                log_err("Invalid format for %s selector "
                        "variable (it must be a non-empty "
                        "string)", OP_SEL.c_str());
                throw std::logic_error("Model error");
            }
        } else {
            this->op = op;
        }

        /* Sanity check on names */
        if (this->in1.length() == 0 ||
            this->in2.length() == 0 ||
            this->out.length() == 0 ||
            this->op.length() == 0) {
            log_err("Invalid model format -- one or more names "
                    "are empty: in1=%s, in2=%s, out=%s, op=%s",
                    in1.c_str(), in2.c_str(),
                    out.c_str(), op.c_str());
            throw std::logic_error("Model error");
        }

        /* Init flags */
        this->placed = false;
        this->extOut = false;

        if (boost::starts_with(this->out, OUTPUT_PREFIX)) {
            this->extOut = true;
        }
    }

    std::string in1;
    std::string in2;
    std::string inSel;
    std::string out;
    std::string op;

    bool placed;
    bool extOut;
};

#endif /* MODEL_NODE_HPP_ */
