#ifndef DFE_HPP_
#define DFE_HPP_

#include <iostream>
#include <cassert>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <climits>
#include <stdexcept>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/graphviz.hpp>

#include "Model.hpp"
#include "ModelNode.hpp"

#define IMPROVE_PR    0

#define SKIP_CELLS    1
#define SHOW_PATHS    0

using namespace std;
using namespace boost;

/**
 * NODE_NAME() - Build the name of a node from row and column number
 *
 * @R: row number
 * @C: column number
 */
#define NODE_NAME(R, C) (to_string(R) + "_" + to_string(C))

/**
 * G_NODE() - Get the graph's node at a specified row/col
 *
 * @R: row number
 * @C: column number
 */
#define G_NODE(R, C) (g[nodes.at(NODE_NAME(R, C))])

/**
 * ROUTE_VAR() - Route a variable/constant
 *
 * @VERTEX  : model node
 * @VAR_NAME: variable name
 * @ROW     : cell's row number
 * @COL     : cell's column number
 */
#define ROUTE_VAR(VERTEX, VAR_NAME, ROW, COL)                           \
    if (VERTEX.VAR_NAME != "") {                                        \
        if (boost::starts_with(VERTEX.VAR_NAME, CONSTANT_PREFIX)) {     \
            ConstantInput c;                                            \
            c.name = VERTEX.VAR_NAME;                                   \
            c.row = ROW;                                                \
            c.col = COL;                                                \
            constants.push_back(c);                                     \
            log_info("Constant %s marked as required by cell "          \
                     "in %d, %d", VERTEX.VAR_NAME.c_str(), ROW, COL);   \
        } else {                                                        \
            log_info("Routing var %s from (%d, %d)",                    \
                     VERTEX.VAR_NAME.c_str(), ROW, COL);                \
            if (routeVariable(VERTEX.VAR_NAME, ROW, COL) == false) {    \
                log_info("Routing var %s FAILED!",                      \
                         VERTEX.VAR_NAME.c_str());                      \
                restoreStateBeforeRoute();                              \
                /* Skip to next position */                             \
                continue;                                               \
            }                                                           \
        }                                                               \
    }

/*
 * struct VertexProperties - Node of the DFE graph (= overlay cell)
 *
 * Instead of marking in the graph each individual connection (for instance,
 * from each external input to each output of the cell), which would result in a
 * huge graph), only cells are linked together. In this way we know that a given
 * cell has, for instance, a certain variable as output (but, if the cell has
 * multiple output -- like a corner cell --, we won't know where exactly this
 * output is). The exact computation of the individual links is done at a later
 * stage, when all the routing is done.
 *
 * @used     : flag marking the FU of the node as used (connections can still
 *             pass even if this value is false!)
 * @modelNode: index of the model node that is placed in this DFE node
 * @row      : DFE row where the node is located
 * @col      : DFE col where the node is located
 * @in1      : name of FU's first input variable
 * @in2      : name of FU's second input variable
 * @sel      : name of FU's selection input variable
 * @out      : name of FU's output variable
 * @hasExtOut: flag marking the cell as having an external output
 * @extIn    : list of external variables that enter this cell
 * @extOut   : list of external variables that leave this cell
 */
struct VertexProperties
{
    bool used;
    int modelNode;

    int row;
    int col;

    string in1;
    string in2;
    string sel;
    string out;
    bool hasExtOut;

    vector< string > extIn;
    vector< string > extOut;

    /**
     * VertexProperties() - Initialize a DFE graph node
     */
    VertexProperties()
    {
        used = false;
        modelNode = -1;
        row = -1;
        col = -1;
        in1 = "";
        in2 = "";
        sel = "";
        out = "";
        hasExtOut = false;
    }
};

/* Edge weight type */
typedef property< edge_weight_t, float > EdgeWeightProperty;

/*
 * Graph type (we place both nodes and edges in vectors, the most efficient
 * solution since we will not perform removals)
 */
typedef adjacency_list< listS,
                        vecS,
                        bidirectionalS,
                        VertexProperties,
                        EdgeWeightProperty > Graph;

/* Vertex and edge types, along with the corresponding iterators */
typedef Graph::vertex_descriptor Vertex;
typedef Graph::edge_descriptor Edge;
typedef graph_traits < Graph >::vertex_iterator VertexIt;
typedef graph_traits< Graph >::out_edge_iterator OutEdgeIt;

/* Type used to store row/col pairs of the attempted positions */
typedef vector< pair< int, int > > PositionList;

/* A path is represented as a simple list of vertices */
typedef list < Vertex > Path;

/*
 * A list of variables that are waiting for the source to be routed is
 * implemented as a list of vertices
 */
typedef list < Vertex > ConsumerList;

/**
 * struct CellConfig - Configuration values for a cell
 *
 * @IMPORTANT: values in arrays are stored in the
 *             NORTH - EAST - SOUTH - WEST
 *             order
 *
 * @inVars  : cell's input variables
 * @isInExt : mark each individual input variable as external
 * @outVars : cell's output variables
 * @isOutExt: mark each individual output variable as external
 * @vProp   : node properties (to ease configurationr retrieval)
 * @conf    : configuration value
 */
struct CellConfig
{
    /**
     * CellConfig() - Initialize cell's configuration
     */
    CellConfig()
    {
        conf = 0x0;
        for (int i = 0; i < CELL_SIDE_COUNT; ++i) {
            isInExt[i] = false;
            isOutExt[i] = false;
        }
    }

    string inVars[CELL_SIDE_COUNT];
    bool isInExt[CELL_SIDE_COUNT];
    string outVars[CELL_SIDE_COUNT];
    bool isOutExt[CELL_SIDE_COUNT];
    VertexProperties vProp;
    int conf;
};

/**
 * struct VarPaths - Routed paths for a given variable
 *
 * @src     : source vertex
 * @external: mark the linked paths as external I/O
 * @output  : mark the paths as output
 * @paths   : list of paths related with the variable
 */
struct VarPaths
{
    /**
     * VarPaths() - Initialize the paths
     */
    VarPaths()
    { external = false; output = false; }

    Vertex src;
    bool external;
    bool output;
    list< Path > paths;
};

/**
 * struct ConstantInput - Constant in the graph (simply a pair row-column, no
 *                        advanced P&R is done here!)
 */
struct ConstantInput
{
    string name;
    int row;
    int col;
};

/* Iterator on the map of variables and constants */
typedef map< string, VarPaths >::const_iterator VarMapIterator;
typedef map< string, pair< int, int > >::const_iterator ConstMapIterator;

/**
 * class DFE - Data Flow Engine (DFE) structure
 *
 * @g                 : graph overimposed to the DFE's structure
 * @model             : model that is P&R on the DFE
 * @rowsNo            : number of rows of the DFE
 * @colsNo            : number of cols of the DFE
 * @freeIN            : list of free input nodes
 * @freeConstIN       : list of free constant input nodes
 * @freeOUT           : list of free output nodes
 * @nodes             : mapping on the graph of the nodes composing the DFE
 * @variablesPaths    : map of the paths joining the variables
 * @waitingMap        : map keeping the list of nodes that are waiting for a
 *                      given variable
 * @constants         : list of model constants
 * @constantsPaths    : map of the paths for each constant
 * @placedNo          : number of nodes already placed
 * @bkp_g             : backup copy of the DFE's graph
 * @bkp_freeIN        : backup copy of the free input nodes' list
 * @bkp_freeOUT       : backup copy of the free output nodes' list
 * @bkp_nodes         : backup copy of the nodes of the graph
 * @bkp_constants     : backup copy of the model constants
 * @bkp_variablesPaths: backup copy of the map of the paths joining the
 *                      variables
 * @bkp_waitingMap    : backup copy of the list of nodes that are waiting for a
 *                      given variable
 */
class DFE
{
public:
    /**
     * DFE() - Given a model and the size of the DFE, initialize it
     *
     * @model : model imposed on the DFE
     * @rowsNo: number of rows of the DFE
     * @colsNo: number of rows of the DFE
     */
    DFE(const Model &model, const int rowsNo, const int colsNo);

    /**
     * DFE() - Copy constructor
     *
     * @obj: source DFE
     */
    DFE(const DFE &obj);

    /**
     * operator=() - Assignment operator
     *
     * @rhs: source DFE
     */
    DFE& operator=(const DFE &rhs);

#if IMPROVE_PR
    /**
     * improvePR() - Improve a terminated P&R by moving a given node
     *
     * @WARNING: Not working yet!
     *
     * @nodeToMove: index of the node to move
     *
     * Return: true if the change was feasible and led to an improved solution,
     *         false otherwise.
     */
    bool improvePR(const int nodeToMove);

    /**
     * selectNodeToMove() - Choose a node whole emplacement could be improved
     *
     * @WARNING: Not working yet!
     *
     * Return: index of the node to move.
     */
    int selectNodeToMove() const;
#endif /* IMPROVE_PR */

    /**
     * PRNode() - Try to place and route a specified model node, keeping into
     *            account previous attempts
     *
     * @modelNodeNo       : index of the model node that has to be P&R
     * @attemptedPositions: list of previously-attempted positions
     *
     * Return: true if the P&R was successful, false otherwise.
     */
    bool PRNode(const int modelNodeNo,
                PositionList& attemptedPositions);

    /**
     * routeConstants() - Route the model's constants
     *
     * Iterate on constants and do the routing. Since we cannot move constants,
     * once a routing fails we have no alternatives other than declaring all the
     * routing (including the variable part) as failed.
     *
     * Return: true if the P&R was successful, false otherwise.
     */
    bool routeConstants();

    /**
     * dumpConstants() - Write on file the configuration of the constants
     *
     * @configFName : output overlay configuration file name
     * @pinListFName: output pin list (mapping between variables and pin
     *                numbers) file name
     */
    void dumpConstants(const string& configFName,
                       const string& pinListFName);

    /**
     * dumpVariables() - Write on file the configuration of the variables
     *
     * @configFName : output overlay configuration file name
     * @pinListFName: output pin list (mapping between variables and pin
     *                numbers) file name
     */
    void dumpVariables(const string& configFName,
                       const string& pinListFName);

    /**
     * getWaitingVarCount() - Get the number of variables that are still waiting
     *                        their source to be P&R
     *
     * Return: number of variables that wait for their source.
     */
    int getWaitingVarCount() const;

    // vector< unsigned long > getConfig() const;

    /**
     * getPRNodeList() - Retrieve the list of nodes that have been successfully
     *                   P&R
     *
     * Return: list of indexes of the nodes that have been P&R.
     */
    std::vector< int > getPRNodeList() const;

private:
    /**
     * suggestPosition() - Suggest a position for a given model node, taking
     *                     into account previous attempts
     *
     * @modelNode         : node that has to be placed
     * @attemptedPositions: list of previously-attempted positions, the latest
     *                      element after function call being the
     *                      newly-suggested position
     *
     * Return: true if a candidate position has been found, false otherwise.
     */
    bool suggestPosition(const ModelNode& modelNode,
                         PositionList& attemptedPositions) const;

    /**
     * routeVariable() - Route a given variable placed at the specified location
     *
     * @var: variable's name
     * @row: cell's row
     * @col: cell's col
     *
     * Return: true if the routing was successful, false otherwise.
     */
    bool routeVariable(const string var,
                       const int row, const int col);

    /**
     * findPath() - Find the multiple-sources shortest path for a given node
     *
     * @start       : originating node
     * @isSrc       : flag marking the start node as a source(true) or a sink
     *                (false)
     * @availableVtx: replicas of the source/target nodes
     * @path        : found path
     *
     * Return: true if a path was found, false otherwise.
     */
    bool findPath(const Vertex start,
                  const bool isSrc,
                  const list< Vertex >& availableVtx,
                  Path& path);

    /**
     * getReplicas() - Build the list of all the replicas of a given source node
     *                 in a path list
     *
     * @vp      : path list
     * @replicas: list of obtained replicas
     */
    void getReplicas(const VarPaths& vp, list< Vertex >& replicas) const;

    /**
     * backupStateBeforeRoute() - Backup the current state before performing a
     *                            route operation
     */
    void backupStateBeforeRoute();

    /**
     * restoreStateBeforeRoute() - Restore the state as it was before the route
     *                             operation
     */
    void restoreStateBeforeRoute();

    /**
     * addDummyNode() - Add a dummy node to the graph to deal with
     *                  multiple-source/sink scenarios
     *
     * @isSrc       : flag marking the node to add as a source
     * @availableVtx: list of nodes to which the dummy node has to be tied
     * @dummyNode   : created dummy node
     * @dummyEdges  : created dummy edges
     */
    void addDummyNode(const bool isSrc,
                      const list< Vertex >& availableVtx,
                      Vertex& dummyNode,
                      vector< Edge >& dummyEdges);

    /**
     * dump() - Write on file the configuration (either of the variables, or the
     *          constants)
     *
     * @configFName : path of the configuration file to be written
     * @pinListFName: path of the pin list file to be written
     * @paths       : configured paths joining the cells
     * @constFlag   : flag to choose between the configuration of constants or
     *                variables
     */
    void dump(const string& configFName,
              const string& pinListFName,
              const map< string, VarPaths >& paths,
              const bool constFlag);

    /**
     * setPaths() - Mark the path of a variable in the cells
     *
     * Given the path of a variable, mark on each cell on the path the
     * entry-leaving points of the variable.
     *
     * @var   : variable that has to be marked
     * @config: configuration array
     * @start : beginning of the path to mark
     * @end   : end of the path to mark
     */
    void setPaths(const string var,
                  vector< CellConfig >& config,
                  const Path::const_iterator& start,
                  const Path::const_iterator& end);

    /**
     * writePin() - Add an I/O pin to the configuration
     *
     * @pos      : position of the cell
     * @dir      : direction faced by the pin
     * @inCount  : selected input pin number
     * @constFlag: the pin receives a constant value
     * @fpPL     : file pointer for the pin list
     * @config   : configuration array where the configuration of the pin
     *             resides
     */
    void writeInputPin(const int pos, const int dir,
                       const int inCount, const bool constFlag, FILE *fpPL,
                       const vector< CellConfig >& config);

    /**
     * writeOutputPin() - Add an output pin to the configuration
     *
     * @pos      : position of the cell
     * @dir      : direction faced by the pin
     * @outCount : selected output pin number
     * @fpPL     : file pointer for the pin list
     * @config   : configuration array where the configuration of the pin
     *             resides
     */
    void writeOutputPin(const int pos, const int dir, const int outCount,
                        FILE *fpPL, const vector< CellConfig >& config);

    /**
     * printVarMap() - Print the list of mapped variables with their paths
     */
    void printVarMap() const;

    /**
     * printConstMap() - Print the list of mapped constants with their paths
     */
    void printConstMap() const;

    /**
     * resetEdges() - Clear previously-configured edges and set them again
     */
    void resetEdges();

    /**
     * getWeightMatrix() - Create the weight matrix used in node selection
     *
     * @modelNode: previously-chosen node
     * @w        : computed weight matrix
     */
    void getWeightMatrix(const ModelNode& modelNode,
                         vector< vector< double > >& w) const;

    Graph g;
    Model model;
    int rowsNo;
    int colsNo;
    list< Vertex > freeIN;
    list< Vertex > freeConstIN;
    list< Vertex > freeOUT;
    map< string, Vertex > nodes;
    map< string, VarPaths > variablesPaths;
    map< string, ConsumerList > waitingMap;
    list< ConstantInput > constants;
    map< string, VarPaths > constantsPaths;
    int placedNo;

    Graph bkp_g;
    list< Vertex > bkp_freeIN;
    list< Vertex > bkp_freeOUT;
    map< string, Vertex > bkp_nodes;
    list< ConstantInput > bkp_constants;
    map< string, VarPaths > bkp_variablesPaths;
    map< string, ConsumerList > bkp_waitingMap;
};

#endif /* DFE_HPP_ */
