#include "DFE.hpp"

DFE::DFE(const Model &model, const int rowsNo, const int colsNo)
    : model(model), rowsNo(rowsNo), colsNo(colsNo)
{
    placedNo = 0;
    /* Build graph */
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            Vertex v = add_vertex(g);
            nodes[NODE_NAME(r, c)] = v;
            g[v].row = r;
            g[v].col = c;

            /* Add I/O vertices to the list of unused ones */
            if (r == 0 || r == rowsNo-1 ||
                c == 0 || c == colsNo-1) {
                freeIN.push_front(v);
                freeOUT.push_front(v);
                /* Add twice for the cells on the corners */
                if ((c == 0 || c == colsNo-1) &&
                    (r == 0  || r == rowsNo-1)) {
                    freeIN.push_front(v);
                    freeOUT.push_front(v);
                }
            }
        }
    }
    /* Add edges */
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            if (r < rowsNo-1) {
                /* N->S */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r+1, c)],
                         1, g);
            }
            if (r > 0) {
                /* S->N */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r-1, c)],
                         1, g);
            }

            if (c < colsNo-1) {
                /* W->E */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r, c+1)],
                         1, g);
            }
            if (c > 0) {
                /* E->W */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r, c-1)],
                         1, g);
            }
        }
    }
    freeConstIN = freeIN;
}

DFE::DFE(const DFE &obj)
    : model(obj.model)
{
    g = obj.g;
    placedNo = obj.placedNo;
    rowsNo = obj.rowsNo;
    colsNo = obj.colsNo;
    freeIN = obj.freeIN;
    freeConstIN = obj.freeConstIN;
    freeOUT = obj.freeOUT;
    nodes = obj.nodes;
    constants = obj.constants;
    variablesPaths = obj.variablesPaths;
    constantsPaths = obj.constantsPaths;
    waitingMap = obj.waitingMap;

    bkp_g = obj.bkp_g;
    bkp_freeIN = obj.bkp_freeIN;
    bkp_freeOUT = obj.bkp_freeOUT;
    bkp_nodes = obj.bkp_nodes;
    bkp_variablesPaths = obj.bkp_variablesPaths;
    bkp_constants = obj.bkp_constants;
    bkp_waitingMap = obj.bkp_waitingMap;
}

DFE& DFE::operator=(const DFE &rhs)
{
    if (this != &rhs) {
        g = rhs.g;
        placedNo = rhs.placedNo;
        model = rhs.model;
        rowsNo = rhs.rowsNo;
        colsNo = rhs.colsNo;
        freeIN = rhs.freeIN;
        freeConstIN = rhs.freeConstIN;
        freeOUT = rhs.freeOUT;
        nodes = rhs.nodes;
        variablesPaths = rhs.variablesPaths;
        constants = rhs.constants;
        constantsPaths = rhs.constantsPaths;
        waitingMap = rhs.waitingMap;

        bkp_g = rhs.bkp_g;
        bkp_freeIN = rhs.bkp_freeIN;
        bkp_freeOUT = rhs.bkp_freeOUT;
        bkp_nodes = rhs.bkp_nodes;
        bkp_variablesPaths = rhs.bkp_variablesPaths;
        bkp_constants = rhs.bkp_constants;
        bkp_waitingMap = rhs.bkp_waitingMap;
    }

    return *this;
}

bool DFE::PRNode(const int modelNodeNo,
                 PositionList& attemptedPositions)
{
    int row;
    int col;

    ModelNode modelNode = model.getModelNode(modelNodeNo);

    // log_info("******* VARIABLE MAP BEFORE PRNode ********");
    // printVarMap();
    // log_info("********************************************");

    while (suggestPosition(modelNode, attemptedPositions)) {
        row = attemptedPositions.back().first;
        col = attemptedPositions.back().second;

        Vertex v = nodes[NODE_NAME(row, col)];
        log_info("Suggested position (node %d): %d %d (%ld)",
                 modelNodeNo, row, col, v);

        backupStateBeforeRoute();

        /* Set node in place */
        g[v].used = true;
        g[v].modelNode = modelNodeNo;

        g[v].in1 = modelNode.getIn1();
        g[v].in2 = modelNode.getIn2();
        g[v].sel = modelNode.getInSel();
        g[v].out = modelNode.getOut();
        g[v].hasExtOut = modelNode.hasExtOut();

        log_info("Variables\n\tin1: %s\n\tin2: %s\n"
                 "\tsel: %s\n\tout: %s",
                 g[v].in1.c_str(), g[v].in2.c_str(),
                 g[v].sel.c_str(), g[v].out.c_str());

        /* Route each I/O */
        ROUTE_VAR(g[v], in1, row, col);
        ROUTE_VAR(g[v], in2, row, col);
        ROUTE_VAR(g[v], sel, row, col);
        ROUTE_VAR(g[v], out, row, col);

        log_info("******** VARIABLE MAP AFTER PRNode *********");
        printVarMap();
        log_info("********************************************");
        log_info("******** CONSTANT MAP AFTER PRNode *********");
        printConstMap();
        log_info("********************************************");

        // getchar();

        ++placedNo;

        return true;
    }
    log_info("Cannot place node %d anywhere in the current overlay, "
             "must BACKTRACK!", modelNodeNo);
    return false;
}

void DFE::getWeightMatrix(const ModelNode& modelNode,
                          vector< vector< double > >& w) const
{
    w.resize(rowsNo);
    for (int r = 0; r < rowsNo; ++r) {
        w[r].resize(colsNo, 1.0);
    }

    vector< string > varList;
    varList.push_back(modelNode.getIn1());
    varList.push_back(modelNode.getIn2());
    varList.push_back(modelNode.getOut());
    if (modelNode.getInSel() != "") {
        varList.push_back(modelNode.getInSel());
    }

    /* UGLY! */
    for (unsigned int i = 0; i < varList.size(); ++i) {
        string var = varList[i];
        if (variablesPaths.find(var) != variablesPaths.end()) {
            // log_info("VAR %s", var.c_str());
            VarPaths varPath = variablesPaths.at(var);
            list< Vertex > replicas;
            getReplicas(varPath, replicas);
            for (list< Vertex >::const_iterator rIt = replicas.begin();
                 rIt != replicas.end(); ++rIt) {
                const int row = *rIt/colsNo;
                const int col = *rIt%colsNo;
                // log_info("\t%lu (%d %d)", *rIt, row, col);
                if (row-2>=0) {
                    w[row-2][col] *= 4;
                    if (col-2>=0) {
                        w[row-2][col-2] *= 2;
                    }
                    if (col+2<colsNo) {
                        w[row-2][col+2] *= 2;
                    }
                }
                if (col-2>=0) {
                    w[row][col-2] *= 4;
                }
                if (col+2<colsNo) {
                    w[row][col+2] *= 4;
                }
                if (row+2<rowsNo) {
                    w[row+2][col] *= 4;
                    if (col-2>=0) {
                        w[row+2][col-2] *= 2;
                    }
                    if (col+2<colsNo) {
                        w[row+2][col+2] *= 2;
                    }
                }
            }
        }
    }
    // log_info("----- Final weight map -----");
    // for (int r = 0; r < rowsNo; ++r) {
    //  for (int c = 0; c < colsNo; ++c) {
    //      fprintf(stderr, "%.2f ", w[r][c]);
    //  }
    //  fprintf(stderr, "\n");
    // }
    // getchar();
}

bool DFE::suggestPosition(const ModelNode& modelNode,
                          PositionList& attemptedPositions) const
{
    /* STUB */
    PositionList freePos;
    vector< vector< double > > weightMatrix;
    vector< double > w;

    getWeightMatrix(modelNode, weightMatrix);

    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            if (G_NODE(r, c).used == false
#if SKIP_CELLS
                /*
                 * Skip rows/cols at first, start filling in when
                 * not possible anymore
                 */
                &&
                (placedNo >= (rowsNo/2)*(colsNo/2) ||
                 (r%2==1 && c%2==1))
#endif /* SKIP_CELLS */
                ) {
                freePos.push_back(make_pair(r, c));
            }
        }
    }

    sort(freePos.begin(), freePos.end());
    sort(attemptedPositions.begin(), attemptedPositions.end());

    PositionList unattempted;
    set_symmetric_difference(freePos.begin(),
                             freePos.end(),
                             attemptedPositions.begin(),
                             attemptedPositions.end(),
                             back_inserter(unattempted));

    if (unattempted.size() == 0) {
        log_info("Cannot suggest a new position "
                 "(no more positions left)!");
        return false;
    }

    for (unsigned int i = 0; i < unattempted.size(); ++i) {
        w.push_back(weightMatrix[unattempted[i].first][unattempted[i].second]);
    }

    // int pos = rand() % unattempted.size();
    // attemptedPositions.push_back(unattempted[pos]);

    int pos = randomWeightedSamplingWithReplacement(w);
    attemptedPositions.push_back(unattempted[pos]);

    return true;
}

void DFE::backupStateBeforeRoute()
{
    bkp_g = g;
    bkp_freeIN = freeIN;
    bkp_freeOUT = freeOUT;
    bkp_nodes = nodes;
    bkp_variablesPaths = variablesPaths;
    bkp_constants = constants;
    bkp_waitingMap = waitingMap;
}

void DFE::restoreStateBeforeRoute()
{
    g = bkp_g;
    freeIN = bkp_freeIN;
    freeOUT = bkp_freeOUT;
    nodes = bkp_nodes;
    variablesPaths = bkp_variablesPaths;
    constants = bkp_constants;
    waitingMap = bkp_waitingMap;
}

bool DFE::routeVariable(const string var,
                        const int row, const int col)
{
    VarPaths varPath;
    Path path;

    if (G_NODE(row, col).out == var) {
        log_info("Var %s is an output one!", var.c_str());

        assert (variablesPaths.find(var) == variablesPaths.end());

        /*
         * We can set src here, since we're sure no one created an
         * entry in variablesPaths[] before!
         */
        varPath.src = nodes.at(NODE_NAME(row, col));

        /* If the variable is sent as output, a pin has to be placed */
        if (G_NODE(row, col).hasExtOut) {
            log_info("*** Seeking an output pin ***");
            if (freeOUT.size() == 0) {
                throw runtime_error("Not enough output pins "
                                    "available!");
            }

            Vertex toMarkUsed;
            if (find(freeOUT.begin(),
                     freeOUT.end(), varPath.src) != freeOUT.end()) {
                /* Output can be routed directly */
                toMarkUsed = varPath.src;
                path.push_back(toMarkUsed);
            } else {
                if (!findPath(varPath.src, true,
                              freeOUT, path)) {
                    return false;
                }

                toMarkUsed = path.back();
            }
            varPath.external = true;
            varPath.output = true;
            /*
             * This excludes from push the case when the placed node
             * is already on the path of the needed variable
             */
            if (path.size() > 0) {
                varPath.paths.push_back(path);
                path.clear();
            } else {
                log_info("Skipping path insertion (already "
                         "implicitly done)");
            }

            list< Vertex >::iterator it = find(freeOUT.begin(),
                                               freeOUT.end(),
                                               toMarkUsed);
            assert(it != freeOUT.end());

            freeOUT.erase(it);
            g[toMarkUsed].extOut.push_back(var);
        }

        /*
         * This node produces out, therefore we have to check if a
         * previously routed node needs it
         */
        if (waitingMap.find(var) != waitingMap.end()) {
            log_info("Node produces output variable %s, checking "
                     "who is waiting for it...", var.c_str());

            list< Vertex > replicas;

            for (ConsumerList::iterator it = waitingMap[var].begin();
                 it != waitingMap[var].end(); ++it) {
                log_info("*** Routing cell's output (var %s) to"
                         " vertex %ld ***", var.c_str(), *it);
                getReplicas(varPath, replicas);
                log_info("\tFound %lu replicas along the path",
                         replicas.size()-1);

                if (!findPath(*it, false, replicas, path)) {
                    return false;
                }

                /*
                 * This excludes from push the case when the placed node
                 * is already on the path of the needed variable
                 */
                if (path.size() > 0) {
                    varPath.paths.push_back(path);
                    path.clear();
                } else {
                    log_info("Skipping path insertion (already "
                             "implicitly done)");
                }
            }

            /* Variable has been routed, now we can drop it from
               the list of waiting ones */
            waitingMap.erase(var);
        } else if (!G_NODE(row, col).hasExtOut) {
            log_info("Variable %s is not needed yet, storing it",
                     var.c_str());
        }
    } else {
        /*
         * This node needs the variable as an input, check if it is
         * already computed by a node
         */
        if (variablesPaths.find(var) == variablesPaths.end()) {
            log_info("Variable %s NOT PRESENT YET!", var.c_str());

            /* Check if var is external */
            if (model.isExtVar(G_NODE(row, col).modelNode, var)) {
                log_info("Variable %s is external, routing it",
                         var.c_str());
                if (freeIN.size() == 0) {
                    throw runtime_error("Not enough input "
                                        "pins available!");
                }

                Vertex toMarkUsed;
                if (find(freeIN.begin(),
                         freeIN.end(),
                         nodes.at(NODE_NAME(row, col))) != freeIN.end()) {
                    log_info("Input can be routed directly "
                             "to (%d %d)!", row, col);
                    toMarkUsed = nodes.at(NODE_NAME(row, col));
                    path.push_back(toMarkUsed);
                } else {
                    log_info("*** Seeking an input pin ***");
                    if (!findPath(nodes.at(NODE_NAME(row, col)),
                                  false, freeIN, path)) {
                        log_info("FAIL!");
                        return false;
                    }

                    toMarkUsed = path.front();
                    log_info("Source pin cell number: %ld",
                             toMarkUsed);
                }
                varPath.paths.push_back(path);
                path.clear();

                list< Vertex >::iterator it = find(freeIN.begin(),
                                                   freeIN.end(),
                                                   toMarkUsed);
                assert(it != freeIN.end());

                freeIN.erase(it);

                g[toMarkUsed].extIn.push_back(var);
                varPath.src = toMarkUsed;
                varPath.external = true;
            } else {
                /*
                 * The variable is produced by a node not yet
                 * in place
                 */
                waitingMap[var].push_back(nodes.at(NODE_NAME(row,
                                                             col)));
                log_info("Variable %s is produced by a node not"
                         " yet in place, putting the input in "
                         "the waiting list", var.c_str());
                return true;
            }
        } else {
            log_info("Variable %s is produced by a node already in "
                     "place (%ld), routing it!",
                     var.c_str(), variablesPaths[var].src);

            varPath = variablesPaths[var];

            list< Vertex > replicas;
            getReplicas(varPath, replicas);

            if (!findPath(nodes.at(NODE_NAME(row, col)), false,
                          replicas, path)) {
                log_info("FAIL!");
                return false;
            }
            /*
             * This excludes from push the case when the placed node
             * is already on the path of the needed variable
             */
            if (path.size() > 0) {
                varPath.paths.push_back(path);
                path.clear();
            } else {
                log_info("Skipping path insertion (already "
                         "implicitly done)");
            }
        }
    }

    variablesPaths[var] = varPath;

    // log_info("========== PATHs ==========");
    // printVarMap();
    // log_info("================================");

    return true;
}

void DFE::addDummyNode(const bool isSrc,
                       const list< Vertex >& availableVtx,
                       Vertex& dummyNode,
                       vector< Edge >& dummyEdges)
{
    dummyNode = add_vertex(g);

    list< Vertex >::const_iterator avNIt;
    Edge e;
    bool b;
    for (avNIt = availableVtx.begin(); avNIt != availableVtx.end();
         ++avNIt) {
        if (isSrc) {
            /* Get a single output node */
            tie(e, b) = add_edge(*avNIt, dummyNode, g);
        } else {
            /* Get a single input source */
            tie(e, b) = add_edge(dummyNode, *avNIt, g);
        }
        dummyEdges.push_back(e);
    }
}

void DFE::printVarMap() const {
#ifdef INFO_MSG
    for (VarMapIterator vIt = variablesPaths.begin();
         vIt != variablesPaths.end(); ++vIt) {
        fprintf(stderr, "Variable %s, source node: %ld, external: %d, output: %d\n",
                vIt->first.c_str(), vIt->second.src,
                vIt->second.external, vIt->second.output);
        for (list< Path >::const_iterator pIt = vIt->second.paths.begin();
             pIt != vIt->second.paths.end(); ++pIt) {
            fprintf(stderr, "\t");
            for (Path::const_iterator it = pIt->begin();
                 it != pIt->end(); ++it) {
                fprintf(stderr, "%ld ", *it);
            }
            fprintf(stderr, "\n");
        }
    }
#endif /* INFO_MSG */
}

void DFE::printConstMap() const {
#ifdef INFO_MSG
    for (list<ConstantInput>::const_iterator cIt = constants.begin();
         cIt != constants.end(); ++cIt) {
        fprintf(stderr, "Constant %s required for node in %d, %d\n",
                cIt->name.c_str(), cIt->row, cIt->col);
    }
    for (VarMapIterator vIt = constantsPaths.begin();
         vIt != constantsPaths.end(); ++vIt) {
        fprintf(stderr, "Constant %s, source node: %ld, external: %d, output: %d\n",
                vIt->first.c_str(), vIt->second.src,
                vIt->second.external, vIt->second.output);
        for (list< Path >::const_iterator pIt = vIt->second.paths.begin();
             pIt != vIt->second.paths.end(); ++pIt) {
            fprintf(stderr, "\t");
            for (Path::const_iterator it = pIt->begin();
                 it != pIt->end(); ++it) {
                fprintf(stderr, "%ld ", *it);
            }
            fprintf(stderr, "\n");
        }
    }
#endif /* INFO_MSG */
}

/*
 * Multiple-sources shortest path:
 * 1. add a dummy node connected to all the possible sources
 * 2. find shortest path
 * 3. drop the dummy connection in the shortest path
 * 4. clear out all the dummy edges and the dummy node
 */
bool DFE::findPath(const Vertex start,
                   const bool isSrc,
                   const list< Vertex >& availableVtx,
                   Path& path)
{
    /*
     * isSrc tells us if the originating node (start) was
     * supposed to send out a signal (thus, a source). For
     * instance, for an output node, this flag will be true.
     */

    Vertex dummyNode;
    vector < Edge > dummyEdges;
    addDummyNode(isSrc, availableVtx, dummyNode, dummyEdges);

    vector< Vertex > parents(num_vertices(g));
    vector< int > distances(num_vertices(g));

    if (isSrc) {
        log_info("\tNode %ld is a source node", start);
        dijkstra_shortest_paths(g,
                                start,
                                predecessor_map(&parents[0]).distance_map(&distances[0]));

        if (distances[dummyNode] > rowsNo*colsNo) {
            /* dummyNode cannot be reached from dummyNode */
            log_info("+++ Output path not found!");
            clear_vertex(dummyNode, g);
            remove_vertex(dummyNode, g);
            return false;
        }
        if (parents[dummyNode] == start) {
            /*
             * Start directly linked with dummyNode (thus it was in
             * the list of possible outputs) -> return directly
             */
            log_info("+++ The node is already connected to the "
                     "desired OUT!");
            clear_vertex(dummyNode, g);
            remove_vertex(dummyNode, g);
            return true;
        }

        log_info("\tPath FOUND! Distance: %d", distances[dummyNode]-1);

        Vertex v = dummyNode;
        while (v != start) {
            v = parents[v];
            path.push_front(v);
            remove_edge(parents[v], v, g);
        }

#if SHOW_PATHS
        log_info("---[ PATH %ld -> %ld]---", start, parents[dummyNode]);

        vector< pair< int, int > > visualPath;
        for (Path::iterator it1 = next(path.begin(), 1);
             it1 != path.end(); ++it1) {
            log_info("+ %ld->%ld", *prev(it1, 1), *it1);
            visualPath.push_back(make_pair(*prev(it1, 1), *it1));
        }
        showPath(start, parents[dummyNode], rowsNo, colsNo, visualPath);
#endif /* SHOW_PATHS */

    } else {
        log_info("\tNode %ld is a target node", start);
        dijkstra_shortest_paths(g,
                                dummyNode,
                                predecessor_map(&parents[0]).distance_map(&distances[0]));

        if (distances[start] > rowsNo*colsNo) {
            /* start cannot be reached from dummyNode */
            log_info("+++ Input path not found!");
            clear_vertex(dummyNode, g);
            remove_vertex(dummyNode, g);
            return false;
        }
        if (parents[start] == dummyNode) {
            /*
             * Start directly linked with dummyNode (thus it was in
             * the list of possible sources) -> return directly
             */
            log_info("+++ The node is already connected to the "
                     "desired IN!");
            clear_vertex(dummyNode, g);
            remove_vertex(dummyNode, g);
            return true;
        }

        log_info("\tPath FOUND! Distance: %d", distances[start]-1);

        Vertex v = start;
        while (v != dummyNode) {
            path.push_front(v);
            log_info("Pushing %ld", v);
            if (parents[v] != dummyNode) {
                remove_edge(parents[v], v, g);
            }
            v = parents[v];
        }

#if SHOW_PATHS
        v = start;
        while (parents[v] != dummyNode) {
            v = parents[v];
        }

        log_info("---[ PATH %ld -> %ld]---", v, start);

        vector< pair< int, int > > visualPath;
        for (Path::iterator it1 = next(path.begin(), 1);
             it1 != path.end(); ++it1) {
            log_info("+ %ld->%ld", *prev(it1, 1), *it1);
            visualPath.push_back(make_pair(*prev(it1, 1), *it1));
        }
        showPath(v, start, rowsNo, colsNo, visualPath);
#endif /* SHOW_PATHS */
    }

    clear_vertex(dummyNode, g);
    remove_vertex(dummyNode, g);

    return true;
}

void DFE::getReplicas(const VarPaths& vp, list< Vertex >& replicas) const
{
    replicas.push_back(vp.src);
    for (list< Path >::const_iterator it = vp.paths.begin();
         it != vp.paths.end(); ++it) {
        replicas.insert(replicas.end(), it->begin(), it->end());
    }
    replicas.sort();
    replicas.erase(unique(replicas.begin(), replicas.end()),
                   replicas.end());

    // log_info("======= REPLICAS ==========");
    // for (Path::const_iterator it = replicas.begin();
    //      it != replicas.end(); ++it) {
    //  fprintf(stderr, "%ld ", *it);
    // }
    // fprintf(stderr, "\n");
}

int DFE::getWaitingVarCount() const
{
    return waitingMap.size();
}

std::vector< int > DFE::getPRNodeList() const
{
    std::vector< int > placed;
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            if (G_NODE(r,c).used) {
                placed.push_back(G_NODE(r,c).modelNode);
            }
        }
    }
    return placed;
}

void DFE::dumpVariables(const string& configFName,
                        const string& pinListFName)
{
    dump(configFName, pinListFName, variablesPaths, false);
}

void DFE::dumpConstants(const string& configFName,
                        const string& pinListFName)
{
    dump(configFName, pinListFName, constantsPaths, true);
}

void DFE::dump(const string& configFName,
               const string& pinListFName,
               const map< string, VarPaths >& paths,
               const bool constFlag)
{
    FILE *fp = fopen(configFName.c_str(), "wt");
    if (fp == NULL) {
        log_err("Cannot open output config file (%s)",
                configFName.c_str());
        throw runtime_error("Output file error");
    }

    /*
     * Write header compatible with FPGA's interface code:
     * - start command (must be 0x0)
     * - 16 bits # of data + 16 bits offset (offset set to 4 -- for the
     *   moment no reason to move this block farther)
     * - status (0x0)
     * - pattern (0x0)
     */
    fprintf(fp, "0x0\n0x%04x%04x\n0x0\n0x0\n",
            rowsNo*colsNo, 0x4);

    /* Initialize empty config */
    vector< CellConfig > config(rowsNo*colsNo);
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            if (!constFlag) {
                config[r*colsNo+c].vProp = g[nodes[NODE_NAME(r, c)]];
            } else {
                VertexProperties vp;
                vp.row = r;
                vp.col = c;
                config[r*colsNo+c].vProp = vp;
            }
        }
    }

    /*
     * Collect variable names and set the variable in each cell IO at the proper
     * location. For instance, if two cells communicate on the N->S axes, the
     * S output of the first cell and the N output of the second one will have
     * their entry set to the considered variable name.
     */
    for (VarMapIterator vIt = paths.begin(); vIt != paths.end(); ++vIt) {
        for (list< Path >::const_iterator pIt = vIt->second.paths.begin();
             pIt != vIt->second.paths.end(); ++pIt) {
            setPaths(vIt->first, config, next(pIt->begin(), 1), pIt->end());
        }
    }

    /* Set external I/O */
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            vector< int > availIN;
            vector< int > availOUT;

            if (r == 0) {
                availIN.push_back(NORTH);
            }
            if (r == rowsNo-1) {
                availIN.push_back(SOUTH);
            }
            if (c == 0) {
                availIN.push_back(WEST);
            }
            if (c == colsNo-1) {
                availIN.push_back(EAST);
            }
            availOUT = availIN;

            Vertex v = nodes[NODE_NAME(r, c)];

            // log_info("++++++++++++++ NODE %d, %d ++++++++", r, c);

            for (int iIn = 0; iIn < (int)g[v].extIn.size(); ++iIn) {

                // log_info("IN var: %s", g[v].extIn[iIn].c_str());

                // log_info("Current config IN: [%s] [%s] [%s] [%s]\n"
                //   "              OUT: [%s] [%s] [%s] [%s]",
                //   config[r*colsNo+c].inVars[0].c_str(),
                //   config[r*colsNo+c].inVars[1].c_str(),
                //   config[r*colsNo+c].inVars[2].c_str(),
                //   config[r*colsNo+c].inVars[3].c_str(),
                //   config[r*colsNo+c].outVars[0].c_str(),
                //   config[r*colsNo+c].outVars[1].c_str(),
                //   config[r*colsNo+c].outVars[2].c_str(),
                //   config[r*colsNo+c].outVars[3].c_str());

                if ((!constFlag &&
                     !boost::starts_with(g[v].extIn[iIn],
                                         CONSTANT_PREFIX)) ||
                    (constFlag &&
                     boost::starts_with(g[v].extIn[iIn],
                                        CONSTANT_PREFIX))) {
                    config[r*colsNo+c].inVars[availIN.back()] =
                        g[v].extIn[iIn];
                    config[r*colsNo+c].isInExt[availIN.back()] =
                        true;
                    availIN.pop_back();
                }
            }
            if (!constFlag) {
                for (int iOut = 0; iOut < (int)g[v].extOut.size(); ++iOut) {

                    // log_info("OUT var: %s", g[v].extOut[iOut].c_str());

                    config[r*colsNo+c].outVars[availOUT.back()] =
                        g[v].extOut[iOut];
                    config[r*colsNo+c].isOutExt[availOUT.back()] =
                        true;

                    availOUT.pop_back();
                }
            }
        }
    }

    /* Connect matching variables inside each cell */
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            for(int i = 0; i < CELL_SIDE_COUNT; ++i) {
                for(int j = i+1; j < CELL_SIDE_COUNT; ++j) {
                    if (config[r*colsNo+c].inVars[i].length() > 0 &&
                        config[r*colsNo+c].inVars[i] ==
                        config[r*colsNo+c].outVars[j]) {
                        config[r*colsNo+c].conf |=
                            INTERNAL_CONNECTIONS[j][i];
                    }
                    if (config[r*colsNo+c].outVars[i].length() > 0 &&
                        config[r*colsNo+c].outVars[i] ==
                        config[r*colsNo+c].inVars[j]) {
                        config[r*colsNo+c].conf |=
                            INTERNAL_CONNECTIONS[i][j];
                    }
                }
            }
        }
    }

    /*
     * Connect FU's I/Os and set operation to perform, then write cell to file
     */
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            Vertex v = nodes[NODE_NAME(r, c)];
            if (g[v].used) {
                for (int i = 0; i < CELL_SIDE_COUNT; ++i) {
                    if (config[r*colsNo+c].inVars[i].length() > 0 &&
                        config[r*colsNo+c].inVars[i] == g[v].in1) {
                        config[r*colsNo+c].conf |=
                            IO_CONNECTIONS[V_IN1][i];
                    }
                    if (config[r*colsNo+c].inVars[i].length() > 0 &&
                        config[r*colsNo+c].inVars[i] == g[v].in2) {
                        config[r*colsNo+c].conf |=
                            IO_CONNECTIONS[V_IN2][i];
                    }
                    if (config[r*colsNo+c].inVars[i].length() > 0 &&
                        config[r*colsNo+c].inVars[i] == g[v].sel) {
                        config[r*colsNo+c].conf |=
                            IO_CONNECTIONS[V_INSEL][i];
                    }
                    if (config[r*colsNo+c].outVars[i].length() > 0 &&
                        config[r*colsNo+c].outVars[i] == g[v].out) {
                        config[r*colsNo+c].conf |=
                            IO_CONNECTIONS[V_OUT][i];
                    }
                }
                if (boost::starts_with(g[v].in1, CONSTANT_PREFIX)) {
                    config[r*colsNo+c].conf |=
                        FU_IN1_CONSTANT;
                }
                if (boost::starts_with(g[v].in2, CONSTANT_PREFIX)) {
                    config[r*colsNo+c].conf |=
                        FU_IN2_CONSTANT;
                }
                config[r*colsNo+c].conf |=
                    model.getModelNode(g[v].modelNode).getOpEncoded();
            }

            fprintf(fp, "%#X\n", config[r*colsNo+c].conf);
        }
    }

    fclose(fp);

    FILE *fpPL = fopen(pinListFName.c_str(), "wt");
    if (fpPL == NULL) {
        log_err("Cannot open pin list file (%s)", pinListFName.c_str());
        throw runtime_error("Output file error");
    }

    /*
     * Change 15.03.2017: introduced separate input FIFOs on the four sides in
     *                    the hardware, this resulted in a change in the
     *                    numbering scheme.
     *
     * -- BEFORE --
     * + Inputs : numbers from 0 to (perimeter-1), starting from bottom-left
     *            corner
     * + Outputs: numbers from 0 to (perimeter-1), starting from top-right
     *            corner
     *
     * -- AFTER --
     * + Inputs : numbers from 0 to (side_length-1) in the lower 6 bits, the two
     *            MSB encode the side (00 = N, 01 = W, 10 = E, 11 = S)
     * + Outputs: numbers from 0 to (perimeter-1), starting from top-right
     *            corner (SAME as BEFORE)
     */

    /* Write inputs */
    /* First and last row */
    for (int c = 0; c < colsNo; ++c) {
        writeInputPin(c, NORTH,
                      0x00 | c,
                      constFlag, fpPL, config);
        writeInputPin((rowsNo-1)*colsNo+c, SOUTH,
                      0xC0 | ((colsNo-1)-c),
                      constFlag, fpPL, config);
    }

    /* First and last column */
    for (int r = 0; r < rowsNo; ++r) {
        writeInputPin(r*colsNo, WEST,
                      0x40 | ((rowsNo-1)-r),
                      constFlag, fpPL, config);
        writeInputPin(r*colsNo+(colsNo-1), EAST,
                      0x80 | r,
                      constFlag, fpPL, config);
    }

    /* Write outputs */
    if (!constFlag) {
        /* First and last column */
        for (int r = 0; r < rowsNo; ++r) {
            writeOutputPin(r*colsNo, WEST,
                           (rowsNo)+(colsNo)+(rowsNo-1-r),
                           fpPL, config);
            writeOutputPin(r*colsNo+(colsNo-1), EAST,
                           r,
                           fpPL, config);
        }
        /* First and last row */
        for (int c = 0; c < colsNo; ++c) {
            writeOutputPin(c, NORTH,
                           (2*rowsNo)+(colsNo)+(c),
                           fpPL, config);
            writeOutputPin((rowsNo-1)*colsNo+c, SOUTH,
                           rowsNo+colsNo-c-1,
                           fpPL, config);
        }
    }

    fclose(fpPL);
}

void DFE::writeInputPin(const int pos, const int dir,
                        const int inCount, const bool constFlag, FILE *fpPL,
                        const vector< CellConfig >& config)
{
    if (config[pos].isInExt[dir] &&
        (!boost::starts_with(config[pos].inVars[dir], CONSTANT_PREFIX) ||
         constFlag)) {
        fprintf(fpPL,
                "%s %d 1\n",
                config[pos].inVars[dir].c_str(),
                inCount);
    }
}

void DFE::writeOutputPin(const int pos, const int dir,
                         const int outCount, FILE *fpPL,
                         const vector< CellConfig >& config)
{
    if (config[pos].isOutExt[dir]) {
        fprintf(fpPL,
                "%s %d 0\n", config[pos].outVars[dir].c_str(), outCount);
    }
}

void DFE::setPaths(const std::string var,
                   vector< CellConfig >& config,
                   const Path::const_iterator& start,
                   const Path::const_iterator& end)
{
    // log_info("############## VAR %s ##########",
    //   var.c_str());

    for (Path::const_iterator it = start; it != end; ++it) {
        int a = *prev(it, 1);
        int b = *it;
        int a_dir;
        int b_dir;

        if (a-b > 1) {
            a_dir = NORTH;
            b_dir = SOUTH;
        } else if (a-b == 1) {
            a_dir = WEST;
            b_dir = EAST;
        } else if (a-b == -1) {
            a_dir = EAST;
            b_dir = WEST;
        } else {
            a_dir = SOUTH;
            b_dir = NORTH;
        }
        config[a].outVars[a_dir] = var;
        config[b].inVars[b_dir] = var;

        // log_info("a = %d -> b = %d -> a: %d, b: %d",
        //   a, b, a_dir, b_dir);
    }
}

void DFE::resetEdges()
{
    /* Remove previous edges and re-create them */
    pair< VertexIt, VertexIt > vertexPair;
    for (vertexPair = vertices(g);
         vertexPair.first != vertexPair.second; ++vertexPair.first) {
        clear_vertex(*vertexPair.first, g);
    }
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            if (r < rowsNo-1) {
                /* N->S */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r+1, c)],
                         1, g);
            }
            if (r > 0) {
                /* S->N */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r-1, c)],
                         1, g);
            }

            if (c < colsNo-1) {
                /* W->E */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r, c+1)],
                         1, g);
            }
            if (c > 0) {
                /* E->W */
                add_edge(nodes[NODE_NAME(r, c)],
                         nodes[NODE_NAME(r, c-1)],
                         1, g);
            }
        }
    }
}

bool DFE::routeConstants()
{
    resetEdges();

    for (list<ConstantInput>::const_iterator cIt = constants.begin();
         cIt != constants.end(); ++cIt) {
        const string var = cIt->name.c_str();
        VarPaths varPath;
        if (constantsPaths.find(var) != constantsPaths.end()) {
            log_info("Resumed existing varPath...");
            varPath = constantsPaths[var];
        }

        Path path;
        const int row = cIt->row;
        const int col = cIt->col;
        log_info("** Routing constant %s to node in %d, %d **",
                 var.c_str(), row, col);

        if (freeConstIN.size() == 0) {
            throw runtime_error("Not enough input pins available for constants!");
        }

        Vertex toMarkUsed;
        bool rerouted = false;
        if (find(freeConstIN.begin(), freeConstIN.end(),
                 nodes.at(NODE_NAME(row, col))) != freeConstIN.end()) {
            log_info("\tConstant can be routed directly to (%d %d)!", row, col);
            toMarkUsed = nodes.at(NODE_NAME(row, col));
            path.push_back(toMarkUsed);
        } else {
            /* Check if constant already routed, otherwise route it */
            if (constantsPaths.find(var) != constantsPaths.end()) {
                log_info("\t*** Constant was previously routed, "
                         "seeking an additional path ***");
                list< Vertex > replicas;
                getReplicas(varPath, replicas);
                if (!findPath(nodes.at(NODE_NAME(row, col)), false,
                              replicas, path)) {
                    log_info("FAILED to branch a previous path");
                    return false;
                } else {
                    rerouted = true;
                }
            } else {
                log_info("\t*** Seeking an input pin ***");
                if (!findPath(nodes.at(NODE_NAME(row, col)),
                              false, freeConstIN, path)) {
                    log_info("\tFAIL!");
                    return false;
                }
            }

            toMarkUsed = path.front();
            log_info("\tSource pin cell number: %ld", toMarkUsed);
        }
        varPath.paths.push_back(path);

        path.clear();

        if (!rerouted) {
            list< Vertex >::iterator it = find(freeConstIN.begin(),
                                               freeConstIN.end(),
                                               toMarkUsed);
            assert(it != freeConstIN.end());
            freeConstIN.erase(it);
            g[toMarkUsed].extIn.push_back(var);
            varPath.external = true;
            varPath.src = toMarkUsed;
        }
        constantsPaths[var] = varPath;
    }

    log_info("******** CONSTANT MAP AFTER routeConstants *********");
    printConstMap();
    log_info("****************************************************");

    return true;
}

#if IMPROVE_PR

bool DFE::improvePR(const int nodeToMove)
{
    ModelNode modelNode = model.getModelNode(nodeToMove);
    log_info("\tMoving node %d will affect the following variables:",
             nodeToMove);
    log_info("\t---- In1 = %s ----",
             modelNode.getIn1().c_str());
    for (list< Path >::const_iterator pIt =
             variablesPaths[modelNode.getIn1()].paths.begin();
         pIt != variablesPaths[modelNode.getIn1()].paths.end(); ++pIt) {
        fprintf(stderr, "\t");
        for (Path::const_iterator it = pIt->begin();
             it != pIt->end(); ++it) {
            fprintf(stderr, "%ld ", *it);
        }
        fprintf(stderr, "\n");
    }

    log_info("\t---- In2 = %s ----",
             modelNode.getIn2().c_str());
    for (list< Path >::const_iterator pIt =
             variablesPaths[modelNode.getIn2()].paths.begin();
         pIt != variablesPaths[modelNode.getIn2()].paths.end(); ++pIt) {
        fprintf(stderr, "\t");
        for (Path::const_iterator it = pIt->begin();
             it != pIt->end(); ++it) {
            fprintf(stderr, "%ld ", *it);
        }
        fprintf(stderr, "\n");
    }

    if (modelNode.getInSel() != "") {
        log_info("\t---- InSel = %s ----",
                 modelNode.getInSel().c_str());
        for (list< Path >::const_iterator pIt =
                 variablesPaths[modelNode.getInSel()].paths.begin();
             pIt != variablesPaths[modelNode.getInSel()].paths.end(); ++pIt) {
            fprintf(stderr, "\t");
            for (Path::const_iterator it = pIt->begin();
                 it != pIt->end(); ++it) {
                fprintf(stderr, "%ld ", *it);
            }
            fprintf(stderr, "\n");
        }
    } else {
        log_info("\t---- InSel not used");
    }

    log_info("\t---- Out = %s ----",
             modelNode.getOut().c_str());
    for (list< Path >::const_iterator pIt =
             variablesPaths[modelNode.getOut()].paths.begin();
         pIt != variablesPaths[modelNode.getOut()].paths.end(); ++pIt) {
        fprintf(stderr, "\t");
        for (Path::const_iterator it = pIt->begin();
             it != pIt->end(); ++it) {
            fprintf(stderr, "%ld ", *it);
        }
        fprintf(stderr, "\n");
    }

    // Vertex initialPos = variablesPaths[modelNode.getOut()].src;
    // log_info("\tNodes' position: %lu", initialPos);

    // getchar();

    return true;
}

int DFE::selectNodeToMove() const
{
    /* STUB */
    vector< int > nodesPos;
    for (int r = 0; r < rowsNo; ++r) {
        for (int c = 0; c < colsNo; ++c) {
            if (G_NODE(r, c).used == true) {
                nodesPos.push_back(G_NODE(r, c).modelNode);
            }
        }
    }
    int pos = rand() % nodesPos.size();
    return nodesPos[pos];
}

#endif /* IMPROVE_PR */
