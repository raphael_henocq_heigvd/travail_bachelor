#ifndef MODEL_HPP_
#define MODEL_HPP_

#include <iostream>
#include <vector>
#include <string>
#include <deque>
#include <fstream>
#include <stdexcept>
#include <cstdio>
#include <cstring>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/isomorphism.hpp>
#include <boost/graph/graphviz.hpp>

#include "ModelNode.hpp"
#include "utils.hpp"

using namespace boost;

#define OUTPUT_MODEL_GRAPH    0

/**
 * class Model - Model of the computations that have to be placed and routed
 *
 * @g    : model's graph
 * @nodes: nodes present in the graph
 */
class Model
{
    typedef adjacency_list< listS,
                            vecS,
                            bidirectionalS,
                            no_property,
                            property< edge_name_t,
                                      std::string > > ModelGraph;

    typedef graph_traits< ModelGraph >::vertex_iterator v_it_t;
    typedef graph_traits< ModelGraph >::edge_iterator e_it_t;
    typedef graph_traits< ModelGraph >::adjacency_iterator adj_it_t;
    typedef graph_traits< ModelGraph >::in_edge_iterator in_e_t;
    typedef graph_traits< ModelGraph >::out_edge_iterator out_e_t;

    typedef property_map< ModelGraph, vertex_index_t >::type v_idx_map_t;

    /**
     * SET_ARC_INPUT() - When the input of the FU in node U is the output of
     *                   node V, link the two nodes with an edge
     *
     * @U      : destination node of the edge (where the variable is consumed)
     * @V      : source node of the edge (where the variable is produced)
     * @E      : edge descriptor
     * @IN_NAME: input suffix (one of "1", "2", "Sel"). Used to identify the
     *           input
     */
#define SET_ARC_INPUT(U, V, E, IN_NAME)                         \
    do {                                                        \
        if (nodes[U].getIn ## IN_NAME() == nodes[V].getOut()) { \
            bool inserted;                                      \
            boost::tie(E, inserted) = add_edge(v_idx[V],        \
                                               v_idx[U],        \
                                               g);              \
            edge_map[E] = #IN_NAME;                             \
        }                                                       \
    } while (0);

public:
    /**
     * Model() - Create a new model reading it from file
     *
     * @modelFName: name of the file containing the model
     */
    Model(const char * const modelFName);

    /**
     * Model() - Create a new model from a vector of node description lines
     *
     * @modelVec: vector of node descriptions
     */
    Model(const std::vector< std::string > &modelVec);

    /**
     * Model() - Create a new model from an existing one
     *
     * @obj: source model used in the construction
     */
    Model(const Model &obj);

    /**
     * operator=() - Assign an existing model to the current one
     *
     * @obj: source model of the assignment
     *
     * Return: reference back to the assigned model
     */
    Model& operator=(const Model &rhs);

    /**
     * getRandomStartNode() - Select a random top-level node. All top-level
     *                        nodes are equiprobables.
     *
     * Return: index of the sampled node, -1 when no more top-level nodes
     */
    int getRandomStartNode() const;

    /**
     * getNextFreeModelNode() - Get the next node to place in the overlay
     *
     * @prevNode: previously sampled node (used to drive the selection)
     *
     * Return: index of the sampled node
     */
    int getNextFreeModelNode(const int prevNode) const;

    /**
     * isExtVar() - Checks the incoming edges on a node to discover if a
     *              variable is produced by an adjacent node or is an external
     *              input
     *
     * @nodeNo: index of the node under investigation
     * @var   : variable to check
     *
     * Return: true if the variable is an external input, false otherwise
     */
    bool isExtVar(const int nodeNo, const std::string var) const;

    /**
     * getNodesNo() - Get the total number of nodes in the model
     *
     * Return: number of nodes in the model
     */
    int getNodesNo() const;

    /**
     * getExtInNo() - Get the total number of external inputs required by
     *                the model
     *
     * Return: number of external inputs of the model
     */
    int getExtInNo() const;

    /**
     * getExtOutNo() - Get the total number of external outputs required by
     *                the model
     *
     * Return: number of external outputs of the model
     */
    int getExtOutNo() const;

    /**
     * getModelNode() - Retrieve a constant reference to a specific model's
     *                  node
     *
     * @n: numerical identifier of the node to retrieve
     *
     * Return: constant reference to the desired node
     */
    const ModelNode& getModelNode(const unsigned int n) const;

    /**
     * setNodePlaced() - Mark a specific node as placed
     *
     * @n: numerical identifier of the node
     */
    void setNodePlaced(const unsigned int n);

    /**
     * operator==() - Compare two models for equality
     *
     * @m1: first model in the comparison
     * @m2: second model in the comparison
     *
     * Return: true if the two models are equal, false otherwise
     */
    friend bool operator==(const Model &m1, const Model &m2)
    {
        if (m1.nodes == m2.nodes) {
            /* Check for an isomorphism */
            v_idx_map_t v_idx = get(vertex_index, m1.g);

            std::vector<graph_traits< ModelGraph >::vertex_descriptor> f(m1.nodes.size());
            return isomorphism(m1.g, m2.g, isomorphism_map
                               (make_iterator_property_map(f.begin(),
                                                           v_idx, f[0])));
        }
        return false;
    }

    /**
     * operator==() - Compare two models for inequality
     *
     * @m1: first model in the comparison
     * @m2: second model in the comparison
     *
     * Return: true if the two models are different, false otherwise
     */
    friend bool operator!=(const Model &m1, const Model &m2)
    {
        return !(m1 == m2);
    }

    /**
     * getNodeList() - Return a list of model nodes (for debugging purposes)
     *
     * Return: list of model nodes
     */
    std::vector< int > getNodeList() const;

private:
    ModelGraph g;
    std::vector< ModelNode > nodes;

    /**
     * init() - Initialize a new model from a vector of node description
     *          lines
     *
     * @modelVec: vector of node descriptions
     */
    void init(const std::vector< std::string > &modelVec);

    /**
     * addNeighborsInSampling() - Weight the unplaced neighbors of a given node.
     *                            All the nodes in the connected component to
     *                            which the given node belongs to are
     *                            investigated
     *
     * This function is used to properly weight the neighbors of a given node
     * when sampling the next node to be placed -- this allows "more popular"
     * nodes to have a higher weight, and therefore to be chosen before the
     * others and thus have a less constrained placement. All the nodes of the
     * connected component are investigated.
     *
     * @w        : weight vector for the model nodes
     * @startNode: node whose neighbors have to be investigated
     *
     * Return: number of neighborhood relations established (a node can be
     *         counted multiple times, in case a node is neighbor of multiple,
     *         already assigned nodes)
     */
    int addNeighborsInSampling(std::vector< int >& w,
                               const int startNode) const;
};

#endif /* MODEL_HPP_ */
