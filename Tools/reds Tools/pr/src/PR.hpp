#ifndef PR_HPP_
#define PR_HPP_

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <list>
#include <pthread.h>

#include "DFE.hpp"
#include "Model.hpp"
#include "ModelNode.hpp"

#define IMPROVE_PR 0

using namespace std;

/**
 * struct Operation - State if the system before an operation performed during
 *                    the PR process, along with tracking information for the
 *                    node placed in the operation
 *
 * These informations are useful to backtrack from a change due to an operation.
 *
 * @stateBefore       : state BEFORE the operation
 * @modelBefore       : state of the model BEFORE the operation
 * @modelNode         : node PR'ed in the current iteration
 * @attemptedPositions: element's position -- the last one will be the current
 *                      position of the node
 */
struct Operation
{
    DFE stateBefore;
    Model modelBefore;
    int modelNode;
    PositionList attemptedPositions;

    /**
     * Operation() - Initialize the operation structure
     *
     * @state    : state before the operation
     * @model    : model before the operation
     * @modelNode: node involved in the operation
     */
    Operation(const DFE& state, const Model& model,
              const int modelNode)
        : stateBefore(state), modelBefore(model),
          modelNode(modelNode) { }
};

class PR;

/**
 * struct thread_params - Control parameters for PR threads
 *
 * @n               : thread number
 * @done_mutex_ptr  : pointer to done mutex
 * @killer_mutex_ptr: pointer to killer mutex
 * @PRDone          : flag marking the PR as over
 * @winner          : number of winner thread in the pool
 * @pr              : pointer to executed PR instance
 */
struct thread_params {
    int n;
    pthread_mutex_t *done_mutex_ptr;
    pthread_mutex_t *killer_mutex_ptr;
    bool *PRDone;
    int *winner;
    std::shared_ptr<PR> pr;
};

/**
 * class PR - Manage PR operations, allowing to backtrack when stuck
 *
 * @opStack     : stack containing the sequence of operations that led to the
 *                current state
 * @currentState: pointer to the current state of the overlay
 * @model       : model that we are currently trying to place on the overlay
 */
class PR
{
public:
    /**
     * PR() - Start a new PR session, with a given model and an overlay of the
     *        specified size
     *
     * @mdl   : model to place
     * @rowsNo: number of rows in the model
     * @colsNo: number of columns in the model
     */
    PR(const Model &mdl, const int rowsNo, const int colsNo);

    /**
     * run() - Start the PR process in the given thread
     *
     * Try to place one model node after the other, and when all the variables
     * have been placed, then place the constants. If a thread finishes within
     * time limits it unlocks the done_mutex, signalling this event to the
     * concurrent threads.
     *
     * @tp: thread parameters
     */
    void run(thread_params *tp);
    bool backtrack(int &node, PositionList& pList);
    void dumpConstants(const char * const configFName,
                       const char * const pinListFName) const;
    void dumpVariables(const char * const configFName,
                       const char * const pinListFName) const;

private:
    vector< Operation > opStack;
    std::shared_ptr<DFE> currentState;
    Model model;
};

#endif /* PR_HPP_ */
