#include "utils.hpp"

int encodeOp(const std::string &opName)
{
    if (opName == OP_SUM) {
        return OPCODE_SUM;
    }
    if (opName == OP_SUB) {
        return OPCODE_SUB;
    }
    if (opName == OP_MUL) {
        return OPCODE_MUL;
    }
    if (opName == OP_DIV) {
        return OPCODE_DIV;
    }
    if (opName == OP_MOD) {
        return OPCODE_MOD;
    }
    if (opName == OP_EQU) {
        return OPCODE_EQU;
    }
    if (opName == OP_NEQ) {
        return OPCODE_NEQ;
    }
    if (opName == OP_GEQ) {
        return OPCODE_GEQ;
    }
    if (opName == OP_GRE) {
        return OPCODE_GRE;
    }
    if (opName == OP_SEL) {
        return OPCODE_SEL;
    }
    if (opName == OP_AND) {
        return OPCODE_AND;
    }
    if (opName == OP_OR) {
        return OPCODE_OR;
    }

    /* Invalid op! */
    return 0x0;
}

void showPath(const int src, const int dst,
              const int rowsNo, const int colsNo,
              const vector< pair< int, int > >& path)
{
    vector< vector< int > > M(rowsNo);
    for (int r = 0; r < rowsNo; ++r) {
        M[r].resize(colsNo, 0);
    }

    for (int n = 0; n < (int)path.size(); ++n) {
        M[path[n].first/colsNo][path[n].first%colsNo] = 3;
        M[path[n].second/colsNo][path[n].second%colsNo] = 3;
    }

    M[src/colsNo][src%colsNo] = 1;
    M[dst/colsNo][dst%colsNo] = 2;

    fprintf(stderr, "     ");
    for (int c = 0; c < colsNo; ++c) {
        fprintf(stderr, " %2d ", c);
    }

    for (int r = 0; r < rowsNo; ++r) {
        fprintf(stderr, "\n    +");
        for (int c = 0; c < colsNo; ++c) {
            fprintf(stderr, "---+");
        }
        fprintf(stderr, "\n%2d  |", r);
        for (int c = 0; c < colsNo; ++c) {
            switch (M[r][c]) {
            case 0:
                fprintf(stderr, " - |");
                break;
            case 1:
                fprintf(stderr, " s |");
                break;
            case 2:
                fprintf(stderr, " e |");
                break;
            case 3:
                fprintf(stderr, " * |");
                break;
            default:
                fprintf(stderr, " ? |");
                break;
            }
        }
    }
    fprintf(stderr, "\n    +");
    for (int c = 0; c < colsNo; ++c) {
        fprintf(stderr, "---+");
    }
    fprintf(stderr, "\n");
}
