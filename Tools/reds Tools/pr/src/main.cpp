#include <iostream>
#include <cstdio>
#include <chrono>
#include <omp.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <ctime>

#include "PR.hpp"
#include "utils.hpp"

using namespace std;
static pthread_mutex_t killer_mutex;

/**
 * signal_handler() - Timer expiration handler that kills the threads after the
 *                    timer has expired
 */
void signal_handler (int /* signum */)
{
    pthread_mutex_unlock(&killer_mutex);
}

/**
 * run_pr() - Main thread body that simply runs PR
 */
static void* run_pr(void *tp)
{
    ((thread_params *)tp)->pr->run((thread_params *)tp);
    return NULL;
}

int main(int argc, char **argv)
{
    if (argc != 9) {
        fprintf(stderr,
                "Usage:\n\t%s model_file rows_no cols_no "
                "out_const_cfg out_const_pinout "
                "out_main_cfg out_main_pinout parallelize_flag\n", argv[0]);
        return -1;
    }

    const int OVERLAY_COLS_NO = strtoimax(argv[2], (char **)NULL, 10);
    const int OVERLAY_ROWS_NO = strtoimax(argv[3], (char **)NULL, 10);
    const char *out_const_cfg = argv[4];
    const char *out_const_pinout = argv[5];
    const char *out_main_cfg = argv[6];
    const char *out_main_pinout = argv[7];

#ifdef DEBUG_RUN
    srand(666);
    const bool parallelize = false;
    fprintf(stderr,
            "##############################################################\n"
            "# Running in debug mode -- fixed seed and no parallelization #\n"
            "##############################################################\n\n");
#else
    const bool parallelize =
        strtoimax(argv[8], (char **)NULL, 10) == 0 ? false : true;
    srand(time(0));
#endif
    const int WORKERS_NO = parallelize ? omp_get_max_threads() : 1;

    fprintf(stderr,
            "---------------------------------------------------------------\n"
            "- model file                        : %s\n"
            "- architecture                      : %d x %d\n"
            "- output constant configuration file: %s\n"
            "- output constant pinout file       : %s\n"
            "- output main configuration file    : %s\n"
            "- output main pinout file           : %s\n"
            "- parallelize                       : %s (%d core(s))\n"
            "---------------------------------------------------------------\n",
            argv[1], OVERLAY_COLS_NO, OVERLAY_ROWS_NO,
            argv[4], argv[5], argv[6], argv[7],
            parallelize ? "true" : "false", WORKERS_NO);

    /* Load model from file */
    Model m(argv[1]);

    /* Check that model fits into current overlay */
    if (m.getExtInNo() > 2*(OVERLAY_COLS_NO+OVERLAY_ROWS_NO)) {
        fprintf(stderr,
                "The model requires too many INPUTS (%d) wrt "
                "overlay's capabilities!\nMax number of inputs: %d\n",
                m.getExtInNo(),
                2*(OVERLAY_COLS_NO+OVERLAY_ROWS_NO));
        return -3;
    }
    if (m.getExtOutNo() > 2*(OVERLAY_COLS_NO+OVERLAY_ROWS_NO)) {
        fprintf(stderr,
                "The model requires too many OUTPUTS (%d) wrt "
                "overlay's capabilities!\nMax number of outputs: %d\n",
                m.getExtOutNo(),
                2*(OVERLAY_COLS_NO+OVERLAY_ROWS_NO));
        return -4;
    }

    /* Initialize timers */
    chrono::time_point< chrono::system_clock > start;
    chrono::time_point< chrono::system_clock > end;

    /*
     * Initialize the timer mechanism that kills threads past a certain amount
     * of time (used to reset PR search periodically)
     */
    vector< std::shared_ptr<PR> > workers(WORKERS_NO);
    vector< pthread_t > workers_thread(WORKERS_NO);
    pthread_mutex_t done_mutex;
    pthread_mutex_init(&done_mutex, NULL);
    pthread_mutex_init(&killer_mutex, NULL);
    int winner = -1;
    bool PRDone = false;
    unsigned int sleep_time = 2; /* Wait for multiples of 2 seconds */
    const unsigned int max_sleep_time = 16; /* Wait at most 128 seconds */
    struct sigevent timer_event;
    struct itimerspec timer_spec;
    timer_t timer;
    timer_spec.it_interval.tv_nsec = 0;
    signal(SIGRTMIN, signal_handler);
    timer_event.sigev_notify = SIGEV_SIGNAL;
    timer_event.sigev_signo = SIGRTMIN;
    if (timer_create(CLOCK_REALTIME, &timer_event, &timer)) {
        fprintf(stderr, "Error encountered while creating timer\n");
        return -5;
    }

    /* Initialize the first set of threads */
    vector< thread_params > tps(WORKERS_NO);
    for (int i = 0; i < WORKERS_NO; ++i) {
        tps[i].n = i;
        tps[i].done_mutex_ptr = &done_mutex;
        tps[i].killer_mutex_ptr = &killer_mutex;
        tps[i].PRDone = &PRDone;
        tps[i].winner = &winner;
    }

    /* Wait at most the specified amount of time, then restart PR */
    start = chrono::system_clock::now();
    while (!PRDone) {
        timer_spec.it_interval.tv_sec = sleep_time;
        if (sleep_time < max_sleep_time) {
            sleep_time <<= 1;
        }
        fprintf(stderr, "WAITING AT MOST %lds\n",
                timer_spec.it_interval.tv_sec);
        timer_spec.it_value = timer_spec.it_interval;
        pthread_mutex_lock(&killer_mutex);

        /* Start workers */
        for (int i = 0; i < WORKERS_NO; ++i) {
            workers[i] = std::shared_ptr<PR>(new PR(m,
                                                    OVERLAY_ROWS_NO,
                                                    OVERLAY_COLS_NO));
            tps[i].pr = workers[i];
            pthread_create(&workers_thread[i], NULL,
                           run_pr, &tps[i]);
        }

        /*
         * Start the timer that will kill the workers once their working time is
         * expired
         */
        if (timer_settime(timer, 0, &timer_spec, NULL)) {
            fprintf(stderr, "Cannot start timer\n");
            return -6;
        }

        /* Wait for the threads to complete */
        for (int i = 0; i < WORKERS_NO; ++i) {
            pthread_join(workers_thread[i], NULL);
        }
    }
    end = chrono::system_clock::now();
    chrono::duration< double > elapsed_s = end-start;

    fprintf(stderr, "\n----[ Place&Route took %.6fs ]----\n",
            elapsed_s.count());
    fprintf(stderr, "Winner thread: %d\n", winner);
    workers[winner]->dumpConstants(out_const_cfg, out_const_pinout);
    workers[winner]->dumpVariables(out_main_cfg, out_main_pinout);

    return 0;
}
