/*
 * deserialisateur.cpp
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#include "deserialisateur.h"
#include <iostream>
#include <iomanip>
#include <bitset>
using namespace std;

Deserialisateur::Deserialisateur() {
	sd = new SignedDigit();
	firstPrint = 1;
	q = 0;
	qm = -1;

}

Deserialisateur::~Deserialisateur() {
	delete sd;
}

void Deserialisateur::convert(SignedDigit sd_, char print) {
	static int i;
	int tmp;
	this->sd->setVal(sd_.getVal());
	std::cout << "conv : " << i++ << endl;
	sdData.addDigit(sd_);
	if (sd->getVal() != 3) {
		endConv = 0;
		if (sd->getVal() == 1 ) {
			q_in = 1;
			qm_in = 0;
			selShiftQ = 1;
			selShiftQm = 0;

		} else if (sd->getVal() == 0) {
			q_in = 0;
			qm_in = 1;
			selShiftQ = 1;
			selShiftQm = 1;
		} else	{
			q_in = 1;
			qm_in = 0;
			selShiftQ = 0;
			selShiftQm = 1;
		}

		tmp = q;
		if (selShiftQ)
			q = (q<<1) + q_in;
		else
			q = (qm<<1) + q_in;

		if (selShiftQm)
			qm = (qm<<1) + qm_in;
		else
			qm = (tmp<<1) + qm_in;

		result = q;

		if (print)
			this->print();
	}
	else {
		if (print)
			this->print();
		endConv = 1;
		firstPrint = 1;
	}

}

bool Deserialisateur::endOfConversion() {
	if (endConv){
		cout << " Result of conversion : " << result << endl;
		sdData.clearDigits();
		endConv = 0;
		q = 0;
		qm = -1;
		return true;
	}
	return false;

}

void Deserialisateur::print() {
	bitset<32> bit_q(q);
	bitset<32> bit_qm(qm);
	static int step;
	int tmp;

	if (firstPrint)
	{
		cout << "|"  << setw(4) << "Step" << "|"  << setw(3) << left << "q" << "|" << setw(3) << "Qin" << "|" << setw(8) << "CshiftQ" << "|" << setw(32) << left << "Q" << "|"<<
										          setw(4) << "QMin" << "|" << setw(8) << "CshiftQM" << "|" << setw(32) << left << "QM" << "|" << "Serial data received"<< endl;
		firstPrint = 0;
		step = 0;
	}

	if (sd->getVal() == 2)
		tmp = -1;
	else
		tmp = sd->getVal();

	cout << "|" << setw(4) << step++ << "|" << setw(3) << tmp << "|" << setw(3 )<<  (int)q_in << "|" << setw(8) << selShiftQ << "|" << setw(32) << bit_q << "|"<<
											          setw(4) << (int)qm_in << "|" << setw(8) << selShiftQm << "|" << setw(32) << bit_qm << "|" ;
	sdData.print();
	cout << endl;
}

void Deserialisateur::clear() {
	endConv = false;
}

