/*
 * onlinesimulator.h
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#ifndef ONLINESIMULATOR_H_
#define ONLINESIMULATOR_H_

#include "serialisateur.h"
#include "deserialisateur.h"
#include "adder.h"
#include "mult.h"
#include "Divider.h"

class OnlineSimulator {
private:
	int userChoice;
	int nbDataToSend;
	int dataLength;
	int* userDatasA;
	int* userDatasB;
	Serialisateur serialA;
	Serialisateur serialB;
	Deserialisateur deserial;
	Adder adder;
	Mult mult;
	Divider div;

public:
	OnlineSimulator();
	virtual ~OnlineSimulator();
	void getUserChoice();
	void startSimulation();
	bool restartSimultaion();
};

#endif /* ONLINESIMULATOR_H_ */
