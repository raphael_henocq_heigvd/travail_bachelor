/*
 * Divider.cpp
 *
 *  Created on: 10 juil. 2017
 *      Author: raphael
 */

#include "Divider.h"
#include <iostream>
#include <iomanip>

Divider::Divider() {
	j=-4;
	v=w=0.;
	jMax=256;
	firstPrint = true;
	validRes = false;

}

Divider::~Divider() {
	// TODO Auto-generated destructor stub
}

void Divider::clear() {
	result.clearDigits();
	d.clearDigits();
	j=-4;
	v=w=0.;
	jMax=256;
	firstPrint = true;
	validRes = false;
}

SignedDigit Divider::step(SignedDigit dataA, SignedDigit dataB, bool print) {

	sdB.setVal(dataB.getVal());
	sdA.setVal(dataA.getVal());

	if (j<0) {
		w = v;
	}
	else {
		w = v - (float)sdB.getSignedVal()*result.getFloatValue();
	}

	if (v>= 0.25)
		sdQ.setVal(1);
	else if (v>= -1/4)
		sdQ.setVal(0);
	else if (v>= -2)
		sdQ.setVal(-1);
	else
		std::cout<< "Invalide v for Sel in div!\n";

	if (dataA.getVal() == 3 && jMax==256) // End of Data, and jMax not set
	{
		jMax = j+3;
	}

	if (j>=0)
		validRes = true;
	else
		validRes = false;


	if (j<0) {
		v = 2*w + (float)sdA.getSignedVal()/16;
	}
	else {
		v = 2*w + (float)sdA.getSignedVal()/16 - result.getFloatValue()*(float)sdB.getSignedVal()/16;
	}


	if (j>jMax)
		sdQ.setVal(3);
	else if (j>=0)
		result.addDigit(sdQ);

	if(print && j<=jMax)
		this->print();

	j++;
	return sdQ;


}

float Divider::getLSB() {
	return w;

}

bool Divider::isValid() {
	return validRes;
}
using namespace std;
void Divider::print() {

	if (firstPrint)
		{
			cout << "|"  << setw(3) << " J " << "|"  << setw(3) << left << " x " << "|" << setw(3) << left << " d" << "|" <<
							setw(10) << "D" << "|"  << setw(10) << left << "v" << "|" <<
							setw(10) << "w" << "|"  << setw(3) << left << " q " << "|" << "Actual Result Produced" << endl;
			firstPrint = 0;
		}

		cout << "|"  << setw(3) << j << "|"  << setw(3) << (int)sdA.getSignedVal() << "|" << setw(3) <<  (int)sdB.getSignedVal() << "|" <<
				 setw(9) << fixed << setprecision(8) << d.getFloatValue() << "|" << setw(9) << setprecision(8) << v << "|" <<
				 setw(9) << setprecision(8) << w << "|"  << setw(3) << (int)sdQ.getSignedVal() << "|";

	//	if (j==jMax)
	//		result.addDigits(w,jMax);

		result.print();
		cout << endl;

}

bool Divider::isEnd(){
	return j>jMax;
}

SignedDigits Divider::getRes()
{
	return result;
}


