/*
 * SignedDigits.cpp
 *
 *  Created on: 6 juil. 2017
 *      Author: raphael
 */

#include "SignedDigits.h"
#include <math.h>
#include <cstddef>

SignedDigits::SignedDigits() {
	digits = new std::vector<SignedDigit> ();

}

SignedDigits::~SignedDigits() {
	delete digits;
}

void SignedDigits::addDigit(SignedDigit sd) {
	if (sd.getVal()!= 3)
		digits->push_back(sd);
}

void SignedDigits::clearDigits() {
	if (!digits->empty())
		//digits->erase(digits->begin(),digits->end());
		digits->clear();
}

int SignedDigits::getValue() {

	int value=0;
	int size = digits->size();
	for (int i= 0; i< size; i++)
	{
		value += digits->at(i).getVal()*pow(2,size-i-1);
	}

	return value;

}

float SignedDigits::getFloatValue() {
	float value;
	int size = digits->size();
	for (int i= 0; i< size; i++)
	{
		value += digits->at(i).getVal()*pow(2,-i-1);
	}
	if (value < 1e-30)
		value = 0.;
	return value;
}

SignedDigit SignedDigits::getDigit(int pos) {

	if (pos < digits->size())
 		return digits->at(pos);
	return digits->at(0); // Error !

}

void SignedDigits::print() {
	int size = digits->size();

	std::cout << "|";
	for (int i = 0; i< size; i++)
	{
		char val = digits->at(i).getVal();
		if (val == 1)
			std::cout  << "01|";
		else if (val == 2)
			std::cout << "10|";
		else if (val == 3)
			std::cout << "11|";
		else
			std::cout << "00|";
	}
}

void SignedDigits::addDigits(float val, int size)
{
	SignedDigit tmp;
	float tmpVal;
	float div;
	while (val >=1)
		val --;

	for (int i=2; i<size+3; i++)
	{
		div = (1/(float)(pow(2,i)));
		tmpVal = val/div;
		if (tmpVal>=1) {
			tmp.setVal(1);
			val-= div;
		} else
			tmp.setVal(0);

		this->addDigit(tmp);
	}

}


