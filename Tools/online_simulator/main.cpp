/*
 * main.cpp
 *
 *  Created on: 22 juin 2017
 *      Author: raphael
 */

#include "onlinesimulator.h"
#include <iostream>

using namespace std;

int main()
{
	OnlineSimulator *simulator = new OnlineSimulator();

	cout <<    " ==================================================================================== \n"
	           " =                                                                                  = \n"
	           " =                         Online Arithmetics Simualtor                            = \n"
	           " =                                                                                  = \n"
	           " ==================================================================================== \n";

		simulator->getUserChoice();
		simulator->startSimulation();

	cout << "Fin de la simulation \n";

}
