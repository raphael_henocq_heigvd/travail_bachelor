/*
 * Mult.h
 *
 *  Created on: 22 juin 2017
 *      Author: raphael
 */

#ifndef MULT_H_
#define MULT_H_
#include "SignedDigits.h"

class Mult {
private:
	bool validRes;
	SignedDigit sdA, sdB, p;
	SignedDigits x,y,result;
	float v,w;
	int j, jMax;
	bool firstPrint;

	void print();
public:
	Mult();
	virtual ~Mult();
	void clear();
	SignedDigit step(SignedDigit dataA, SignedDigit dataB, bool print = false);
	bool isValid();
	float getLSB(); // The float value has to be converted into integer representation !!
	SignedDigits getRes();
	bool isEnd();
};

#endif /* MULT_H_ */
