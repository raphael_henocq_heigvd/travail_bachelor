/*
 * adder.cpp
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#include "adder.h"
#include <iomanip>

Adder::Adder() {
}

Adder::~Adder() {
}

void Adder::clear(){
	j = -1;
	t.setVal(0);
	w1.setVal(0);
	w2.setVal(0);
	z.setVal(0);
	z1.setVal(0);
	validRes = false;
	firstPrint = true;
	result.clearDigits();
	jMax = 256; // initialize at determined value
}

SignedDigit Adder::step(SignedDigit dataA, SignedDigit dataB, bool print) {

	char tmp;

	z.setVal(z1.getVal());
	w1.setVal(w2.getVal());
	sdA.setVal(dataA.getVal());
	sdB.setVal(dataB.getVal());
	if (dataA.getVal() == 3 && jMax==256) // End of Data, and jMax not set
	{
		jMax = j+1;
	}

	if (j>=1)
		validRes = true;
	else
		validRes = false;

	tmp = dataA.getSignedVal() + dataB.getSignedVal();
	if (tmp == 0) {
		t.setVal(0);
		w2.setVal(0);

	} else if (tmp > 0) {
		t.setVal(1);
		w2.setVal(tmp-2);

	} else if (tmp < 0 ) {
		t.setVal(-1);
		w2.setVal(tmp+2);
	}

	z1.setVal(t.getSignedVal()+w1.getSignedVal());
	if(validRes)
		result.addDigit(z);
	if(print && j<=jMax)
		this->print();
	if (j>jMax) {
		z.setVal(3); // end of data
	}
	j++;
	return z;
}

bool Adder::isValid() {
	return validRes;
}

using namespace std;

void Adder::print() {

	if (firstPrint)
	{
		cout << "|"  << setw(3) << " J " << "|"  << setw(3) << left << " X " << "|" << setw(3) << left << " Y" << "|" <<
						setw(6) << "t(j+1)" << "|"  << setw(6) << left << "w(j+2)" << "|" << setw(6) << left << "w(j+1)" << "|" <<
						setw(6) << "z(j+1)" << "|"  << setw(6) << left << " z(j) " << "|" << "Actual Result Produced" << endl;
		firstPrint = 0;
	}

	cout << "|"  << setw(3) << j << "|"  << setw(3) << (int)sdA.getSignedVal() << "|" << setw(3) <<  (int)sdB.getSignedVal() << "|" <<
					setw(6) << (int)t.getSignedVal() << "|"  << setw(6) << (int)w2.getSignedVal() << "|" << setw(6) << (int)w1.getSignedVal() << "|" <<
					setw(6) << (int)z1.getSignedVal() << "|"  << setw(6) << (int)z.getSignedVal() << "|";

	result.print();
	cout << endl;
}
