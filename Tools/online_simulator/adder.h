/*
 * adder.h
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#ifndef ADDER_H_
#define ADDER_H_
#include "SignedDigits.h"

class Adder {
private :
	SignedDigit t;
	SignedDigit w1;
	SignedDigit w2;
	SignedDigit z;
	SignedDigit z1;
	SignedDigits result;
	SignedDigit sdA;
	SignedDigit sdB;
	int j;
	bool validRes;
	bool firstPrint;
	int jMax;

	void print();

public:
	Adder();
	virtual ~Adder();
	void clear();
	SignedDigit step(SignedDigit dataA, SignedDigit dataB, bool print = false);
	bool isValid();
};

#endif /* ADDER_H_ */
