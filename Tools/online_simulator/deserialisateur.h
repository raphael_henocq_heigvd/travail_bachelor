/*
 * deserialisateur.h
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#ifndef DESERIALISATEUR_H_
#define DESERIALISATEUR_H_

#include "SignedDigits.h"

class Deserialisateur {
private:
	SignedDigit *sd;
	SignedDigits sdData;
	int result;
	char q_in;
	char qm_in;
	bool selShiftQ;
	bool selShiftQm;
	int q;
	int qm;
	bool endConv;
	bool firstPrint;

	void print();

public:
	Deserialisateur();
	virtual ~Deserialisateur();
	void convert(SignedDigit sd, char print=0);
	bool endOfConversion();
	void clear();
};

#endif /* DESERIALISATEUR_H_ */
