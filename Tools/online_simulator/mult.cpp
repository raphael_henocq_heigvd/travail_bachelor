/*
 * Mult.cpp
 *
 *  Created on: 22 juin 2017
 *      Author: raphael
 */

#include "mult.h"
#include <iostream>
#include <iomanip>

Mult::Mult() {
	firstPrint = true;
	j=-3;
	v=w=0;
	jMax=256;
}

Mult::~Mult() {
	// TODO Auto-generated destructor stub
}

void Mult::clear() {
	result.clearDigits();
	x.clearDigits();
	y.clearDigits();
	j=-3;
	v=w=0.;
	jMax=256;
	firstPrint = true;
}

SignedDigit Mult::step(SignedDigit dataA, SignedDigit dataB, bool print) {

	const float delta = 0.25;
	sdB.setVal(dataB.getVal());
	if (dataA.getVal() == 3 && jMax==256) // End of Data, and jMax not set
	{
		jMax = j+2;
	}

	if (j>=1)
		validRes = true;
	else
		validRes = false;

	y.addDigit(sdB);
	sdA.setVal(dataA.getVal());
	v = 2*w + (x.getFloatValue()*sdB.getSignedVal() + y.getFloatValue()*sdA.getSignedVal())/16;
	if (v >= 4)
		v-= 4;
	x.addDigit(sdA);
	if (v>= 3.5 || (v>=-delta && v<= delta))
		p.setVal(0);
	else if (v>=0.5)
		p.setVal(1);
	else
		p.setVal(-1);

	if (j<0)
		w = v;
	else
		w = v - p.getSignedVal();

	if (j>jMax)
		p.setVal(3);
	else if (j>=0)
		result.addDigit(p);

	if(print && j<=jMax)
		this->print();

	j++;
	return p;


}

float Mult::getLSB() {
	return w;

}

bool Mult::isValid() {
	return validRes;
}
using namespace std;
void Mult::print() {

	if (firstPrint)
		{
			cout << "|"  << setw(3) << " J " << "|"  << setw(3) << left << " x " << "|" << setw(3) << left << " y" << "|" <<
							setw(10) << "X" << "|"  << setw(10) << left << "Y" << "|" << setw(10) << left << "v" << "|" <<
							setw(10) << "w" << "|"  << setw(3) << left << " p " << "|" << "Actual Result Produced" << endl;
			firstPrint = 0;
		}

		cout << "|"  << setw(3) << j << "|"  << setw(3) << (int)sdA.getSignedVal() << "|" << setw(3) <<  (int)sdB.getSignedVal() << "|" <<
				 setw(9) << fixed << setprecision(8) << x.getFloatValue() << "|"  << setw(9) << setprecision(8) << y.getFloatValue() << "|" << setw(9) << setprecision(8) << v << "|" <<
				 setw(9) << setprecision(8) << w << "|"  << setw(3) << (int)p.getSignedVal() << "|";

		if (j==jMax)
			result.addDigits(w,jMax);

		result.print();
		cout << endl;

}

bool Mult::isEnd(){
	return j>jMax;
}

SignedDigits Mult::getRes()
{
	return result;
}
