/*
 * SignedDigits.h
 *
 *  Created on: 6 juil. 2017
 *      Author: raphael
 */

#ifndef SIGNEDDIGITS_H_
#define SIGNEDDIGITS_H_

#include <iostream>
#include <vector>
#include "SignedDigit.h"

class SignedDigits {
private:
	std::vector<SignedDigit> *digits;
public:
	SignedDigits();
	virtual ~SignedDigits();
	void addDigit(SignedDigit sd);
	void clearDigits();
	int getValue();
	float getFloatValue();
	SignedDigit getDigit(int pos);
	void print();
	void addDigits(float val, int size);
};

#endif /* SIGNEDDIGITS_H_ */
