/*
 * SignedDigit.cpp
 *
 *  Created on: 6 juil. 2017
 *      Author: raphael
 */

#include "SignedDigit.h"

SignedDigit::SignedDigit() {

}

SignedDigit::~SignedDigit() {

}

char SignedDigit::getVal() {
	return val;
}


void SignedDigit::setVal(char val) {
	if (val == -1)
		this->val = 2;
	else if (val == -2 )
		this->val = 0;
	else
		this->val = val;
}

char SignedDigit::getSignedVal() {
	if (val == 2)
		return -1;
	else if (val== 1)
		return 1;
	else
		return 0;
}
