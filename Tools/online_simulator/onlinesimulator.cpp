/*
 * onlinesimulator.cpp
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#include "onlinesimulator.h"
#include <iostream>
#include <cstdlib>
#include <math.h>

using namespace std;

OnlineSimulator::OnlineSimulator() {
	// TODO Auto-generated constructor stub

}

OnlineSimulator::~OnlineSimulator() {
	// TODO Auto-generated destructor stub
}

void OnlineSimulator::getUserChoice()
{
	do
	{
		cout << " Choose one of the following simulation : \n";
		cout << " 1 : Serializer/deserializer \n";
		cout << " 2 : Simple Online Adder \n";
		cout << " 3 : Simple Online Mult  \n";
		cout << " 4 : Simple Online Divideur \n";
		cin >> userChoice;
	} while (userChoice == 0 || userChoice >4);

	nbDataToSend = 1;

	do
	{
		cout << " What data lenght (in bit) do you want ? [1-32] \n";
		cin >> dataLength;
	}while (dataLength <1 || dataLength > 32 );

	switch (userChoice)
   {
	  case 1:
		  userDatasA = (int*)malloc(nbDataToSend*sizeof(int));
		  for (int i = 0; i< nbDataToSend; i++)
		  {
			  do {
				  cout << "Serial input data num " << i+1 << "\n";
				  cin >> userDatasA[i];
			  } while (userDatasA[i]< (-pow(2,dataLength)) || userDatasA[i] >  pow(2,dataLength)-1);

		  }
		  break;
	  case 2:
	  case 3:
	  case 4:
		  userDatasA = (int*)malloc(nbDataToSend*sizeof(int));
		  userDatasB = (int*)malloc(nbDataToSend*sizeof(int));
		  for (int i = 0; i< nbDataToSend; i++)
		  {
			  do {
				  cout << "Serial input data A num " << i+1 << "\n";
				  cin >> userDatasA[i];
			  } while (userDatasA[i]< (-pow(2,dataLength)) || userDatasA[i] >  pow(2,dataLength)-1);

			  do {
				  cout << "Serial input data B num " << i+1 << "\n";
				  cin >> userDatasB[i];
			  } while (userDatasB[i]< (-pow(2,dataLength)) || userDatasB[i] >  pow(2,dataLength)-1);

		  }

		 break;
	   }
}

void OnlineSimulator::startSimulation()
{
	SignedDigit sdA;
	SignedDigit sdB;
	SignedDigit sdRes;
	SignedDigits sdsRes;
	int state=0;
	char print = 1;
	char usr;
	bool end;
	int i;

	cout << "Simulation Start \n\n" <<
	        "Press q for quit simulation\n" <<
	        "Press enter to make one step\n";
	for ( int i=0; i < nbDataToSend; i++)
	{
		serialA.load(userDatasA[i],dataLength);
		cout << "Data A : " << userDatasA[i] << endl;
		if (userChoice != 1) {  // serial-deserial case don't need 2 data
			serialB.load(userDatasB[i],dataLength);
			cout << "Data B : " << userDatasB[i] << endl;
		}
		cout << " Datas " << i << endl;
		adder.clear();
		mult.clear();
		deserial.clear();
		div.clear();
		state = 0;
		i=0;
		end = false;
		while (!deserial.endOfConversion()&&!end)
		{
			switch (userChoice)
			{
			case 1:  // serial-deserial
				sdA = serialA.getInMsbOrder();
				deserial.convert(sdA, print);
				break;
			case 2 : // Adder
				sdA = serialA.getInMsbOrder();
				sdB = serialB.getInMsbOrder();
				sdRes = adder.step(sdA,sdB,print);
				if (adder.isValid())
					deserial.convert(sdRes);
				break;
			case 3 :  // Mult
				switch (state) {
				case 0 :
					if (mult.isEnd()){
						state = 1;
					}
					else {
						sdA = serialA.getInMsbOrder();
						sdB = serialB.getInMsbOrder();
						sdRes = mult.step(sdA,sdB,print);
					}
					break;
				case 1:
					std::cout << "Result of multplication : " << mult.getRes().getValue() << endl;
					end = true;
					break;

				}
				break;
				case 4 :  // Divider
					switch (state) {
					case 0 :
						if (div.isEnd()){
							state = 1;
						}
						else {
							sdA = serialA.getInMsbOrder();
							sdB = serialB.getInMsbOrder();
							sdRes = div.step(sdA,sdB,print);
						}
						break;
					case 1:
						std::cout << "Result of Division : " << mult.getRes().getValue() << endl;
						end = true;
						break;

					}
					break;

			}
		}
	}
}

bool OnlineSimulator::restartSimultaion()
{
	char response;
	do {
		cout << "Would you like to start a new simulation ? [y/n] \n";
		cin >> response;
	}while(!(response == 'y' or response == 'n'));

	if (response == 'y')
	{
		adder.clear();
		return true;
	} else
	{
		return false;
	}

}
