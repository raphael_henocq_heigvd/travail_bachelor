/*
 * serialisateur.cpp
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#include "serialisateur.h"
#include <stdlib.h>

Serialisateur::Serialisateur() {

}

Serialisateur::~Serialisateur() {
}

void Serialisateur::load(int data_, unsigned int dataSize_)
{
	char signBit=(data_ < 0) ?1:0;
	int mask;
	int j=0;
	SignedDigit sd;

	sdData.clearDigits();

	if (signBit)
		data=-data_;
	else
		data=data_;

	dataSize=dataSize_;
	count = 0;

	for (int i=dataSize-1; i >= 0; i--)
	{
		if (signBit)
			if (data & (1 << i))
				sd.setVal(2);
			else
				sd.setVal(0);
		else
			sd.setVal(((data & (1 << i)) >> (i)));

		sdData.addDigit(sd);
	}

}

SignedDigit Serialisateur::getInMsbOrder()
{

	SignedDigit end;
	end.setVal(3);
	if (count < dataSize)
		return sdData.getDigit(count++);
	else
		return end; // End_of_data = 0b11
}

