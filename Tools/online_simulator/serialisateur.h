/*
 * serialisateur.h
 *
 *  Created on: 19 juin 2017
 *      Author: raphael
 */

#ifndef SERIALISATEUR_H_
#define SERIALISATEUR_H_

#include "SignedDigits.h"

class Serialisateur {

private:
	int data;
	SignedDigits sdData;
	unsigned int dataSize;
	int count;

public:
	Serialisateur();
	virtual ~Serialisateur();
	void load(int data, unsigned int dataSize);
	SignedDigit getInMsbOrder();
};

#endif /* SERIALISATEUR_H_ */
