/*
 * SignedDigit.h
 *
 *  Created on: 6 juil. 2017
 *      Author: raphael
 */

#ifndef SIGNEDDIGIT_H_
#define SIGNEDDIGIT_H_

class SignedDigit {

private :
	char val;
public:
	SignedDigit();
	virtual ~SignedDigit();
	char getVal();
	char getSignedVal();
	void setVal(char val);
};

#endif /* SIGNEDDIGIT_H_ */
