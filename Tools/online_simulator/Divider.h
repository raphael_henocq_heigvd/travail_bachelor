/*
 * Divider.h
 *
 *  Created on: 10 juil. 2017
 *      Author: raphael
 */

#ifndef DIVIDER_H_
#define DIVIDER_H_
#include "SignedDigits.h"

class Divider {
private:
	int j, jMax;
	SignedDigit sdA;
	SignedDigit sdB;
	SignedDigit sdQ;
	SignedDigits d,result;
	float v;
	float w;
	bool firstPrint, validRes;

public:
	Divider();
	virtual ~Divider();
	void clear();
	SignedDigit step(SignedDigit dataA, SignedDigit dataB, bool print = false);
	bool isValid();
	float getLSB(); // The float value has to be converted into integer representation !!
	SignedDigits getRes();
	bool isEnd();
	void print();
};

#endif /* DIVIDER_H_ */
