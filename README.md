.
## Structure du répertoire


├── Rapport  
│   ├── Bibliographie                         -- Publications cités dans le Rapport  
│   ├── Images  
│   │   ├── Schema bloc  
│   │   └── StateMachine  
│   └── Rapport  
├── Tools  
│   ├── pr                                    -- Outil pour faire le Place & Route d'un DFG  
│   ├── Simulateur  
│   ├── Simulateur_online                     -- Simulateur d'opérateur MSB First  
│   └── simulator  
└── VHDL  
 .  ├── tfa_src                               -- Projet Hardware TFA avec opérateur MSB-First  
 .  │   ├── sim_overlay                       -- Scripts de simulation  
 .  │   ├── src_overlay                       -- Source VHDL  
 .  │   │   └── online_arithm                 -- Source des opérateurs et convertisseur on-line               
 .  │   ├── src_pcie                          -- Source VHDL pour la transmission pcie  
 .  │   └── src_user  
 .  └── VHDL_onlineArithmetic                 -- Répertoire de test des opérateurs  
 .  .       ├── scripts                           -- Scripts de simulations  
 .  .       ├── sim                               -- Répertoire de simulation  
 .  .       ├── src_tb                            -- Source des tests bench  
 .  .       ├── src_vhdl                          -- Source VHDL du projet  
 .  .       ├── synth                             -- Répertoire de synthétisation  
  .  .      └── tlmvm                             -- Librairie pour test bench


## Simulation du projet tfa
### Préparation des fichiers pour la simulation
- Définir la taille de l'overlay dans HDL/tfa_src/src_overlay/overlay_pkg.vhd
- Utiliser le programme Tools/pr pour définir la configuration de la FPGA pour un DFG
- Modifier le fichier VHDL/tfa_src/sim_overlay/tb_data_in_out_overlay.vhd avec la configuration du DFG
- Modifier le fichier DL/tfa_src/sim_overlay/input_data.txt avec les données à envoyer au design, ainsi que leur mapping

### Exécution de la simulation
- Lancez Questasim dans le répertoire VHDL/tfa_src/sim_overlay
- Exécutez le script proj_pcie_top_compile.do afin de compiler les librairies
- Exécutez le script sim_tb_data_in_out.do pour lancer la simulation
- Valider les résultats en regardant le fichier data_output.txt
- Si vous obtenez un message d'erreur de type "Error cannot load design unit", supprimé les fichiers VHDL/tfa_src/sim_overlay/msim et VHDL/tfa_src/sim_overlay/work et recommencez