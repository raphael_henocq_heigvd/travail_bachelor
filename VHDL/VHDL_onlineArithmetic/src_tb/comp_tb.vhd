-----------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- Fichier      : add_tb.vhd
-- Description  : Testbench pour la simulation d'un online adder, d'un serialisateur
--                et d'un deserialisateur
--
-- Auteur       : Raphael Henocq
-- Date         : 13.04.2017
-- Version      : 1.0
--
--
--| Modifications |-----------------------------------------------------------
-- Ver   Auteur       Date        Description
-- 1.0   R.Henocq     13.04.2017  Cération du fichier
--
------------------------------------------------------------------------------

library IEEE;
  use IEEE.Std_Logic_1164.all;
  use IEEE.Numeric_Std.all;

library tlmvm;
context tlmvm.tlmvm_context;

entity mult_tb is

end mult_tb;

architecture Testbench of mult_tb is

component overlay_1x1_mult is
    generic (
            FIFOSIZE : integer :=32;
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i            : in std_logic;
        rst_i            : in std_logic;
        data_prv_a_i     : in std_logic_vector(FIFOSIZE-1 downto 0); --donnée sérialisée
        val_prv_a_i      : in std_logic;
        data_prv_b_i     : in std_logic_vector(FIFOSIZE-1 downto 0); --donnée sérialisée
        val_prv_b_i      : in std_logic;
        acc_nxt_i        : in std_logic;
        data_size_i      : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);

        -- Sorties
        acc_prv_a_o    : out std_logic;
        acc_prv_b_o    : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end component;

  -- constantes internes au test-bench
  constant CLK_PERIOD : time := 10 ns;

  -- constante donnant la taille de l'additionneur
  constant FIFOSIZE : integer := 32;
  constant DATA_SIZE_LENGTH : integer :=10;
  constant data_size_sti : integer :=  8;

signal clk_sti         : std_logic;
signal rst_sti         : std_logic;
signal data_prv_a_sti  : std_logic_vector(FIFOSIZE-1 downto 0);
signal data_prv_b_sti  : std_logic_vector(FIFOSIZE-1 downto 0);
signal val_prv_a_sti   : std_logic;
signal val_prv_b_sti   : std_logic;
signal acc_nxt_sti     : std_logic;
signal acc_prv_a_obs   : std_logic;
signal acc_prv_b_obs   : std_logic;
signal val_nxt_obs     : std_logic;
signal data_nxt_obs    : std_logic_vector(FIFOSIZE-1 downto 0);
signal end_sim         : std_logic;

begin

  ---------------------------------------------------------------------------
  -- Interconnexion du module VHDL a simuler
  ---------------------------------------------------------------------------
  dut: overlay_1x1_mult
    generic map (
            FIFOSIZE => FIFOSIZE,
            DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
    )
    port map(
        -- Entrées
         -- Entrées
        clk_i            => clk_sti,
        rst_i            => rst_sti,
        data_prv_a_i     => data_prv_a_sti,
        val_prv_a_i      => val_prv_a_sti,
        data_prv_b_i     => data_prv_b_sti,
        val_prv_b_i      => val_prv_b_sti,
        acc_nxt_i        => acc_nxt_sti,
        data_size_i      => std_logic_vector(To_Unsigned(data_size_sti,DATA_SIZE_LENGTH)),

        -- Sorties
        acc_prv_a_o    => acc_prv_a_obs,
        acc_prv_b_o    => acc_prv_b_obs,
        val_nxt_o      => val_nxt_obs,
        data_nxt_o     => data_nxt_obs
    );
---------------------------------------------------------------------------
-- Debut des pas de simulation
---------------------------------------------------------------------------
-- Clock generation
  clk_process :process
   begin
    clk_sti <= '1';
    wait for CLK_PERIOD/2;
    clk_sti <= '0';
    wait for CLK_PERIOD/2;
    if end_sim = '1' then
      wait;
    end if;
   end process;

-- Reset generation
    simple_startup_reset(rst_sti, CLK_PERIOD * 10);

    val_prv_a_sti <= '0' when data_prv_a_sti(0) = 'U' else
                     '1';
    val_prv_b_sti <= '0' when data_prv_b_sti(0) = 'U' else
                     '1';

-- Stimuli generator
process_send : process is

begin
   report "Debut de la simulation";
   val_prv_a_sti <= '0';
   val_prv_b_sti <= '0';
   wait until falling_edge(rst_sti);

   for I in 20 to 40 loop --2**data_size_sti-1 loop
      data_prv_a_sti <= Std_Logic_Vector(To_Unsigned(I,data_prv_a_sti'length));
      for J in 10 to 40 loop
        data_prv_b_sti <= Std_Logic_Vector(To_Unsigned(J,data_prv_b_sti'length));
        wait until rising_edge(clk_sti);
        val_prv_a_sti <= '1';
        val_prv_b_sti <= '1';
        wait until acc_prv_a_obs = '1' and acc_prv_b_obs = '1';


      end loop;
   end loop;
    wait;
end process process_send;

-- output reader and error detection
process_read : process is

variable ref : integer :=0;
variable data_read_ref : std_logic_vector(FIFOSIZE-1 downto 0);
variable nb_error      : Natural;
variable data_read     : std_logic_vector(FIFOSIZE-1 downto 0);
variable data_mask     : std_logic_vector(FIFOSIZE-1 downto 0) := (others => '0');
variable ref_v         : std_logic_vector(FIFOSIZE-1 downto 0);

begin
   nb_error := 0;
  end_sim <= '0';
   wait until falling_edge(rst_sti);
   --mask init
   for I in 0 to data_size_sti-1 loop
    data_mask(I) := '1';
   end loop;

   for I in 20 to 40 loop
    for J in 10 to 40 loop
      acc_nxt_sti <= '1';
      if I>J then
        ref := 1;
      else 
        ref := 0;
      end if;
      ref_v := std_logic_vector(to_unsigned(ref,data_mask'length));
      ref_v := ref_v and data_mask;
      ref := to_integer(unsigned(ref_v));
      data_read_ref := Std_Logic_Vector(To_Unsigned(ref,data_read_ref'length));
      wait until falling_edge(clk_sti);
      wait until val_nxt_obs = '1';
      data_read := data_nxt_obs and data_mask;
      data_read_ref := data_read_ref and data_mask;
      if data_read_ref/=data_read then
         nb_error := nb_error + 1;
         report " Erreur lors de la reception de donne data_read_ref = "
                  & integer'image(ref) & " data_read_obs = " & integer'image(to_integer(unsigned(data_read)));
      end if;
    end loop;
   end loop;

   report "Nombre d'erreurs detectees = " & integer'image(nb_error);
   report "Fin de la simulation";
   end_sim <= '1';
   wait;
end process process_read;


end Testbench;
