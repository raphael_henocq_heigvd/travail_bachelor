-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : serial_deserial.vhd
-- Description  : Instancie un serializer et un deserializer afin d'effectuer un test bench
--
-- Author       : Raphael Henocq
-- Date         : 10.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    10.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.overlay_pkg.all;

entity serial_deserial is
    generic (
            FIFOSIZE : integer :=32;
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(FIFOSIZE-1 downto 0); --donnée sérialisée
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end serial_deserial;

architecture struct of serial_deserial is
-- components
component signed_digit_deserializer is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
    end component;

component signed_digit_serializer is
    generic (
            FIFOSIZE : integer :=32;
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(FIFOSIZE-1 downto 0);
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0); -- Taille à définir ! Voir comment apporter cette valeur sur la FPGA

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        data_ser_o     : out std_logic_vector(size_bus-1 downto 0)
    );
end component;

signal data_conv_s  : std_logic_vector(size_bus-1 downto 0);
signal val_conv_s   : std_logic;
signal acc_conv_s   : std_logic;
signal val_nxt_s    : std_logic;
signal val_nxt1_s   : std_logic;


begin

signed_digit_deserializer_s : signed_digit_deserializer
    generic map(
            FIFOSIZE => FIFOSIZE
    )
    port map(
        -- Entrées
        clk_i          => clk_i,
        rst_i          => rst_i,
        data_prv_i     => data_conv_s,
        val_prv_i      => val_conv_s,
        acc_nxt_i      => acc_nxt_i,

        -- Sorties
        acc_prv_o      => acc_conv_s,
        val_nxt_o      => val_nxt_o,
        data_nxt_o     => data_nxt_o
    );

signed_digit_serializer_s : signed_digit_serializer
    generic map(
            FIFOSIZE => FIFOSIZE,
            DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
    )
    port map(
        -- Entrées
        clk_i          => clk_i,
        rst_i          => rst_i,
        data_prv_i     => data_prv_i,
        val_prv_i      => val_prv_i,
        acc_nxt_i      => acc_conv_s,
        data_size_i    => data_size_i,

        -- Sorties
        acc_prv_o      => acc_prv_o,
        val_nxt_o      => val_conv_s,
        data_ser_o     => data_conv_s
    );

    process(clk_i, rst_i)
    begin
      if rst_i = '1' then
        val_nxt1_s <= '0';
      elsif rising_edge(clk_i) then
        val_nxt1_s <= val_nxt_s;
      end if;
    end process;

    --val_nxt_o <= val_nxt1_s;
end struct;
