-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : signed_digit_serializer.vhd
-- Description  : Converti une donnée de représentation classique de type signed integer
--                en sa représentation en signed digit, et la serialise. Ajoute aussi à
--                la donnée sérialiser l'information permettant de savoir s'il s'agit
--                de la dernière donnée sérialisée ( 1 : not last data, 0 : last data)
--                Signed Digit représentation : 00 = 0
--                                              01 = 1
--                                              10 = -1
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.overlay_pkg.all;

entity signed_digit_serializer is
    generic (
            FIFOSIZE : integer :=32;
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(FIFOSIZE-1 downto 0);
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0); -- Taille à définir ! Voir comment apporter cette valeur sur la FPGA

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        data_ser_o     : out std_logic_vector(size_bus-1 downto 0)
    );
end signed_digit_serializer;

architecture struct of signed_digit_serializer is

-- component
component uc_signed_digit_serializer is
    generic (
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        en_get_msb_o   : out std_logic;
        nLoad_shift_o  : out std_logic;
        end_of_data_o  : out std_logic;
        sel_out_o      : out std_logic
    );
end component;

component get_in_MSB_order is
    generic (
            FIFOSIZE: integer:= 8;
            DATA_SIZE_LENGTH: integer := 32
    );
    port ( clk_i         : in  std_logic;
           rst_i         : in  std_logic;
           enable_i      : in  std_logic;
           nLoad_get_i   : in  std_logic;
           data_size_i   : in  std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
           D_load_i      : in  std_logic_vector (FIFOSIZE-1 downto 0);
           D_o           : out std_logic;
           signe_bit_o   : out std_logic
    );
end component;
-- Signals
signal en_get_msb_s  : std_logic;
signal nLoad_get_s   : std_logic;
signal end_of_data_s : std_logic;
signal get_out_s     : std_logic;
signal data_ser_s    : std_logic_vector(size_bus-1 downto 0);
signal data_ser1_s   : std_logic_vector(size_bus-1 downto 0);
signal sign_bit_s    : std_logic;
signal sel_out_s     : std_logic;


begin

uc_signed_digit_serializer_s :  entity work.uc_signed_digit_serializer(moore)
    generic map (
            DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
    )
    port map (
            -- Entrées
            clk_i          => clk_i,
            rst_i          => rst_i,
            val_prv_i      => val_prv_i,
            acc_nxt_i      => acc_nxt_i,
            data_size_i    => data_size_i,

            -- Sorties
            acc_prv_o      => acc_prv_o,
            val_nxt_o      => val_nxt_o,
            en_get_msb_o   => en_get_msb_s,
            nLoad_get_o    => nLoad_get_s,
            end_of_data_o  => end_of_data_s,
            sel_out_o      => sel_out_s
        );

get_in_MSB_order_s : get_in_MSB_order
    generic map(
            FIFOSIZE => FIFOSIZE,
            DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
    )
    port map(
           clk_i         => clk_i,
           rst_i         => rst_i,
           enable_i      => en_get_msb_s,
           nLoad_get_i   => nLoad_get_s,
           data_size_i   => data_size_i,
           D_load_i      => data_prv_i,
           D_o           => get_out_s,
           signe_bit_o   => sign_bit_s
    );

process (clk_i, rst_i)
begin
  if rst_i = '1' then
    data_ser1_s <= (others => '0');
  elsif rising_edge(clk_i) then
    if sel_out_s = '0' then
      data_ser1_s <= data_ser_s;
    end if;
  end if;
end process;
data_ser_s <= ('0' & get_out_s) when (sign_bit_s = '0' and end_of_data_s = '0') else  -- positif number
              (get_out_s & '0') when (sign_bit_s = '1' and end_of_data_s = '0') else  -- negativ number
              "11";  -- End of data

data_ser_o <= data_ser_s when sel_out_s = '0' else
              data_ser1_s;
end struct;
