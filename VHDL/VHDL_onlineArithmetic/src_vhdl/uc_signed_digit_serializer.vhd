-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : uc_signed_digit_serializer.vhd
-- Description  : Converti une donnée de représentation classique de type signed integer
--                en sa représentation en signed digit, et la serialise. Ajoute aussi à
--                la donnée sérialiser l'information permettant de savoir s'il s'agit
--                de la dernière donnée sérialisée ( 1 : not last data, 0 : last data)
--                Signed Digit représentation : 00 = 0
--                                              01 = 1
--                                              10 = -1
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.overlay_pkg.all;

entity uc_signed_digit_serializer is
    generic (
        DATA_SIZE_LENGTH: integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0); -- Taille à définir ! Voir comment apporter cette valeur sur la FPGA

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        en_get_msb_o   : out std_logic;
        nLoad_get_o    : out std_logic;
        end_of_data_o  : out std_logic;
        sel_out_o      : out std_logic  -- Select data_last_out or data_out
    );
end uc_signed_digit_serializer;

architecture moore of uc_signed_digit_serializer is
-- components
component decompteur is
    generic (
            DATASIZE: integer:= 8
    );
    port ( clk_i      : in  std_logic;
           rst_i      : in  std_logic;
           enable_i   : in  std_logic;
           load_i     : in  std_logic;
           D_load_i   : in  std_logic_vector (DATASIZE-1 downto 0);
           Q          : out std_logic_vector (DATASIZE-1 downto 0)
    );
end component;
-- Signals
signal nLoad_cpt_s        : std_logic;
signal en_cpt_s           : std_logic;
signal cpt_out_s          : std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
signal det2_s             : std_logic;
signal cpt_load_s         : std_logic;
signal end_of_data_s      : std_logic;
        -- State machine signals
type state_t is (sInit, sStartConv, sConversion, sEndOfConv0, sEndOfConv1, sWaitAcc0, sWaitAcc1);
signal state_s      : state_t;
signal next_state_s : state_t;

begin

decompteur_s : decompteur
    generic map(
            DATASIZE => DATA_SIZE_LENGTH
    )
    port map( clk_i      => clk_i,
              rst_i      => rst_i,
              enable_i   => en_cpt_s,
              load_i     => cpt_load_s,
              D_load_i   => data_size_i,
              Q          => cpt_out_s
    );

cpt_load_s <= not nLoad_cpt_s;
-- Comparaison
process (cpt_out_s)
begin
    det2_s <= '0';
    if (unsigned(cpt_out_s)="10") then
        det2_s <= '1';
    end if;
end process;

-- State machine
process(clk_i, rst_i) is
begin
    if rst_i = '1' then
        state_s <= sInit;
    elsif rising_edge(clk_i) then
        state_s <= next_state_s;
    end if;
end process;

process(val_prv_i, acc_nxt_i, det2_s, state_s)
begin
    acc_prv_o      <= '0';
    val_nxt_o      <= '0';
    en_get_msb_o   <= '0';
    nLoad_get_o    <= '0';
    end_of_data_s  <= '0';
    en_cpt_s       <= '0';
    nLoad_cpt_s    <= '0';
    sel_out_o      <= '0';
    next_state_s   <= state_s;

    case state_s is
        when sInit =>
            if val_prv_i = '1' then
                next_state_s <= sStartConv;
            end if;
            acc_prv_o     <= '1';
            en_cpt_s      <= '1';
            en_get_msb_o  <= '1';

        when sStartConv =>
            --if acc_nxt_i = '1' then
                next_state_s <= sConversion;
            --else
            --    next_state_s <= sWaitAcc0;
            --end if;
          --  val_nxt_o     <= '1';
            en_cpt_s      <= '1';

        when sConversion =>
            if det2_s = '1' and acc_nxt_i = '1' then
              next_state_s <= sEndOfConv0;
            elsif acc_nxt_i = '0' then
              next_state_s <= sWaitAcc1;
            end if;
            nLoad_get_o   <= '1';
            en_get_msb_o  <= '1';
            en_cpt_s      <= '1';
            nLoad_cpt_s   <= '1';
            val_nxt_o     <= '1';

        when sEndOfConv0 =>
          if acc_nxt_i = '1' then
              next_state_s <= sEndOfConv1;
          end if;
            val_nxt_o     <= '1';
            nLoad_get_o   <= '1';

        when sEndOfConv1 =>
            if acc_nxt_i = '1' then
                next_state_s <= sInit;
            end if;
            val_nxt_o     <= '1';
            end_of_data_s <= '1';

        when sWaitAcc0 =>
            if det2_s = '1' and acc_nxt_i = '1' then
                next_state_s <= sEndOfConv0;
            elsif det2_s = '0' and acc_nxt_i = '1' then
                next_state_s <= sConversion;
            else
                next_state_s <= sWaitAcc1;
            end if;
            val_nxt_o     <= '1';
            nLoad_cpt_s   <= '1';
            sel_out_o     <= '1';
            en_cpt_s      <= '1';

        when sWaitAcc1 =>
            if det2_s = '1' and acc_nxt_i = '1' then
                next_state_s <= sEndOfConv0;
            elsif det2_s = '0' and acc_nxt_i = '1' then
                next_state_s <= sConversion;
            end if;
            val_nxt_o <= '1';
            sel_out_o <= '1';
    end case;

end process;

end_of_data_o <= end_of_data_s;

end moore;
