-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : multiplicateur.vhd
-- Description  : Effectue une multiplication/division de type msb first. Utilise des données en signed digit
--                représentation
--
-- Author       : Raphael Henocq
-- Date         : 23.06.17
-- Version      : 1.0
--
-- Remarques :
--       data_a_full_s contiens la valeur de A dans la multiplication, et celle de Q pour la division
--       data_b_full_s contiens la valeur de B dans la multiplication, et celle du diviseur pour la division
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    23.06.17           Creation
-- 1.0       RHE    27.06.17           Ajout de la division
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity mult_div is
   port (
     clk_i    : in std_logic;
     rst_i    : in std_logic;
     en_i     : in std_logic;
     clear_i  : in std_logic;
     data_a_i : in std_logic_vector(1 downto 0);
     data_b_i : in std_logic_vector(1 downto 0);
     cfg_op_i : in std_logic_vector(7 downto 0);

     result_o  : out std_logic_vector(FIFOSIZE - 1 downto 0);
     cmp_res_o : out std_logic
   );

end mult_div;

architecture data_flow of mult_div is
component on_fly_conv is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
      -- Entrées
      clk_i          : in std_logic;
      rst_i          : in std_logic;
      data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
      en_conv_i      : in std_logic;
      clear_conv_i   : in std_logic;
      -- Sorties
      data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end component;


signal data_a_full_s : std_logic_vector(FIFOSIZE-1 downto 0);
signal data_b_full_s : std_logic_vector(FIFOSIZE-1 downto 0);
signal mult_s        : integer;
signal div_s         : integer;
signal cmp_grt_s     : std_logic;
signal cmp_let_s     : std_logic;
signal cmp_equ_s     : std_logic;
signal result_s      : std_logic_vector(FIFOSIZE-1 downto 0);

begin

  convA : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_a_i,
      en_conv_i      => en_i,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_a_full_s
    );

  convB : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_b_i,
      en_conv_i      => en_i,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_b_full_s
    );

    mult_s <= to_integer(unsigned(data_a_full_s)) * to_integer(unsigned(data_b_full_s));
    div_s  <= 0 when to_integer(unsigned(data_b_full_s)) = 0 else
              to_integer(unsigned(data_a_full_s)) / to_integer(unsigned(data_b_full_s));
    cmp_grt_s <= '1' when data_a_full_s > data_b_full_s else '0';
    cmp_equ_s <= '0' when data_a_i /= "11" and data_b_i /= "11" else
                 '1' when data_a_full_s = data_b_full_s else
                 '0';

    result_s <= std_logic_vector(to_unsigned(mult_s, result_o'length)) when cfg_op_i = FU_OPCODE_MUL else
                std_logic_vector(to_unsigned(div_s, result_o'length));

    cmp_res_o <= cmp_grt_s when cfg_op_i = FU_OPCODE_GRE else
                 (cmp_grt_s  or cmp_equ_s) when cfg_op_i = FU_OPCODE_GEQ else
                 cmp_equ_s when cfg_op_i = FU_OPCODE_EQU else
                 not cmp_equ_s when data_a_i="11" and data_b_i = "11" else
                 '0';

    process(clk_i, rst_i)
    begin
      if rst_i='1'then
        result_o <= (others => '0');
      elsif rising_edge(clk_i) then
        if en_i = '1' then
          result_o <= result_s;
        end if;
      end if;
    end process;

end data_flow;
