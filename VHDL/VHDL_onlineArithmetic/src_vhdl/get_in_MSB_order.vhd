-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : get_in_MSB_order.vhd
-- Description  : Sérialise un nombre donné en sa décomposition bit par bit
--                en commancant par le MSB (bit de signe exclus). Fournit aussi
--                le bit de signe en sortie.
--
-- Author       : Raphael Henocq
-- Date         : 19.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    19.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity get_in_MSB_order is
    generic (
            FIFOSIZE: integer:= 8;
            DATA_SIZE_LENGTH: integer := 32
    );
    port ( clk_i         : in  std_logic;
           rst_i         : in  std_logic;
           enable_i      : in  std_logic;
           nLoad_get_i   : in  std_logic;
           data_size_i   : in  std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
           D_load_i      : in  std_logic_vector (FIFOSIZE-1 downto 0);
           D_o           : out std_logic;
           signe_bit_o   : out std_logic
    );
end get_in_MSB_order;

architecture behave of get_in_MSB_order is

component decompteur is
    generic (
            DATASIZE: integer:= 8
    );
    port ( clk_i      : in  std_logic;
           rst_i      : in  std_logic;
           enable_i   : in  std_logic;
           load_i     : in  std_logic;
           D_load_i   : in  std_logic_vector (DATASIZE-1 downto 0);
           Q          : out std_logic_vector (DATASIZE-1 downto 0)
    );
end component;

signal data_size_s   : std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
signal reg_pres      : std_logic_vector(FIFOSIZE-1 downto 0);
signal reg_fut       : std_logic_vector(FIFOSIZE-1 downto 0);
signal sign_bit_pres : std_logic;
signal sign_bit_fut  : std_logic;
signal decompt_out_s : std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
signal load_cpt_s    : std_logic;

begin

data_size_s <= (others => '0') when data_size_i(0) = 'X' else
               std_logic_vector(unsigned(data_size_i)-1);
signe_bit_o <= sign_bit_pres;
D_o         <= reg_pres(to_integer(unsigned(decompt_out_s)));
load_cpt_s  <= not nLoad_get_i;

sign_bit_fut <= D_load_i(to_integer(unsigned(data_size_s)));
reg_fut <= reg_pres when nLoad_get_i = '1' else
           D_load_i when sign_bit_fut = '0' else  --positive number
           std_logic_vector(unsigned (not D_load_i(FIFOSIZE-1 downto 0)) + 1); -- negative number

process (clk_i, rst_i)
begin
    if rst_i = '1' then
        reg_pres <= (others => '0');
        sign_bit_pres <= '0';
    elsif rising_edge(clk_i) then
        if enable_i = '1' then
            reg_pres <= reg_fut;
            if nLoad_get_i = '0' then
              sign_bit_pres <= sign_bit_fut;
            end if;
        end if;
    end if;
end process;

decompteur_s : decompteur
    generic map(
            DATASIZE => DATA_SIZE_LENGTH
    )
    port map(
           clk_i      => clk_i,
           rst_i      => rst_i,
           enable_i   => enable_i,
           load_i     => load_cpt_s,
           D_load_i   => data_size_s,
           Q          => decompt_out_s
    );

end behave;
