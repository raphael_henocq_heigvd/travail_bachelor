-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : online_adder_cell.vhd
-- Description  : Récupère des données sérialisées de type signed integer en représentation
--                signed digit et effectue une addition de type MSB first.
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.overlay_pkg.all;

entity mult_cell is
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_a_i   : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        val_prv_a_i    : in std_logic;
        data_prv_b_i   : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        val_prv_b_i    : in std_logic;
        acc_nxt_i      : in std_logic;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);

        -- Sorties
        acc_prv_a_o    : out std_logic;
        acc_prv_b_o    : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(1 downto 0)
    );
end mult_cell;

architecture struct of mult_cell is
-- components
component uc_fu_all is
    port (
        -- Entrées
        clk_i         : in std_logic;
        rst_i         : in std_logic;
        val_prv_a_i   : in std_logic;
        val_prv_b_i   : in std_logic;
        val_prv_sel_i : in std_logic;
        acc_nxt_i     : in std_logic;
        end_of_data_i : in std_logic;
        delta_i       : in std_logic_vector(2 downto 0);
        mult_ready_i  : in std_logic;
        cfg_op_i      : in std_logic_vector(7 downto 0);

        -- Sorties
        acc_prv_a_o   : out std_logic;
        acc_prv_b_o   : out std_logic;
        val_nxt_o     : out std_logic;
        end_of_data_o : out std_logic;
        en_fu_o      : out std_logic;
        clear_fu_o   : out std_logic;
        in_add_fu_o  : out std_logic
    );
end component;

component online_mult_div is
  port (
     clk_i         : in std_logic;
     rst_i         : in std_logic;
     en_i          : in std_logic;
     clear_i       : in std_logic;
     data_a_i      : in std_logic_vector(1 downto 0);   -- |a+,a-|
     data_b_i      : in std_logic_vector(1 downto 0);   -- |b+,b-|
     end_of_data_i : in std_logic;
     acc_nxt_i     : in std_logic;
     data_size_i   : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
     cfg_op_i      : in std_logic_vector(7 downto 0);

     ready_o         : out std_logic;
     val_nxt_mult_o  : out std_logic;
     result_o        : out std_logic_vector(1 downto 0)
  );
end component;

-- Signals
signal end_of_data_in_s : std_logic;
signal end_of_data_o_s  : std_logic;
signal en_fu_s          : std_logic;
signal clear_fu_s       : std_logic;
signal in_fu_sel_s      : std_logic;
signal data_a_s         : std_logic_vector(size_bus-1 downto 0); -- Sans le bit end_of_data
signal data_b_s         : std_logic_vector(size_bus-1 downto 0);
signal data_a_s1        : std_logic_vector(size_bus-1 downto 0);
signal data_b_s1        : std_logic_vector(size_bus-1 downto 0);
signal delta_s          : std_logic_vector(2 downto 0);
signal result_s         : std_logic_vector(1 downto 0);
signal ready_s          : std_logic;
signal cfg_mult_s       : std_logic_vector(7 downto 0);
signal val_prv_sel_s    : std_logic;

begin
val_prv_sel_s <= '0';

uc_fu_all_s : entity work.uc_fu_all(moore)
    port map (
        -- Entrées
        clk_i         => clk_i,
        rst_i         => rst_i,
        val_prv_a_i   => val_prv_a_i,
        val_prv_b_i   => val_prv_b_i,
        val_prv_sel_i => val_prv_sel_s,
        acc_nxt_i     => acc_nxt_i,
        end_of_data_i => end_of_data_in_s,
        delta_i       => delta_s,
        mult_ready_i  => ready_s,
        cfg_op_i      => cfg_mult_s,

        -- Sorties
        acc_prv_a_o   => acc_prv_a_o,
        acc_prv_b_o   => acc_prv_b_o,
        val_nxt_o     => open,
        end_of_data_o => end_of_data_o_s,
        en_fu_o       => en_fu_s,
        clear_fu_o    => clear_fu_s,
        in_fu_sel_o   => in_fu_sel_s
    );

online_mult_div_s : online_mult_div
  port map(
     clk_i         => clk_i,
     rst_i         => rst_i,
     en_i          => en_fu_s,
     clear_i       => clear_fu_s,
     data_a_i      => data_a_s,
     data_b_i      => data_b_s,
     end_of_data_i => end_of_data_in_s,
     acc_nxt_i     => acc_nxt_i,
     data_size_i   => data_size_i,
     cfg_op_i      => cfg_mult_s,

     ready_o  => ready_s,
     val_nxt_mult_o  => val_nxt_o,
     result_o => result_s
  );


   process (rst_i, clk_i) is
   begin
     if rst_i = '1' then
       data_a_s1 <= (others => '0');
       data_b_s1 <= (others => '0');
     elsif rising_edge(clk_i) then
       data_a_s1 <= data_a_s;
       data_b_s1 <= data_b_s;
     end if;
   end process;

  cfg_mult_s <= FU_OPCODE_GRE;
  delta_s <= "000";  -- delta = 3 for multiplication
  end_of_data_in_s <= '1' when data_prv_a_i = "11"  and data_prv_b_i = "11"  and val_prv_a_i = '1' and val_prv_b_i='1' else
                      '0';
  data_a_s <= data_prv_a_i when in_fu_sel_s = '0' else
              "11";
  data_b_s <= data_prv_b_i when in_fu_sel_s = '0' else
              "11";
  data_nxt_o <= result_s;

end struct;
