-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : signed_digit_deserializer.vhd
-- Description  : Récupère des données sérialisées de type signed integer en représentation
--                signed digit et les converti en représentation classique non-sérialisée.
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.overlay_pkg.all;

entity signed_digit_deserializer is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end signed_digit_deserializer;

architecture struct of signed_digit_deserializer is
-- components
component uc_signed_digit_deserializer is
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        end_of_data_i  : in std_logic;

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        en_conv_o      : out std_logic;
        clear_conv_o   : out std_logic
    );
end component;

component on_fly_conv is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        en_conv_i      : in std_logic;
        clear_conv_i   : in std_logic;

        -- Sorties
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end component;

-- Signals
signal en_conv_s     : std_logic;
signal clear_conv_s  : std_logic;
signal data1_s       : std_logic_vector(size_bus-1 downto 0);
signal end_of_data_s : std_logic;


begin


end_of_data_s <= '1' when data_prv_i = "11" else
                 '0';

uc_signed_digit_deserializer_s : entity  work.uc_signed_digit_deserializer(moore)
    port map(
        -- Entrées
        clk_i          =>clk_i,
        rst_i          =>rst_i,
        val_prv_i      =>val_prv_i,
        acc_nxt_i      =>acc_nxt_i,
        end_of_data_i  =>end_of_data_s,
        -- Sorties
        acc_prv_o      =>acc_prv_o,
        val_nxt_o      =>val_nxt_o,
        en_conv_o      =>en_conv_s,
        clear_conv_o   =>clear_conv_s
    );

on_fly_conv_s : on_fly_conv
    generic map(
            FIFOSIZE => FIFOSIZE
    )
    port map(
        -- Entrées
        clk_i          =>clk_i,
        rst_i          =>rst_i,
        data_prv_i     =>data1_s,
        en_conv_i      =>en_conv_s,
        clear_conv_i   =>clear_conv_s,
        -- Sorties
        data_nxt_o     =>data_nxt_o
    );

    process(clk_i, rst_i) is
    begin
        if rst_i = '1' then
            data1_s <= (others => '0');
        elsif Rising_Edge(clk_i) then
            data1_s <= data_prv_i;
        end if;
    end process;
end struct;
