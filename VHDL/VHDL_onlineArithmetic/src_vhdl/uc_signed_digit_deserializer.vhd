-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : uc_signed_digit_deserializer.vhd
-- Description  : Machine séquentiel permettant le fonctionnement de la déserialisation et de la conversion
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.overlay_pkg.all;

entity uc_signed_digit_deserializer is
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        end_of_data_i  : in std_logic;

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        en_conv_o      : out std_logic;
        clear_conv_o   : out std_logic
    );
end uc_signed_digit_deserializer;

architecture moore of uc_signed_digit_deserializer is
-- Signals
type state_t is (sInit, sConv, sEnd , sWait, sWaitEnd);

signal state_s      : state_t;
signal next_state_s : state_t;

begin

process (val_prv_i, acc_nxt_i, end_of_data_i, state_s)
begin
acc_prv_o <= '0';
val_nxt_o <= '0';
en_conv_o <= '0';
clear_conv_o <= '0';
next_state_s <= state_s;

    case state_s is
        when sInit =>
          if val_prv_i = '1' then
              next_state_s <= sConv;
          end if;
          clear_conv_o <= '1';
          acc_prv_o <= '1';

        when sConv =>
          if end_of_data_i = '1' then
            next_state_s <= sEnd;
          elsif val_prv_i <= '0' then
            next_state_s <= sWait;
          end if;
          en_conv_o <= '1';
          acc_prv_o <= '1';

        when sEnd =>
          if acc_nxt_i = '1' then
              next_state_s <= sInit;
          else
              next_state_s <= sWaitEnd;
          end if;
          val_nxt_o <= '1';
          en_conv_o <= '1';

          when sWaitEnd =>
          if val_prv_i = '1'  then
              next_state_s <= sInit;
          end if;
          acc_prv_o <= '1';

        when sWait =>
          if val_prv_i = '1' and end_of_data_i = '1' then
              next_state_s <= sEnd;
          elsif  val_prv_i = '1' then
              next_state_s <= sConv;            
          end if;
          acc_prv_o <= '1';
    end case;
end process;


process(clk_i, rst_i) is
begin
    if rst_i = '1' then
        state_s <= sInit;
    elsif rising_edge(clk_i) then
        state_s <= next_state_s;
    end if;
end process;

end moore;
