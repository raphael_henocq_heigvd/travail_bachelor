library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_Std.all;

package overlay_pkg is

constant size_bus_ext     : integer := 32;        -- Modif by RHE
constant size_bus         : integer := 2;         -- Modif by RHE
constant DATA_SIZE_LENGTH : integer := 10;        --Taille du bus contenant la taille des données envoyés à l'overlay
constant FIFOSIZE         : integer := 32;

--FU operation opcodes
constant FU_OPCODE_SUM   : std_logic_vector(07 downto 0) := X"01";	--in1 + in2
constant FU_OPCODE_SUB   : std_logic_vector(07 downto 0) := X"02";	--in1 - in2
constant FU_OPCODE_MUL   : std_logic_vector(07 downto 0) := X"03";	--in1 * in2
constant FU_OPCODE_DIV   : std_logic_vector(07 downto 0) := X"04";	--in1 / in2
constant FU_OPCODE_MOD   : std_logic_vector(07 downto 0) := X"05";	--in1 % in2
--phy
constant FU_OPCODE_EQU   : std_logic_vector(07 downto 0) := X"10";	--in1 == in2
constant FU_OPCODE_NEQ   : std_logic_vector(07 downto 0) := X"11";	--in1 != in2
constant FU_OPCODE_GEQ   : std_logic_vector(07 downto 0) := X"12";	--in1 >= in2
constant FU_OPCODE_GRE   : std_logic_vector(07 downto 0) := X"13";	--in1 >  in2
constant FU_OPCODE_SEL   : std_logic_vector(07 downto 0) := X"14";	--if(sel) in1 else in2
end overlay_pkg;
