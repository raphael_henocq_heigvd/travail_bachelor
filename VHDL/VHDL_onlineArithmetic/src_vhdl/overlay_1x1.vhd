-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : overlay_1x1.vhd
-- Description  : Fichier top pour permettre la simulation et le testbench de l'online arithmetics
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.overlay_pkg.all;

entity overlay_1x1 is
    generic (
            FIFOSIZE : integer :=32;
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i            : in std_logic;
        rst_i            : in std_logic;
        data_prv_a_i     : in std_logic_vector(FIFOSIZE-1 downto 0); --donnée sérialisée
        val_prv_a_i      : in std_logic;
        data_prv_b_i     : in std_logic_vector(FIFOSIZE-1 downto 0); --donnée sérialisée
        val_prv_b_i      : in std_logic;
        acc_nxt_i        : in std_logic;
        data_size_i      : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);

        -- Sorties
        acc_prv_a_o    : out std_logic;
        acc_prv_b_o    : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end overlay_1x1;

architecture struct of overlay_1x1 is
--components
component online_adder_cell is
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_a_i   : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        val_prv_a_i    : in std_logic;
        data_prv_b_i   : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        val_prv_b_i    : in std_logic;
        acc_nxt_i      : in std_logic;

        -- Sorties
        acc_prv_a_o    : out std_logic;
        acc_prv_b_o    : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(size_bus-1 downto 0)
    );
end component;

component signed_digit_serializer is
    generic (
            FIFOSIZE : integer :=32;
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(FIFOSIZE-1 downto 0);
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        data_ser_o     : out std_logic_vector(size_bus-1 downto 0)
    );
end component;

component signed_digit_deserializer is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        val_prv_i      : in std_logic;
        acc_nxt_i      : in std_logic;

        -- Sorties
        acc_prv_o      : out std_logic;
        val_nxt_o      : out std_logic;
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end component;

-- Signals
signal serial_a_val_next_s : std_logic;
signal serial_b_val_next_s : std_logic;
signal serial_a_acc_next_s : std_logic;
signal serial_b_acc_next_s : std_logic;
signal serial_data_a_s     : std_logic_vector(size_bus-1 downto 0);
signal serial_data_b_s     : std_logic_vector(size_bus-1 downto 0);
signal deserial_data_prev_s: std_logic_vector(size_bus-1 downto 0);
signal deserial_val_prev_s : std_logic;
signal deserial_acc_prev_s : std_logic;


begin

signed_digit_serializer_a_s : signed_digit_serializer
    generic map(
            FIFOSIZE => FIFOSIZE,
            DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
    )
    port map(
        -- Entrées
        clk_i          => clk_i,
        rst_i          => rst_i,
        data_prv_i     => data_prv_a_i,
        val_prv_i      => val_prv_a_i,
        acc_nxt_i      => serial_a_acc_next_s,
        data_size_i    => data_size_i,
        -- Sorties
        acc_prv_o      => acc_prv_a_o,
        val_nxt_o      => serial_a_val_next_s,
        data_ser_o     => serial_data_a_s
    );

signed_digit_serializer_b_s : signed_digit_serializer
    generic map(
            FIFOSIZE => FIFOSIZE,
            DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
    )
    port map(
        -- Entrées
        clk_i          => clk_i,
        rst_i          => rst_i,
        data_prv_i     => data_prv_b_i,
        val_prv_i      => val_prv_b_i,
        acc_nxt_i      => serial_b_acc_next_s,
        data_size_i    => data_size_i,
        -- Sorties
        acc_prv_o      => acc_prv_b_o,
        val_nxt_o      => serial_b_val_next_s,
        data_ser_o     => serial_data_b_s
    );

online_adder_cell_s : online_adder_cell
    port map(
        -- Entrées
        clk_i          => clk_i,
        rst_i          => rst_i,
        data_prv_a_i   => serial_data_a_s,
        val_prv_a_i    => serial_a_val_next_s,
        data_prv_b_i   => serial_data_b_s,
        val_prv_b_i    => serial_b_val_next_s,
        acc_nxt_i      => deserial_acc_prev_s,

        -- Sorties
        acc_prv_a_o    => serial_a_acc_next_s,
        acc_prv_b_o    => serial_b_acc_next_s,
        val_nxt_o      => deserial_val_prev_s,
        data_nxt_o     => deserial_data_prev_s
    );

signed_digit_deserializer_s : signed_digit_deserializer
    generic map(
            FIFOSIZE => FIFOSIZE
    )
    port map(
        -- Entrées
        clk_i          => clk_i,
        rst_i          => rst_i,
        data_prv_i     => deserial_data_prev_s,
        val_prv_i      => deserial_val_prev_s,
        acc_nxt_i      => acc_nxt_i,

        -- Sorties
        acc_prv_o      => deserial_acc_prev_s,
        val_nxt_o      => val_nxt_o,
        data_nxt_o     => data_nxt_o
    );

end struct;
