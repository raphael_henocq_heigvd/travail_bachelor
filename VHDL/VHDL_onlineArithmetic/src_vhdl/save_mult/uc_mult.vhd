-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : multiplicateur.vhd
-- Description  : Effectue une multiplication de type msb first. Utilise des données en signed digit
--                représentation
--
-- Author       : Raphael Henocq
-- Date         : 23.06.17
-- Version      : 1.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    23.06.17           Creation
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity uc_mult is
   port (
      clk_i          : in std_logic;
      rst_i          : in std_logic;
      end_of_data_i  : in std_logic;
      acc_nxt_i      : in std_logic;
      data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
      en_get_msb_o   : out std_logic;
      val_nxt_o      : out std_logic;
      ready_o        : out std_logic;
      nLoad_get_o    : out std_logic;
      end_of_data_o  : out std_logic
   );

end uc_mult;

architecture data_flow of uc_mult is

  type state_t is  (sInit,sWait0,sWait1, sConv, sWaitAcc, sEnd);
  signal state           : state_t;
  signal state_fut       : state_t;
  signal cpt_pres        : unsigned(DATA_SIZE_LENGTH-1 downto 0);
  signal cpt_fut         : unsigned(DATA_SIZE_LENGTH-1 downto 0);
  signal en_cpt          : std_logic;
  signal load_nCount_cpt : std_logic;
  signal end_of_data_s1  : std_logic;
  signal end_of_data_s2  : std_logic;


begin

  process(state, end_of_data_s2, acc_nxt_i, clk_i)
  begin
    state_fut <= state;
    en_get_msb_o <= '0';
    val_nxt_o <= '0';
    ready_o <= '0';
    nLoad_get_o <= '0';
    en_cpt <= '0';
    load_nCount_cpt <= '0';
  end_of_data_o <= '0';

    case state is
      when sInit =>
        en_get_msb_o <= '1';
        en_cpt <= '1';
        load_nCount_cpt <= '1';
        ready_o <= '1';
        if end_of_data_s2 = '1' then
          state_fut <= sWait0;
        end if;

      when sWait0 =>
        en_get_msb_o <= '1';
        en_cpt <= '1';
        load_nCount_cpt <= '1';
        state_fut <= sWait1;

      when sWait1 =>
        en_get_msb_o <= '1';
        en_cpt <= '1';
        ready_o <= '1';
        load_nCount_cpt <= '1';
        state_fut <= sConv;

      when sConv =>
        en_cpt <= '1';
        en_get_msb_o <= '1';
        nLoad_get_o <= '1';
        val_nxt_o <= '1';
        if cpt_pres = 1 and acc_nxt_i = '1' then
          state_fut <= sEnd;
        elsif acc_nxt_i = '0' then
          state_fut <= sWaitAcc;
        end if;

      when sWaitAcc =>
        val_nxt_o <= '1';
        if acc_nxt_i = '1' and cpt_pres = 1 then
          state_fut <= sEnd;
        elsif acc_nxt_i = '1' then
          state_fut <= sConv;
        end if;

      when sEnd =>
        val_nxt_o <= '1';
        end_of_data_o <= '1';
        if acc_nxt_i = '1' then
          state_fut <= sInit;
        end if;
      end case;
  end process;

  cpt_fut <= cpt_pres when en_cpt = '0' else
             unsigned(data_size_i) when load_nCount_cpt = '1' else
             to_unsigned(0,cpt_fut'length) when cpt_pres = 0 else
             cpt_pres-1;

    process (clk_i, rst_i)
    begin
      if rst_i = '1' then
        state <= sInit;
        cpt_pres <= (others => '0');
        end_of_data_s1  <= '0';
        end_of_data_s2 <= '0';
      elsif rising_edge(clk_i) then
        state <= state_fut ;
        cpt_pres <= cpt_fut;
        end_of_data_s2 <= end_of_data_s1;
        end_of_data_s1 <= end_of_data_i;
      end if;
    end process;

end data_flow;
