-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : multiplicateur.vhd
-- Description  : Effectue une multiplication/division de type msb first. Utilise des données en signed digit
--                représentation
--
-- Author       : Raphael Henocq
-- Date         : 23.06.17
-- Version      : 1.0
--
-- Remarques :
--       data_a_full_s contiens la valeur de A dans la multiplication, et celle de Q pour la division
--       data_b_full_s contiens la valeur de B dans la multiplication, et celle du diviseur pour la division
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    23.06.17           Creation
-- 1.0       RHE    27.06.17           Ajout de la division
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity mult_div is
   port (
     clk_i    : in std_logic;
     rst_i    : in std_logic;
     en_i     : in std_logic;
     clear_i  : in std_logic;
     data_a_i : in std_logic_vector(1 downto 0);
     data_b_i : in std_logic_vector(1 downto 0);
     cfg_op_i : in std_logic_vector(7 downto 0);

     result_mult_o : out std_logic_vector(FIFOSIZE - 1 downto 0);
     result_div_o  : out std_logic_vector(1 downto 0)
   );

end mult_div;

architecture data_flow of mult_div is
component on_fly_conv is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
      -- Entrées
      clk_i          : in std_logic;
      rst_i          : in std_logic;
      data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
      en_conv_i      : in std_logic;
      clear_conv_i   : in std_logic;
      -- Sorties
      data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end component;

component on_fly_conv_1bit is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        en_conv_i      : in std_logic;
        clear_conv_i   : in std_logic;

        -- Sorties
        data_nxt_o     : out std_logic
    );
end component;

signal data_a1_s         : std_logic_vector(1 downto 0);
signal data_b1_s         : std_logic_vector(1 downto 0);
signal en_conv_a_s       : std_logic;
signal en_conv_b_s       : std_logic;
signal a_b_dec           : std_logic_vector(FIFOSIZE+3 downto 0);   -- contiens (a+b)*2^-3
signal data_a_full_s     : std_logic_vector(FIFOSIZE-1 downto 0);
signal data_b_full_s     : std_logic_vector(FIFOSIZE-1 downto 0);
signal data_a_full_sel_s : std_logic_vector(FIFOSIZE-1 downto 0);
signal data_b_full_sel_s : std_logic_vector(FIFOSIZE-1 downto 0);
signal data_d_full_sel_s : std_logic_vector(FIFOSIZE-1 downto 0);

signal cpt_fut           : std_logic_vector(4 downto 0);            -- position du bit de poid faible de v̂ conpris entre 2 et 31
signal cpt_pres          : std_logic_vector(4 downto 0);
signal en_cpt_s          : std_logic;
signal v_s               : std_logic_vector(FIFOSIZE+3 downto 0);
signal v_norm_s          : std_logic_vector(FIFOSIZE+3 downto 0);
signal w_fut             : std_logic_vector(FIFOSIZE+3 downto 0);
signal w_pres            : std_logic_vector(FIFOSIZE+3 downto 0);
signal en_s              : std_logic;
signal result_s          : std_logic_vector(FIFOSIZE - 1 downto 0);
signal carry_s           : unsigned(1 downto 0);   -- Carry for data_a and data_b !
signal q_s               : std_logic_vector(1 downto 0);

begin

  convA : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_a1_s,
      en_conv_i      => en_conv_a_s,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_a_full_s
    );

  convB : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_b_i,
      en_conv_i      => en_conv_b_s,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_b_full_s
    );

  en_conv_a_s <= '0' when data_a1_s = "11" else
                 en_i;
  en_conv_b_s <= '0' when data_b_i = "11" else
                 en_i;
  en_cpt_s <= en_conv_b_s and en_s;

  carry_s <= "00" when cfg_op_i = FU_OPCODE_DIV else
             "10" when data_b1_s = "10" and data_a1_s = "10" else
             "01" when data_b1_s = "10" or data_a1_s = "10" else
             "00";

  data_a_full_sel_s <= (others => '0') when (data_b1_s = "00" or data_b1_s = "11") else
                       data_a_full_s(data_a_full_s'length-2 downto 0) & '0' when data_b1_s = "01" else
                       not(data_a_full_s(data_a_full_s'length-2 downto 0) & '0');

  data_b_full_sel_s <= (others => '0') when (data_a1_s = "00" or data_a1_s = "11") else
                       data_b_full_s when data_a1_s= "01" else
                      not data_b_full_s;

  data_d_full_sel_s <= (others => '0') when q_s = "00" else
                       data_b_full_s when q_s= "01" else
                       not data_b_full_s;

  a_b_dec <= "0000" & std_logic_vector(unsigned(data_a_full_sel_s) + unsigned(data_b_full_sel_s) + carry_s) when cfg_op_i = FU_OPCODE_MUL else
             "0000" & std_logic_vector(unsigned(data_a_full_sel_s) + unsigned(data_d_full_sel_s) + carry_s);

  v_norm_s <=  std_logic_vector(unsigned(a_b_dec) + unsigned(w_pres(w_pres'length-2 downto 0) & '0'));
  v_s <= v_norm_s;-- when en_cpt_s = '0' else
        -- v_norm_s(v_s'length-2 downto 0) & '0'; -- Décalage à droite pour rester dans la même norme que data_b_full qui est décalé
  cpt_fut <= std_logic_vector(unsigned(cpt_pres)+1);

  process(v_s, en_cpt_s)
  variable v_v : std_logic_vector(2 downto 0);  -- correspnd à v̂
  variable p_v : std_logic;
  variable w_v : std_logic_vector(FIFOSIZE+3 downto 0);
  begin
    w_v := v_s;
    v_v := v_s(to_integer(unsigned(cpt_pres))+2 downto to_integer(unsigned(cpt_pres)));
    if v_v = "000" or v_v = "001" then    -- Simplifie SELM
      p_v := '0';
    else
      p_v := '1';
    end if;
    w_v(to_integer(unsigned(cpt_pres))+1) := v_v(1) xor p_v;
    if en_cpt_s = '1' then
      w_v := w_v(w_v'length-2 downto 0) & '0';
    end if;
    w_fut <= w_v;
  end process;

  process(clk_i, rst_i, clear_i)
  begin
    if rst_i = '1' or clear_i = '1' then
      en_s <= '0';
    elsif rising_edge(clk_i) then
      en_s <= en_i;
    end if;
  end process;

  process(clk_i, rst_i, clear_i)
  begin
    if rst_i = '1' or clear_i = '1' then
      data_a1_s <= (others => '0');
      data_b1_s <= (others => '0');
      if cfg_op_i = FU_OPCODE_DIV then
        cpt_pres <= "00100";
      else
        cpt_pres  <= "00011";  -- position initial du bit de poid faible de v̂ est 3
      end if;
      w_pres <= (others => '0');
      result_s <= (others => '0');
    elsif (rising_edge(clk_i)) then
      if en_s = '1' then
        data_a1_s <= data_a_i;
        data_b1_s <= data_b_i;
        w_pres <= w_fut;
        result_s <=  w_pres(w_pres'length-3 downto 2);
      end if;
      if en_cpt_s = '1' then
        cpt_pres <= cpt_fut;
      end if;
    end if;
  end process;

  result_mult_o <= result_s;
  result_div_o <= result_s(1 downto 0);
end data_flow;
