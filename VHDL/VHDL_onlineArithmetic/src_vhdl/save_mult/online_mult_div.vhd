-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : online_mult_div.vhd
-- Description  : Effectue une multiplication de type msb first. Utilise des données en signed digit
--                représentation
--
-- Author       : Raphael Henocq
-- Date         : 23.06.17
-- Version      : 1.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    17.04.17           Creation
-- 1.0       RHE    19.04.17           Changement d'algorithme pour l'addition
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity online_mult_div is
   port (
      clk_i         : in std_logic;
      rst_i         : in std_logic;
      en_i          : in std_logic;
      clear_i       : in std_logic;
      data_a_i      : in std_logic_vector(1 downto 0);   -- |a+,a-|
      data_b_i      : in std_logic_vector(1 downto 0);   -- |b+,b-|
      end_of_data_i : in std_logic;
      acc_nxt_i     : in std_logic;
      data_size_i   : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
      cfg_op_i      : in std_logic_vector(7 downto 0);

      ready_o        : out std_logic;
      val_nxt_mult_o : out std_logic;
      result_o       : out std_logic_vector(1 downto 0)
   );

end online_mult_div;

architecture struct of online_mult_div is

  component mult_div is
     port (
        clk_i    : in std_logic;
        rst_i    : in std_logic;
        en_i     : in std_logic;
        clear_i  : in std_logic;
        data_a_i : in std_logic_vector(1 downto 0);
        data_b_i : in std_logic_vector(1 downto 0);
        cfg_op_i : in std_logic_vector(7 downto 0);

        result_mult_o : out std_logic_vector(FIFOSIZE - 1 downto 0);
        result_div_o  : out std_logic_vector(1 downto 0)
     );
  end component;

    component get_in_MSB_order is
        generic (
                FIFOSIZE: integer:= 8;
                DATA_SIZE_LENGTH: integer := 32
        );
        port ( clk_i         : in  std_logic;
               rst_i         : in  std_logic;
               enable_i      : in  std_logic;
               nLoad_get_i   : in  std_logic;
               data_size_i   : in  std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
               D_load_i      : in  std_logic_vector (FIFOSIZE-1 downto 0);
               D_o           : out std_logic;
               signe_bit_o   : out std_logic
        );
    end component;

    component uc_mult is
       port (
          clk_i          : in std_logic;
          rst_i          : in std_logic;
          end_of_data_i  : in std_logic;
          acc_nxt_i      : in std_logic;
          data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
          en_get_msb_o   : out std_logic;
          val_nxt_o      : out std_logic;
          ready_o        : out std_logic;
          nLoad_get_o    : out std_logic;
          end_of_data_o  : out std_logic
       );
    end component;

    signal result_mult_s : std_logic_vector(FIFOSIZE - 1 downto 0);
    signal result_div_s  : std_logic_vector(1 downto 0);
    signal sign_bit_s : std_logic;
    signal en_get_msb : std_logic;
    signal out_get_msb_s : std_logic;
    signal end_of_data_o_s : std_logic;
    signal nLoad_get_s : std_logic;

begin
  mult : mult_div
  port map (
     clk_i    => clk_i,
     rst_i    => rst_i,
     en_i     => en_i,
     clear_i  => clear_i,
     data_a_i => data_a_i,
     data_b_i => data_b_i,
     cfg_op_i => cfg_op_i,
     result_mult_o => result_mult_s,
     result_div_o => result_div_s
  );

  get_in_msb : get_in_MSB_order
  generic map(
          FIFOSIZE => FIFOSIZE,
          DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
  )
  port map( clk_i         => clk_i,
         rst_i         => rst_i,
         enable_i      => en_get_msb,
         nLoad_get_i   => nLoad_get_s,
         data_size_i   => data_size_i,
         D_load_i      => result_mult_s,
         D_o           => out_get_msb_s,
         signe_bit_o   => sign_bit_s
  );

  uc: uc_mult
     port map(
        clk_i          => clk_i,
        rst_i          => rst_i,
        end_of_data_i  => end_of_data_i,
        acc_nxt_i      => acc_nxt_i,
        data_size_i    => data_size_i,
        en_get_msb_o   => en_get_msb,
        val_nxt_o      => val_nxt_mult_o,
        ready_o        => ready_o,
        nLoad_get_o    => nLoad_get_s,
        end_of_data_o  => end_of_data_o_s
     );

  result_o <=  result_div_s when cfg_op_i = FU_OPCODE_DIV else
               "11" when end_of_data_o_s = '1' else
               ('0' & out_get_msb_s) when sign_bit_s = '0' else  -- positif number
               out_get_msb_s & '0';                           -- negativ number

end struct;
