------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : decompteur.vhd
-- Author               : Raphael Henocq
-- Date                 : 05.04.17
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Decompteur
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    05.05.17    RHE
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decompteur is
    generic (
            DATASIZE: integer:= 8
    );
    port ( clk_i      : in  std_logic;
           rst_i    : in  std_logic;
           enable_i   : in  std_logic;
           load_i     : in  std_logic;
           D_load_i   : in  std_logic_vector (DATASIZE-1 downto 0);
           Q          : out std_logic_vector (DATASIZE-1 downto 0)
    );
end decompteur;

architecture compteur_arch of decompteur is

signal cpt_s : std_logic_vector(DATASIZE-1 downto 0);

begin

process (clk_i,rst_i)
begin
	if rst_i='1' then
		cpt_s <= (others =>'0');
	elsif rising_edge(clk_i) then
            if enable_i='1' then
                if load_i='1' then
                    cpt_s <= D_load_i;
                elsif cpt_s /= 0 then
                    cpt_s <= cpt_s - 1;
                end if;
            end if;
	end if;
end process;

Q <= cpt_s;

end compteur_arch;
