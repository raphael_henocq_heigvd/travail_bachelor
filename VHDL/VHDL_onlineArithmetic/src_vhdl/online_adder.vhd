-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : online_adder.vhd
-- Description  : Effectue une addition de type msb first. Utilise des données en signed digit 
--                représentation
--
-- Author       : Raphael Henocq
-- Date         : 17.04.17
-- Version      : 1.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    17.04.17           Creation
-- 1.0       RHE    19.04.17           Changement d'algorithme pour l'addition
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity online_adder is
   port (
      clk_i    : in std_logic;
      rst_i    : in std_logic;
      en_i     : in std_logic;
      clear_i  : in std_logic;
      data_a_i : in std_logic_vector(1 downto 0);   -- |a+,a-|
      data_b_i : in std_logic_vector(1 downto 0);   -- |b+,b-|
      
      result_o : out std_logic_vector(1 downto 0)
   );
   
end online_adder;

architecture data_flow of online_adder is 

component fa_adder is
    port ( 
        a_in  : in std_logic;
        b_in  : in std_logic;
        c_in  : in std_logic;
        c_o   : out std_logic;
        res_o : out std_logic
    );
end component;

signal a_p_s    : std_logic; --a+
signal na_m_s   : std_logic; -- not a-
signal b_p_s    : std_logic; --b+
signal b_m_s    : std_logic; --b-
signal ng_fut_1 : std_logic;
signal ng_pres  : std_logic_vector (1 downto 0);
signal g_fut    : std_logic_vector (1 downto 0);
signal g_pres   : std_logic_vector (1 downto 0);
signal w_fut    : std_logic;
signal w_pres   : std_logic;
signal h_s      : std_logic;
signal t_s      : std_logic;
signal nt_s     : std_logic;

begin

fa_adder_1 : fa_adder
    port map( 
        a_in  => a_p_s,
        b_in  => na_m_s,
        c_in  => b_p_s,
        c_o   => h_s,
        res_o => ng_fut_1
    );
    
fa_adder_2 : fa_adder
    port map( 
        a_in  => ng_pres(1),
        b_in  => ng_pres(0),
        c_in  => h_s,
        c_o   => nt_s,
        res_o => w_fut
    );
    
process (clk_i, rst_i)
begin
if rst_i = '1' then
    w_pres <= '0';
    g_pres <= "00";
elsif rising_edge(clk_i) then
    if clear_i = '1' then
        w_pres <= '0';
        g_pres <= "00";
    elsif en_i = '1' then
        w_pres <= w_fut;
        g_pres <= g_fut;
    end if;
end if;
end process;

g_fut(1) <= not ng_fut_1;
g_fut(0) <= b_m_s;
ng_pres <= not g_pres;
    
a_p_s <= data_a_i(0);
na_m_s <= not data_a_i(1);

t_s <= not nt_s;

b_p_s <= data_b_i(0);
b_m_s <= data_b_i(1);

result_o <= t_s & w_pres;

end data_flow;