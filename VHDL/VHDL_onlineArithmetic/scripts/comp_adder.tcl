###########################################################################
# HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
# Institut REDS, Reconfigurable & Embedded Digital Systems
#
# Fichier      : comp_sadder.tcl
# Description  : Script de compilation des fichiers
#
# Auteur       : Henocq Raphael
# Date         : 17.04.17
# Version      : 1.0

#
#--| Modifications |--------------------------------------------------------
# Ver  Aut.  Date        Description
# 1.1  RHE   17.04.17  Création
#
############################################################################

#create library work
vlib work
#map library work to work
vmap work work

# serializer_files compilation
vcom -reportprogress 300 -work work   ../src_vhdl/uc_fu_all.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/online_adder_cell.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/online_adder.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/fa_adder.vhd
