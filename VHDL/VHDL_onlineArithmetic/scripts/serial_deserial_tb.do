set NumericStdNoWarnings 1

file delete -force "work"
file mkdir "work"

vlib work
vmap work work


cd ../tlmvm/comp
do ../scripts/compile.do
cd ../../scripts
vmap tlmvm ../tlmvm/comp/tlmvm
vcom -reportprogress 300 -work work   ../src_vhdl/online_pkg.vhd
do comp_deserializer.tcl 
do comp_serializer.tcl 

vcom -reportprogress 300 -work work   ../src_vhdl/serial_deserial.vhd
vcom -2008 -reportprogress 300 -work work   ../src_tb/serial_deserial_tb.vhd





#Chargement fichier pour la simulation
vsim -novopt work.serial_deserial_tb 

add wave -r *

run -all
