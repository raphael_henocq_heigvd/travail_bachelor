###########################################################################
# HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
# Institut REDS, Reconfigurable & Embedded Digital Systems
#
# Fichier      : comp_mult.tcl
# Description  : Script de compilation des fichiers
#
# Auteur       : Henocq Raphael
# Date         : 05.04.17
# Version      : 1.0

#
#--| Modifications |--------------------------------------------------------
# Ver  Aut.  Date        Description
# 1.1  RHE   05.04.17  Création
#
############################################################################

#create library work
vlib work
#map library work to work
vmap work work

# serializer_files compilation
vcom -reportprogress 300 -work work   ../src_vhdl/uc_fu_all.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/uc_mult.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/mult_div.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/online_mult_div.vhd
vcom -reportprogress 300 -work work   ../src_vhdl/mult_cell.vhd
