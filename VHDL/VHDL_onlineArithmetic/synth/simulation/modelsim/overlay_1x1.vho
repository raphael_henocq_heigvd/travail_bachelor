-- Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus Prime License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus Prime"
-- VERSION "Version 16.0.1 Build 218 06/01/2016 SJ Standard Edition"

-- DATE "04/24/2017 11:53:25"

-- 
-- Device: Altera 5CSXFC6D6F31C6 Package FBGA896
-- 

-- 
-- This VHDL file should be used for QuestaSim (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY ALTERA_LNSIM;
LIBRARY CYCLONEV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE ALTERA_LNSIM.ALTERA_LNSIM_COMPONENTS.ALL;
USE CYCLONEV.CYCLONEV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	overlay_1x1 IS
    PORT (
	clk_i : IN std_logic;
	rst_i : IN std_logic;
	data_prv_a_i : IN std_logic_vector(31 DOWNTO 0);
	val_prv_a_i : IN std_logic;
	data_prv_b_i : IN std_logic_vector(31 DOWNTO 0);
	val_prv_b_i : IN std_logic;
	acc_nxt_i : IN std_logic;
	data_size_i : IN std_logic_vector(31 DOWNTO 0);
	acc_prv_a_o : BUFFER std_logic;
	acc_prv_b_o : BUFFER std_logic;
	val_nxt_o : BUFFER std_logic;
	data_nxt_o : BUFFER std_logic_vector(31 DOWNTO 0)
	);
END overlay_1x1;

-- Design Ports Information
-- acc_prv_a_o	=>  Location: PIN_AK22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- acc_prv_b_o	=>  Location: PIN_AE18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- val_nxt_o	=>  Location: PIN_B8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[0]	=>  Location: PIN_AF13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[1]	=>  Location: PIN_AB12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[2]	=>  Location: PIN_AA13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[3]	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[4]	=>  Location: PIN_AB13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[5]	=>  Location: PIN_AG7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[6]	=>  Location: PIN_AC12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[7]	=>  Location: PIN_AE23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[8]	=>  Location: PIN_AJ24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[9]	=>  Location: PIN_AG11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[10]	=>  Location: PIN_AG5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[11]	=>  Location: PIN_AJ6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[12]	=>  Location: PIN_AK23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[13]	=>  Location: PIN_AG2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[14]	=>  Location: PIN_AD14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[15]	=>  Location: PIN_E13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[16]	=>  Location: PIN_AK6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[17]	=>  Location: PIN_AE14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[18]	=>  Location: PIN_AJ7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[19]	=>  Location: PIN_AK24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[20]	=>  Location: PIN_AD12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[21]	=>  Location: PIN_AK3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[22]	=>  Location: PIN_AJ5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[23]	=>  Location: PIN_AH9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[24]	=>  Location: PIN_AG10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[25]	=>  Location: PIN_AE13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[26]	=>  Location: PIN_AF11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[27]	=>  Location: PIN_AG8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[28]	=>  Location: PIN_AK2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[29]	=>  Location: PIN_AJ4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[30]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_nxt_o[31]	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- val_prv_a_i	=>  Location: PIN_AH18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk_i	=>  Location: PIN_Y26,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rst_i	=>  Location: PIN_AB27,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- val_prv_b_i	=>  Location: PIN_F14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- acc_nxt_i	=>  Location: PIN_AK9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[16]	=>  Location: PIN_AF6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[20]	=>  Location: PIN_AB15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[24]	=>  Location: PIN_AK4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[28]	=>  Location: PIN_AJ9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[17]	=>  Location: PIN_AH3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[21]	=>  Location: PIN_AF21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[25]	=>  Location: PIN_AG6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[29]	=>  Location: PIN_AJ1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[18]	=>  Location: PIN_E9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[22]	=>  Location: PIN_AJ20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[26]	=>  Location: PIN_AK8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[30]	=>  Location: PIN_W19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[19]	=>  Location: PIN_AK11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[23]	=>  Location: PIN_AJ21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[27]	=>  Location: PIN_AG13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[31]	=>  Location: PIN_AH14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[8]	=>  Location: PIN_AH13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[9]	=>  Location: PIN_AH8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[10]	=>  Location: PIN_A3,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[11]	=>  Location: PIN_A5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[12]	=>  Location: PIN_AJ2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[13]	=>  Location: PIN_AK7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[14]	=>  Location: PIN_F13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[15]	=>  Location: PIN_H14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[4]	=>  Location: PIN_AG12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[5]	=>  Location: PIN_G13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[6]	=>  Location: PIN_AK14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[7]	=>  Location: PIN_AF14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[0]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[1]	=>  Location: PIN_AF15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[2]	=>  Location: PIN_AC14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_a_i[3]	=>  Location: PIN_AA12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[16]	=>  Location: PIN_W15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[20]	=>  Location: PIN_AK13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[24]	=>  Location: PIN_AJ10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[28]	=>  Location: PIN_AE19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[17]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[21]	=>  Location: PIN_AK19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[25]	=>  Location: PIN_AH7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[29]	=>  Location: PIN_AH15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[18]	=>  Location: PIN_AJ12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[22]	=>  Location: PIN_AJ11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[26]	=>  Location: PIN_K14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[30]	=>  Location: PIN_AG15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[19]	=>  Location: PIN_W17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[23]	=>  Location: PIN_AA14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[27]	=>  Location: PIN_AA15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[31]	=>  Location: PIN_AH10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[8]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[9]	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[10]	=>  Location: PIN_AH28,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[11]	=>  Location: PIN_AA18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[12]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[13]	=>  Location: PIN_AD17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[14]	=>  Location: PIN_AG20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[15]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[4]	=>  Location: PIN_AH12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[5]	=>  Location: PIN_AG23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[6]	=>  Location: PIN_Y16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[7]	=>  Location: PIN_AG22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[0]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[1]	=>  Location: PIN_AK12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[2]	=>  Location: PIN_AH24,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_prv_b_i[3]	=>  Location: PIN_Y18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[2]	=>  Location: PIN_AC22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[3]	=>  Location: PIN_AJ14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[0]	=>  Location: PIN_AH22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[1]	=>  Location: PIN_AJ17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[4]	=>  Location: PIN_AK18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[21]	=>  Location: PIN_AE17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[22]	=>  Location: PIN_AJ16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[23]	=>  Location: PIN_AF18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[17]	=>  Location: PIN_AE16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[18]	=>  Location: PIN_AJ22,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[19]	=>  Location: PIN_AH17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[24]	=>  Location: PIN_AG16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[20]	=>  Location: PIN_W16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[31]	=>  Location: PIN_AG17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[30]	=>  Location: PIN_AF16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[29]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[28]	=>  Location: PIN_AG21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[27]	=>  Location: PIN_AC18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[25]	=>  Location: PIN_AF20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[26]	=>  Location: PIN_AF19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[16]	=>  Location: PIN_AA16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[15]	=>  Location: PIN_AG18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[13]	=>  Location: PIN_AK16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[14]	=>  Location: PIN_AH23,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[12]	=>  Location: PIN_AH20,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[11]	=>  Location: PIN_AB17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[10]	=>  Location: PIN_AH19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[9]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[8]	=>  Location: PIN_AK21,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[7]	=>  Location: PIN_Y17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[6]	=>  Location: PIN_AJ19,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- data_size_i[5]	=>  Location: PIN_AH27,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF overlay_1x1 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk_i : std_logic;
SIGNAL ww_rst_i : std_logic;
SIGNAL ww_data_prv_a_i : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_val_prv_a_i : std_logic;
SIGNAL ww_data_prv_b_i : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_val_prv_b_i : std_logic;
SIGNAL ww_acc_nxt_i : std_logic;
SIGNAL ww_data_size_i : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_acc_prv_a_o : std_logic;
SIGNAL ww_acc_prv_b_o : std_logic;
SIGNAL ww_val_nxt_o : std_logic;
SIGNAL ww_data_nxt_o : std_logic_vector(31 DOWNTO 0);
SIGNAL \~QUARTUS_CREATED_GND~I_combout\ : std_logic;
SIGNAL \clk_i~input_o\ : std_logic;
SIGNAL \clk_i~inputCLKENA0_outclk\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\ : std_logic;
SIGNAL \data_size_i[0]~input_o\ : std_logic;
SIGNAL \rst_i~input_o\ : std_logic;
SIGNAL \rst_i~inputCLKENA0_outclk\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector4~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\ : std_logic;
SIGNAL \val_prv_b_i~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~1_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\ : std_logic;
SIGNAL \data_size_i[1]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\ : std_logic;
SIGNAL \data_size_i[2]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\ : std_logic;
SIGNAL \data_size_i[3]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\ : std_logic;
SIGNAL \data_size_i[4]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\ : std_logic;
SIGNAL \data_size_i[5]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\ : std_logic;
SIGNAL \data_size_i[6]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\ : std_logic;
SIGNAL \data_size_i[7]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\ : std_logic;
SIGNAL \data_size_i[8]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\ : std_logic;
SIGNAL \data_size_i[9]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~5_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\ : std_logic;
SIGNAL \data_size_i[10]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\ : std_logic;
SIGNAL \data_size_i[11]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\ : std_logic;
SIGNAL \data_size_i[12]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\ : std_logic;
SIGNAL \data_size_i[13]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\ : std_logic;
SIGNAL \data_size_i[14]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\ : std_logic;
SIGNAL \data_size_i[15]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\ : std_logic;
SIGNAL \data_size_i[16]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\ : std_logic;
SIGNAL \data_size_i[17]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\ : std_logic;
SIGNAL \data_size_i[18]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\ : std_logic;
SIGNAL \data_size_i[19]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\ : std_logic;
SIGNAL \data_size_i[20]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\ : std_logic;
SIGNAL \data_size_i[21]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\ : std_logic;
SIGNAL \data_size_i[22]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\ : std_logic;
SIGNAL \data_size_i[23]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\ : std_logic;
SIGNAL \data_size_i[24]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\ : std_logic;
SIGNAL \data_size_i[25]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\ : std_logic;
SIGNAL \data_size_i[26]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\ : std_logic;
SIGNAL \data_size_i[27]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\ : std_logic;
SIGNAL \data_size_i[28]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\ : std_logic;
SIGNAL \data_size_i[29]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\ : std_logic;
SIGNAL \data_size_i[30]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~34\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\ : std_logic;
SIGNAL \data_size_i[31]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~3_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~1_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~6_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector2~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector5~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ : std_logic;
SIGNAL \val_prv_a_i~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector1~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~78\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~1_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~5_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~3_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector2~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector5~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector3~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector5~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector7~6_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sEnd1~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector10~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector0~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector0~1_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sInit~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector2~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sStep0~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector12~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sWait_val_step0~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector2~1_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sStep1~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector8~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector7~7_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector7~5_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector4~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector7~2_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~DUPLICATE_q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector7~3_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector7~4_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector4~2_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sEnd0~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector9~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|WideOr8~combout\ : std_logic;
SIGNAL \acc_nxt_i~input_o\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector0~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector3~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector4~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sWait~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector1~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sReady~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~1_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector4~1_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|Selector3~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|state_s.sStepN~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector4~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~10\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~14\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~2\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~6\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ : std_logic;
SIGNAL \data_prv_b_i[7]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ : std_logic;
SIGNAL \data_prv_b_i[5]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[6]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ : std_logic;
SIGNAL \data_prv_b_i[4]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~7_combout\ : std_logic;
SIGNAL \data_prv_b_i[11]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[9]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[10]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[8]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~5_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ : std_logic;
SIGNAL \data_prv_b_i[14]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[13]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[12]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[15]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~6_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ : std_logic;
SIGNAL \data_prv_b_i[0]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[1]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[2]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[3]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\ : std_logic;
SIGNAL \data_prv_b_i[16]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[24]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[20]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[28]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~0_combout\ : std_logic;
SIGNAL \data_prv_b_i[25]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[29]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[17]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[21]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~1_combout\ : std_logic;
SIGNAL \data_prv_b_i[23]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[31]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[27]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[19]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~3_combout\ : std_logic;
SIGNAL \data_prv_b_i[30]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[22]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[18]~input_o\ : std_logic;
SIGNAL \data_prv_b_i[26]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~74\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~70\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~78\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~66\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~10\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~2\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~14\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~6\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~90\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~86\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~94\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~82\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~26\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~17_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~18\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~30\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~21_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[15]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~29_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~18\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~14\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~93_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~81_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~85_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~89_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~25_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~10\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~2\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~1_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~13_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~5_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~65_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~77_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~69_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~73_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~9_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~22\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~106\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~102\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~110\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~98\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~42\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~34\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~46\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~38\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~122\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~118\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~126\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~114\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~58\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~49_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~50\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~62\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~53_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~61_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~117_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~125_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~113_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~121_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~57_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~33_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~37_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~45_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~109_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~97_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~101_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~105_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~41_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ : std_logic;
SIGNAL \online_adder_cell_s|data_b_s[1]~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|w_pres~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~18\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~14\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~10\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~2\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\ : std_logic;
SIGNAL \data_prv_a_i[20]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[24]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[16]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[28]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\ : std_logic;
SIGNAL \data_prv_a_i[23]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[31]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[19]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[27]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\ : std_logic;
SIGNAL \data_prv_a_i[25]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[21]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[29]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[17]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\ : std_logic;
SIGNAL \data_prv_a_i[22]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[30]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[18]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[26]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\ : std_logic;
SIGNAL \data_prv_a_i[13]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[15]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[14]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[12]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~6_combout\ : std_logic;
SIGNAL \data_prv_a_i[1]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[0]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[3]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[2]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~8_combout\ : std_logic;
SIGNAL \data_prv_a_i[11]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[8]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[9]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[10]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~5_combout\ : std_logic;
SIGNAL \data_prv_a_i[4]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[7]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[5]~input_o\ : std_logic;
SIGNAL \data_prv_a_i[6]~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~74\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~70\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~78\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~66\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~10\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~2\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~14\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~6\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~90\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~86\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~94\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~82\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~26\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~18\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~30\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~22\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~106\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~102\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~110\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~98\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~42\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~34\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~46\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~38\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~122\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~118\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~126\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~114\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~58\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~50\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~62\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~53_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~49_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~61_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~113_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~125_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~117_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~121_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~57_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~1_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~13_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~5_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~65_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[3]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~77_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~69_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~73_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~9_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~21_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~29_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~17_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~81_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~93_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~85_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~89_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~25_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~37_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~45_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~33_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~97_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~109_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~101_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~105_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~41_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|fa_adder_1|Mux1~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_wirecell_combout\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|w_pres~q\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_fut[0]~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[1]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_fut[0]~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[2]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[2]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[5]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[7]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[8]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[7]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[8]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[9]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[11]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[10]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[12]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[14]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[15]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[14]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[16]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[15]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[18]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[17]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[20]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[19]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[20]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[22]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[21]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[23]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[22]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[24]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[23]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[25]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[26]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[27]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[27]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[29]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[29]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[31]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[30]~feeder_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \online_adder_cell_s|online_adder_s|g_pres\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~DUPLICATE_q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~DUPLICATE_q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~DUPLICATE_q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres[26]~DUPLICATE_q\ : std_logic;
SIGNAL \ALT_INV_rst_i~inputCLKENA0_outclk\ : std_logic;
SIGNAL \ALT_INV_data_size_i[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_size_i[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_size_i[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_size_i[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_size_i[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[15]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[14]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[13]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[12]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[11]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[10]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[9]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[8]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[31]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[27]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[23]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[19]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[30]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[26]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[22]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[18]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[29]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[25]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[21]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[17]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[28]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[24]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[20]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_b_i[16]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[3]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[2]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[1]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[7]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[6]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[5]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[4]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[15]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[14]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[13]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[12]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[11]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[10]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[9]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[8]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[31]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[27]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[23]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[19]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[30]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[26]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[22]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[18]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[29]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[25]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[21]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[17]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[28]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[24]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[20]~input_o\ : std_logic;
SIGNAL \ALT_INV_data_prv_a_i[16]~input_o\ : std_logic;
SIGNAL \ALT_INV_acc_nxt_i~input_o\ : std_logic;
SIGNAL \ALT_INV_val_prv_b_i~input_o\ : std_logic;
SIGNAL \ALT_INV_rst_i~input_o\ : std_logic;
SIGNAL \ALT_INV_val_prv_a_i~input_o\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~5_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~3_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector5~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~5_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~3_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~1_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~8_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~7_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~6_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~5_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~3_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~1_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~8_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~7_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~6_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~5_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~3_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~2_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~1_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~6_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr8~combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_Selector0~0_combout\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sWait~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sReady~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~1_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~5_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~4_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~3_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~2_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end0~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_step0~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_end_of_data_save_s~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_end_of_data_save_s~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\ : std_logic_vector(29 DOWNTO 0);
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_Selector2~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|fa_adder_1|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStepN~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep1~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd0~q\ : std_logic;
SIGNAL \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep0~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\ : std_logic;
SIGNAL \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\ : std_logic_vector(30 DOWNTO 0);
SIGNAL \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sEnd~q\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|fa_adder_2|ALT_INV_Mux1~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add1~65_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Add1~21_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\ : std_logic_vector(4 DOWNTO 0);
SIGNAL \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\ : std_logic_vector(31 DOWNTO 0);
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~29_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~25_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~21_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~17_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~29_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~25_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~21_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~17_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~12_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~8_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~12_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~8_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~4_combout\ : std_logic;
SIGNAL \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~0_combout\ : std_logic;
SIGNAL \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \online_adder_cell_s|online_adder_s|ALT_INV_w_pres~q\ : std_logic;

BEGIN

ww_clk_i <= clk_i;
ww_rst_i <= rst_i;
ww_data_prv_a_i <= data_prv_a_i;
ww_val_prv_a_i <= val_prv_a_i;
ww_data_prv_b_i <= data_prv_b_i;
ww_val_prv_b_i <= val_prv_b_i;
ww_acc_nxt_i <= acc_nxt_i;
ww_data_size_i <= data_size_i;
acc_prv_a_o <= ww_acc_prv_a_o;
acc_prv_b_o <= ww_acc_prv_b_o;
val_nxt_o <= ww_val_nxt_o;
data_nxt_o <= ww_data_nxt_o;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~DUPLICATE_q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~DUPLICATE_q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~DUPLICATE_q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~DUPLICATE_q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~DUPLICATE_q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~DUPLICATE_q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~DUPLICATE_q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~DUPLICATE_q\;
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres[26]~DUPLICATE_q\ <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~DUPLICATE_q\;
\ALT_INV_rst_i~inputCLKENA0_outclk\ <= NOT \rst_i~inputCLKENA0_outclk\;
\ALT_INV_data_size_i[4]~input_o\ <= NOT \data_size_i[4]~input_o\;
\ALT_INV_data_size_i[1]~input_o\ <= NOT \data_size_i[1]~input_o\;
\ALT_INV_data_size_i[0]~input_o\ <= NOT \data_size_i[0]~input_o\;
\ALT_INV_data_size_i[3]~input_o\ <= NOT \data_size_i[3]~input_o\;
\ALT_INV_data_size_i[2]~input_o\ <= NOT \data_size_i[2]~input_o\;
\ALT_INV_data_prv_b_i[3]~input_o\ <= NOT \data_prv_b_i[3]~input_o\;
\ALT_INV_data_prv_b_i[2]~input_o\ <= NOT \data_prv_b_i[2]~input_o\;
\ALT_INV_data_prv_b_i[1]~input_o\ <= NOT \data_prv_b_i[1]~input_o\;
\ALT_INV_data_prv_b_i[0]~input_o\ <= NOT \data_prv_b_i[0]~input_o\;
\ALT_INV_data_prv_b_i[7]~input_o\ <= NOT \data_prv_b_i[7]~input_o\;
\ALT_INV_data_prv_b_i[6]~input_o\ <= NOT \data_prv_b_i[6]~input_o\;
\ALT_INV_data_prv_b_i[5]~input_o\ <= NOT \data_prv_b_i[5]~input_o\;
\ALT_INV_data_prv_b_i[4]~input_o\ <= NOT \data_prv_b_i[4]~input_o\;
\ALT_INV_data_prv_b_i[15]~input_o\ <= NOT \data_prv_b_i[15]~input_o\;
\ALT_INV_data_prv_b_i[14]~input_o\ <= NOT \data_prv_b_i[14]~input_o\;
\ALT_INV_data_prv_b_i[13]~input_o\ <= NOT \data_prv_b_i[13]~input_o\;
\ALT_INV_data_prv_b_i[12]~input_o\ <= NOT \data_prv_b_i[12]~input_o\;
\ALT_INV_data_prv_b_i[11]~input_o\ <= NOT \data_prv_b_i[11]~input_o\;
\ALT_INV_data_prv_b_i[10]~input_o\ <= NOT \data_prv_b_i[10]~input_o\;
\ALT_INV_data_prv_b_i[9]~input_o\ <= NOT \data_prv_b_i[9]~input_o\;
\ALT_INV_data_prv_b_i[8]~input_o\ <= NOT \data_prv_b_i[8]~input_o\;
\ALT_INV_data_prv_b_i[31]~input_o\ <= NOT \data_prv_b_i[31]~input_o\;
\ALT_INV_data_prv_b_i[27]~input_o\ <= NOT \data_prv_b_i[27]~input_o\;
\ALT_INV_data_prv_b_i[23]~input_o\ <= NOT \data_prv_b_i[23]~input_o\;
\ALT_INV_data_prv_b_i[19]~input_o\ <= NOT \data_prv_b_i[19]~input_o\;
\ALT_INV_data_prv_b_i[30]~input_o\ <= NOT \data_prv_b_i[30]~input_o\;
\ALT_INV_data_prv_b_i[26]~input_o\ <= NOT \data_prv_b_i[26]~input_o\;
\ALT_INV_data_prv_b_i[22]~input_o\ <= NOT \data_prv_b_i[22]~input_o\;
\ALT_INV_data_prv_b_i[18]~input_o\ <= NOT \data_prv_b_i[18]~input_o\;
\ALT_INV_data_prv_b_i[29]~input_o\ <= NOT \data_prv_b_i[29]~input_o\;
\ALT_INV_data_prv_b_i[25]~input_o\ <= NOT \data_prv_b_i[25]~input_o\;
\ALT_INV_data_prv_b_i[21]~input_o\ <= NOT \data_prv_b_i[21]~input_o\;
\ALT_INV_data_prv_b_i[17]~input_o\ <= NOT \data_prv_b_i[17]~input_o\;
\ALT_INV_data_prv_b_i[28]~input_o\ <= NOT \data_prv_b_i[28]~input_o\;
\ALT_INV_data_prv_b_i[24]~input_o\ <= NOT \data_prv_b_i[24]~input_o\;
\ALT_INV_data_prv_b_i[20]~input_o\ <= NOT \data_prv_b_i[20]~input_o\;
\ALT_INV_data_prv_b_i[16]~input_o\ <= NOT \data_prv_b_i[16]~input_o\;
\ALT_INV_data_prv_a_i[3]~input_o\ <= NOT \data_prv_a_i[3]~input_o\;
\ALT_INV_data_prv_a_i[2]~input_o\ <= NOT \data_prv_a_i[2]~input_o\;
\ALT_INV_data_prv_a_i[1]~input_o\ <= NOT \data_prv_a_i[1]~input_o\;
\ALT_INV_data_prv_a_i[0]~input_o\ <= NOT \data_prv_a_i[0]~input_o\;
\ALT_INV_data_prv_a_i[7]~input_o\ <= NOT \data_prv_a_i[7]~input_o\;
\ALT_INV_data_prv_a_i[6]~input_o\ <= NOT \data_prv_a_i[6]~input_o\;
\ALT_INV_data_prv_a_i[5]~input_o\ <= NOT \data_prv_a_i[5]~input_o\;
\ALT_INV_data_prv_a_i[4]~input_o\ <= NOT \data_prv_a_i[4]~input_o\;
\ALT_INV_data_prv_a_i[15]~input_o\ <= NOT \data_prv_a_i[15]~input_o\;
\ALT_INV_data_prv_a_i[14]~input_o\ <= NOT \data_prv_a_i[14]~input_o\;
\ALT_INV_data_prv_a_i[13]~input_o\ <= NOT \data_prv_a_i[13]~input_o\;
\ALT_INV_data_prv_a_i[12]~input_o\ <= NOT \data_prv_a_i[12]~input_o\;
\ALT_INV_data_prv_a_i[11]~input_o\ <= NOT \data_prv_a_i[11]~input_o\;
\ALT_INV_data_prv_a_i[10]~input_o\ <= NOT \data_prv_a_i[10]~input_o\;
\ALT_INV_data_prv_a_i[9]~input_o\ <= NOT \data_prv_a_i[9]~input_o\;
\ALT_INV_data_prv_a_i[8]~input_o\ <= NOT \data_prv_a_i[8]~input_o\;
\ALT_INV_data_prv_a_i[31]~input_o\ <= NOT \data_prv_a_i[31]~input_o\;
\ALT_INV_data_prv_a_i[27]~input_o\ <= NOT \data_prv_a_i[27]~input_o\;
\ALT_INV_data_prv_a_i[23]~input_o\ <= NOT \data_prv_a_i[23]~input_o\;
\ALT_INV_data_prv_a_i[19]~input_o\ <= NOT \data_prv_a_i[19]~input_o\;
\ALT_INV_data_prv_a_i[30]~input_o\ <= NOT \data_prv_a_i[30]~input_o\;
\ALT_INV_data_prv_a_i[26]~input_o\ <= NOT \data_prv_a_i[26]~input_o\;
\ALT_INV_data_prv_a_i[22]~input_o\ <= NOT \data_prv_a_i[22]~input_o\;
\ALT_INV_data_prv_a_i[18]~input_o\ <= NOT \data_prv_a_i[18]~input_o\;
\ALT_INV_data_prv_a_i[29]~input_o\ <= NOT \data_prv_a_i[29]~input_o\;
\ALT_INV_data_prv_a_i[25]~input_o\ <= NOT \data_prv_a_i[25]~input_o\;
\ALT_INV_data_prv_a_i[21]~input_o\ <= NOT \data_prv_a_i[21]~input_o\;
\ALT_INV_data_prv_a_i[17]~input_o\ <= NOT \data_prv_a_i[17]~input_o\;
\ALT_INV_data_prv_a_i[28]~input_o\ <= NOT \data_prv_a_i[28]~input_o\;
\ALT_INV_data_prv_a_i[24]~input_o\ <= NOT \data_prv_a_i[24]~input_o\;
\ALT_INV_data_prv_a_i[20]~input_o\ <= NOT \data_prv_a_i[20]~input_o\;
\ALT_INV_data_prv_a_i[16]~input_o\ <= NOT \data_prv_a_i[16]~input_o\;
\ALT_INV_acc_nxt_i~input_o\ <= NOT \acc_nxt_i~input_o\;
\ALT_INV_val_prv_b_i~input_o\ <= NOT \val_prv_b_i~input_o\;
\ALT_INV_rst_i~input_o\ <= NOT \rst_i~input_o\;
\ALT_INV_val_prv_a_i~input_o\ <= NOT \val_prv_a_i~input_o\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(26) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(26);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(24) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(24);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(27) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(27);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(25) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(25);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(18) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(18);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(16) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(16);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(19) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(19);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(17) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(17);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(10) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(10);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(8) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(8);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(11) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(11);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(9) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(9);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(2) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(2);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(0) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(0);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(3) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(3);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(1) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(1);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(26) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(26);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(24) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(24);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(27) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(27);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(25) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(25);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(18) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(18);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(16) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(16);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(19) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(19);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(17) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(17);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(10) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(10);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(8) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(8);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(11) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(11);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(9) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(9);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(2) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(2);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(0) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(0);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(3) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(3);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(1) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(1);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~6_combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~5_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~5_combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~4_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~4_combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~3_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~3_combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~2_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~2_combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~1_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~1_combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~0_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~0_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector5~0_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector5~0_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector0~0_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector0~0_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~5_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~5_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~4_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~4_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~3_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~3_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~2_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~2_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~1_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~1_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~0_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~0_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(30) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(30);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(28) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(28);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(31) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(31);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(29) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(29);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(22) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(22);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(20) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(20);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(23) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(23);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(21) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(21);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(14) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(14);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(12) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(12);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(15) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(15);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(13) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(13);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(6) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(6);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(4) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(4);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(7) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(7);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(5) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(5);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~8_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~7_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~7_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~6_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~6_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~5_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~5_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~3_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~3_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~2_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~2_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~1_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~1_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~0_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~0_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(30) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(30);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(28) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(28);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(31) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(31);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(29) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(29);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(22) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(22);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(20) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(20);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(23) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(23);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(21) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(21);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(14) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(14);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(12) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(12);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(15) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(15);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(13) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(13);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(6) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(6);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(4) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(4);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(7) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(7);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(5) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(5);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~8_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~8_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~7_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~6_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~6_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~5_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~5_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~3_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~2_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~1_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~0_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~6_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector7~6_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_WideOr8~combout\ <= NOT \online_adder_cell_s|uc_adder_s|WideOr8~combout\;
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_Selector0~0_combout\ <= NOT \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector0~0_combout\;
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sWait~q\ <= NOT \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sWait~q\;
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sReady~q\ <= NOT \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sReady~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~1_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector4~1_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~5_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector7~5_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~4_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector7~4_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~3_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector7~3_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~2_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector7~2_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end0~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_step0~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sWait_val_step0~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~0_combout\ <= NOT \online_adder_cell_s|uc_adder_s|Selector4~0_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_end_of_data_save_s~q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_end_of_data_save_s~q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sInit~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\;
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(0) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(0);
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_Selector2~0_combout\ <= NOT \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\;
\online_adder_cell_s|online_adder_s|fa_adder_1|ALT_INV_Mux0~0_combout\ <= NOT \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sEnd1~q\;
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\ <= NOT \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\;
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\ <= NOT \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\ <= NOT \online_adder_cell_s|uc_adder_s|WideOr6~0_combout\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStepN~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sStepN~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep1~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sStep1~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd0~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sEnd0~q\;
\online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep0~q\ <= NOT \online_adder_cell_s|uc_adder_s|state_s.sStep0~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\;
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(0) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(0);
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sEnd~q\ <= NOT \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\;
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\ <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\;
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\ <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\;
\online_adder_cell_s|online_adder_s|fa_adder_2|ALT_INV_Mux1~0_combout\ <= NOT \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add1~65_sumout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~65_sumout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Add1~21_sumout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~21_sumout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0);
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(4) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(5) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(6) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(7) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(8) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(9) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(10) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(11) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(19) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(12) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(13) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(14) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(31) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(0) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(16) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(1) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(2) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(3) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(25) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(26) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(27) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(28) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(29) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(30) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(17) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(18) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(20) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(21) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(22) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(23) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(24) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24);
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(15) <= NOT \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(3) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(4) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(5) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(6) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(7) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(8) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(9) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(10) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(11) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(12) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(14) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(13) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(0) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(15) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(16) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(26) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(1) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(2) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(25) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(27) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(28) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(29) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(30) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(31) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(20) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(24) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(19) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(18) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(17) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(23) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(22) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22);
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(21) <= NOT \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~29_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~25_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~21_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~17_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1);
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~29_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~25_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~21_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~17_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1);
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\;
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(29) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(29);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(28) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(28);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(27) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(27);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(25) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(25);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(24) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(24);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(23) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(23);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(22) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(22);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(21) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(21);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(20) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(20);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(19) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(19);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(18) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(18);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(17) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(17);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(16) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(16);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(15) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(15);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(14) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(14);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(13) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(13);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(12) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(12);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(11) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(11);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(10) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(10);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(9) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(9);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(8) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(8);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(7) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(7);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(6) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(6);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(5) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(5);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(4) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(4);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(3) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(3);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(2) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(2);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(1) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(1);
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(4) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4);
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(3) <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3);
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~12_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~8_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~4_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\;
\signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~0_combout\ <= NOT \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(4) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4);
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(3) <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3);
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~12_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~8_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~4_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\;
\signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~0_combout\ <= NOT \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\;
\online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(0) <= NOT \online_adder_cell_s|online_adder_s|g_pres\(0);
\online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(1) <= NOT \online_adder_cell_s|online_adder_s|g_pres\(1);
\online_adder_cell_s|online_adder_s|ALT_INV_w_pres~q\ <= NOT \online_adder_cell_s|online_adder_s|w_pres~q\;
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(30) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(30);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(29) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(29);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(28) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(28);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(27) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(27);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(26) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(26);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(25) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(25);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(24) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(24);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(23) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(23);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(22) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(22);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(21) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(21);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(20) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(20);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(19) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(19);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(18) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(18);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(17) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(17);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(16) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(16);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(15) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(15);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(14) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(14);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(13) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(13);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(12) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(12);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(11) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(11);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(10) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(10);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(9) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(9);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(8) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(8);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(7) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(7);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(6) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(6);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(5) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(5);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(4) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(4);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(3) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(3);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(2) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(2);
\signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(1) <= NOT \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(1);

-- Location: IOOBUF_X68_Y0_N53
\acc_prv_a_o~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	devoe => ww_devoe,
	o => ww_acc_prv_a_o);

-- Location: IOOBUF_X66_Y0_N42
\acc_prv_b_o~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	devoe => ww_devoe,
	o => ww_acc_prv_b_o);

-- Location: IOOBUF_X30_Y81_N53
\val_nxt_o~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\,
	devoe => ww_devoe,
	o => ww_val_nxt_o);

-- Location: IOOBUF_X22_Y0_N19
\data_nxt_o[0]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(0),
	devoe => ww_devoe,
	o => ww_data_nxt_o(0));

-- Location: IOOBUF_X12_Y0_N19
\data_nxt_o[1]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(1),
	devoe => ww_devoe,
	o => ww_data_nxt_o(1));

-- Location: IOOBUF_X20_Y0_N2
\data_nxt_o[2]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(2),
	devoe => ww_devoe,
	o => ww_data_nxt_o(2));

-- Location: IOOBUF_X26_Y81_N76
\data_nxt_o[3]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(3),
	devoe => ww_devoe,
	o => ww_data_nxt_o(3));

-- Location: IOOBUF_X20_Y0_N19
\data_nxt_o[4]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(4));

-- Location: IOOBUF_X10_Y0_N93
\data_nxt_o[5]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(5),
	devoe => ww_devoe,
	o => ww_data_nxt_o(5));

-- Location: IOOBUF_X16_Y0_N2
\data_nxt_o[6]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(6));

-- Location: IOOBUF_X78_Y0_N19
\data_nxt_o[7]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(7),
	devoe => ww_devoe,
	o => ww_data_nxt_o(7));

-- Location: IOOBUF_X74_Y0_N76
\data_nxt_o[8]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(8),
	devoe => ww_devoe,
	o => ww_data_nxt_o(8));

-- Location: IOOBUF_X18_Y0_N59
\data_nxt_o[9]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(9));

-- Location: IOOBUF_X14_Y0_N36
\data_nxt_o[10]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(10));

-- Location: IOOBUF_X26_Y0_N76
\data_nxt_o[11]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(11),
	devoe => ww_devoe,
	o => ww_data_nxt_o(11));

-- Location: IOOBUF_X72_Y0_N36
\data_nxt_o[12]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(12));

-- Location: IOOBUF_X16_Y0_N36
\data_nxt_o[13]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(13));

-- Location: IOOBUF_X24_Y0_N2
\data_nxt_o[14]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(14),
	devoe => ww_devoe,
	o => ww_data_nxt_o(14));

-- Location: IOOBUF_X26_Y81_N59
\data_nxt_o[15]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(15),
	devoe => ww_devoe,
	o => ww_data_nxt_o(15));

-- Location: IOOBUF_X24_Y0_N53
\data_nxt_o[16]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(16),
	devoe => ww_devoe,
	o => ww_data_nxt_o(16));

-- Location: IOOBUF_X24_Y0_N19
\data_nxt_o[17]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(17));

-- Location: IOOBUF_X26_Y0_N93
\data_nxt_o[18]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(18),
	devoe => ww_devoe,
	o => ww_data_nxt_o(18));

-- Location: IOOBUF_X72_Y0_N53
\data_nxt_o[19]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(19));

-- Location: IOOBUF_X16_Y0_N19
\data_nxt_o[20]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(20),
	devoe => ww_devoe,
	o => ww_data_nxt_o(20));

-- Location: IOOBUF_X20_Y0_N53
\data_nxt_o[21]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(21));

-- Location: IOOBUF_X24_Y0_N36
\data_nxt_o[22]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(22),
	devoe => ww_devoe,
	o => ww_data_nxt_o(22));

-- Location: IOOBUF_X18_Y0_N93
\data_nxt_o[23]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(23),
	devoe => ww_devoe,
	o => ww_data_nxt_o(23));

-- Location: IOOBUF_X18_Y0_N76
\data_nxt_o[24]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(24),
	devoe => ww_devoe,
	o => ww_data_nxt_o(24));

-- Location: IOOBUF_X22_Y0_N2
\data_nxt_o[25]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(25),
	devoe => ww_devoe,
	o => ww_data_nxt_o(25));

-- Location: IOOBUF_X18_Y0_N42
\data_nxt_o[26]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(26),
	devoe => ww_devoe,
	o => ww_data_nxt_o(26));

-- Location: IOOBUF_X8_Y0_N53
\data_nxt_o[27]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(27),
	devoe => ww_devoe,
	o => ww_data_nxt_o(27));

-- Location: IOOBUF_X20_Y0_N36
\data_nxt_o[28]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(28));

-- Location: IOOBUF_X22_Y0_N36
\data_nxt_o[29]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(29),
	devoe => ww_devoe,
	o => ww_data_nxt_o(29));

-- Location: IOOBUF_X28_Y81_N36
\data_nxt_o[30]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~DUPLICATE_q\,
	devoe => ww_devoe,
	o => ww_data_nxt_o(30));

-- Location: IOOBUF_X30_Y81_N36
\data_nxt_o[31]~output\ : cyclonev_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false",
	shift_series_termination_control => "false")
-- pragma translate_on
PORT MAP (
	i => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(31),
	devoe => ww_devoe,
	o => ww_data_nxt_o(31));

-- Location: IOIBUF_X89_Y25_N4
\clk_i~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk_i,
	o => \clk_i~input_o\);

-- Location: CLKCTRL_G11
\clk_i~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \clk_i~input_o\,
	outclk => \clk_i~inputCLKENA0_outclk\);

-- Location: LABCELL_X42_Y3_N0
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(0),
	cin => GND,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\);

-- Location: IOIBUF_X66_Y0_N92
\data_size_i[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(0),
	o => \data_size_i[0]~input_o\);

-- Location: IOIBUF_X89_Y23_N21
\rst_i~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rst_i,
	o => \rst_i~input_o\);

-- Location: CLKCTRL_G9
\rst_i~inputCLKENA0\ : cyclonev_clkena
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	disable_mode => "low",
	ena_register_mode => "always enabled",
	ena_register_power_up => "high",
	test_syn => "high")
-- pragma translate_on
PORT MAP (
	inclk => \rst_i~input_o\,
	outclk => \rst_i~inputCLKENA0_outclk\);

-- Location: FF_X36_Y2_N22
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector5~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~DUPLICATE_q\);

-- Location: LABCELL_X36_Y2_N12
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~0_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~DUPLICATE_q\ & ( (!\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~6_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001010000010100000101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~DUPLICATE_q\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~0_combout\);

-- Location: FF_X36_Y2_N13
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\);

-- Location: LABCELL_X36_Y2_N15
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector4~0_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( 
-- (\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector4~0_combout\);

-- Location: FF_X36_Y2_N17
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector4~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\);

-- Location: IOIBUF_X36_Y81_N18
\val_prv_b_i~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_val_prv_b_i,
	o => \val_prv_b_i~input_o\);

-- Location: LABCELL_X36_Y2_N57
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector0~0_combout\ = ( \val_prv_b_i~input_o\ & ( ((!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\) # (\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\)) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\) ) ) # ( !\val_prv_b_i~input_o\ & ( (!\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & (((!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\ 
-- & \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\)) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\))) # (\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & 
-- (((\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010011110111010001001111011111110111111101111111011111110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \ALT_INV_val_prv_b_i~input_o\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector0~0_combout\);

-- Location: FF_X36_Y2_N59
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector0~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\);

-- Location: LABCELL_X36_Y2_N42
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~1_combout\ = ( \val_prv_b_i~input_o\ & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111000000001111111100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \ALT_INV_val_prv_b_i~input_o\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~1_combout\);

-- Location: FF_X36_Y2_N44
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector3~1_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\);

-- Location: LABCELL_X42_Y2_N57
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ ) ) # ( 
-- !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ ) ) # ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ & 
-- ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ ) ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ & ( 
-- !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010111111111111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\);

-- Location: FF_X42_Y3_N2
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\,
	asdata => \data_size_i[0]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0));

-- Location: LABCELL_X42_Y3_N3
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(1),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\);

-- Location: IOIBUF_X58_Y0_N41
\data_size_i[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(1),
	o => \data_size_i[1]~input_o\);

-- Location: FF_X42_Y3_N5
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\,
	asdata => \data_size_i[1]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1));

-- Location: LABCELL_X42_Y3_N6
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(2),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\);

-- Location: IOIBUF_X86_Y0_N1
\data_size_i[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(2),
	o => \data_size_i[2]~input_o\);

-- Location: FF_X42_Y3_N8
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\,
	asdata => \data_size_i[2]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2));

-- Location: LABCELL_X42_Y3_N9
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(3),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\);

-- Location: IOIBUF_X40_Y0_N35
\data_size_i[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(3),
	o => \data_size_i[3]~input_o\);

-- Location: FF_X42_Y3_N11
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\,
	asdata => \data_size_i[3]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3));

-- Location: LABCELL_X42_Y3_N12
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(4),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\);

-- Location: IOIBUF_X58_Y0_N58
\data_size_i[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(4),
	o => \data_size_i[4]~input_o\);

-- Location: FF_X42_Y3_N14
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\,
	asdata => \data_size_i[4]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4));

-- Location: LABCELL_X42_Y3_N15
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(5),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\);

-- Location: IOIBUF_X84_Y0_N52
\data_size_i[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(5),
	o => \data_size_i[5]~input_o\);

-- Location: FF_X42_Y3_N17
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\,
	asdata => \data_size_i[5]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5));

-- Location: LABCELL_X42_Y3_N18
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(6),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\);

-- Location: IOIBUF_X60_Y0_N35
\data_size_i[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(6),
	o => \data_size_i[6]~input_o\);

-- Location: FF_X42_Y3_N20
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\,
	asdata => \data_size_i[6]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6));

-- Location: LABCELL_X42_Y3_N21
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(7),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\);

-- Location: IOIBUF_X68_Y0_N1
\data_size_i[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(7),
	o => \data_size_i[7]~input_o\);

-- Location: FF_X42_Y3_N23
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\,
	asdata => \data_size_i[7]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7));

-- Location: LABCELL_X42_Y3_N24
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(8),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\);

-- Location: IOIBUF_X68_Y0_N35
\data_size_i[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(8),
	o => \data_size_i[8]~input_o\);

-- Location: FF_X42_Y3_N26
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\,
	asdata => \data_size_i[8]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8));

-- Location: LABCELL_X42_Y3_N27
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(9),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\);

-- Location: IOIBUF_X40_Y81_N18
\data_size_i[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(9),
	o => \data_size_i[9]~input_o\);

-- Location: FF_X42_Y3_N29
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\,
	asdata => \data_size_i[9]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9));

-- Location: MLABCELL_X39_Y3_N54
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~5_combout\ = ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9) & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7) & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4) & 
-- !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(6),
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(5),
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(4),
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(8),
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(9),
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(7),
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~5_combout\);

-- Location: LABCELL_X42_Y3_N30
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(10),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\);

-- Location: IOIBUF_X58_Y0_N92
\data_size_i[10]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(10),
	o => \data_size_i[10]~input_o\);

-- Location: FF_X42_Y3_N32
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\,
	asdata => \data_size_i[10]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10));

-- Location: LABCELL_X42_Y3_N33
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(11),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\);

-- Location: IOIBUF_X56_Y0_N18
\data_size_i[11]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(11),
	o => \data_size_i[11]~input_o\);

-- Location: FF_X42_Y3_N35
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\,
	asdata => \data_size_i[11]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11));

-- Location: LABCELL_X42_Y3_N36
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(12),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\);

-- Location: IOIBUF_X54_Y0_N18
\data_size_i[12]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(12),
	o => \data_size_i[12]~input_o\);

-- Location: FF_X42_Y3_N38
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\,
	asdata => \data_size_i[12]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12));

-- Location: LABCELL_X42_Y3_N39
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(13),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\);

-- Location: IOIBUF_X54_Y0_N52
\data_size_i[13]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(13),
	o => \data_size_i[13]~input_o\);

-- Location: FF_X42_Y3_N41
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\,
	asdata => \data_size_i[13]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13));

-- Location: LABCELL_X42_Y3_N42
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(14),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\);

-- Location: IOIBUF_X70_Y0_N35
\data_size_i[14]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(14),
	o => \data_size_i[14]~input_o\);

-- Location: FF_X42_Y3_N44
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\,
	asdata => \data_size_i[14]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14));

-- Location: LABCELL_X42_Y3_N45
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(15),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\);

-- Location: IOIBUF_X58_Y0_N75
\data_size_i[15]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(15),
	o => \data_size_i[15]~input_o\);

-- Location: FF_X42_Y3_N47
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\,
	asdata => \data_size_i[15]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15));

-- Location: LABCELL_X42_Y3_N48
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(16),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\);

-- Location: IOIBUF_X56_Y0_N1
\data_size_i[16]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(16),
	o => \data_size_i[16]~input_o\);

-- Location: FF_X42_Y3_N50
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\,
	asdata => \data_size_i[16]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16));

-- Location: LABCELL_X42_Y3_N51
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(17),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\);

-- Location: IOIBUF_X52_Y0_N35
\data_size_i[17]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(17),
	o => \data_size_i[17]~input_o\);

-- Location: FF_X42_Y3_N53
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\,
	asdata => \data_size_i[17]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17));

-- Location: LABCELL_X42_Y3_N54
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(18),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\);

-- Location: IOIBUF_X70_Y0_N52
\data_size_i[18]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(18),
	o => \data_size_i[18]~input_o\);

-- Location: FF_X42_Y3_N56
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\,
	asdata => \data_size_i[18]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18));

-- Location: LABCELL_X42_Y3_N57
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(19),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\);

-- Location: IOIBUF_X56_Y0_N35
\data_size_i[19]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(19),
	o => \data_size_i[19]~input_o\);

-- Location: FF_X42_Y3_N59
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\,
	asdata => \data_size_i[19]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19));

-- Location: LABCELL_X42_Y2_N0
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(20),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\);

-- Location: IOIBUF_X52_Y0_N18
\data_size_i[20]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(20),
	o => \data_size_i[20]~input_o\);

-- Location: FF_X42_Y2_N2
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\,
	asdata => \data_size_i[20]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20));

-- Location: LABCELL_X42_Y2_N3
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(21),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\);

-- Location: IOIBUF_X50_Y0_N41
\data_size_i[21]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(21),
	o => \data_size_i[21]~input_o\);

-- Location: FF_X42_Y2_N5
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\,
	asdata => \data_size_i[21]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21));

-- Location: LABCELL_X42_Y2_N6
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(22),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\);

-- Location: IOIBUF_X54_Y0_N35
\data_size_i[22]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(22),
	o => \data_size_i[22]~input_o\);

-- Location: FF_X42_Y2_N8
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\,
	asdata => \data_size_i[22]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22));

-- Location: LABCELL_X42_Y2_N9
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(23),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\);

-- Location: IOIBUF_X50_Y0_N58
\data_size_i[23]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(23),
	o => \data_size_i[23]~input_o\);

-- Location: FF_X42_Y2_N11
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\,
	asdata => \data_size_i[23]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23));

-- Location: LABCELL_X42_Y2_N12
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(24),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\);

-- Location: IOIBUF_X50_Y0_N75
\data_size_i[24]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(24),
	o => \data_size_i[24]~input_o\);

-- Location: FF_X42_Y2_N14
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\,
	asdata => \data_size_i[24]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24));

-- Location: LABCELL_X42_Y2_N15
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(25),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\);

-- Location: IOIBUF_X70_Y0_N1
\data_size_i[25]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(25),
	o => \data_size_i[25]~input_o\);

-- Location: FF_X42_Y2_N17
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\,
	asdata => \data_size_i[25]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25));

-- Location: LABCELL_X42_Y2_N18
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(26),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\);

-- Location: IOIBUF_X62_Y0_N1
\data_size_i[26]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(26),
	o => \data_size_i[26]~input_o\);

-- Location: FF_X42_Y2_N20
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\,
	asdata => \data_size_i[26]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26));

-- Location: LABCELL_X42_Y2_N21
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(27),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\);

-- Location: IOIBUF_X64_Y0_N1
\data_size_i[27]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(27),
	o => \data_size_i[27]~input_o\);

-- Location: FF_X42_Y2_N23
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\,
	asdata => \data_size_i[27]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27));

-- Location: LABCELL_X42_Y2_N24
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(28),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\);

-- Location: IOIBUF_X54_Y0_N1
\data_size_i[28]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(28),
	o => \data_size_i[28]~input_o\);

-- Location: FF_X42_Y2_N26
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\,
	asdata => \data_size_i[28]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28));

-- Location: LABCELL_X42_Y2_N27
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(29),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\);

-- Location: IOIBUF_X52_Y0_N1
\data_size_i[29]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(29),
	o => \data_size_i[29]~input_o\);

-- Location: FF_X42_Y2_N29
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\,
	asdata => \data_size_i[29]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29));

-- Location: LABCELL_X42_Y2_N30
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\ ))
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~34\ = CARRY(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(30),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\,
	cout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~34\);

-- Location: IOIBUF_X52_Y0_N52
\data_size_i[30]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(30),
	o => \data_size_i[30]~input_o\);

-- Location: FF_X42_Y2_N32
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\,
	asdata => \data_size_i[30]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30));

-- Location: LABCELL_X42_Y2_N33
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\ = SUM(( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31) ) + ( VCC ) + ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(31),
	cin => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~34\,
	sumout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\);

-- Location: IOIBUF_X50_Y0_N92
\data_size_i[31]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_size_i(31),
	o => \data_size_i[31]~input_o\);

-- Location: FF_X42_Y2_N35
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\,
	asdata => \data_size_i[31]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31));

-- Location: MLABCELL_X39_Y3_N30
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~3_combout\ = ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3) & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31) & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2) & 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(16),
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(2),
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(1),
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(3),
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(31),
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~3_combout\);

-- Location: LABCELL_X42_Y2_N42
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~1_combout\ = ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18) & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17) & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20) 
-- & !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(21),
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(22),
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(20),
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(23),
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(18),
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(17),
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~1_combout\);

-- Location: MLABCELL_X39_Y3_N36
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~4_combout\ = ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10) & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11) & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13) 
-- & !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(14),
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(19),
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(13),
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(12),
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(10),
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(11),
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~4_combout\);

-- Location: LABCELL_X42_Y2_N36
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~2_combout\ = ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29) & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26) & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25) & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28) 
-- & !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(27),
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(25),
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(28),
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(30),
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(29),
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(26),
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~2_combout\);

-- Location: LABCELL_X42_Y2_N48
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~0_combout\ = ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15) & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(15),
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(24),
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~0_combout\);

-- Location: MLABCELL_X39_Y3_N3
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~6_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~2_combout\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~0_combout\ & ( 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~5_combout\ & (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~3_combout\ & (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~1_combout\ & 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~4_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~5_combout\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~3_combout\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~1_combout\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~4_combout\,
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~2_combout\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~0_combout\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~6_combout\);

-- Location: LABCELL_X36_Y2_N54
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector2~0_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ & ( !\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ ) ) # ( 
-- !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ & ( (!\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Equal0~6_combout\ & 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011000000000000001100000011001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector2~0_combout\);

-- Location: FF_X36_Y2_N56
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector2~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\);

-- Location: LABCELL_X36_Y2_N21
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector5~0_combout\ = ( \online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & ( ((\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\)) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\) ) ) # ( !\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101011111111111110101111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector5~0_combout\);

-- Location: FF_X36_Y2_N23
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector5~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\);

-- Location: FF_X36_Y2_N38
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\,
	ena => \ALT_INV_rst_i~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\);

-- Location: LABCELL_X36_Y2_N36
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ ) ) # ( 
-- !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( ((!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\)) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) ) ) ) # ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( (((!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\)) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\)) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\) ) ) 
-- ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( ((!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\) 
-- # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\)) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100111111111111110111111111111111001111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_end_of_data_save_s~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\);

-- Location: IOIBUF_X56_Y0_N52
\val_prv_a_i~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_val_prv_a_i,
	o => \val_prv_a_i~input_o\);

-- Location: LABCELL_X36_Y2_N0
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector1~0_combout\ = (\val_prv_a_i~input_o\ & !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100000000001100110000000000110011000000000011001100000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_val_prv_a_i~input_o\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector1~0_combout\);

-- Location: FF_X36_Y2_N2
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector1~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\);

-- Location: LABCELL_X40_Y3_N0
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~78\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(0),
	cin => GND,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~78\);

-- Location: LABCELL_X36_Y2_N45
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\ = (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # ((\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) # 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010111111111111101011111111111110101111111111111010111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\);

-- Location: FF_X40_Y3_N2
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~77_sumout\,
	asdata => \data_size_i[0]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0));

-- Location: LABCELL_X40_Y3_N3
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~78\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(1),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~78\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\);

-- Location: FF_X40_Y3_N5
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~61_sumout\,
	asdata => \data_size_i[1]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1));

-- Location: LABCELL_X40_Y3_N6
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(2),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~62\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\);

-- Location: FF_X40_Y3_N8
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~57_sumout\,
	asdata => \data_size_i[2]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2));

-- Location: LABCELL_X40_Y3_N9
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(3),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~58\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\);

-- Location: FF_X40_Y3_N11
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~125_sumout\,
	asdata => \data_size_i[3]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3));

-- Location: LABCELL_X40_Y3_N12
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(4),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~126\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\);

-- Location: FF_X40_Y3_N14
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~121_sumout\,
	asdata => \data_size_i[4]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4));

-- Location: LABCELL_X40_Y3_N15
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(5),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~122\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\);

-- Location: FF_X40_Y3_N17
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~117_sumout\,
	asdata => \data_size_i[5]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5));

-- Location: LABCELL_X40_Y3_N18
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(6),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~118\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\);

-- Location: FF_X40_Y3_N20
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~113_sumout\,
	asdata => \data_size_i[6]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6));

-- Location: LABCELL_X40_Y3_N21
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(7),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~114\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\);

-- Location: FF_X40_Y3_N23
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~109_sumout\,
	asdata => \data_size_i[7]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7));

-- Location: LABCELL_X40_Y3_N24
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(8),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~110\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\);

-- Location: FF_X40_Y3_N26
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~105_sumout\,
	asdata => \data_size_i[8]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8));

-- Location: LABCELL_X40_Y3_N27
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(9),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~106\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\);

-- Location: FF_X40_Y3_N29
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~101_sumout\,
	asdata => \data_size_i[9]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9));

-- Location: LABCELL_X40_Y3_N30
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(10),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~102\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\);

-- Location: FF_X40_Y3_N32
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~97_sumout\,
	asdata => \data_size_i[10]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10));

-- Location: LABCELL_X40_Y3_N33
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(11),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~98\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\);

-- Location: FF_X40_Y3_N35
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~93_sumout\,
	asdata => \data_size_i[11]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11));

-- Location: LABCELL_X40_Y3_N36
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(12),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~94\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\);

-- Location: FF_X40_Y3_N38
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~89_sumout\,
	asdata => \data_size_i[12]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12));

-- Location: LABCELL_X40_Y3_N39
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(13),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~90\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\);

-- Location: FF_X40_Y3_N41
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~81_sumout\,
	asdata => \data_size_i[13]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13));

-- Location: LABCELL_X40_Y3_N42
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(14),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~82\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\);

-- Location: FF_X40_Y3_N44
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~85_sumout\,
	asdata => \data_size_i[14]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14));

-- Location: LABCELL_X40_Y3_N45
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(15),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~86\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\);

-- Location: FF_X40_Y3_N47
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~73_sumout\,
	asdata => \data_size_i[15]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15));

-- Location: LABCELL_X40_Y3_N48
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(16),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~74\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\);

-- Location: FF_X40_Y3_N50
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~69_sumout\,
	asdata => \data_size_i[16]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16));

-- Location: LABCELL_X40_Y3_N51
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(17),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~70\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\);

-- Location: FF_X40_Y3_N53
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~13_sumout\,
	asdata => \data_size_i[17]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17));

-- Location: LABCELL_X40_Y3_N54
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(18),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~14\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\);

-- Location: FF_X40_Y3_N56
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~17_sumout\,
	asdata => \data_size_i[18]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18));

-- Location: LABCELL_X40_Y3_N57
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(19),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~18\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\);

-- Location: FF_X40_Y3_N59
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~21_sumout\,
	asdata => \data_size_i[19]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19));

-- Location: LABCELL_X40_Y2_N0
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(20),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~22\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\);

-- Location: FF_X40_Y2_N2
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~29_sumout\,
	asdata => \data_size_i[20]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20));

-- Location: LABCELL_X40_Y2_N3
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(21),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~30\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\);

-- Location: FF_X40_Y2_N5
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~1_sumout\,
	asdata => \data_size_i[21]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21));

-- Location: LABCELL_X40_Y2_N6
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(22),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~2\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\);

-- Location: FF_X40_Y2_N8
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~5_sumout\,
	asdata => \data_size_i[22]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22));

-- Location: LABCELL_X40_Y2_N48
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~0_combout\ = ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(21) & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(22) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(21),
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(22),
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~0_combout\);

-- Location: LABCELL_X40_Y2_N9
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(23),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~6\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\);

-- Location: FF_X40_Y2_N11
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~9_sumout\,
	asdata => \data_size_i[23]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23));

-- Location: LABCELL_X40_Y2_N12
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(24),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~10\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\);

-- Location: FF_X40_Y2_N14
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~25_sumout\,
	asdata => \data_size_i[24]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24));

-- Location: LABCELL_X40_Y2_N42
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~1_combout\ = ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(18) & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(17) & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(19) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(24) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(20) 
-- & !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(23)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(19),
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(24),
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(20),
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(23),
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(18),
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(17),
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~1_combout\);

-- Location: MLABCELL_X39_Y3_N18
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~4_combout\ = ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(9) & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(10) & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(11) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(14) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(13) 
-- & !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(12)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(11),
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(14),
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(13),
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(12),
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(9),
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(10),
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~4_combout\);

-- Location: MLABCELL_X39_Y3_N24
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~5_combout\ = ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(3) & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(8) & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(5) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(4) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(7) & 
-- !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(6)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(5),
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(4),
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(7),
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(6),
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(3),
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(8),
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~5_combout\);

-- Location: LABCELL_X40_Y2_N15
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(25),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~26\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\);

-- Location: FF_X40_Y2_N17
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~53_sumout\,
	asdata => \data_size_i[25]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25));

-- Location: LABCELL_X40_Y2_N18
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(26),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~54\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\);

-- Location: FF_X40_Y2_N20
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~65_sumout\,
	asdata => \data_size_i[26]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26));

-- Location: LABCELL_X40_Y2_N21
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(27),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~66\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\);

-- Location: FF_X40_Y2_N23
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~49_sumout\,
	asdata => \data_size_i[27]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27));

-- Location: LABCELL_X40_Y2_N24
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(28),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~50\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\);

-- Location: FF_X40_Y2_N26
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~45_sumout\,
	asdata => \data_size_i[28]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28));

-- Location: LABCELL_X40_Y2_N27
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(29),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~46\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\);

-- Location: FF_X40_Y2_N29
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~41_sumout\,
	asdata => \data_size_i[29]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29));

-- Location: LABCELL_X40_Y2_N30
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ ))
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\ = CARRY(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(30),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~42\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\,
	cout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\);

-- Location: FF_X40_Y2_N32
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~37_sumout\,
	asdata => \data_size_i[30]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30));

-- Location: LABCELL_X40_Y2_N33
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\ = SUM(( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31) ) + ( VCC ) + ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(31),
	cin => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~38\,
	sumout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\);

-- Location: FF_X40_Y2_N35
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|Add0~33_sumout\,
	asdata => \data_size_i[31]~input_o\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31));

-- Location: LABCELL_X40_Y2_N36
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~2_combout\ = ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(29) & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(31) & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(27) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(25) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(28) 
-- & !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(30)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(27),
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(25),
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(28),
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(30),
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(29),
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(31),
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~2_combout\);

-- Location: MLABCELL_X39_Y3_N12
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~3_combout\ = ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(16) & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(0) & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(15) & (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(26) & (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(1) & 
-- !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|cpt_s\(2)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(15),
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(26),
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(1),
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(16),
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|decompteur_s|ALT_INV_cpt_s\(0),
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~3_combout\);

-- Location: LABCELL_X40_Y2_N57
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6_combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~2_combout\ & ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~3_combout\ & ( 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~0_combout\ & (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~1_combout\ & (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~4_combout\ & 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~5_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~0_combout\,
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~1_combout\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~4_combout\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~5_combout\,
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~2_combout\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~3_combout\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6_combout\);

-- Location: LABCELL_X36_Y2_N9
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector2~0_combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6_combout\ & ( (!\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) ) ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6_combout\ & ( (!\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & 
-- ((\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101010101010000010101010101000000000101010100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector2~0_combout\);

-- Location: FF_X36_Y2_N11
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector2~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\);

-- Location: LABCELL_X36_Y2_N6
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector5~0_combout\ = ((\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & ((\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\) # 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\)))) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011011101110111001101110111011100110111011101110011011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector5~0_combout\);

-- Location: FF_X36_Y2_N8
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector5~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\);

-- Location: LABCELL_X36_Y2_N24
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector3~0_combout\ = ( !\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & ( (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\ & 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Equal0~6_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011000000110000001100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Equal0~6_combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector3~0_combout\);

-- Location: FF_X36_Y2_N26
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector3~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~DUPLICATE_q\);

-- Location: LABCELL_X35_Y2_N36
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\ & ( (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\) # 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~DUPLICATE_q\) ) ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111001111110011111111111111111111110011111100111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~DUPLICATE_q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\);

-- Location: LABCELL_X36_Y2_N18
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100001111111111110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\);

-- Location: FF_X36_Y2_N50
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\,
	ena => \ALT_INV_rst_i~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\);

-- Location: FF_X36_Y2_N25
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector3~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\);

-- Location: LABCELL_X36_Y2_N48
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ ) ) # ( 
-- !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( ((!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\)) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) ) ) ) # ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( (((!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\)) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\)) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) ) ) 
-- ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|end_of_data_save_s~q\ & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( ((!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\) 
-- # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\)) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111011111110111111101111111111111110111111101111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_end_of_data_save_s~q\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\);

-- Location: LABCELL_X31_Y2_N6
\online_adder_cell_s|uc_adder_s|Selector5~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector5~0_combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\))) ) ) ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\))) ) ) ) # ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000100000000000000010000000000000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datac => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~DUPLICATE_q\,
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\,
	combout => \online_adder_cell_s|uc_adder_s|Selector5~0_combout\);

-- Location: LABCELL_X30_Y1_N27
\online_adder_cell_s|uc_adder_s|Selector7~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector7~6_combout\ = ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & ( \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end0~q\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector7~6_combout\);

-- Location: FF_X31_Y1_N59
\online_adder_cell_s|uc_adder_s|state_s.sEnd1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \online_adder_cell_s|uc_adder_s|Selector7~6_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sEnd1~q\);

-- Location: FF_X31_Y1_N58
\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \online_adder_cell_s|uc_adder_s|Selector7~6_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\);

-- Location: LABCELL_X31_Y1_N6
\online_adder_cell_s|uc_adder_s|Selector10~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector10~0_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ ) # ( !\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ & ( 
-- (!\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011110000000000001111000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~DUPLICATE_q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector10~0_combout\);

-- Location: FF_X31_Y1_N8
\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector10~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\);

-- Location: LABCELL_X31_Y1_N24
\online_adder_cell_s|uc_adder_s|Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector0~0_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\ & ( (!\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- (((!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\)) # (\online_adder_cell_s|uc_adder_s|state_s.sInit~q\))) ) ) # ( 
-- !\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\ & ( ((!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\)) # 
-- (\online_adder_cell_s|uc_adder_s|state_s.sInit~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111001100110011111100110011001110100010001000101010001000100010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector0~0_combout\);

-- Location: LABCELL_X31_Y1_N39
\online_adder_cell_s|uc_adder_s|Selector0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector0~1_combout\ = ( \online_adder_cell_s|uc_adder_s|Selector0~0_combout\ ) # ( !\online_adder_cell_s|uc_adder_s|Selector0~0_combout\ & ( (((!\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\) # 
-- (\online_adder_cell_s|uc_adder_s|Selector7~6_combout\)) # (\online_adder_cell_s|uc_adder_s|state_s.sEnd1~q\)) # (\online_adder_cell_s|uc_adder_s|Selector5~0_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111101111111111111110111111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector5~0_combout\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~6_combout\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector0~0_combout\,
	combout => \online_adder_cell_s|uc_adder_s|Selector0~1_combout\);

-- Location: FF_X31_Y1_N41
\online_adder_cell_s|uc_adder_s|state_s.sInit\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector0~1_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sInit~q\);

-- Location: LABCELL_X31_Y1_N15
\online_adder_cell_s|uc_adder_s|Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector2~0_combout\ = (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & 
-- !\online_adder_cell_s|uc_adder_s|state_s.sInit~q\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1000000010000000100000001000000010000000100000001000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector2~0_combout\);

-- Location: FF_X31_Y1_N16
\online_adder_cell_s|uc_adder_s|state_s.sStep0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector2~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sStep0~q\);

-- Location: LABCELL_X31_Y1_N21
\online_adder_cell_s|uc_adder_s|Selector12~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector12~0_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sStep0~q\ ) # ( !\online_adder_cell_s|uc_adder_s|state_s.sStep0~q\ & ( (\online_adder_cell_s|uc_adder_s|state_s.sWait_val_step0~q\ & 
-- ((\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001110111000000000111011111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_step0~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep0~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector12~0_combout\);

-- Location: FF_X31_Y1_N22
\online_adder_cell_s|uc_adder_s|state_s.sWait_val_step0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector12~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sWait_val_step0~q\);

-- Location: LABCELL_X31_Y1_N51
\online_adder_cell_s|uc_adder_s|Selector2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector2~1_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sWait_val_step0~q\ & ( (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & 
-- !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011000000110000001100000011000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_step0~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector2~1_combout\);

-- Location: FF_X31_Y1_N53
\online_adder_cell_s|uc_adder_s|state_s.sStep1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector2~1_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sStep1~q\);

-- Location: FF_X31_Y1_N50
\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector8~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\);

-- Location: LABCELL_X31_Y1_N48
\online_adder_cell_s|uc_adder_s|Selector8~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector8~0_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sStepN~q\ ) # ( !\online_adder_cell_s|uc_adder_s|state_s.sStepN~q\ & ( ((!\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\)) # (\online_adder_cell_s|uc_adder_s|state_s.sStep1~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111110101111000011111010111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep1~q\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStepN~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector8~0_combout\);

-- Location: FF_X31_Y1_N49
\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector8~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\);

-- Location: LABCELL_X31_Y1_N9
\online_adder_cell_s|uc_adder_s|Selector7~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector7~7_combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( ((\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\)) # (\online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\) ) ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (((\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\)) # 
-- (\online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100001111000000010000111100010001111111110001000111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~DUPLICATE_q\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~q\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	combout => \online_adder_cell_s|uc_adder_s|Selector7~7_combout\);

-- Location: FF_X31_Y1_N10
\online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector7~7_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\);

-- Location: LABCELL_X31_Y1_N18
\online_adder_cell_s|uc_adder_s|Selector7~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector7~5_combout\ = ( !\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ & ( (\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & (((!\online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\)) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000111000011110000011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~DUPLICATE_q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector7~5_combout\);

-- Location: LABCELL_X31_Y1_N0
\online_adder_cell_s|uc_adder_s|Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector4~0_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\ & ( (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & 
-- (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001100000000000000110000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datac => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector4~0_combout\);

-- Location: LABCELL_X30_Y1_N24
\online_adder_cell_s|uc_adder_s|Selector7~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector7~2_combout\ = ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & ( (\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\) # 
-- (\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000001011111010111110101111101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end0~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector7~2_combout\);

-- Location: FF_X31_Y1_N11
\online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector7~7_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~DUPLICATE_q\);

-- Location: LABCELL_X31_Y1_N27
\online_adder_cell_s|uc_adder_s|Selector7~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector7~3_combout\ = ( !\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\ & ( !\online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~DUPLICATE_q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111000011110000111100001111000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~DUPLICATE_q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector7~3_combout\);

-- Location: LABCELL_X31_Y1_N30
\online_adder_cell_s|uc_adder_s|Selector7~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector7~4_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- (\online_adder_cell_s|uc_adder_s|Selector7~2_combout\ & \online_adder_cell_s|uc_adder_s|Selector7~3_combout\) ) ) ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( (\online_adder_cell_s|uc_adder_s|Selector7~2_combout\ & \online_adder_cell_s|uc_adder_s|Selector7~3_combout\) ) ) ) # ( 
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( (\online_adder_cell_s|uc_adder_s|Selector7~2_combout\ & 
-- \online_adder_cell_s|uc_adder_s|Selector7~3_combout\) ) ) ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- (\online_adder_cell_s|uc_adder_s|Selector7~3_combout\ & (((!\online_adder_cell_s|uc_adder_s|state_s.sInit~q\) # (\online_adder_cell_s|uc_adder_s|state_s.sWait_val_step0~q\)) # (\online_adder_cell_s|uc_adder_s|Selector7~2_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100000111000001010000010100000101000001010000010100000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~2_combout\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_step0~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~3_combout\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\,
	datae => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	combout => \online_adder_cell_s|uc_adder_s|Selector7~4_combout\);

-- Location: LABCELL_X31_Y1_N54
\online_adder_cell_s|uc_adder_s|Selector4~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector4~2_combout\ = ( \online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( \online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & 
-- !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\) ) ) ) # ( !\online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( \online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & (\online_adder_cell_s|uc_adder_s|Selector4~1_combout\ & !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\)) ) ) ) # ( 
-- \online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( !\online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & 
-- !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\) ) ) ) # ( !\online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( !\online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\ & (\online_adder_cell_s|uc_adder_s|Selector4~1_combout\ & (!\online_adder_cell_s|uc_adder_s|Selector7~5_combout\ & 
-- !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010000000000000101010100000000000100010000000001010101000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~1_combout\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~5_combout\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\,
	datae => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~0_combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~4_combout\,
	combout => \online_adder_cell_s|uc_adder_s|Selector4~2_combout\);

-- Location: FF_X31_Y1_N56
\online_adder_cell_s|uc_adder_s|state_s.sEnd0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector4~2_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sEnd0~q\);

-- Location: LABCELL_X30_Y1_N21
\online_adder_cell_s|uc_adder_s|Selector9~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector9~0_combout\ = ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & ( \online_adder_cell_s|uc_adder_s|state_s.sEnd0~q\ ) ) # ( 
-- !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & ( (\online_adder_cell_s|uc_adder_s|state_s.sEnd0~q\) # (\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end0~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd0~q\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector9~0_combout\);

-- Location: FF_X31_Y1_N29
\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \online_adder_cell_s|uc_adder_s|Selector9~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0~q\);

-- Location: LABCELL_X30_Y1_N18
\online_adder_cell_s|uc_adder_s|WideOr8\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|WideOr8~combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\ ) # ( !\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~DUPLICATE_q\ & ( 
-- (\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\) # (\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end0~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101111101011111010111110101111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end0~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~DUPLICATE_q\,
	combout => \online_adder_cell_s|uc_adder_s|WideOr8~combout\);

-- Location: IOIBUF_X30_Y0_N52
\acc_nxt_i~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_acc_nxt_i,
	o => \acc_nxt_i~input_o\);

-- Location: LABCELL_X30_Y1_N54
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector0~0_combout\ = ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\ & ( !\acc_nxt_i~input_o\ ) ) # ( 
-- !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_acc_nxt_i~input_o\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sEnd~q\,
	combout => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector0~0_combout\);

-- Location: FF_X30_Y1_N56
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector0~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\);

-- Location: LABCELL_X30_Y1_N57
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector3~0_combout\ = ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ & ( (!\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- (((!\acc_nxt_i~input_o\ & \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\)))) # (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & (\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\)) ) ) # 
-- ( !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ & ( (\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\ & \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010100000101000001010000010100000101110001010000010111000101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\,
	datab => \ALT_INV_acc_nxt_i~input_o\,
	datac => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datad => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sEnd~q\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	combout => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector3~0_combout\);

-- Location: FF_X30_Y1_N59
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector3~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\);

-- Location: LABCELL_X30_Y1_N36
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector4~0_combout\ = ( !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\ & ( (!\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & 
-- (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sWait~q\ & (!\online_adder_cell_s|uc_adder_s|WideOr8~combout\ & ((\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\))))) # 
-- (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & ((((!\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\))))) ) ) # ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sEnd~q\ & ( 
-- (!\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sWait~q\ & (!\acc_nxt_i~input_o\ & 
-- ((\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\))))) # (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & ((((!\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_end1~q\))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0011001100000000001100110000000001110011010000000111001101000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sWait~q\,
	datab => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datac => \ALT_INV_acc_nxt_i~input_o\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_end1~q\,
	datae => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sEnd~q\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	datag => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr8~combout\,
	combout => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector4~0_combout\);

-- Location: FF_X30_Y1_N38
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sWait\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector4~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sWait~q\);

-- Location: MLABCELL_X25_Y1_N21
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector1~0_combout\ = ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ & ( (!\online_adder_cell_s|uc_adder_s|WideOr8~combout\ & 
-- \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sReady~q\) ) ) # ( !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000101010100000000010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr8~combout\,
	datad => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sReady~q\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	combout => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector1~0_combout\);

-- Location: FF_X25_Y1_N23
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sReady\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector1~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sReady~q\);

-- Location: MLABCELL_X25_Y1_N39
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\ = ( \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ ) # ( !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & ( 
-- !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	combout => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\);

-- Location: MLABCELL_X25_Y1_N18
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~1_combout\ = ( !\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\ & ( (\online_adder_cell_s|uc_adder_s|WideOr8~combout\ & 
-- (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector0~0_combout\ & ((\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sReady~q\) # (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sWait~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000010101000000000001010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr8~combout\,
	datab => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sWait~q\,
	datac => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sReady~q\,
	datad => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_Selector0~0_combout\,
	dataf => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_Selector2~0_combout\,
	combout => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~1_combout\);

-- Location: FF_X25_Y1_N20
\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~1_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\);

-- Location: LABCELL_X31_Y1_N12
\online_adder_cell_s|uc_adder_s|Selector4~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector4~1_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\ & ( (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (!\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sConv~q\ & \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\))) ) ) # ( 
-- !\online_adder_cell_s|uc_adder_s|state_s.sWait_acc_nxt~q\ & ( (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & 
-- \online_adder_cell_s|uc_adder_s|state_s.sWait_val_prv~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010001000000000001000100000000000100000000000000010000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	datac => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sConv~q\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_val_prv~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sWait_acc_nxt~q\,
	combout => \online_adder_cell_s|uc_adder_s|Selector4~1_combout\);

-- Location: LABCELL_X31_Y1_N42
\online_adder_cell_s|uc_adder_s|Selector3~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|Selector3~0_combout\ = ( \online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( \online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\) ) ) ) # ( !\online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( \online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( 
-- (\online_adder_cell_s|uc_adder_s|Selector4~1_combout\ & ((\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\))) ) ) ) # ( 
-- \online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( !\online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\) ) ) ) # ( !\online_adder_cell_s|uc_adder_s|Selector4~0_combout\ & ( !\online_adder_cell_s|uc_adder_s|Selector7~4_combout\ & ( 
-- (\online_adder_cell_s|uc_adder_s|Selector4~1_combout\ & (!\online_adder_cell_s|uc_adder_s|Selector7~5_combout\ & ((\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector6~0_combout\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|Selector6~0_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000110000010101011111111100010001001100110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~1_combout\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~5_combout\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_Selector6~0_combout\,
	datae => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector4~0_combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_Selector7~4_combout\,
	combout => \online_adder_cell_s|uc_adder_s|Selector3~0_combout\);

-- Location: FF_X31_Y1_N43
\online_adder_cell_s|uc_adder_s|state_s.sStepN\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|uc_adder_s|Selector3~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|uc_adder_s|state_s.sStepN~q\);

-- Location: LABCELL_X31_Y1_N3
\online_adder_cell_s|uc_adder_s|WideOr6~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ = ( !\online_adder_cell_s|uc_adder_s|state_s.sStep1~q\ & ( (!\online_adder_cell_s|uc_adder_s|state_s.sStepN~q\ & (!\online_adder_cell_s|uc_adder_s|state_s.sStep0~q\ & 
-- !\online_adder_cell_s|uc_adder_s|state_s.sEnd0~q\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010000000000000101000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStepN~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep0~q\,
	datad => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd0~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sStep1~q\,
	combout => \online_adder_cell_s|uc_adder_s|WideOr6~0_combout\);

-- Location: LABCELL_X36_Y2_N3
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector4~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector4~0_combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( 
-- (\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000001010101000000000101010111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector4~0_combout\);

-- Location: FF_X36_Y2_N5
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector4~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\);

-- Location: LABCELL_X36_Y2_N27
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector0~0_combout\ = ( \online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & ( (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # (\val_prv_a_i~input_o\) ) ) # ( 
-- !\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\ & ( ((!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv1~q\ & ((\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # (\val_prv_a_i~input_o\)))) # 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011101110111011001110111011101100001111111111110000111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv1~q\,
	datab => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datac => \ALT_INV_val_prv_a_i~input_o\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector0~0_combout\);

-- Location: FF_X36_Y2_N29
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|Selector0~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\);

-- Location: MLABCELL_X34_Y2_N0
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ = SUM(( \data_size_i[0]~input_o\ ) + ( VCC ) + ( !VCC ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~10\ = CARRY(( \data_size_i[0]~input_o\ ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_data_size_i[0]~input_o\,
	cin => GND,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~10\);

-- Location: MLABCELL_X34_Y2_N3
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ = SUM(( \data_size_i[1]~input_o\ ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~10\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~14\ = CARRY(( \data_size_i[1]~input_o\ ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_size_i[1]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~10\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~14\);

-- Location: MLABCELL_X34_Y2_N6
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ = SUM(( \data_size_i[2]~input_o\ ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~14\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~2\ = CARRY(( \data_size_i[2]~input_o\ ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_data_size_i[2]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~14\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~2\);

-- Location: MLABCELL_X34_Y2_N9
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ = SUM(( \data_size_i[3]~input_o\ ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~2\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~6\ = CARRY(( \data_size_i[3]~input_o\ ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_size_i[3]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~2\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~6\);

-- Location: MLABCELL_X34_Y2_N12
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ = SUM(( \data_size_i[4]~input_o\ ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \ALT_INV_data_size_i[4]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~6\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\);

-- Location: IOIBUF_X66_Y0_N75
\data_prv_b_i[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(7),
	o => \data_prv_b_i[7]~input_o\);

-- Location: IOIBUF_X64_Y0_N35
\data_prv_b_i[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(5),
	o => \data_prv_b_i[5]~input_o\);

-- Location: IOIBUF_X40_Y0_N18
\data_prv_b_i[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(6),
	o => \data_prv_b_i[6]~input_o\);

-- Location: IOIBUF_X38_Y0_N35
\data_prv_b_i[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(4),
	o => \data_prv_b_i[4]~input_o\);

-- Location: LABCELL_X33_Y2_N36
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~7_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( \data_prv_b_i[4]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & 
-- ((\data_prv_b_i[5]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_b_i[7]~input_o\)) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( \data_prv_b_i[4]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) # (\data_prv_b_i[6]~input_o\) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( !\data_prv_b_i[4]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_b_i[5]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_b_i[7]~input_o\)) ) ) ) # ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( !\data_prv_b_i[4]~input_o\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & \data_prv_b_i[6]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000110011000111010001110111001100111111110001110100011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_b_i[7]~input_o\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datac => \ALT_INV_data_prv_b_i[5]~input_o\,
	datad => \ALT_INV_data_prv_b_i[6]~input_o\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	dataf => \ALT_INV_data_prv_b_i[4]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~7_combout\);

-- Location: IOIBUF_X68_Y0_N18
\data_prv_b_i[11]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(11),
	o => \data_prv_b_i[11]~input_o\);

-- Location: IOIBUF_X36_Y81_N52
\data_prv_b_i[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(9),
	o => \data_prv_b_i[9]~input_o\);

-- Location: IOIBUF_X89_Y4_N95
\data_prv_b_i[10]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(10),
	o => \data_prv_b_i[10]~input_o\);

-- Location: IOIBUF_X60_Y0_N1
\data_prv_b_i[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(8),
	o => \data_prv_b_i[8]~input_o\);

-- Location: LABCELL_X33_Y2_N12
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~5_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( \data_prv_b_i[8]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & 
-- ((\data_prv_b_i[9]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_b_i[11]~input_o\)) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( \data_prv_b_i[8]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) # (\data_prv_b_i[10]~input_o\) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( !\data_prv_b_i[8]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_b_i[9]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_b_i[11]~input_o\)) ) ) ) # ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( !\data_prv_b_i[8]~input_o\ & ( (\data_prv_b_i[10]~input_o\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000001111001100110101010111111111000011110011001101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_b_i[11]~input_o\,
	datab => \ALT_INV_data_prv_b_i[9]~input_o\,
	datac => \ALT_INV_data_prv_b_i[10]~input_o\,
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	dataf => \ALT_INV_data_prv_b_i[8]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~5_combout\);

-- Location: IOIBUF_X62_Y0_N18
\data_prv_b_i[14]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(14),
	o => \data_prv_b_i[14]~input_o\);

-- Location: IOIBUF_X64_Y0_N18
\data_prv_b_i[13]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(13),
	o => \data_prv_b_i[13]~input_o\);

-- Location: IOIBUF_X36_Y81_N35
\data_prv_b_i[12]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(12),
	o => \data_prv_b_i[12]~input_o\);

-- Location: IOIBUF_X30_Y81_N18
\data_prv_b_i[15]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(15),
	o => \data_prv_b_i[15]~input_o\);

-- Location: LABCELL_X33_Y2_N6
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~6_combout\ = ( \data_prv_b_i[12]~input_o\ & ( \data_prv_b_i[15]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)) # (\data_prv_b_i[14]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) # 
-- (\data_prv_b_i[13]~input_o\)))) ) ) ) # ( !\data_prv_b_i[12]~input_o\ & ( \data_prv_b_i[15]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (\data_prv_b_i[14]~input_o\ & 
-- ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) # (\data_prv_b_i[13]~input_o\)))) ) ) ) # ( 
-- \data_prv_b_i[12]~input_o\ & ( !\data_prv_b_i[15]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)) # (\data_prv_b_i[14]~input_o\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((\data_prv_b_i[13]~input_o\ & !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) ) ) ) # ( !\data_prv_b_i[12]~input_o\ & ( !\data_prv_b_i[15]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (\data_prv_b_i[14]~input_o\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- (((\data_prv_b_i[13]~input_o\ & !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001101010000111100110101000000000011010111111111001101011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_b_i[14]~input_o\,
	datab => \ALT_INV_data_prv_b_i[13]~input_o\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datae => \ALT_INV_data_prv_b_i[12]~input_o\,
	dataf => \ALT_INV_data_prv_b_i[15]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~6_combout\);

-- Location: IOIBUF_X80_Y0_N1
\data_prv_b_i[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(0),
	o => \data_prv_b_i[0]~input_o\);

-- Location: IOIBUF_X36_Y0_N35
\data_prv_b_i[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(1),
	o => \data_prv_b_i[1]~input_o\);

-- Location: IOIBUF_X64_Y0_N52
\data_prv_b_i[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(2),
	o => \data_prv_b_i[2]~input_o\);

-- Location: IOIBUF_X72_Y0_N1
\data_prv_b_i[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(3),
	o => \data_prv_b_i[3]~input_o\);

-- Location: MLABCELL_X34_Y2_N39
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( \data_prv_b_i[3]~input_o\ & ( (\data_prv_b_i[2]~input_o\) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( \data_prv_b_i[3]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- (\data_prv_b_i[0]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ((\data_prv_b_i[1]~input_o\))) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( !\data_prv_b_i[3]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & \data_prv_b_i[2]~input_o\) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( !\data_prv_b_i[3]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (\data_prv_b_i[0]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ((\data_prv_b_i[1]~input_o\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0010011100100111000000001010101000100111001001110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	datab => \ALT_INV_data_prv_b_i[0]~input_o\,
	datac => \ALT_INV_data_prv_b_i[1]~input_o\,
	datad => \ALT_INV_data_prv_b_i[2]~input_o\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	dataf => \ALT_INV_data_prv_b_i[3]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\);

-- Location: LABCELL_X33_Y2_N30
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~7_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~6_combout\))) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~5_combout\) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( 
-- !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~7_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~6_combout\))) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( 
-- !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~8_combout\ & ( (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~5_combout\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001100000011010100000101111111110011111100110101000001011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~7_combout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~5_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~6_combout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~8_combout\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\);

-- Location: IOIBUF_X40_Y0_N1
\data_prv_b_i[16]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(16),
	o => \data_prv_b_i[16]~input_o\);

-- Location: IOIBUF_X34_Y0_N92
\data_prv_b_i[24]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(24),
	o => \data_prv_b_i[24]~input_o\);

-- Location: IOIBUF_X36_Y0_N52
\data_prv_b_i[20]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(20),
	o => \data_prv_b_i[20]~input_o\);

-- Location: IOIBUF_X66_Y0_N58
\data_prv_b_i[28]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(28),
	o => \data_prv_b_i[28]~input_o\);

-- Location: MLABCELL_X34_Y2_N45
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~0_combout\ = ( \data_prv_b_i[20]~input_o\ & ( \data_prv_b_i[28]~input_o\ & ( ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[16]~input_o\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_b_i[24]~input_o\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\) ) ) ) # ( !\data_prv_b_i[20]~input_o\ & ( \data_prv_b_i[28]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[16]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- ((\data_prv_b_i[24]~input_o\))))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\)))) ) ) ) # ( \data_prv_b_i[20]~input_o\ & ( !\data_prv_b_i[28]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[16]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- ((\data_prv_b_i[24]~input_o\))))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\)))) ) ) ) # ( !\data_prv_b_i[20]~input_o\ & ( !\data_prv_b_i[28]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[16]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- ((\data_prv_b_i[24]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001001100011100000111110001000011010011110111001101111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_b_i[16]~input_o\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datad => \ALT_INV_data_prv_b_i[24]~input_o\,
	datae => \ALT_INV_data_prv_b_i[20]~input_o\,
	dataf => \ALT_INV_data_prv_b_i[28]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~0_combout\);

-- Location: IOIBUF_X32_Y0_N35
\data_prv_b_i[25]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(25),
	o => \data_prv_b_i[25]~input_o\);

-- Location: IOIBUF_X38_Y0_N18
\data_prv_b_i[29]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(29),
	o => \data_prv_b_i[29]~input_o\);

-- Location: IOIBUF_X34_Y81_N41
\data_prv_b_i[17]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(17),
	o => \data_prv_b_i[17]~input_o\);

-- Location: IOIBUF_X60_Y0_N52
\data_prv_b_i[21]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(21),
	o => \data_prv_b_i[21]~input_o\);

-- Location: LABCELL_X33_Y2_N42
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~1_combout\ = ( \data_prv_b_i[17]~input_o\ & ( \data_prv_b_i[21]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\) # 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (\data_prv_b_i[25]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((\data_prv_b_i[29]~input_o\)))) ) ) ) # ( !\data_prv_b_i[17]~input_o\ & ( 
-- \data_prv_b_i[21]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (\data_prv_b_i[25]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((\data_prv_b_i[29]~input_o\))))) ) ) ) # ( \data_prv_b_i[17]~input_o\ & ( 
-- !\data_prv_b_i[21]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (\data_prv_b_i[25]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((\data_prv_b_i[29]~input_o\))))) ) ) ) # ( !\data_prv_b_i[17]~input_o\ & ( 
-- !\data_prv_b_i[21]~input_o\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (\data_prv_b_i[25]~input_o\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((\data_prv_b_i[29]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010101101100001011010100011010000111111011101010111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datab => \ALT_INV_data_prv_b_i[25]~input_o\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	datad => \ALT_INV_data_prv_b_i[29]~input_o\,
	datae => \ALT_INV_data_prv_b_i[17]~input_o\,
	dataf => \ALT_INV_data_prv_b_i[21]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~1_combout\);

-- Location: IOIBUF_X36_Y0_N1
\data_prv_b_i[23]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(23),
	o => \data_prv_b_i[23]~input_o\);

-- Location: IOIBUF_X34_Y0_N75
\data_prv_b_i[31]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(31),
	o => \data_prv_b_i[31]~input_o\);

-- Location: IOIBUF_X36_Y0_N18
\data_prv_b_i[27]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(27),
	o => \data_prv_b_i[27]~input_o\);

-- Location: IOIBUF_X60_Y0_N18
\data_prv_b_i[19]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(19),
	o => \data_prv_b_i[19]~input_o\);

-- Location: LABCELL_X33_Y2_N48
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~3_combout\ = ( \data_prv_b_i[27]~input_o\ & ( \data_prv_b_i[19]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\) # 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[23]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_b_i[31]~input_o\)))) ) ) ) # ( !\data_prv_b_i[27]~input_o\ & ( 
-- \data_prv_b_i[19]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\)) # (\data_prv_b_i[23]~input_o\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & \data_prv_b_i[31]~input_o\)))) ) ) ) # ( \data_prv_b_i[27]~input_o\ & ( !\data_prv_b_i[19]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[23]~input_o\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\) # (\data_prv_b_i[31]~input_o\)))) ) ) ) # ( !\data_prv_b_i[27]~input_o\ & ( !\data_prv_b_i[19]~input_o\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[23]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_b_i[31]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000001000000111010100100101011110100010101001111111001011110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datab => \ALT_INV_data_prv_b_i[23]~input_o\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	datad => \ALT_INV_data_prv_b_i[31]~input_o\,
	datae => \ALT_INV_data_prv_b_i[27]~input_o\,
	dataf => \ALT_INV_data_prv_b_i[19]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~3_combout\);

-- Location: IOIBUF_X38_Y0_N1
\data_prv_b_i[30]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(30),
	o => \data_prv_b_i[30]~input_o\);

-- Location: IOIBUF_X34_Y0_N41
\data_prv_b_i[22]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(22),
	o => \data_prv_b_i[22]~input_o\);

-- Location: IOIBUF_X38_Y0_N52
\data_prv_b_i[18]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(18),
	o => \data_prv_b_i[18]~input_o\);

-- Location: IOIBUF_X32_Y81_N1
\data_prv_b_i[26]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_b_i(26),
	o => \data_prv_b_i[26]~input_o\);

-- Location: LABCELL_X30_Y2_N45
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~2_combout\ = ( \data_prv_b_i[18]~input_o\ & ( \data_prv_b_i[26]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\) # 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_b_i[22]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[30]~input_o\))) ) ) ) # ( !\data_prv_b_i[18]~input_o\ & ( 
-- \data_prv_b_i[26]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & \data_prv_b_i[22]~input_o\)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\)) # (\data_prv_b_i[30]~input_o\))) ) ) ) # ( \data_prv_b_i[18]~input_o\ & ( !\data_prv_b_i[26]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\) # (\data_prv_b_i[22]~input_o\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- (\data_prv_b_i[30]~input_o\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\))) ) ) ) # ( !\data_prv_b_i[18]~input_o\ & ( !\data_prv_b_i[26]~input_o\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_b_i[22]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_b_i[30]~input_o\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000100001011101000011010101101010001010110111111000111111011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datab => \ALT_INV_data_prv_b_i[30]~input_o\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	datad => \ALT_INV_data_prv_b_i[22]~input_o\,
	datae => \ALT_INV_data_prv_b_i[18]~input_o\,
	dataf => \ALT_INV_data_prv_b_i[26]~input_o\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~2_combout\);

-- Location: LABCELL_X33_Y2_N54
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~3_combout\ ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~2_combout\ ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~1_combout\ ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~0_combout\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101001100110011001100000000111111110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~0_combout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~1_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~3_combout\,
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~2_combout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\);

-- Location: LABCELL_X30_Y3_N0
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~73_sumout\ = SUM(( !\data_prv_b_i[0]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))) ) + ( !VCC ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~74\ = CARRY(( !\data_prv_b_i[0]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110001101100000000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[0]~input_o\,
	cin => GND,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~73_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~74\);

-- Location: LABCELL_X30_Y3_N3
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~69_sumout\ = SUM(( !\data_prv_b_i[1]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~74\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~70\ = CARRY(( !\data_prv_b_i[1]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[1]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~74\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~69_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~70\);

-- Location: LABCELL_X30_Y3_N6
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~77_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[2]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~70\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~78\ = CARRY(( GND ) + ( !\data_prv_b_i[2]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[2]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~70\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~77_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~78\);

-- Location: LABCELL_X30_Y3_N9
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~65_sumout\ = SUM(( !\data_prv_b_i[3]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~78\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~66\ = CARRY(( !\data_prv_b_i[3]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[3]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~78\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~65_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~66\);

-- Location: LABCELL_X30_Y3_N12
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~9_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[4]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~66\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~10\ = CARRY(( GND ) + ( !\data_prv_b_i[4]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[4]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~66\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~9_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~10\);

-- Location: LABCELL_X30_Y3_N15
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~1_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[5]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~10\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~2\ = CARRY(( GND ) + ( !\data_prv_b_i[5]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[5]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~10\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~1_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~2\);

-- Location: LABCELL_X30_Y3_N18
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~13_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[6]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~2\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~14\ = CARRY(( GND ) + ( !\data_prv_b_i[6]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[6]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~2\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~13_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~14\);

-- Location: LABCELL_X30_Y3_N21
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~5_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[7]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~14\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~6\ = CARRY(( GND ) + ( !\data_prv_b_i[7]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[7]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~14\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~5_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~6\);

-- Location: LABCELL_X30_Y3_N24
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~89_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[8]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~6\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~90\ = CARRY(( GND ) + ( !\data_prv_b_i[8]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[8]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~6\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~89_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~90\);

-- Location: LABCELL_X30_Y3_N27
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~85_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[9]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~90\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~86\ = CARRY(( GND ) + ( !\data_prv_b_i[9]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[9]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~90\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~85_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~86\);

-- Location: LABCELL_X30_Y3_N30
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~93_sumout\ = SUM(( !\data_prv_b_i[10]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~86\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~94\ = CARRY(( !\data_prv_b_i[10]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[10]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~86\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~93_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~94\);

-- Location: LABCELL_X30_Y3_N33
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~81_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[11]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~94\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~82\ = CARRY(( GND ) + ( !\data_prv_b_i[11]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[11]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~94\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~81_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~82\);

-- Location: LABCELL_X30_Y3_N36
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~25_sumout\ = SUM(( !\data_prv_b_i[12]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~82\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~26\ = CARRY(( !\data_prv_b_i[12]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[12]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~82\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~25_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~26\);

-- Location: LABCELL_X30_Y3_N39
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~17_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[13]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~26\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~18\ = CARRY(( GND ) + ( !\data_prv_b_i[13]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[13]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~26\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~17_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~18\);

-- Location: LABCELL_X36_Y2_N30
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ & ( ((!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\)) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\) ) ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~q\ 
-- & ( (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # ((\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ & ((\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) # 
-- (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100000111111111110000011111111111011101111111111101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datab => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~q\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\);

-- Location: FF_X29_Y2_N23
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~17_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(13));

-- Location: LABCELL_X30_Y3_N42
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~29_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[14]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~18\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~30\ = CARRY(( GND ) + ( !\data_prv_b_i[14]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[14]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~18\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~29_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~30\);

-- Location: LABCELL_X30_Y3_N45
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~21_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[15]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~30\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~22\ = CARRY(( GND ) + ( !\data_prv_b_i[15]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[15]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~30\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~21_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~22\);

-- Location: LABCELL_X29_Y2_N42
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[15]~feeder_combout\ = ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~21_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Add1~21_sumout\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[15]~feeder_combout\);

-- Location: FF_X29_Y2_N43
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[15]~feeder_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(15));

-- Location: FF_X29_Y2_N25
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~29_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(14));

-- Location: LABCELL_X29_Y2_N0
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\ = SUM(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~18\ = CARRY(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	cin => GND,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~18\);

-- Location: LABCELL_X36_Y2_N33
\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\ = ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ ) # ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\ & ( 
-- (!\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # (\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|state_s.sWaitConv~DUPLICATE_q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111100001111111111110000111111111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~DUPLICATE_q\,
	datad => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	combout => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\);

-- Location: FF_X29_Y2_N2
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0));

-- Location: LABCELL_X29_Y2_N3
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\ = SUM(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~18\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~14\ = CARRY(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~18\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~14\);

-- Location: FF_X29_Y2_N5
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1));

-- Location: LABCELL_X29_Y2_N6
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\ = SUM(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~14\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~10\ = CARRY(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~14\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~10\);

-- Location: FF_X29_Y2_N8
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2));

-- Location: FF_X29_Y2_N46
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~93_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(10));

-- Location: FF_X29_Y2_N19
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~81_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(11));

-- Location: FF_X29_Y3_N4
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~85_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(9));

-- Location: FF_X30_Y2_N34
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~89_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(8));

-- Location: LABCELL_X29_Y2_N48
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(8)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(9)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) ) ) # ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(10)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(11)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0001100100011001000110010101110101011101010111010001100101011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(10),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(11),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(9),
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(8),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\);

-- Location: FF_X29_Y2_N49
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~25_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(12));

-- Location: LABCELL_X29_Y2_N54
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\ & 
-- ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(12)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(13)))))) ) ) # ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(14)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~21_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(15)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000000001111000000000000111111111111010101011111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(13),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(15),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(14),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~21_combout\,
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(12),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\);

-- Location: LABCELL_X29_Y2_N9
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\ = SUM(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~10\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~2\ = CARRY(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(3),
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~10\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~2\);

-- Location: FF_X29_Y2_N11
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3));

-- Location: LABCELL_X29_Y2_N12
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\ = SUM(( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) ) + ( VCC ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(4),
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~2\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\);

-- Location: FF_X29_Y2_N14
\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4));

-- Location: FF_X30_Y3_N16
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~1_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(5));

-- Location: FF_X30_Y3_N19
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~13_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(6));

-- Location: FF_X30_Y2_N11
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~5_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(7));

-- Location: FF_X30_Y2_N32
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~65_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(3));

-- Location: FF_X30_Y3_N7
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~77_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(2));

-- Location: FF_X30_Y3_N5
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~69_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(1));

-- Location: FF_X30_Y2_N14
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~73_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(0));

-- Location: LABCELL_X30_Y2_N54
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(0) & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(1)) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) ) ) # ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(2) & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000101001010101000110110101010101011111010101010001101101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(3),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(2),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(1),
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(0),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\);

-- Location: FF_X30_Y2_N2
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~9_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(4));

-- Location: LABCELL_X29_Y2_N36
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\))))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\ & 
-- ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(4)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(5)))))) ) ) # ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(6))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~17_combout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(7))))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000001100000011000000110000001111011101110111011100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(5),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(6),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(7),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~17_combout\,
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(4),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\);

-- Location: LABCELL_X30_Y3_N48
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~105_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[16]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~22\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~106\ = CARRY(( GND ) + ( !\data_prv_b_i[16]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[16]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~22\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~105_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~106\);

-- Location: LABCELL_X30_Y3_N51
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~101_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[17]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~106\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~102\ = CARRY(( GND ) + ( !\data_prv_b_i[17]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[17]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~106\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~101_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~102\);

-- Location: LABCELL_X30_Y3_N54
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~109_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[18]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~102\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~110\ = CARRY(( GND ) + ( !\data_prv_b_i[18]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[18]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~102\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~109_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~110\);

-- Location: LABCELL_X30_Y3_N57
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~97_sumout\ = SUM(( !\data_prv_b_i[19]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~110\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~98\ = CARRY(( !\data_prv_b_i[19]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[19]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~110\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~97_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~98\);

-- Location: LABCELL_X30_Y2_N0
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~41_sumout\ = SUM(( !\data_prv_b_i[20]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~98\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~42\ = CARRY(( !\data_prv_b_i[20]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[20]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~98\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~41_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~42\);

-- Location: LABCELL_X30_Y2_N3
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~33_sumout\ = SUM(( !\data_prv_b_i[21]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~42\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~34\ = CARRY(( !\data_prv_b_i[21]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[21]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~42\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~33_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~34\);

-- Location: LABCELL_X30_Y2_N6
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~45_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[22]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~34\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~46\ = CARRY(( GND ) + ( !\data_prv_b_i[22]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[22]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~34\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~45_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~46\);

-- Location: LABCELL_X30_Y2_N9
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~37_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[23]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~46\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~38\ = CARRY(( GND ) + ( !\data_prv_b_i[23]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[23]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~46\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~37_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~38\);

-- Location: LABCELL_X30_Y2_N12
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~121_sumout\ = SUM(( !\data_prv_b_i[24]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~38\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~122\ = CARRY(( !\data_prv_b_i[24]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[24]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~38\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~121_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~122\);

-- Location: LABCELL_X30_Y2_N15
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~117_sumout\ = SUM(( !\data_prv_b_i[25]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~122\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~118\ = CARRY(( !\data_prv_b_i[25]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[25]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~122\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~117_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~118\);

-- Location: LABCELL_X30_Y2_N18
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~125_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[26]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~118\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~126\ = CARRY(( GND ) + ( !\data_prv_b_i[26]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[26]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~118\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~125_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~126\);

-- Location: LABCELL_X30_Y2_N21
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~113_sumout\ = SUM(( !\data_prv_b_i[27]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~126\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~114\ = CARRY(( !\data_prv_b_i[27]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_b_i[27]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~126\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~113_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~114\);

-- Location: LABCELL_X30_Y2_N24
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~57_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[28]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~114\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~58\ = CARRY(( GND ) + ( !\data_prv_b_i[28]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[28]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~114\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~57_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~58\);

-- Location: LABCELL_X30_Y2_N27
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~49_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[29]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~58\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~50\ = CARRY(( GND ) + ( !\data_prv_b_i[29]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[29]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~58\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~49_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~50\);

-- Location: FF_X30_Y2_N29
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~49_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(29));

-- Location: LABCELL_X30_Y2_N30
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~61_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[30]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~50\ ))
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~62\ = CARRY(( GND ) + ( !\data_prv_b_i[30]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[30]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~50\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~61_sumout\,
	cout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~62\);

-- Location: LABCELL_X30_Y2_N33
\signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~53_sumout\ = SUM(( GND ) + ( !\data_prv_b_i[31]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_b_i[31]~input_o\,
	cin => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~62\,
	sumout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~53_sumout\);

-- Location: FF_X30_Y2_N47
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~53_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(31));

-- Location: FF_X30_Y2_N56
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~61_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(30));

-- Location: FF_X30_Y2_N44
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~117_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(25));

-- Location: FF_X30_Y2_N38
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~125_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(26));

-- Location: FF_X30_Y2_N22
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~113_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(27));

-- Location: FF_X30_Y2_N16
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~121_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(24));

-- Location: LABCELL_X30_Y2_N48
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(24) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2))))))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(25)))) ) ) # ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(26) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2))))))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(27)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0001101100011011000010100101111101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(25),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(26),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(27),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(24),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\);

-- Location: FF_X30_Y2_N5
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~57_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(28));

-- Location: LABCELL_X30_Y2_N36
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\ & 
-- ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(28)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(29)))))) ) ) # ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(30)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~29_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(31)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000000001111000000000000111111111111010101011111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(29),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(31),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(30),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~29_combout\,
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(28),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\);

-- Location: FF_X30_Y2_N49
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~33_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(21));

-- Location: FF_X29_Y2_N55
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~37_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(23));

-- Location: FF_X29_Y2_N37
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~45_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(22));

-- Location: FF_X30_Y3_N55
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~109_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(18));

-- Location: FF_X29_Y2_N31
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~97_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(19));

-- Location: FF_X30_Y3_N52
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~101_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(17));

-- Location: FF_X30_Y3_N49
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~105_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(16));

-- Location: LABCELL_X29_Y2_N24
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(16)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(17)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) ) ) # ( \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(18)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(19)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0001100100011001000110010101110101011101010111010001100101011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(18),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(19),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(17),
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(16),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\);

-- Location: FF_X30_Y2_N19
\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_b_s|get_in_MSB_order_s|Add1~41_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(20));

-- Location: LABCELL_X29_Y2_N30
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\ = ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\ & 
-- ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(20)))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(21)))))) ) ) # ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(22)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~25_combout\ & (\signed_digit_serializer_b_s|get_in_MSB_order_s|reg_pres\(23)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000000001111000000000000111111111111010101011111111100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(21),
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(23),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(22),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~25_combout\,
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_reg_pres\(20),
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\);

-- Location: LABCELL_X29_Y2_N21
\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ = ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\ & ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\ & ( 
-- ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4)) ) ) ) # ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\ & ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\ & ( (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4))))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\ & 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4)))) ) ) ) # ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\ & ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\ & ( 
-- (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) & \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\)))) # 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (((\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\))) ) ) ) # ( 
-- !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~12_combout\ & ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~8_combout\ & ( (!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) & 
-- ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~0_combout\))) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~4_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000011010000000100111101001100011100110111000001111111011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~4_combout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(3),
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(4),
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~0_combout\,
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~12_combout\,
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~8_combout\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\);

-- Location: MLABCELL_X28_Y3_N54
\signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~0_combout\ = ( \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\)) ) ) ) # ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~9_combout\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux1~4_combout\)) ) ) ) # ( \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( !\signed_digit_serializer_b_s|uc_signed_digit_serializer_s|WideOr3~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100110101001101010011010100110101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datab => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	dataf => \signed_digit_serializer_b_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	combout => \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~0_combout\);

-- Location: FF_X28_Y3_N56
\signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\);

-- Location: MLABCELL_X28_Y3_N3
\online_adder_cell_s|data_b_s[1]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|data_b_s[1]~0_combout\ = ( \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( (!\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ & \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000001010000010100000101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~DUPLICATE_q\,
	datac => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\,
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	combout => \online_adder_cell_s|data_b_s[1]~0_combout\);

-- Location: LABCELL_X31_Y1_N36
\online_adder_cell_s|online_adder_s|w_pres~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|online_adder_s|w_pres~0_combout\ = ( \online_adder_cell_s|uc_adder_s|state_s.sInit~q\ & ( (!\online_adder_cell_s|uc_adder_s|WideOr6~0_combout\) # (\online_adder_cell_s|uc_adder_s|state_s.sEnd1~q\) ) ) # ( 
-- !\online_adder_cell_s|uc_adder_s|state_s.sInit~q\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111110011111100111111001111110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~q\,
	datac => \online_adder_cell_s|uc_adder_s|ALT_INV_WideOr6~0_combout\,
	dataf => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\,
	combout => \online_adder_cell_s|online_adder_s|w_pres~0_combout\);

-- Location: FF_X28_Y3_N5
\online_adder_cell_s|online_adder_s|g_pres[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|data_b_s[1]~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\,
	ena => \online_adder_cell_s|online_adder_s|w_pres~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|online_adder_s|g_pres\(0));

-- Location: LABCELL_X27_Y2_N0
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\ = SUM(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~18\ = CARRY(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) ) + ( VCC ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	cin => GND,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~18\);

-- Location: LABCELL_X35_Y2_N42
\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\ ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\ & ( 
-- (!\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010111110101111111111111111111110101111101011111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	combout => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\);

-- Location: FF_X27_Y2_N2
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~17_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0));

-- Location: LABCELL_X27_Y2_N3
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\ = SUM(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~18\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~14\ = CARRY(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~18\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~14\);

-- Location: FF_X27_Y2_N5
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~13_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1));

-- Location: LABCELL_X27_Y2_N6
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\ = SUM(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~14\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~10\ = CARRY(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~14\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~10\);

-- Location: FF_X27_Y2_N8
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~9_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2));

-- Location: LABCELL_X27_Y2_N9
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\ = SUM(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~10\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~2\ = CARRY(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(3),
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~10\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~2\);

-- Location: FF_X27_Y2_N10
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~1_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3));

-- Location: LABCELL_X27_Y2_N12
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\ = SUM(( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) ) + ( VCC ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(4),
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~2\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\);

-- Location: FF_X27_Y2_N14
\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|Add0~5_sumout\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr4~combout\,
	ena => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4));

-- Location: IOIBUF_X28_Y0_N1
\data_prv_a_i[20]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(20),
	o => \data_prv_a_i[20]~input_o\);

-- Location: IOIBUF_X22_Y0_N52
\data_prv_a_i[24]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(24),
	o => \data_prv_a_i[24]~input_o\);

-- Location: IOIBUF_X12_Y0_N35
\data_prv_a_i[16]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(16),
	o => \data_prv_a_i[16]~input_o\);

-- Location: IOIBUF_X30_Y0_N35
\data_prv_a_i[28]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(28),
	o => \data_prv_a_i[28]~input_o\);

-- Location: LABCELL_X33_Y2_N27
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( \data_prv_a_i[28]~input_o\ & ( (\data_prv_a_i[20]~input_o\) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( \data_prv_a_i[28]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- ((\data_prv_a_i[16]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[24]~input_o\)) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( !\data_prv_a_i[28]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & \data_prv_a_i[20]~input_o\) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( !\data_prv_a_i[28]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[16]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[24]~input_o\)) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010110101111001000100010001000000101101011110111011101110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datab => \ALT_INV_data_prv_a_i[20]~input_o\,
	datac => \ALT_INV_data_prv_a_i[24]~input_o\,
	datad => \ALT_INV_data_prv_a_i[16]~input_o\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	dataf => \ALT_INV_data_prv_a_i[28]~input_o\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\);

-- Location: IOIBUF_X62_Y0_N52
\data_prv_a_i[23]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(23),
	o => \data_prv_a_i[23]~input_o\);

-- Location: IOIBUF_X30_Y0_N18
\data_prv_a_i[31]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(31),
	o => \data_prv_a_i[31]~input_o\);

-- Location: IOIBUF_X34_Y0_N58
\data_prv_a_i[19]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(19),
	o => \data_prv_a_i[19]~input_o\);

-- Location: IOIBUF_X26_Y0_N58
\data_prv_a_i[27]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(27),
	o => \data_prv_a_i[27]~input_o\);

-- Location: LABCELL_X31_Y2_N48
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\ = ( \data_prv_a_i[19]~input_o\ & ( \data_prv_a_i[27]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\) # 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[23]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[31]~input_o\)))) ) ) ) # ( !\data_prv_a_i[19]~input_o\ & ( 
-- \data_prv_a_i[27]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[23]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[31]~input_o\))))) ) ) ) # ( \data_prv_a_i[19]~input_o\ & ( 
-- !\data_prv_a_i[27]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[23]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[31]~input_o\))))) ) ) ) # ( !\data_prv_a_i[19]~input_o\ & ( 
-- !\data_prv_a_i[27]~input_o\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[23]~input_o\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[31]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000010000010101100011001001110100100110001101111010111010111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datac => \ALT_INV_data_prv_a_i[23]~input_o\,
	datad => \ALT_INV_data_prv_a_i[31]~input_o\,
	datae => \ALT_INV_data_prv_a_i[19]~input_o\,
	dataf => \ALT_INV_data_prv_a_i[27]~input_o\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\);

-- Location: IOIBUF_X12_Y0_N52
\data_prv_a_i[25]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(25),
	o => \data_prv_a_i[25]~input_o\);

-- Location: IOIBUF_X70_Y0_N18
\data_prv_a_i[21]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(21),
	o => \data_prv_a_i[21]~input_o\);

-- Location: IOIBUF_X14_Y0_N1
\data_prv_a_i[29]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(29),
	o => \data_prv_a_i[29]~input_o\);

-- Location: IOIBUF_X16_Y0_N52
\data_prv_a_i[17]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(17),
	o => \data_prv_a_i[17]~input_o\);

-- Location: LABCELL_X31_Y2_N0
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ( \data_prv_a_i[29]~input_o\ ) ) ) # ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ( \data_prv_a_i[25]~input_o\ ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ( \data_prv_a_i[21]~input_o\ ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ( 
-- \data_prv_a_i[17]~input_o\ ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000011111111001100110011001101010101010101010000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_a_i[25]~input_o\,
	datab => \ALT_INV_data_prv_a_i[21]~input_o\,
	datac => \ALT_INV_data_prv_a_i[29]~input_o\,
	datad => \ALT_INV_data_prv_a_i[17]~input_o\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\);

-- Location: IOIBUF_X62_Y0_N35
\data_prv_a_i[22]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(22),
	o => \data_prv_a_i[22]~input_o\);

-- Location: IOIBUF_X80_Y0_N18
\data_prv_a_i[30]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(30),
	o => \data_prv_a_i[30]~input_o\);

-- Location: IOIBUF_X30_Y81_N1
\data_prv_a_i[18]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(18),
	o => \data_prv_a_i[18]~input_o\);

-- Location: IOIBUF_X28_Y0_N52
\data_prv_a_i[26]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(26),
	o => \data_prv_a_i[26]~input_o\);

-- Location: LABCELL_X31_Y2_N18
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\ = ( \data_prv_a_i[18]~input_o\ & ( \data_prv_a_i[26]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\) # 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[22]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[30]~input_o\)))) ) ) ) # ( !\data_prv_a_i[18]~input_o\ & ( 
-- \data_prv_a_i[26]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[22]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[30]~input_o\))))) ) ) ) # ( \data_prv_a_i[18]~input_o\ & ( 
-- !\data_prv_a_i[26]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[22]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[30]~input_o\))))) ) ) ) # ( !\data_prv_a_i[18]~input_o\ & ( 
-- !\data_prv_a_i[26]~input_o\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\data_prv_a_i[22]~input_o\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\data_prv_a_i[30]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000100000101101110110000010100010001101011111011101110101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	datab => \ALT_INV_data_prv_a_i[22]~input_o\,
	datac => \ALT_INV_data_prv_a_i[30]~input_o\,
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datae => \ALT_INV_data_prv_a_i[18]~input_o\,
	dataf => \ALT_INV_data_prv_a_i[26]~input_o\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\);

-- Location: LABCELL_X31_Y2_N33
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\)))) ) ) ) # ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & 
-- (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\)))) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\)))) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~1_combout\ & ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~2_combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~0_combout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~3_combout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100000001000011010011000100111101110000011100110111110001111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~0_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~3_combout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~1_combout\,
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~2_combout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\);

-- Location: IOIBUF_X28_Y0_N35
\data_prv_a_i[13]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(13),
	o => \data_prv_a_i[13]~input_o\);

-- Location: IOIBUF_X28_Y81_N1
\data_prv_a_i[15]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(15),
	o => \data_prv_a_i[15]~input_o\);

-- Location: IOIBUF_X26_Y81_N41
\data_prv_a_i[14]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(14),
	o => \data_prv_a_i[14]~input_o\);

-- Location: IOIBUF_X14_Y0_N18
\data_prv_a_i[12]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(12),
	o => \data_prv_a_i[12]~input_o\);

-- Location: LABCELL_X31_Y2_N54
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~6\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~6_combout\ = ( \data_prv_a_i[14]~input_o\ & ( \data_prv_a_i[12]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\) # 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_a_i[13]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_a_i[15]~input_o\)))) ) ) ) # ( !\data_prv_a_i[14]~input_o\ & ( 
-- \data_prv_a_i[12]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_a_i[13]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_a_i[15]~input_o\))))) ) ) ) # ( \data_prv_a_i[14]~input_o\ & ( 
-- !\data_prv_a_i[12]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_a_i[13]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_a_i[15]~input_o\))))) ) ) ) # ( !\data_prv_a_i[14]~input_o\ & ( 
-- !\data_prv_a_i[12]~input_o\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_a_i[13]~input_o\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_a_i[15]~input_o\))))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001000000010011000111000001111111010000110100111101110011011111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_a_i[13]~input_o\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datad => \ALT_INV_data_prv_a_i[15]~input_o\,
	datae => \ALT_INV_data_prv_a_i[14]~input_o\,
	dataf => \ALT_INV_data_prv_a_i[12]~input_o\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~6_combout\);

-- Location: IOIBUF_X32_Y0_N18
\data_prv_a_i[1]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(1),
	o => \data_prv_a_i[1]~input_o\);

-- Location: IOIBUF_X28_Y81_N52
\data_prv_a_i[0]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(0),
	o => \data_prv_a_i[0]~input_o\);

-- Location: IOIBUF_X12_Y0_N1
\data_prv_a_i[3]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(3),
	o => \data_prv_a_i[3]~input_o\);

-- Location: IOIBUF_X28_Y0_N18
\data_prv_a_i[2]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(2),
	o => \data_prv_a_i[2]~input_o\);

-- Location: LABCELL_X31_Y2_N42
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~8_combout\ = ( \data_prv_a_i[2]~input_o\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & 
-- (\data_prv_a_i[1]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_a_i[3]~input_o\))) ) ) ) # ( !\data_prv_a_i[2]~input_o\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_a_i[1]~input_o\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_a_i[3]~input_o\))) ) ) ) # ( \data_prv_a_i[2]~input_o\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( (\data_prv_a_i[0]~input_o\) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) ) ) ) # ( !\data_prv_a_i[2]~input_o\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & \data_prv_a_i[0]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100001111110011111101000100011101110100010001110111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_a_i[1]~input_o\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datac => \ALT_INV_data_prv_a_i[0]~input_o\,
	datad => \ALT_INV_data_prv_a_i[3]~input_o\,
	datae => \ALT_INV_data_prv_a_i[2]~input_o\,
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~8_combout\);

-- Location: IOIBUF_X26_Y81_N92
\data_prv_a_i[11]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(11),
	o => \data_prv_a_i[11]~input_o\);

-- Location: IOIBUF_X30_Y0_N1
\data_prv_a_i[8]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(8),
	o => \data_prv_a_i[8]~input_o\);

-- Location: IOIBUF_X32_Y0_N52
\data_prv_a_i[9]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(9),
	o => \data_prv_a_i[9]~input_o\);

-- Location: IOIBUF_X24_Y81_N52
\data_prv_a_i[10]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(10),
	o => \data_prv_a_i[10]~input_o\);

-- Location: LABCELL_X31_Y2_N36
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~5_combout\ = ( \data_prv_a_i[10]~input_o\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & 
-- ((\data_prv_a_i[9]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_a_i[11]~input_o\)) ) ) ) # ( !\data_prv_a_i[10]~input_o\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & ((\data_prv_a_i[9]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & (\data_prv_a_i[11]~input_o\)) ) ) ) # ( \data_prv_a_i[10]~input_o\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( (\data_prv_a_i[8]~input_o\) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) ) ) ) # ( !\data_prv_a_i[10]~input_o\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\ & \data_prv_a_i[8]~input_o\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000110000001100001111110011111100010001110111010001000111011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_a_i[11]~input_o\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datac => \ALT_INV_data_prv_a_i[8]~input_o\,
	datad => \ALT_INV_data_prv_a_i[9]~input_o\,
	datae => \ALT_INV_data_prv_a_i[10]~input_o\,
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~5_combout\);

-- Location: IOIBUF_X26_Y0_N41
\data_prv_a_i[4]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(4),
	o => \data_prv_a_i[4]~input_o\);

-- Location: IOIBUF_X32_Y0_N1
\data_prv_a_i[7]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(7),
	o => \data_prv_a_i[7]~input_o\);

-- Location: IOIBUF_X28_Y81_N18
\data_prv_a_i[5]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(5),
	o => \data_prv_a_i[5]~input_o\);

-- Location: IOIBUF_X40_Y0_N52
\data_prv_a_i[6]~input\ : cyclonev_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_data_prv_a_i(6),
	o => \data_prv_a_i[6]~input_o\);

-- Location: LABCELL_X31_Y2_N24
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\ = ( \data_prv_a_i[5]~input_o\ & ( \data_prv_a_i[6]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)) # (\data_prv_a_i[4]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) # 
-- (\data_prv_a_i[7]~input_o\)))) ) ) ) # ( !\data_prv_a_i[5]~input_o\ & ( \data_prv_a_i[6]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)) # 
-- (\data_prv_a_i[4]~input_o\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((\data_prv_a_i[7]~input_o\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) ) ) ) # ( \data_prv_a_i[5]~input_o\ & ( 
-- !\data_prv_a_i[6]~input_o\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (\data_prv_a_i[4]~input_o\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\) # (\data_prv_a_i[7]~input_o\)))) ) ) ) # ( !\data_prv_a_i[5]~input_o\ & ( !\data_prv_a_i[6]~input_o\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & (\data_prv_a_i[4]~input_o\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~9_sumout\ & 
-- (((\data_prv_a_i[7]~input_o\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~13_sumout\)))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0100010000000011011101110000001101000100110011110111011111001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_data_prv_a_i[4]~input_o\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~9_sumout\,
	datac => \ALT_INV_data_prv_a_i[7]~input_o\,
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~13_sumout\,
	datae => \ALT_INV_data_prv_a_i[5]~input_o\,
	dataf => \ALT_INV_data_prv_a_i[6]~input_o\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\);

-- Location: LABCELL_X31_Y2_N12
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~6_combout\) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~8_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~5_combout\))) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\ & ( (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~6_combout\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\) ) ) ) # ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~1_sumout\ & ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~7_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~8_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~5_sumout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~5_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100001111000000000101010100110011000011111111111101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~6_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~8_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~5_combout\,
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~5_sumout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~1_sumout\,
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~7_combout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\);

-- Location: MLABCELL_X28_Y2_N0
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~73\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~73_sumout\ = SUM(( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))) ) + ( !\data_prv_a_i[0]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( !VCC ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~74\ = CARRY(( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))) ) + ( !\data_prv_a_i[0]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( !VCC ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000010011100100111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[0]~input_o\,
	cin => GND,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~73_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~74\);

-- Location: MLABCELL_X28_Y2_N3
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~69\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~69_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[1]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~74\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~70\ = CARRY(( GND ) + ( !\data_prv_a_i[1]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~74\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[1]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~74\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~69_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~70\);

-- Location: MLABCELL_X28_Y2_N6
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~77\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~77_sumout\ = SUM(( !\data_prv_a_i[2]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~70\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~78\ = CARRY(( !\data_prv_a_i[2]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~70\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_a_i[2]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~70\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~77_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~78\);

-- Location: MLABCELL_X28_Y2_N9
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~65\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~65_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[3]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~78\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~66\ = CARRY(( GND ) + ( !\data_prv_a_i[3]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~78\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[3]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~78\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~65_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~66\);

-- Location: MLABCELL_X28_Y2_N12
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~9\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~9_sumout\ = SUM(( !\data_prv_a_i[4]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~66\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~10\ = CARRY(( !\data_prv_a_i[4]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~66\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_a_i[4]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~66\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~9_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~10\);

-- Location: MLABCELL_X28_Y2_N15
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~1\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~1_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[5]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~10\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~2\ = CARRY(( GND ) + ( !\data_prv_a_i[5]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~10\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[5]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~10\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~1_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~2\);

-- Location: MLABCELL_X28_Y2_N18
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~13\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~13_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[6]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~2\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~14\ = CARRY(( GND ) + ( !\data_prv_a_i[6]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~2\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[6]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~2\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~13_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~14\);

-- Location: MLABCELL_X28_Y2_N21
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~5\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~5_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[7]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~14\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~6\ = CARRY(( GND ) + ( !\data_prv_a_i[7]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~14\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[7]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~14\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~5_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~6\);

-- Location: MLABCELL_X28_Y2_N24
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~89\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~89_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[8]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~6\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~90\ = CARRY(( GND ) + ( !\data_prv_a_i[8]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~6\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[8]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~6\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~89_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~90\);

-- Location: MLABCELL_X28_Y2_N27
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~85\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~85_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[9]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~90\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~86\ = CARRY(( GND ) + ( !\data_prv_a_i[9]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~90\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[9]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~90\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~85_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~86\);

-- Location: MLABCELL_X28_Y2_N30
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~93\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~93_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[10]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~86\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~94\ = CARRY(( GND ) + ( !\data_prv_a_i[10]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~86\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110010100011010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	dataf => \ALT_INV_data_prv_a_i[10]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~86\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~93_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~94\);

-- Location: MLABCELL_X28_Y2_N33
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~81\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~81_sumout\ = SUM(( !\data_prv_a_i[11]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~94\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~82\ = CARRY(( !\data_prv_a_i[11]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~94\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011010111001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datad => \ALT_INV_data_prv_a_i[11]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~94\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~81_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~82\);

-- Location: MLABCELL_X28_Y2_N36
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~25_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[12]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~82\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~26\ = CARRY(( GND ) + ( !\data_prv_a_i[12]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~82\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110010100011010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	dataf => \ALT_INV_data_prv_a_i[12]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~82\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~25_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~26\);

-- Location: MLABCELL_X28_Y2_N39
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~17_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[13]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~26\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~18\ = CARRY(( GND ) + ( !\data_prv_a_i[13]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~26\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110010100011010100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	dataf => \ALT_INV_data_prv_a_i[13]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~26\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~17_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~18\);

-- Location: MLABCELL_X28_Y2_N42
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~29_sumout\ = SUM(( !\data_prv_a_i[14]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~18\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~30\ = CARRY(( !\data_prv_a_i[14]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~18\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011010111001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datad => \ALT_INV_data_prv_a_i[14]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~18\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~29_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~30\);

-- Location: MLABCELL_X28_Y2_N45
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~21_sumout\ = SUM(( !\data_prv_a_i[15]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~30\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~22\ = CARRY(( !\data_prv_a_i[15]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~30\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000011010111001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datad => \ALT_INV_data_prv_a_i[15]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~30\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~21_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~22\);

-- Location: MLABCELL_X28_Y2_N48
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~105\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~105_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[16]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~22\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~106\ = CARRY(( GND ) + ( !\data_prv_a_i[16]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~22\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[16]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~22\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~105_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~106\);

-- Location: MLABCELL_X28_Y2_N51
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~101\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~101_sumout\ = SUM(( !\data_prv_a_i[17]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~106\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~102\ = CARRY(( !\data_prv_a_i[17]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~106\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_a_i[17]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~106\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~101_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~102\);

-- Location: MLABCELL_X28_Y2_N54
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~109\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~109_sumout\ = SUM(( !\data_prv_a_i[18]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~102\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~110\ = CARRY(( !\data_prv_a_i[18]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~102\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000010011111011000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datad => \ALT_INV_data_prv_a_i[18]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~102\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~109_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~110\);

-- Location: MLABCELL_X28_Y2_N57
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~97\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~97_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[19]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~110\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~98\ = CARRY(( GND ) + ( !\data_prv_a_i[19]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\)) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\))))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~110\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000110110000010011100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	dataf => \ALT_INV_data_prv_a_i[19]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~110\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~97_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~98\);

-- Location: MLABCELL_X28_Y1_N0
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~41\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~41_sumout\ = SUM(( !\data_prv_a_i[20]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~98\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~42\ = CARRY(( !\data_prv_a_i[20]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~98\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000001101111100100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datad => \ALT_INV_data_prv_a_i[20]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~98\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~41_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~42\);

-- Location: MLABCELL_X28_Y1_N3
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~33\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~33_sumout\ = SUM(( !\data_prv_a_i[21]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~42\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~34\ = CARRY(( !\data_prv_a_i[21]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~42\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000001101111100100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datad => \ALT_INV_data_prv_a_i[21]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~42\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~33_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~34\);

-- Location: MLABCELL_X28_Y1_N6
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~45\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~45_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[22]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~34\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~46\ = CARRY(( GND ) + ( !\data_prv_a_i[22]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~34\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111001000001101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	dataf => \ALT_INV_data_prv_a_i[22]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~34\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~45_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~46\);

-- Location: MLABCELL_X28_Y1_N9
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~37\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~37_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[23]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~46\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~38\ = CARRY(( GND ) + ( !\data_prv_a_i[23]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~46\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111001000001101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	dataf => \ALT_INV_data_prv_a_i[23]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~46\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~37_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~38\);

-- Location: MLABCELL_X28_Y1_N12
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~121\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~121_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[24]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~38\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~122\ = CARRY(( GND ) + ( !\data_prv_a_i[24]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~38\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111001000001101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	dataf => \ALT_INV_data_prv_a_i[24]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~38\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~121_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~122\);

-- Location: MLABCELL_X28_Y1_N15
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~117\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~117_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[25]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~122\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~118\ = CARRY(( GND ) + ( !\data_prv_a_i[25]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~122\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111001000001101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	dataf => \ALT_INV_data_prv_a_i[25]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~122\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~117_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~118\);

-- Location: MLABCELL_X28_Y1_N18
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~125\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~125_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[26]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~118\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~126\ = CARRY(( GND ) + ( !\data_prv_a_i[26]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~118\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111001000001101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	dataf => \ALT_INV_data_prv_a_i[26]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~118\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~125_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~126\);

-- Location: MLABCELL_X28_Y1_N21
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~113\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~113_sumout\ = SUM(( !\data_prv_a_i[27]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~126\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~114\ = CARRY(( !\data_prv_a_i[27]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~126\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000001101111100100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datad => \ALT_INV_data_prv_a_i[27]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~126\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~113_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~114\);

-- Location: MLABCELL_X28_Y1_N24
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~57\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~57_sumout\ = SUM(( !\data_prv_a_i[28]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~114\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~58\ = CARRY(( !\data_prv_a_i[28]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~114\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000001101111100100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datad => \ALT_INV_data_prv_a_i[28]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~114\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~57_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~58\);

-- Location: MLABCELL_X28_Y1_N27
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~49\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~49_sumout\ = SUM(( !\data_prv_a_i[29]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~58\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~50\ = CARRY(( !\data_prv_a_i[29]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~58\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000001101111100100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datad => \ALT_INV_data_prv_a_i[29]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~58\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~49_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~50\);

-- Location: MLABCELL_X28_Y1_N30
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~61\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~61_sumout\ = SUM(( !\data_prv_a_i[30]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~50\ ))
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~62\ = CARRY(( !\data_prv_a_i[30]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( GND ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~50\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100000000000000000001101111100100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datad => \ALT_INV_data_prv_a_i[30]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~50\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~61_sumout\,
	cout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~62\);

-- Location: MLABCELL_X28_Y1_N33
\signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~53\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~53_sumout\ = SUM(( GND ) + ( !\data_prv_a_i[31]~input_o\ $ (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)))) ) + ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~62\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111001000001101100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	dataf => \ALT_INV_data_prv_a_i[31]~input_o\,
	cin => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~62\,
	sumout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~53_sumout\);

-- Location: LABCELL_X35_Y2_N51
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\ = ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\ & ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ & ( 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) # (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\) ) ) ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\ & ( 
-- \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ ) ) # ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\ & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ & ( 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sEndOfConv0~DUPLICATE_q\ & ((\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sStartConv~q\) # 
-- (\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sWaitConv~q\))) ) ) ) # ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sInit~q\ & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|state_s.sConversion~q\ 
-- ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111000001010000111111111111111111110101010111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sWaitConv~q\,
	datac => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sEndOfConv0~DUPLICATE_q\,
	datad => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sStartConv~q\,
	datae => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_state_s.sConversion~q\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\);

-- Location: FF_X28_Y1_N35
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~53_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(31));

-- Location: FF_X28_Y1_N28
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~49_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(29));

-- Location: FF_X28_Y1_N43
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~61_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(30));

-- Location: FF_X28_Y1_N22
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~113_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(27));

-- Location: FF_X28_Y1_N53
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~125_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(26));

-- Location: FF_X28_Y1_N17
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~117_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(25));

-- Location: FF_X28_Y1_N37
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~121_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(24));

-- Location: MLABCELL_X28_Y1_N42
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(24) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2))))))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(25)))))) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(26) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2))))))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(27)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000101001011111000110110001101101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(27),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(26),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(25),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(24),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\);

-- Location: FF_X28_Y1_N25
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~57_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(28));

-- Location: MLABCELL_X28_Y1_N36
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\ & 
-- ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(28)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(29)))))) ) ) # ( 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(30)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~29_combout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(31)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000000000001111000000000000111111111111001100111111111101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(31),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(29),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(30),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~29_combout\,
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(28),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\);

-- Location: FF_X27_Y2_N35
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~1_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(5));

-- Location: FF_X27_Y2_N56
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~13_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(6));

-- Location: FF_X27_Y2_N25
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~5_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(7));

-- Location: LABCELL_X27_Y2_N30
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[3]~feeder_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~65_sumout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add1~65_sumout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[3]~feeder_combout\);

-- Location: FF_X27_Y2_N32
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[3]~feeder_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(3));

-- Location: FF_X27_Y2_N20
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~77_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(2));

-- Location: FF_X28_Y2_N4
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~69_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(1));

-- Location: FF_X27_Y2_N38
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~73_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(0));

-- Location: LABCELL_X27_Y2_N42
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(0) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2))))))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(1)))))) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(2) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2))))))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(3)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000101001011111000110110001101101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(3),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(2),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(1),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(0),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\);

-- Location: FF_X28_Y2_N13
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~9_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(4));

-- Location: LABCELL_X27_Y2_N48
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\))))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\ & 
-- ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(4)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(5)))))) ) ) # ( 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(6))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~17_combout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(7))))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000001100000011000000110000001111011101110111011100110011111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(5),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(6),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(7),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~17_combout\,
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(4),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\);

-- Location: FF_X28_Y2_N47
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~21_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(15));

-- Location: FF_X27_Y2_N50
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~29_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(14));

-- Location: FF_X28_Y2_N41
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~17_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(13));

-- Location: FF_X27_Y2_N43
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~81_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(11));

-- Location: FF_X28_Y2_N32
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~93_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(10));

-- Location: FF_X28_Y2_N28
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~85_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(9));

-- Location: FF_X28_Y2_N26
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~89_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(8));

-- Location: LABCELL_X27_Y2_N24
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(8) & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(9)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(10) & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(11)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000101001010101000110110101010101011111010101010001101101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(11),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(10),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(9),
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(8),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\);

-- Location: FF_X28_Y2_N38
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~25_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(12));

-- Location: LABCELL_X27_Y2_N36
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\ & 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(12))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(13))))))) ) ) # ( 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\))))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(14)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~21_combout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(15)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000001100000011000000110000001111001100111111111101110111011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(15),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(14),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(13),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~21_combout\,
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(12),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\);

-- Location: FF_X28_Y2_N2
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~37_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(23));

-- Location: FF_X28_Y1_N7
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~45_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(22));

-- Location: FF_X28_Y2_N44
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	asdata => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~33_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sload => VCC,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(21));

-- Location: FF_X28_Y2_N59
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~97_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(19));

-- Location: FF_X28_Y2_N55
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~109_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(18));

-- Location: FF_X28_Y2_N53
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~101_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(17));

-- Location: FF_X28_Y2_N50
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~105_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(16));

-- Location: LABCELL_X27_Y2_N18
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(16) & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & 
-- ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(17)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(18) & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(0) & ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(19)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000101001010101000110110101010101011111010101010001101101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(0),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(19),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(18),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(17),
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(16),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\);

-- Location: FF_X28_Y1_N1
\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|Add1~41_sumout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres[31]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(20));

-- Location: LABCELL_X27_Y2_N54
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\ & 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(20))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(21))))))) ) ) # ( 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(1) & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & ((((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\))))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(2) & (((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(22)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~25_combout\ & (\signed_digit_serializer_a_s|get_in_MSB_order_s|reg_pres\(23)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "0000001100000011000000110000001111001100111111111101110111011101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(23),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(2),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(22),
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(21),
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(1),
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~25_combout\,
	datag => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_reg_pres\(20),
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\);

-- Location: MLABCELL_X28_Y1_N48
\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4)) # ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\)))) ) ) ) # ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\ & ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & 
-- (((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\)) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\))) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\ & ( 
-- !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) & 
-- ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\)))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4)) # 
-- ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\)))) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~4_combout\ & ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~8_combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (!\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~0_combout\)))) # 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(3) & (\signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|cpt_s\(4) & (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~12_combout\))) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000110001001010001011100110100100011101010110110011111101111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(3),
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|decompteur_s|ALT_INV_cpt_s\(4),
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~12_combout\,
	datad => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~0_combout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~4_combout\,
	dataf => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~8_combout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\);

-- Location: MLABCELL_X28_Y3_N33
\online_adder_cell_s|online_adder_s|fa_adder_1|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|online_adder_s|fa_adder_1|Mux1~0_combout\ = ( \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ & ( 
-- (!\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\) ) ) ) # ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ & ( (!\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ & !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\) ) ) ) # ( 
-- \signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ & ( (!\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ & 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\) ) ) ) # ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ & ( 
-- (!\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\) ) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000001010000010100000101010100000101000000000101000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~DUPLICATE_q\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\,
	datae => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\,
	combout => \online_adder_cell_s|online_adder_s|fa_adder_1|Mux1~0_combout\);

-- Location: FF_X28_Y3_N34
\online_adder_cell_s|online_adder_s|g_pres[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|online_adder_s|fa_adder_1|Mux1~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\,
	ena => \online_adder_cell_s|online_adder_s|w_pres~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|online_adder_s|g_pres\(1));

-- Location: MLABCELL_X28_Y3_N24
\signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~0_combout\ = ( \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)) ) ) ) # ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ & ( 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & ((\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~9_combout\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Add0~17_sumout\ & 
-- (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux1~4_combout\)) ) ) ) # ( \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\ & ( !\signed_digit_serializer_a_s|uc_signed_digit_serializer_s|WideOr3~combout\ ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000111111111111111100011011000110110001101100011011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Add0~17_sumout\,
	datab => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~4_combout\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux1~9_combout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	dataf => \signed_digit_serializer_a_s|uc_signed_digit_serializer_s|ALT_INV_WideOr3~combout\,
	combout => \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~0_combout\);

-- Location: FF_X28_Y3_N26
\signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\);

-- Location: MLABCELL_X28_Y3_N42
\online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_combout\ = ( !\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\ & ( !\online_adder_cell_s|online_adder_s|g_pres\(0) $ (!\online_adder_cell_s|online_adder_s|g_pres\(1) $ 
-- ((((!\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\) # (\signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\)) # (\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\)))) ) ) # ( 
-- \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\ & ( !\online_adder_cell_s|online_adder_s|g_pres\(0) $ (((!\online_adder_cell_s|online_adder_s|g_pres\(1) $ (((\signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\) # 
-- (\online_adder_cell_s|uc_adder_s|state_s.sEnd1~DUPLICATE_q\)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "on",
	lut_mask => "1010101001101010011010100110101001010101100101011001010110010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(0),
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~DUPLICATE_q\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\,
	datae => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\,
	dataf => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(1),
	datag => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	combout => \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_combout\);

-- Location: MLABCELL_X28_Y3_N48
\online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_wirecell\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_wirecell_combout\ = ( !\online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_combout\ )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111100000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \online_adder_cell_s|online_adder_s|fa_adder_2|ALT_INV_Mux1~0_combout\,
	combout => \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_wirecell_combout\);

-- Location: FF_X28_Y3_N49
\online_adder_cell_s|online_adder_s|w_pres\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \online_adder_cell_s|online_adder_s|fa_adder_2|Mux1~0_wirecell_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sInit~q\,
	ena => \online_adder_cell_s|online_adder_s|w_pres~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \online_adder_cell_s|online_adder_s|w_pres~q\);

-- Location: MLABCELL_X28_Y1_N54
\online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ = ( \signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ & ( (!\online_adder_cell_s|uc_adder_s|state_s.sEnd1~q\ & 
-- ((!\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\ & ((!\signed_digit_serializer_b_s|get_in_MSB_order_s|sign_bit_pres~q\))) # (\signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\ & 
-- (!\signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\)))) ) ) # ( !\signed_digit_serializer_b_s|get_in_MSB_order_s|Mux0~16_combout\ & ( (!\signed_digit_serializer_a_s|get_in_MSB_order_s|sign_bit_pres~q\ & 
-- (!\online_adder_cell_s|uc_adder_s|state_s.sEnd1~q\ & \signed_digit_serializer_a_s|get_in_MSB_order_s|Mux0~16_combout\)) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000100000001000000010000000100011001000000010001100100000001000",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	datab => \online_adder_cell_s|uc_adder_s|ALT_INV_state_s.sEnd1~q\,
	datac => \signed_digit_serializer_a_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\,
	datad => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_sign_bit_pres~q\,
	dataf => \signed_digit_serializer_b_s|get_in_MSB_order_s|ALT_INV_Mux0~16_combout\,
	combout => \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\);

-- Location: MLABCELL_X25_Y1_N36
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_fut[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_fut[0]~0_combout\ = ( \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ & 
-- (!\online_adder_cell_s|online_adder_s|w_pres~q\ $ (((!\online_adder_cell_s|online_adder_s|g_pres\(0)) # (!\online_adder_cell_s|online_adder_s|g_pres\(1)))))) ) ) # ( !\online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( 
-- (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ & (!\online_adder_cell_s|online_adder_s|w_pres~q\ $ (((!\online_adder_cell_s|online_adder_s|g_pres\(0) & !\online_adder_cell_s|online_adder_s|g_pres\(1)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0001010101000000000101010100000000000001010101000000000101010100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	datab => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(0),
	datac => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(1),
	datad => \online_adder_cell_s|online_adder_s|ALT_INV_w_pres~q\,
	dataf => \online_adder_cell_s|online_adder_s|fa_adder_1|ALT_INV_Mux0~0_combout\,
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_fut[0]~0_combout\);

-- Location: FF_X25_Y1_N37
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_fut[0]~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(0));

-- Location: LABCELL_X29_Y1_N36
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[1]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(0)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(0),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[1]~feeder_combout\);

-- Location: MLABCELL_X25_Y1_N27
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_fut[0]~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_fut[0]~0_combout\ = ( \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ & 
-- (!\online_adder_cell_s|online_adder_s|w_pres~q\ $ (((\online_adder_cell_s|online_adder_s|g_pres\(1) & \online_adder_cell_s|online_adder_s|g_pres\(0)))))) ) ) # ( !\online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( 
-- (\signed_digit_deserializer_s|uc_signed_digit_deserializer_s|state_s.sInit~q\ & (!\online_adder_cell_s|online_adder_s|w_pres~q\ $ (((\online_adder_cell_s|online_adder_s|g_pres\(0)) # (\online_adder_cell_s|online_adder_s|g_pres\(1)))))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000010010011000000001001001100000000110010010000000011001001",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(1),
	datab => \online_adder_cell_s|online_adder_s|ALT_INV_w_pres~q\,
	datac => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(0),
	datad => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	dataf => \online_adder_cell_s|online_adder_s|fa_adder_1|ALT_INV_Mux0~0_combout\,
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_fut[0]~0_combout\);

-- Location: FF_X25_Y1_N28
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[0]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_fut[0]~0_combout\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(0));

-- Location: LABCELL_X29_Y1_N51
\signed_digit_deserializer_s|on_fly_conv_s|Equal2~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\ = ( \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( (!\online_adder_cell_s|online_adder_s|w_pres~q\ & (\online_adder_cell_s|online_adder_s|g_pres\(1) & 
-- \online_adder_cell_s|online_adder_s|g_pres\(0))) ) ) # ( !\online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( (!\online_adder_cell_s|online_adder_s|w_pres~q\ & ((\online_adder_cell_s|online_adder_s|g_pres\(0)) # 
-- (\online_adder_cell_s|online_adder_s|g_pres\(1)))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101010101010000010101010101000000000000010100000000000001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|online_adder_s|ALT_INV_w_pres~q\,
	datac => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(1),
	datad => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(0),
	dataf => \online_adder_cell_s|online_adder_s|fa_adder_1|ALT_INV_Mux0~0_combout\,
	combout => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\);

-- Location: FF_X29_Y1_N38
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[1]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(0),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(1));

-- Location: LABCELL_X29_Y1_N54
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[2]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(1)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(1),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[2]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N57
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(0) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(0),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N3
\signed_digit_deserializer_s|on_fly_conv_s|Equal1~0\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\ = ( \online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( (\online_adder_cell_s|online_adder_s|w_pres~q\ & ((!\online_adder_cell_s|online_adder_s|g_pres\(1)) # 
-- (!\online_adder_cell_s|online_adder_s|g_pres\(0)))) ) ) # ( !\online_adder_cell_s|online_adder_s|fa_adder_1|Mux0~0_combout\ & ( (!\online_adder_cell_s|online_adder_s|g_pres\(1) & (\online_adder_cell_s|online_adder_s|w_pres~q\ & 
-- !\online_adder_cell_s|online_adder_s|g_pres\(0))) ) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000101000000000000010100000000000001111000010100000111100001010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(1),
	datac => \online_adder_cell_s|online_adder_s|ALT_INV_w_pres~q\,
	datad => \online_adder_cell_s|online_adder_s|ALT_INV_g_pres\(0),
	dataf => \online_adder_cell_s|online_adder_s|fa_adder_1|ALT_INV_Mux0~0_combout\,
	combout => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\);

-- Location: FF_X27_Y1_N58
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(0),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N55
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[2]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(2));

-- Location: LABCELL_X29_Y1_N42
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(2)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(2),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~feeder_combout\);

-- Location: FF_X27_Y1_N59
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[1]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(0),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(1));

-- Location: LABCELL_X27_Y1_N48
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[2]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[2]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(1)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(1),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[2]~feeder_combout\);

-- Location: FF_X27_Y1_N49
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[2]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[2]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(1),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(2));

-- Location: FF_X29_Y1_N44
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(2),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(3));

-- Location: LABCELL_X29_Y1_N12
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(3)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(3),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~feeder_combout\);

-- Location: MLABCELL_X25_Y1_N0
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(2) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(2),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~feeder_combout\);

-- Location: FF_X25_Y1_N1
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(2),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N13
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N14
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(4));

-- Location: LABCELL_X29_Y1_N0
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[5]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(4)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(4),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[5]~feeder_combout\);

-- Location: FF_X25_Y1_N2
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[3]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(2),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(3));

-- Location: MLABCELL_X25_Y1_N30
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(3)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(3),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~feeder_combout\);

-- Location: FF_X29_Y1_N43
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(2),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~DUPLICATE_q\);

-- Location: FF_X25_Y1_N31
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N1
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[5]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(5));

-- Location: LABCELL_X29_Y1_N33
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(5) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(5),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~feeder_combout\);

-- Location: FF_X25_Y1_N32
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[4]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[3]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(4));

-- Location: MLABCELL_X25_Y1_N51
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(4)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(4),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~feeder_combout\);

-- Location: FF_X25_Y1_N52
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N34
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N35
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(6));

-- Location: LABCELL_X29_Y1_N30
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[7]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(6)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(6),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[7]~feeder_combout\);

-- Location: FF_X25_Y1_N53
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[5]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[4]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(5));

-- Location: MLABCELL_X25_Y1_N6
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(5)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(5),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~feeder_combout\);

-- Location: FF_X25_Y1_N7
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(5),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N31
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[7]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(7));

-- Location: LABCELL_X30_Y1_N48
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[8]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(7) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(7),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[8]~feeder_combout\);

-- Location: FF_X25_Y1_N8
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[6]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(5),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(6));

-- Location: MLABCELL_X25_Y1_N9
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[7]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[7]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(6)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(6),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[7]~feeder_combout\);

-- Location: FF_X25_Y1_N10
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[7]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[7]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[6]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(7));

-- Location: FF_X30_Y1_N49
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[8]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(7),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(8));

-- Location: LABCELL_X30_Y1_N33
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(8)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(8),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N51
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[8]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[8]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(7)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(7),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[8]~feeder_combout\);

-- Location: FF_X27_Y1_N52
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[8]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[8]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(7),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(8));

-- Location: FF_X30_Y1_N34
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(8),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~DUPLICATE_q\);

-- Location: FF_X30_Y1_N35
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(8),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(9));

-- Location: LABCELL_X30_Y1_N51
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(9)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(9),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N27
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[9]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[9]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(8)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(8),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[9]~feeder_combout\);

-- Location: FF_X27_Y1_N28
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[9]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[9]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(8),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(9));

-- Location: FF_X30_Y1_N52
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(9),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~DUPLICATE_q\);

-- Location: FF_X30_Y1_N53
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(9),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(10));

-- Location: LABCELL_X30_Y1_N0
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[11]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[11]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(10)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(10),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[11]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N39
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[10]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[10]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(9) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(9),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[10]~feeder_combout\);

-- Location: FF_X27_Y1_N40
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[10]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[10]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[9]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(10));

-- Location: FF_X30_Y1_N1
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[11]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(10),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(11));

-- Location: LABCELL_X29_Y1_N39
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(11) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(11),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N12
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(10)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(10),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~feeder_combout\);

-- Location: FF_X27_Y1_N13
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N40
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N41
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(12));

-- Location: LABCELL_X29_Y1_N24
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(12) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(12),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~feeder_combout\);

-- Location: FF_X27_Y1_N14
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[11]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[10]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(11));

-- Location: LABCELL_X27_Y1_N33
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[12]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[12]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(11)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(11),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[12]~feeder_combout\);

-- Location: FF_X27_Y1_N35
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[12]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[12]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(11),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(12));

-- Location: FF_X29_Y1_N25
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(12),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N26
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(12),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(13));

-- Location: LABCELL_X29_Y1_N21
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[14]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(13)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(13),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[14]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N18
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(12)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(12),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~feeder_combout\);

-- Location: FF_X27_Y1_N19
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N22
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[14]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(14));

-- Location: LABCELL_X30_Y1_N30
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[15]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(14)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(14),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[15]~feeder_combout\);

-- Location: FF_X27_Y1_N20
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[13]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[12]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(13));

-- Location: LABCELL_X27_Y1_N21
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[14]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[14]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(13)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(13),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[14]~feeder_combout\);

-- Location: FF_X27_Y1_N23
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[14]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[14]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[13]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(14));

-- Location: FF_X30_Y1_N31
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[15]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(14),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(15));

-- Location: LABCELL_X30_Y1_N42
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[16]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[16]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(15) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(15),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[16]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N15
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[15]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[15]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(14)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(14),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[15]~feeder_combout\);

-- Location: FF_X27_Y1_N16
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[15]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[15]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(14),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(15));

-- Location: FF_X30_Y1_N43
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[16]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(15),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(16));

-- Location: LABCELL_X30_Y1_N3
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(16)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(16),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~feeder_combout\);

-- Location: MLABCELL_X25_Y1_N33
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(15)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(15),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~feeder_combout\);

-- Location: FF_X25_Y1_N34
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(15),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~DUPLICATE_q\);

-- Location: FF_X30_Y1_N4
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~DUPLICATE_q\);

-- Location: FF_X30_Y1_N5
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(17));

-- Location: LABCELL_X30_Y1_N45
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[18]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[18]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(17)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(17),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[18]~feeder_combout\);

-- Location: FF_X25_Y1_N35
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[16]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(15),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(16));

-- Location: MLABCELL_X25_Y1_N12
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[17]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[17]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(16)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(16),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[17]~feeder_combout\);

-- Location: FF_X25_Y1_N13
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[17]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[17]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(16),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(17));

-- Location: FF_X30_Y1_N46
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[18]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(17),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(18));

-- Location: LABCELL_X30_Y1_N15
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(18)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(18),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~feeder_combout\);

-- Location: MLABCELL_X25_Y1_N57
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(17) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(17),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~feeder_combout\);

-- Location: FF_X25_Y1_N58
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~DUPLICATE_q\);

-- Location: FF_X30_Y1_N16
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~DUPLICATE_q\);

-- Location: FF_X30_Y1_N17
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(19));

-- Location: LABCELL_X30_Y1_N12
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[20]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[20]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(19) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(19),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[20]~feeder_combout\);

-- Location: FF_X25_Y1_N59
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[18]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[17]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(18));

-- Location: MLABCELL_X28_Y1_N57
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[19]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[19]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(18) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(18),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[19]~feeder_combout\);

-- Location: FF_X28_Y1_N58
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[19]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[19]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(18),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(19));

-- Location: FF_X30_Y1_N13
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[20]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(19),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(20));

-- Location: LABCELL_X30_Y1_N9
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(20)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(20),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N36
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[20]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[20]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(19)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(19),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[20]~feeder_combout\);

-- Location: FF_X27_Y1_N37
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[20]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[20]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[19]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(20));

-- Location: FF_X30_Y1_N10
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(20),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~DUPLICATE_q\);

-- Location: FF_X30_Y1_N11
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(20),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(21));

-- Location: LABCELL_X30_Y1_N6
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[22]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[22]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(21)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(21),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[22]~feeder_combout\);

-- Location: MLABCELL_X25_Y1_N24
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[21]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[21]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(20) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(20),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[21]~feeder_combout\);

-- Location: FF_X25_Y1_N25
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[21]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[21]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(20),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(21));

-- Location: FF_X30_Y1_N7
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[22]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(21),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(22));

-- Location: LABCELL_X29_Y1_N27
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[23]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[23]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(22) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(22),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[23]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N24
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[22]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[22]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(21)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(21),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[22]~feeder_combout\);

-- Location: FF_X27_Y1_N25
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[22]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[22]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[21]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(22));

-- Location: FF_X29_Y1_N29
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[23]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(22),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(23));

-- Location: LABCELL_X29_Y1_N3
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[24]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[24]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(23)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(23),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[24]~feeder_combout\);

-- Location: MLABCELL_X25_Y1_N42
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[23]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[23]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(22) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(22),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[23]~feeder_combout\);

-- Location: FF_X25_Y1_N43
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[23]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[23]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(22),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(23));

-- Location: FF_X29_Y1_N4
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[24]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(23),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(24));

-- Location: LABCELL_X29_Y1_N57
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[25]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[25]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(24)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(24),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[25]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N0
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(23) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(23),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~feeder_combout\);

-- Location: FF_X27_Y1_N1
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(23),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N58
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[25]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(25));

-- Location: LABCELL_X29_Y1_N48
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[26]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[26]~feeder_combout\ = ( \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(25) )

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000011111111111111111111111111111111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataf => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(25),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[26]~feeder_combout\);

-- Location: FF_X27_Y1_N2
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[24]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(23),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(24));

-- Location: LABCELL_X27_Y1_N30
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(24)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(24),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~feeder_combout\);

-- Location: FF_X27_Y1_N31
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(24),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N50
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[26]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(26));

-- Location: LABCELL_X29_Y1_N15
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[27]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[27]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(26)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(26),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[27]~feeder_combout\);

-- Location: FF_X27_Y1_N32
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[25]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(24),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(25));

-- Location: LABCELL_X27_Y1_N42
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(25)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(25),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~feeder_combout\);

-- Location: FF_X27_Y1_N43
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(25),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(26));

-- Location: FF_X29_Y1_N16
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[27]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(26),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(27));

-- Location: LABCELL_X29_Y1_N9
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(27)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(27),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~feeder_combout\);

-- Location: FF_X27_Y1_N44
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(25),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~DUPLICATE_q\);

-- Location: LABCELL_X27_Y1_N45
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[27]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[27]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[26]~DUPLICATE_q\

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres[26]~DUPLICATE_q\,
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[27]~feeder_combout\);

-- Location: FF_X27_Y1_N46
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[27]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[27]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(26),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(27));

-- Location: FF_X29_Y1_N10
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(27),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N11
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(27),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(28));

-- Location: LABCELL_X29_Y1_N6
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[29]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[29]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(28)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(28),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[29]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N54
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(27)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(27),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~feeder_combout\);

-- Location: FF_X27_Y1_N55
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(27),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N7
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[29]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(29));

-- Location: LABCELL_X29_Y1_N45
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(29)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000111100001111000011110000111100001111000011110000111100001111",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datac => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(29),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~feeder_combout\);

-- Location: FF_X27_Y1_N56
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[28]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(27),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(28));

-- Location: LABCELL_X27_Y1_N9
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[29]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[29]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(28)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(28),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[29]~feeder_combout\);

-- Location: FF_X27_Y1_N11
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[29]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[29]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[28]~DUPLICATE_q\,
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(29));

-- Location: FF_X29_Y1_N46
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~DUPLICATE\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(29),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~DUPLICATE_q\);

-- Location: FF_X29_Y1_N47
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[30]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(29),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(30));

-- Location: LABCELL_X29_Y1_N18
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[31]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[31]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(30)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_q_reg_pres\(30),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[31]~feeder_combout\);

-- Location: LABCELL_X27_Y1_N6
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[30]~feeder\ : cyclonev_lcell_comb
-- Equation(s):
-- \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[30]~feeder_combout\ = \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(29)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0011001100110011001100110011001100110011001100110011001100110011",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \signed_digit_deserializer_s|on_fly_conv_s|ALT_INV_qm_reg_pres\(29),
	combout => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[30]~feeder_combout\);

-- Location: FF_X27_Y1_N7
\signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[30]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres[30]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(29),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal1~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(30));

-- Location: FF_X29_Y1_N19
\signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[31]\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk_i~inputCLKENA0_outclk\,
	d => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres[31]~feeder_combout\,
	asdata => \signed_digit_deserializer_s|on_fly_conv_s|qm_reg_pres\(30),
	clrn => \ALT_INV_rst_i~inputCLKENA0_outclk\,
	sclr => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|ALT_INV_state_s.sInit~q\,
	sload => \signed_digit_deserializer_s|on_fly_conv_s|Equal2~0_combout\,
	ena => \signed_digit_deserializer_s|uc_signed_digit_deserializer_s|Selector2~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \signed_digit_deserializer_s|on_fly_conv_s|q_reg_pres\(31));

-- Location: MLABCELL_X87_Y3_N0
\~QUARTUS_CREATED_GND~I\ : cyclonev_lcell_comb
-- Equation(s):

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0000000000000000000000000000000000000000000000000000000000000000",
	shared_arith => "off")
-- pragma translate_on
;
END structure;


