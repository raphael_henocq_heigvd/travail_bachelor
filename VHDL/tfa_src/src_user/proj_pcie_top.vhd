------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : proj_pcie_top.vhd
-- Author               : Convers Anthony
-- Date                 : 21.04.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design (top for vc707 board)
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.PIO_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity proj_pcie_top is

  port (
    pci_exp_txp       : out std_logic_vector(7 downto 0);
    pci_exp_txn       : out std_logic_vector(7 downto 0);
    pci_exp_rxp       : in  std_logic_vector(7 downto 0);
    pci_exp_rxn       : in  std_logic_vector(7 downto 0);

    led_0             : out std_logic;
    led_1             : out std_logic;
    led_2             : out std_logic;
    led_3             : out std_logic;

    Emcclk            : in  std_logic;

    sys_clk_p         : in  std_logic;
    sys_clk_n         : in  std_logic;
    sys_rst_n         : in  std_logic

    );

end proj_pcie_top;

architecture behaviour of proj_pcie_top is

--| Types declarations     |--------------------------------------------------------------

--| Constants declarations |--------------------------------------------------------------
  constant REF_CLK_FREQ      : integer := 0;
  constant C_DATA_WIDTH      : integer := 128;
  constant KEEP_WIDTH        : integer := 16;
  constant TCQ               : integer := 1;
  constant USER_CLK_FREQ     : integer := 4;
  constant USER_CLK2_DIV2    : string  := "TRUE";

  constant USERCLK2_FREQ     : integer := 3;
   -- localparam USERCLK2_FREQ     = (USER_CLK2_DIV2 == "TRUE") ? (USER_CLK_FREQ == 4) ? 3 : (USER_CLK_FREQ == 3) ? 2 : USER_CLK_FREQ: USER_CLK_FREQ;

--| Signals declarations   |--------------------------------------------------------------

attribute keep : string;

--attribute keep: boolean;
signal emcclk_s   : std_logic;
attribute keep of emcclk_s: signal is "true";


 signal pipe_mmcm_rst_n            : std_logic;

 signal user_clk                   : std_logic;
 signal user_reset                 : std_logic;
 signal user_lnk_up                : std_logic;


 signal s_axis_tx_tready           : std_logic;
 signal s_axis_tx_tuser            : std_logic_vector(3 downto 0);
 signal s_axis_tx_tdata            : std_logic_vector(C_DATA_WIDTH-1 downto 0);
 signal s_axis_tx_tkeep            : std_logic_vector(KEEP_WIDTH-1 downto 0);
 signal s_axis_tx_tlast            : std_logic;
 signal s_axis_tx_tvalid           : std_logic;


 signal m_axis_rx_tdata            : std_logic_vector(C_DATA_WIDTH-1 downto 0);
 attribute keep of m_axis_rx_tdata: signal is "true";
 signal m_axis_rx_tkeep            : std_logic_vector(KEEP_WIDTH-1 downto 0);
 signal m_axis_rx_tlast            : std_logic;
 signal m_axis_rx_tvalid           : std_logic;
 signal m_axis_rx_tready           : std_logic;
 signal m_axis_rx_tuser            : std_logic_vector(21 downto 0);

 signal tx_cfg_gnt                 : std_logic;
 signal rx_np_ok                   : std_logic;
 signal rx_np_req                  : std_logic;
 signal cfg_turnoff_ok             : std_logic;
 signal cfg_trn_pending            : std_logic;
 signal cfg_pm_halt_aspm_l0s       : std_logic;
 signal cfg_pm_halt_aspm_l1        : std_logic;
 signal cfg_pm_force_state_en      : std_logic;
 signal cfg_pm_force_state         : std_logic_vector(1 downto 0);
 signal cfg_pm_wake                : std_logic;
 signal cfg_dsn                    : std_logic_vector(63 downto 0);


 signal fc_sel                     : std_logic_vector(2 downto 0);
 
 
 --------- TO DO Modify
 signal overlay_data_size_s : std_logic_vector(31 downto 0);


-- ---------------------------------------------------------
-- -- DDR3
-- ---------------------------------------------------------

--  signal ddr_in_s  : ddr2mem_t;
--  signal ddr_out_s : mem2ddr_t;

--  signal app_addr_s                  :     std_logic_vector(31 downto 0);
--  signal app_cmd_s                   :     std_logic_vector(2 downto 0);
--  signal app_en_s                    :     std_logic;
--  signal app_wdf_data_s              :     std_logic_vector(511 downto 0);
--  signal app_wdf_end_s               :     std_logic;
--  signal app_wdf_mask_s              :     std_logic_vector(63 downto 0);
--  signal app_wdf_wren_s              :  std_logic;
--  signal app_rd_data_s               :  std_logic_vector(511 downto 0);
--  signal app_rd_data_end_s           :  std_logic;
--  signal app_rd_data_valid_s         :  std_logic;
--  signal app_rdy_s                   :  std_logic;
--  signal app_wdf_rdy_s               :  std_logic;
--  signal init_calib_complete_s       :  std_logic;
--  signal ui_clk_s                    :  std_logic;


 ---------------------------------------------------------
 -- Configuration (CFG) Interface
 ---------------------------------------------------------
  signal cfg_err_ecrc                   : std_logic;
  attribute keep of cfg_err_ecrc      : signal is "true";
  signal cfg_err_cor                    : std_logic;
  attribute keep of cfg_err_cor      : signal is "true";
  signal cfg_err_ur                     : std_logic;
  attribute keep of cfg_err_ur      : signal is "true";
  signal cfg_err_cpl_timeout            : std_logic;
  attribute keep of cfg_err_cpl_timeout      : signal is "true";
  signal cfg_err_cpl_abort              : std_logic;
  attribute keep of cfg_err_cpl_abort      : signal is "true";
  signal cfg_err_cpl_unexpect           : std_logic;
  attribute keep of cfg_err_cpl_unexpect      : signal is "true";
  signal cfg_err_posted                 : std_logic;
  attribute keep of cfg_err_posted      : signal is "true";
  signal cfg_err_locked                 : std_logic;
  attribute keep of cfg_err_locked      : signal is "true";
  signal cfg_err_tlp_cpl_header         : std_logic_vector(47 downto 0);
  signal cfg_err_aer_headerlog          : std_logic_vector(127 downto 0);
  signal cfg_aer_interrupt_msgnum       : std_logic_vector(4 downto 0);
  attribute keep of cfg_aer_interrupt_msgnum      : signal is "true";
  signal cfg_err_atomic_egress_blocked  : std_logic;
  attribute keep of cfg_err_atomic_egress_blocked      : signal is "true";
  signal cfg_err_internal_cor           : std_logic;
  attribute keep of cfg_err_internal_cor      : signal is "true";
  signal cfg_err_malformed              : std_logic;
  attribute keep of cfg_err_malformed      : signal is "true";
  signal cfg_err_mc_blocked             : std_logic;
  attribute keep of cfg_err_mc_blocked      : signal is "true";
  signal cfg_err_poisoned               : std_logic;
  attribute keep of cfg_err_poisoned      : signal is "true";
  signal cfg_err_norecovery             : std_logic;
  attribute keep of cfg_err_norecovery      : signal is "true";
 -- signal cfg_err_cpl_rdy                : std_logic;
  signal cfg_err_acs                    : std_logic;
  attribute keep of cfg_err_acs      : signal is "true";
  signal cfg_err_internal_uncor         : std_logic;
  attribute keep of cfg_err_internal_uncor      : signal is "true";

  signal cfg_interrupt_rdy              : std_logic;
  signal cfg_interrupt_di               : std_logic_vector(7 downto 0);
  signal cfg_interrupt_msienable        : std_logic;
  signal cfg_interrupt                : std_logic;
  signal cfg_interrupt_assert         : std_logic;
  signal cfg_interrupt_stat           : std_logic;
  signal cfg_pciecap_interrupt_msgnum : std_logic_vector(4 downto 0);

  signal cfg_to_turnoff            : std_logic;
  signal cfg_bus_number            : std_logic_vector(7 downto 0);
  signal cfg_device_number         : std_logic_vector(4 downto 0);
  signal cfg_function_number       : std_logic_vector(2 downto 0);

  signal cfg_mgmt_di               : std_logic_vector(31 downto 0);
  signal cfg_mgmt_byte_en          : std_logic_vector(3 downto 0);
  signal cfg_mgmt_dwaddr           : std_logic_vector(9 downto 0);
  signal cfg_mgmt_wr_en            : std_logic;

  signal cfg_mgmt_rd_en            : std_logic;
  signal cfg_mgmt_wr_readonly      : std_logic;

  ---------------------------------------------------------
  -- Physical Layer Control and Status (PL) Interface
  ---------------------------------------------------------
  signal pl_directed_link_auton    : std_logic;
  signal pl_directed_link_change   : std_logic_vector(1 downto 0);
  signal pl_directed_link_speed    : std_logic;
  signal pl_directed_link_width    : std_logic_vector(1 downto 0);
  signal pl_upstream_prefer_deemph : std_logic;

  signal sys_rst_n_c               : std_logic;
  signal sys_clk                   : std_logic;

  signal user_reset_q              : std_logic;
  signal user_lnk_up_q             : std_logic;
  signal user_clk_heartbeat        : std_logic_vector(27 downto 0);

  ---------------------------------------------------------
  -- Overlay
  ---------------------------------------------------------
  -- fifo in and out
  signal din_fin            : std_logic_vector(39 downto 0);
  signal dout_fin           : std_logic_vector(39 downto 0);
  signal wr_en_fin          : std_logic;
  signal rd_en_fin          : std_logic;
  signal full_fin           : std_logic;
  signal empty_fin          : std_logic;
  signal almost_full_fin    : std_logic;
  signal rd_data_count_fin  : std_logic_vector(10 downto 0);
  signal wr_data_count_fin  : std_logic_vector(10 downto 0);

  signal din_fout           : std_logic_vector(39 downto 0);
  signal dout_fout          : std_logic_vector(39 downto 0);
  signal wr_en_fout         : std_logic;
  signal rd_en_fout         : std_logic;
  signal full_fout          : std_logic;
  signal empty_fout         : std_logic;
  signal almost_full_fout   : std_logic;
  signal rd_data_count_fout : std_logic_vector(10 downto 0);
  signal wr_data_count_fout : std_logic_vector(10 downto 0);

  signal fifo_in_empty_s    : std_logic;
  signal fifo_in_full_s     : std_logic;
  signal fifo_in_count_s    : std_logic_vector(10 downto 0);
  signal fifo_out_empty_s   : std_logic;
  signal fifo_out_full_s    : std_logic;
  signal fifo_out_count_s   : std_logic_vector(10 downto 0);

  signal user_rst_all_s     : std_logic;
  signal user_rst_data_s    : std_logic;
  signal rst_all_reg1       : std_logic;
  signal rst_all_reg2       : std_logic;
  signal rst_data_reg1      : std_logic;
  signal rst_data_reg2      : std_logic;
  signal user_data_loopbk_s : std_logic;
  signal data_loopbk_reg1   : std_logic;
  signal data_loopbk_reg2   : std_logic;

  signal dat_prv_00 : std_logic_vector(39 downto 0);
  signal val_prv_00 : std_logic;
  signal acc_prv_00 : std_logic;
  signal dat_prv_01 : std_logic_vector(39 downto 0);
  signal val_prv_01 : std_logic;
  signal acc_prv_01 : std_logic;

  signal dat_nxt_00 : std_logic_vector(39 downto 0);
  signal val_nxt_00 : std_logic;
  signal acc_nxt_00 : std_logic;
  signal dat_nxt_01 : std_logic_vector(39 downto 0);
  signal val_nxt_01 : std_logic;
  signal acc_nxt_01 : std_logic;

  -- overlay interface
  signal locked_pll           : std_logic;
  signal clk_ovly_s           : std_logic;
  signal rst_config_s         : std_logic;
  signal rst_overlay_s        : std_logic;
  signal overlay_ram_we_s     : std_logic;
  signal overlay_ram_we_vec_s : std_logic_vector( 0 downto 0);
  signal overlay_ram_addr_s   : std_logic_vector(10 downto 0);
  signal overlay_ram_din_s    : std_logic_vector(31 downto 0);
  signal overlay_ram_dout_s   : std_logic_vector(31 downto 0);
  signal overlay_dat_prv_s    : std_logic_vector(39 downto 0);
  signal overlay_val_prv_s    : std_logic;
  signal overlay_acc_prv_s    : std_logic;
  signal overlay_dat_nxt_s    : std_logic_vector(39 downto 0);
  signal overlay_val_nxt_s    : std_logic;
  signal overlay_acc_nxt_s    : std_logic;

  signal overlay_dat_nxt_02_s    : std_logic_vector(39 downto 0);
  signal overlay_val_nxt_02_s    : std_logic;
  signal overlay_acc_nxt_02_s    : std_logic;

  signal wr_en_fout_02    : std_logic;
  signal rd_en_fout_02    : std_logic;
  signal full_fout_02    : std_logic;
  signal empty_fout_02    : std_logic;


begin

process(user_clk)
   begin
    if rising_edge(user_clk) then
        emcclk_s <= Emcclk;
    end if;
   end process;



   sys_reset_n_ibuf : IBUF
   port map(
        I => sys_rst_n,
        O => sys_rst_n_c);




    refclk_ibuf : IBUFDS_GTE2
    port map(
        I      => sys_clk_p,
        IB     => sys_clk_n,
        CEB    => '0',
        ODIV2  => open,
        O      => sys_clk);

   led_0  <= '1';
   led_1  <= '0';
   led_2  <= '1';
   led_3  <= user_clk_heartbeat(25);

   process(user_clk)
   begin
    if rising_edge(user_clk) then
        user_reset_q  <= user_reset;
        user_lnk_up_q <= user_lnk_up;
    end if;
   end process;

   --Create a Clock Heartbeat on LED #3
   process(user_clk, sys_rst_n_c)
   begin
    if rising_edge(user_clk) then
        if (sys_rst_n_c = '0') then
            user_clk_heartbeat  <= (others => '0');
        else
            user_clk_heartbeat <= std_logic_vector(unsigned(user_clk_heartbeat) + 1);
        end if;
     end if;
   end process;

   pipe_mmcm_rst_n <= '1';

--==============================================================================
PCIE_SUPPORT : entity work.vc707_pcie_x8_gen2
--==============================================================================
  Port map (
    pci_exp_txp                                 => pci_exp_txp,
    pci_exp_txn                                 => pci_exp_txn,
    pci_exp_rxp                                 => pci_exp_rxp,
    pci_exp_rxn                                 => pci_exp_rxn,
    user_clk_out                                => user_clk,
    user_reset_out                              => user_reset,
    user_lnk_up                                 => user_lnk_up,
    user_app_rdy                                => open,
    tx_buf_av                                   => open,
    tx_cfg_req                                  => open,
    tx_err_drop                                 => open,
    s_axis_tx_tready                            => s_axis_tx_tready,
    s_axis_tx_tdata                             => s_axis_tx_tdata,
    s_axis_tx_tkeep                             => s_axis_tx_tkeep,
    s_axis_tx_tlast                             => s_axis_tx_tlast,
    s_axis_tx_tvalid                            => s_axis_tx_tvalid,
    s_axis_tx_tuser                             => s_axis_tx_tuser,
    tx_cfg_gnt                                  => tx_cfg_gnt,
    m_axis_rx_tdata                             => m_axis_rx_tdata,
    m_axis_rx_tkeep                             => m_axis_rx_tkeep,
    m_axis_rx_tlast                             => m_axis_rx_tlast,
    m_axis_rx_tvalid                            => m_axis_rx_tvalid,
    m_axis_rx_tready                            => m_axis_rx_tready,
    m_axis_rx_tuser                             => m_axis_rx_tuser,
    rx_np_ok                                    => rx_np_ok,
    rx_np_req                                   => rx_np_req,
    fc_cpld                                     => open,
    fc_cplh                                     => open,
    fc_npd                                      => open,
    fc_nph                                      => open,
    fc_pd                                       => open,
    fc_ph                                       => open,
    fc_sel                                      => fc_sel,
    cfg_mgmt_do                                 => open,
    cfg_mgmt_rd_wr_done                         => open,
    cfg_status                                  => open,
    cfg_command                                 => open,
    cfg_dstatus                                 => open,
    cfg_dcommand                                => open,
    cfg_lstatus                                 => open,
    cfg_lcommand                                => open,
    cfg_dcommand2                               => open,
    cfg_pcie_link_state                         => open,
    cfg_pmcsr_pme_en                            => open,
    cfg_pmcsr_powerstate                        => open,
    cfg_pmcsr_pme_status                        => open,
    cfg_received_func_lvl_rst                   => open,
    cfg_mgmt_di                                 => cfg_mgmt_di,
    cfg_mgmt_byte_en                            => cfg_mgmt_byte_en,
    cfg_mgmt_dwaddr                             => cfg_mgmt_dwaddr,
    cfg_mgmt_wr_en                              => cfg_mgmt_wr_en,
    cfg_mgmt_rd_en                              => cfg_mgmt_rd_en,
    cfg_mgmt_wr_readonly                        => cfg_mgmt_wr_readonly,
    cfg_err_ecrc                                => cfg_err_ecrc,
    cfg_err_ur                                  => cfg_err_ur,
    cfg_err_cpl_timeout                         => cfg_err_cpl_timeout,
    cfg_err_cpl_unexpect                        => cfg_err_cpl_unexpect,
    cfg_err_cpl_abort                           => cfg_err_cpl_abort,
    cfg_err_posted                              => cfg_err_posted,
    cfg_err_cor                                 => cfg_err_cor,
    cfg_err_atomic_egress_blocked               => cfg_err_atomic_egress_blocked,
    cfg_err_internal_cor                        => cfg_err_internal_cor,
    cfg_err_malformed                           => cfg_err_malformed,
    cfg_err_mc_blocked                          => cfg_err_mc_blocked,
    cfg_err_poisoned                            => cfg_err_poisoned,
    cfg_err_norecovery                          => cfg_err_norecovery,
    cfg_err_tlp_cpl_header                      => cfg_err_tlp_cpl_header,
    cfg_err_cpl_rdy                             => open,
    cfg_err_locked                              => cfg_err_locked,
    cfg_err_acs                                 => cfg_err_acs,
    cfg_err_internal_uncor                      => cfg_err_internal_uncor,
    cfg_trn_pending                             => cfg_trn_pending,
    cfg_pm_halt_aspm_l0s                        => cfg_pm_halt_aspm_l0s,
    cfg_pm_halt_aspm_l1                         => cfg_pm_halt_aspm_l1,
    cfg_pm_force_state_en                       => cfg_pm_force_state_en,
    cfg_pm_force_state                          => cfg_pm_force_state,
    cfg_dsn                                     => cfg_dsn,
    cfg_interrupt                               => cfg_interrupt,
    cfg_interrupt_rdy                           => cfg_interrupt_rdy,
    cfg_interrupt_assert                        => cfg_interrupt_assert,
    cfg_interrupt_di                            => cfg_interrupt_di,
    cfg_interrupt_do                            => open,
    cfg_interrupt_mmenable                      => open,
    cfg_interrupt_msienable                     => cfg_interrupt_msienable,
    cfg_interrupt_msixenable                    => open,
    cfg_interrupt_msixfm                        => open,
    cfg_interrupt_stat                          => cfg_interrupt_stat,
    cfg_pciecap_interrupt_msgnum                => cfg_pciecap_interrupt_msgnum,
    cfg_to_turnoff                              => cfg_to_turnoff,
    cfg_turnoff_ok                              => cfg_turnoff_ok,
    cfg_bus_number                              => cfg_bus_number,
    cfg_device_number                           => cfg_device_number,
    cfg_function_number                         => cfg_function_number,
    cfg_pm_wake                                 => cfg_pm_wake,
    cfg_pm_send_pme_to                          => '0',
    cfg_ds_bus_number                           => "00000000",
    cfg_ds_device_number                        => "00000",
    cfg_ds_function_number                      => "000",
    cfg_mgmt_wr_rw1c_as_rw                      => '0',
    cfg_msg_received                            => open,
    cfg_msg_data                                => open,
    cfg_bridge_serr_en                          => open,
    cfg_slot_control_electromech_il_ctl_pulse   => open,
    cfg_root_control_syserr_corr_err_en         => open,
    cfg_root_control_syserr_non_fatal_err_en    => open,
    cfg_root_control_syserr_fatal_err_en        => open,
    cfg_root_control_pme_int_en                 => open,
    cfg_aer_rooterr_corr_err_reporting_en       => open,
    cfg_aer_rooterr_non_fatal_err_reporting_en  => open,
    cfg_aer_rooterr_fatal_err_reporting_en      => open,
    cfg_aer_rooterr_corr_err_received           => open,
    cfg_aer_rooterr_non_fatal_err_received      => open,
    cfg_aer_rooterr_fatal_err_received          => open,
    cfg_msg_received_err_cor                    => open,
    cfg_msg_received_err_non_fatal              => open,
    cfg_msg_received_err_fatal                  => open,
    cfg_msg_received_pm_as_nak                  => open,
    cfg_msg_received_pm_pme                     => open,
    cfg_msg_received_pme_to_ack                 => open,
    cfg_msg_received_assert_int_a               => open,
    cfg_msg_received_assert_int_b               => open,
    cfg_msg_received_assert_int_c               => open,
    cfg_msg_received_assert_int_d               => open,
    cfg_msg_received_deassert_int_a             => open,
    cfg_msg_received_deassert_int_b             => open,
    cfg_msg_received_deassert_int_c             => open,
    cfg_msg_received_deassert_int_d             => open,
    cfg_msg_received_setslotpowerlimit          => open,
    pl_directed_link_change                     => pl_directed_link_change,
    pl_directed_link_width                      => pl_directed_link_width,
    pl_directed_link_speed                      => pl_directed_link_speed,
    pl_directed_link_auton                      => pl_directed_link_auton,
    pl_upstream_prefer_deemph                   => pl_upstream_prefer_deemph,
    pl_sel_lnk_rate                             => open,
    pl_sel_lnk_width                            => open,
    pl_ltssm_state                              => open,
    pl_lane_reversal_mode                       => open,
    pl_phy_lnk_up                               => open,
    pl_tx_pm_state                              => open,
    pl_rx_pm_state                              => open,
    pl_link_upcfg_cap                           => open,
    pl_link_gen2_cap                            => open,
    pl_link_partner_gen2_supported              => open,
    pl_initial_link_width                       => open,
    pl_directed_change_done                     => open,
    pl_received_hot_rst                         => open,
    pl_transmit_hot_rst                         => '0',
    pl_downstream_deemph_source                 => '0',
    cfg_err_aer_headerlog                       => cfg_err_aer_headerlog,
    cfg_aer_interrupt_msgnum                    => cfg_aer_interrupt_msgnum,
    cfg_err_aer_headerlog_set                   => open,
    cfg_aer_ecrc_check_en                       => open,
    cfg_aer_ecrc_gen_en                         => open,
    cfg_vc_tcvc_map                             => open,
    sys_clk                                     => sys_clk,
    sys_rst_n                                   => sys_rst_n_c,
    pcie_drp_clk                                => '1',
    pcie_drp_en                                 => '0',
    pcie_drp_we                                 => '0',
    pcie_drp_addr                               => "000000000",
    pcie_drp_di                                 => x"0000",
    pcie_drp_do                                 => open,
    pcie_drp_rdy                                => open
  );


--==============================================================================
PCIeModule : entity work.pcie_app_7x

--==============================================================================

generic map(
  C_DATA_WIDTH => C_DATA_WIDTH,
  TCQ          => TCQ
)

port map(
  ------------------------------------------------------------------------------------------------------------------//

  -- AXI-S Interface                                                                                                //

  ------------------------------------------------------------------------------------------------------------------//



  -- Common

  user_clk                       => user_clk,

  user_reset                     => user_reset_q,

  user_lnk_up                    => user_lnk_up_q,



  -- Tx

  s_axis_tx_tready               => s_axis_tx_tready,

  s_axis_tx_tdata                => s_axis_tx_tdata,

  s_axis_tx_tkeep                => s_axis_tx_tkeep,

  s_axis_tx_tuser                => s_axis_tx_tuser,

  s_axis_tx_tlast                => s_axis_tx_tlast,

  s_axis_tx_tvalid               => s_axis_tx_tvalid,



  -- Rx

  m_axis_rx_tdata                => m_axis_rx_tdata,

  m_axis_rx_tkeep                => m_axis_rx_tkeep,

  m_axis_rx_tlast                => m_axis_rx_tlast,

  m_axis_rx_tvalid               => m_axis_rx_tvalid,

  m_axis_rx_tready               => m_axis_rx_tready,

  m_axis_rx_tuser                => m_axis_rx_tuser,



  tx_cfg_gnt                     => tx_cfg_gnt,

  rx_np_ok                       => rx_np_ok,

  rx_np_req                      => rx_np_req,

  cfg_turnoff_ok                 => cfg_turnoff_ok,

  cfg_trn_pending                => cfg_trn_pending,

  cfg_pm_halt_aspm_l0s           => cfg_pm_halt_aspm_l0s,

  cfg_pm_halt_aspm_l1            => cfg_pm_halt_aspm_l1,

  cfg_pm_force_state_en          => cfg_pm_force_state_en,

  cfg_pm_force_state             => cfg_pm_force_state,

  cfg_pm_wake                    => cfg_pm_wake,

  cfg_dsn                        => cfg_dsn,



  -- Flow Control

  fc_sel                         => fc_sel,



  ------------------------------------------------------------------------------------------------------------------//

  -- Configuration (CFG) Interface                                                                                  //

  ------------------------------------------------------------------------------------------------------------------//

  cfg_err_cor                    => cfg_err_cor,

  cfg_err_atomic_egress_blocked  => cfg_err_atomic_egress_blocked,

  cfg_err_internal_cor           => cfg_err_internal_cor,

  cfg_err_malformed              => cfg_err_malformed,

  cfg_err_mc_blocked             => cfg_err_mc_blocked,

  cfg_err_poisoned               => cfg_err_poisoned,

  cfg_err_norecovery             => cfg_err_norecovery,

  cfg_err_ur                     => cfg_err_ur,

  cfg_err_ecrc                   => cfg_err_ecrc,

  cfg_err_cpl_timeout            => cfg_err_cpl_timeout,

  cfg_err_cpl_abort              => cfg_err_cpl_abort,

  cfg_err_cpl_unexpect           => cfg_err_cpl_unexpect,

  cfg_err_posted                 => cfg_err_posted,

  cfg_err_locked                 => cfg_err_locked,

  cfg_err_acs                    => cfg_err_acs,

  cfg_err_internal_uncor         => cfg_err_internal_uncor,

  cfg_err_tlp_cpl_header         => cfg_err_tlp_cpl_header,

  ------------------------------------------------------------------------------------------------------------------//

  -- Advanced Error Reporting (AER) Interface                                                                       //

  ------------------------------------------------------------------------------------------------------------------//

  cfg_err_aer_headerlog          => cfg_err_aer_headerlog,

  cfg_aer_interrupt_msgnum       => cfg_aer_interrupt_msgnum ,



  cfg_to_turnoff                 => cfg_to_turnoff,

  cfg_bus_number                 => cfg_bus_number,

  cfg_device_number              => cfg_device_number,

  cfg_function_number            => cfg_function_number,



  ------------------------------------------------------------------------------------------------------------------//

  -- Management (MGMT) Interface                                                                                    //

  ------------------------------------------------------------------------------------------------------------------//

  cfg_mgmt_di                    => cfg_mgmt_di,

  cfg_mgmt_byte_en               => cfg_mgmt_byte_en,

  cfg_mgmt_dwaddr                => cfg_mgmt_dwaddr,

  cfg_mgmt_wr_en                 => cfg_mgmt_wr_en,
  cfg_mgmt_rd_en                 => cfg_mgmt_rd_en,
  cfg_mgmt_wr_readonly           => cfg_mgmt_wr_readonly,

  ------------------------------------------------------------------------------------------------------------------//
  -- Physical Layer Control and Status (PL) Interface                                                               //
  ------------------------------------------------------------------------------------------------------------------//
  pl_directed_link_auton         => pl_directed_link_auton,
  pl_directed_link_change        => pl_directed_link_change,
  pl_directed_link_speed         => pl_directed_link_speed,
  pl_directed_link_width         => pl_directed_link_width,
  pl_upstream_prefer_deemph      => pl_upstream_prefer_deemph,

  cfg_interrupt                  => cfg_interrupt,
  cfg_interrupt_assert           => cfg_interrupt_assert,
  cfg_interrupt_di               => cfg_interrupt_di,
  cfg_interrupt_stat             => cfg_interrupt_stat,
  cfg_interrupt_rdy              => cfg_interrupt_rdy,
  cfg_interrupt_msienable        => cfg_interrupt_msienable,
  cfg_pciecap_interrupt_msgnum   => cfg_pciecap_interrupt_msgnum,

  -- fifo interface info
  fifo_in_empty_i         => fifo_in_empty_s,
  fifo_in_full_i          => fifo_in_full_s,
  fifo_in_count_i         => fifo_in_count_s,
  fifo_out_empty_i        => fifo_out_empty_s,
  fifo_out_full_i         => fifo_out_full_s,
  fifo_out_count_i        => fifo_out_count_s,

  -- user reset
  user_rst_all_o          => user_rst_all_s,
  user_rst_data_o         => user_rst_data_s,
  user_data_loopbk_o      => user_data_loopbk_s,

  -- overlay interface
  overlay_ram_we_o        => overlay_ram_we_s,
  overlay_ram_addr_o      => overlay_ram_addr_s,
  overlay_ram_din_o       => overlay_ram_din_s,
  overlay_ram_dout_i      => overlay_ram_dout_s,
  overlay_dat_prv_o       => dat_prv_00,
  overlay_val_prv_o       => val_prv_00,
  overlay_acc_prv_i       => acc_prv_00,
  overlay_dat_nxt_i       => dat_nxt_01,
  overlay_val_nxt_i       => val_nxt_01,
  overlay_acc_nxt_o       => acc_nxt_01

--  ddr_in        => ddr_in_s,
--  ddr_out       => ddr_out_s


);

-------------------- overlay ----------------------------------------------------
PLL_01: entity work.clk_wiz_0 port map(
     clk_in1 => user_clk,
     clk_out1 => clk_ovly_s,
     reset => user_reset_q,
     locked => locked_pll);

-- reset_ovly_q <= not locked_pll;

-- rst_config_s <= (not locked_pll) or user_rst_all_s;
-- rst_overlay_s <= rst_config_s or user_rst_data_s;

---- generate overlay reset (changing clk domaine)
rst_ovly_p : process(locked_pll,clk_ovly_s)
begin
    if locked_pll='0' then
        rst_config_s <= '1';
        rst_overlay_s <= '1';
        rst_all_reg1 <= '1';
        rst_all_reg2 <= '1';
        rst_data_reg1 <= '1';
        rst_data_reg2 <= '1';
        --data_loopbk_reg1 <= '0';
        --data_loopbk_reg2 <= '0';
    elsif rising_edge(clk_ovly_s) then
        rst_all_reg1 <= user_rst_all_s;
        rst_all_reg2 <= rst_all_reg1;
        rst_data_reg1 <= user_rst_data_s;
        rst_data_reg2 <= rst_data_reg1;

        --data_loopbk_reg1 <= user_data_loopbk_s;
        --data_loopbk_reg2 <= data_loopbk_reg1;

        if rst_all_reg2='1' then
            rst_config_s <= '1';
            rst_overlay_s <= '1';
        elsif rst_data_reg2='1' then
            rst_config_s <= '0';
            rst_overlay_s <= '1';
        else
            rst_config_s <= '0';
            rst_overlay_s <= '0';
        end if;

    end if;
end process;

p_user : process(locked_pll,user_clk)
begin
    if locked_pll='0' then
        data_loopbk_reg1 <= '0';
        data_loopbk_reg2 <= '0';
    elsif rising_edge(user_clk) then
        data_loopbk_reg1 <= user_data_loopbk_s;
        data_loopbk_reg2 <= data_loopbk_reg1;
    end if;
end process;

-------------------- fifo in and out (changing clk domaine)
din_fin <= dat_prv_00;
wr_en_fin <=  val_prv_00 and acc_prv_00;
acc_prv_00 <= not full_fin;

Fi01: entity work.fifo_40b_inout port map (
    wr_clk => user_clk,
    rd_clk => user_clk, --user_clk,
    rst => rst_overlay_s,
    din => din_fin,
    wr_en => wr_en_fin,
    rd_en => rd_en_fin,
    dout => dout_fin,
    full => full_fin,
    almost_full => almost_full_fin,
    empty => empty_fin,
    rd_data_count => rd_data_count_fin,
    wr_data_count => wr_data_count_fin
  );

dat_prv_01 <= dout_fin;
rd_en_fin <= val_prv_01 and acc_prv_01;
val_prv_01 <= not empty_fin;

fifo_in_empty_s <= empty_fin;
fifo_in_full_s <= almost_full_fin;
fifo_in_count_s <= wr_data_count_fin;

-- out
din_fout <= dat_nxt_00;
wr_en_fout <=  val_nxt_00 and acc_nxt_00;
acc_nxt_00 <= not full_fout;

Fo02: entity work.fifo_40b_inout port map (
    wr_clk => user_clk, --user_clk,
    rd_clk => user_clk,
    rst => rst_overlay_s,
    din => din_fout,
    wr_en => wr_en_fout,
    rd_en => rd_en_fout,
    dout => dout_fout,
    full => full_fout,
    almost_full => almost_full_fout,
    empty => empty_fout,
    rd_data_count => rd_data_count_fout,
    wr_data_count => wr_data_count_fout
  );

dat_nxt_01 <= dout_fout;
rd_en_fout <= val_nxt_01 and acc_nxt_01;
val_nxt_01 <= not empty_fout;

fifo_out_empty_s <= empty_fout;
fifo_out_full_s <= almost_full_fout;
fifo_out_count_s <= rd_data_count_fout;

-------------------- overlay arch
overlay_ram_we_vec_s(0) <= overlay_ram_we_s;

----test: fifo in -> fifo out
-- dat_nxt_00 <= dat_prv_01;
-- val_nxt_00 <= val_prv_01;
-- acc_prv_01 <= acc_nxt_00;

-- overlay_dat_prv_s   <= x"0000000000";
-- overlay_val_prv_s   <= '0';
-- overlay_acc_nxt_s   <= '1';

----test: fifo in-> overlay -> fifo out
-- overlay_dat_prv_s   <= dat_prv_01;
-- overlay_val_prv_s   <= val_prv_01;
-- acc_prv_01          <= overlay_acc_prv_s;

-- dat_nxt_00          <= overlay_dat_nxt_s;
-- val_nxt_00          <= overlay_val_nxt_s;
-- overlay_acc_nxt_s   <= acc_nxt_00;

----test: overlay or loopback
overlay_dat_prv_s   <= x"0000000000" when data_loopbk_reg2='1' else dat_prv_01;
overlay_val_prv_s   <= '0' when data_loopbk_reg2='1' else val_prv_01;
acc_prv_01          <= acc_nxt_00 when data_loopbk_reg2='1' else overlay_acc_prv_s;

dat_nxt_00          <= dat_prv_01 when data_loopbk_reg2='1' else overlay_dat_nxt_02_s;
val_nxt_00          <= val_prv_01 when data_loopbk_reg2='1' else overlay_val_nxt_02_s;
overlay_acc_nxt_02_s   <= '1' when data_loopbk_reg2='1' else acc_nxt_00;


--Overlay_01: entity work.cfg_data_overlay port map (
--                clk             => clk_ovly_s,
--                rst_config_i    => rst_config_s,
--                rst_overlay_i   => rst_overlay_s,
--                clk_ram         => user_clk,
--                ram_we_i        => overlay_ram_we_vec_s,
--                ram_addr_i      => overlay_ram_addr_s,
--                ram_din_i       => overlay_ram_din_s,
--                ram_dout_o      => overlay_ram_dout_s,
--                dat_prv_i       => overlay_dat_prv_s,
--                val_prv_i       => overlay_val_prv_s,
--                acc_prv_o       => overlay_acc_prv_s,
--                dat_nxt_o       => overlay_dat_nxt_s,
--                val_nxt_o       => overlay_val_nxt_s,
--                acc_nxt_i       => overlay_acc_nxt_s);

overlay_data_size_s <= std_logic_vector(to_unsigned(32,overlay_data_size_s'length)); -- RHE : TO DO: Get Data Size from CPU ! 

Overlay_01: entity work.cfg_data_patern_overlay port map (
                                clk             => clk_ovly_s,
                                clk_usr         => user_clk,  --user_clk,
                                rst_config_i    => rst_config_s,
                                rst_overlay_i   => rst_overlay_s,
                                clk_ram         => user_clk,
                                ram_we_i        => overlay_ram_we_vec_s,
                                ram_addr_i      => overlay_ram_addr_s,
                                ram_din_i       => overlay_ram_din_s,
                                ram_dout_o      => overlay_ram_dout_s,
                                dat_prv_i       => overlay_dat_prv_s(31 downto 0),
                                val_prv_i       => overlay_val_prv_s,
                                acc_prv_o       => overlay_acc_prv_s,
                                dat_nxt_o       => overlay_dat_nxt_s,
                                val_nxt_o       => overlay_val_nxt_s,
                                acc_nxt_i       => overlay_acc_nxt_s,
                                data_size_i     => overlay_data_size_s);



wr_en_fout_02 <=  overlay_val_nxt_s and overlay_acc_nxt_s;
overlay_acc_nxt_s <= not full_fout_02;

F_01: entity work.fifo_40b_inout port map (
    wr_clk => clk_ovly_s,
    rd_clk => user_clk, --user_clk,
    rst => rst_overlay_s,
    din => overlay_dat_nxt_s,
    wr_en => wr_en_fout_02,
    rd_en => rd_en_fout_02,
    dout => overlay_dat_nxt_02_s,
    full => full_fout_02,
    almost_full => open,
    empty => empty_fout_02,
    rd_data_count => open,
    wr_data_count => open
  );

rd_en_fout_02 <= overlay_val_nxt_02_s and overlay_acc_nxt_02_s;
overlay_val_nxt_02_s <= not empty_fout_02;

end behaviour;
