------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : PIO_EP_MEM_ACCESS.vhd
-- Author               : Flavio Capitao (FCC)
-- Date                 : 19.02.2016
--
-- Context              : PoSeNoGap
--
------------------------------------------------------------------------------------------
-- Description : Endpoint Memory Access Unit. This module provides access functions
--               to the Endpoint memory aperture.
--   
------------------------------------------------------------------------------------------
-- Dependencies : pio_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  FCC           Initial version. Merge PCIe2FPGA and old PoSeNoGap
--
------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.pio_pkg.all;
--    use work.simsynth_pkg.all;
--    use work.clustering_pkg.all;

entity PIO_EP_MEM_ACCESS is
    generic (
        TCQ                   : integer := 1
    );
    port(
        clk                   : in  std_logic;                                                
        rst_n                 : in  std_logic;                                            
        rd_addr               : in  memory_addr_t;
        rd_be                 : in  std_logic_vector( 3 downto 0);
        rd_data               : out std_logic_vector(31 downto 0);
        rd_en                 : in  std_logic;

        wr_addr               : in  memory_addr_t;  -- Memory Read Address;
        wr_be                 : in  std_logic_vector( 7 downto 0);
        wr_data               : in  std_logic_vector(31 downto 0);
        wr_en                 : in  std_logic;                    
        wr_busy               : out std_logic;

        interrupt_mask_wr_o   : out std_logic;
        interrupt_irq_clear_o : out std_logic;
        interrupt_data_wr_o   : out std_logic_vector(31 downto 0);
        interrupt_data_rd_i   : in  std_logic_vector(31 downto 0);
        interrupt_irq_o       : out std_logic_vector(31 downto 0);   

        mem128_wr_i           : in  mem128_wr_interface;
        mem128_rd_ctrl_i      : in  mem128_rd_ctrl_interface;
        mem128_rd_data_o      : out mem128_rd_data_interface;

        -- tag table interface                                  
        rx_eng_tag_i          : in  std_logic_vector( 4 downto 0);
        rx_eng_rd_tag_i       : in  std_logic;                    
        rx_eng_data_tag_o     : out std_logic_vector(31 downto 0);
        tx_eng_tag_i          : in  std_logic_vector( 4 downto 0);
        tx_eng_wr_tag_i       : in  std_logic;                    
        tx_eng_data_tag_i     : in  std_logic_vector(31 downto 0);

        -- internal register                               
        param_tx_i            : in  param_tx_t;
        param_rx_i            : in  param_rx_t;
        config_register_o     : out config_register_t

--        mem2user_o            : out mem2user_t;
--        user2mem_i            : in  user2mem_t;

--        ddr_in                : in  ddr2mem_t;
--        ddr_out               : out mem2ddr_t
    );                                                                     
end PIO_EP_MEM_ACCESS;                                                         

architecture Behavioral of PIO_EP_MEM_ACCESS is

    --| Types declarations     |--------------------------------------------------------------  
    -- 

    --| Constants declarations |--------------------------------------------------------------
    --

    --| Signals declarations   |--------------------------------------------------------------

    signal rst_s                : std_logic;


--    signal rd_data_seq_s        : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);


    signal rd_data_bar0_s       : std_logic_vector(31 downto 0);  
    signal rd_data_bar1_s       : std_logic_vector(31 downto 0);  

    signal wr_data_s            : std_logic_vector(31 downto 0);  
    signal rd_data_s            : std_logic_vector(31 downto 0);
    signal rd_addr_s            : memory_addr_t;
    signal rd_be_s              : std_logic_vector(3 downto 0);

    signal rd_en_bar0_s         : std_logic;
    signal rd_en_bar0_int_s     : std_logic; 
    signal rd_en_bar1_s         : std_logic;
    signal rd_en_bar1_int_s     : std_logic;

    signal wr_en_bar0_s         : std_logic;
    signal wr_en_bar0_int_s     : std_logic; 
    signal wr_en_bar1_s         : std_logic;
    signal wr_en_bar1_int_s     : std_logic;

    --signal mem_receive_header_data_out_s : std_logic_vector(31 downto 0);

    signal config_register_s    : config_register_t;
    signal tag_table_wen_s      : std_logic_vector(0 downto 0);

    signal reg_dword_wr_s : std_logic_vector(31 downto 0);

    signal wen_in_s            : std_logic;
    signal data_in_wr_s        : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);
    signal full_in_s           : std_logic;

    signal ren_out_s            : std_logic;
    signal data_out_rd_s       : std_logic_vector(FIFO_OUT_DATA_WIDTH-1 downto 0);




    -- regarder s'il y a besoint de tout ces signaux  ??? :

    signal endOfParsing_s           : std_logic;  
--    signal seq2ddr_wr_id_s          : std_logic;
--    signal seq2ddr_save_d1_s        : std_logic;   
--    signal subSeq2DDR_s             : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);     
    
    signal taskComleated_s          : std_logic; 
--    signal dataSourceMux_s          : std_logic;
    
--    signal rd_en_s                  : std_logic;                       
--    signal fifo_rd_empty_s          : std_logic;                       
--    signal readsInCache_s           : std_logic;                --indique s'il y a eu des missmatch et des reads ont �t�s transfer�s dans la DDR3
--    signal incClusterId_s           : std_logic;

    signal cluster_size_s           : std_logic_vector(31 downto 0);  
    signal lastFifoRead_s           : std_logic;

    signal fifo_in_nb_available_s   : std_logic_vector(FIFO_IN_LOGSIZE downto 0);  
    signal fifo_out_nb_available_s  : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);  


--    signal ddr3_rx_cnt              : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);
--    signal ddr3_tx_cnt              : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);
    
--    signal ddr3_rx_rd_en            : std_logic;
--    signal ddr3_rx_dout             : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);
--    signal ddr3_rx_empty            : std_logic;

--    signal ddr3_tx_full             : std_logic;  

begin

--    ddr_out.cnumber <= std_logic_vector(to_unsigned(N_CLUSTER, 16)) ;

    --| Inputs processing      |--------------------------------------------------------------

    rst_s <= not rst_n;

    bar0_proc : process(clk) is

    begin
        if (rst_n = '0') then

            reg_dword_wr_s <= (others => '0');
            data_in_wr_s  <= (others => '0');
            wen_in_s <= '0';
            
            --mem128_rd_data_o.data <= (others => '0');
            ren_out_s <= '0';

        elsif rising_edge(clk) then

            wen_in_s <= '0';
            ren_out_s <= '0';

            -- Burst write access -> Sequence FIFO
            if (mem128_wr_i.wr = '1') then

                -- TODO : test address

                if (mem128_wr_i.dword_en="1000") then -- start burst

                    reg_dword_wr_s  <=  mem128_wr_i.data(127 downto 96);

                elsif (mem128_wr_i.dword_en="1111") then -- middle burst

                    data_in_wr_s   <=   reg_dword_wr_s & 
                                        mem128_wr_i.data( 31 downto 00) & 
                                        mem128_wr_i.data( 63 downto 32) & 
                                        mem128_wr_i.data( 95 downto 64);
                    

                    reg_dword_wr_s  <=  mem128_wr_i.data(127 downto 96);

                    if (full_in_s = '0') then
                         wen_in_s <= '1';
--                        note("[PIO_EP_MEM_ACCESS] data write to fifo : " &  to_hstring(reg_dword_wr_s & 
--                                                                            mem128_wr_i.data( 31 downto 00) & 
--                                                                            mem128_wr_i.data( 63 downto 32) & 
--                                                                            mem128_wr_i.data( 95 downto 64))); -- tmp FCC
                    else
                        -- data ignored !!! TODO : Generate interrupt ?? FCC
--                        error("[PIO_EP_MEM_ACCESS] FIFO full ! Data ignored");
                    end if;

                elsif (mem128_wr_i.dword_en="0111") then -- end burst

                    data_in_wr_s   <=   reg_dword_wr_s & 
                                        mem128_wr_i.data( 31 downto 00) & 
                                        mem128_wr_i.data( 63 downto 32) & 
                                        mem128_wr_i.data( 95 downto 64);
                    
                    if (full_in_s = '0') then
                        wen_in_s <= '1';
                        --note("[PIO_EP_MEM_ACCESS] final data write to fifo : " &  to_hstring(reg_dword_wr_s & 
                                                                            --mem128_wr_i.data( 31 downto 00) & 
                                                                            --mem128_wr_i.data( 63 downto 32) & 
                                                                            --mem128_wr_i.data( 95 downto 64))); -- tmp FCC
                    else
                        -- data ignored !!! TODO : Generate interrupt before that !! FCC
--                        error("[PIO_EP_MEM_ACCESS] FIFO full ! Data ignored");
                    end if;

                elsif (mem128_wr_i.dword_en="0011") then -- end burst, this means that burst lenght isn't 4 multiple
--                    error("[PIO_EP_MEM_ACCESS] Burst lenght need to be 4 multiple");
                elsif (mem128_wr_i.dword_en="0001") then -- end burst, this means that burst lenght isn't 4 multiple
--                    error("[PIO_EP_MEM_ACCESS] Burst lenght need to be 4 multiple");
                else
--                    error("[PIO_EP_MEM_ACCESS] Dword enable not handled by the system");
                end if;

            -- single/double dword write in bar 0 
            elsif wr_en_bar0_s = '1' then
--                error("[PIO_EP_MEM_ACCESS] Single or double dword write in bar 0 not used");
            end if;


            -- pour l'instant lecture classique... 32bits par 32bits   
            if (rd_en_bar0_s = '1') then
                ren_out_s <= '1';
            end if;

            -- TODO : fifo de 128 : a verifier ....
            -- if mem128_rd_ctrl_i.rd_en = '1' then
            --     -- mem128_rd_data_o.data(31 downto 0) <= memory128(0)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            --     -- mem128_rd_data_o.data(63 downto 32) <= memory128(1)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            --     -- mem128_rd_data_o.data(95 downto 64) <= memory128(2)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            --     -- mem128_rd_data_o.data(127 downto 96) <= memory128(3)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            -- end if;

        end if;
    end process;

    -- test FCC
    mem128_rd_data_o.data( 95 downto  0) <= (others => '0');
    mem128_rd_data_o.data(127 downto 96) <= data_out_rd_s;
    rd_data_bar0_s <= data_out_rd_s;


    -- assignment of interal write value
    wr_data_s  <= wr_data;

    -- memory Write Controller
    wr_en_bar0_int_s <= '1' when (wr_addr.region = "00") else '0';
    wr_en_bar1_int_s <= '1' when (wr_addr.region = "01") else '0';
    wr_en_bar0_s     <= wr_en and wr_en_bar0_int_s; 
    wr_en_bar1_s     <= wr_en and wr_en_bar1_int_s;

    -- memory Read Controller
    rd_en_bar0_int_s <= '1' when (rd_addr.region = "00") else '0';
    rd_en_bar1_int_s <= '1' when (rd_addr.region = "01") else '0';  
    rd_en_bar0_s     <= rd_en and rd_en_bar0_int_s; 
    rd_en_bar1_s     <= rd_en and rd_en_bar1_int_s; 

    -- write controller busy
    wr_busy <= wr_en;

    process(rst_n, clk)
    begin
        if (rst_n = '0') then
            rd_addr_s <= ((others=>'0'),(others=>'0'));
            rd_be_s   <= (others=>'0');
        elsif rising_edge(clk) then
            rd_addr_s <= rd_addr;
            rd_be_s   <= rd_be;
        end if;
    end process;


--    ddr_out.csize <= cluster_size_s;

    -- read bar selection
    rd_bar1_proc : process(rd_addr_s, rd_data_bar0_s, rd_data_bar1_s)--, mem_receive_header_data_out_s)
    begin
        case (rd_addr_s.region) is 
            when "00" => 
                rd_data_s <= rd_data_bar0_s;
            when "01" => 
                --if(rd_addr_s.addr(LOG_ADDR_SIZE) = '1') then    -- FCC ?? 
                --    rd_data_s  <= mem_receive_header_data_out_s;
                --else
                    rd_data_s <= rd_data_bar1_s;
                --end if;
            when others => rd_data_s <= rd_data_bar0_s;
        end case;
    end process;
                                                                                                                                  
    interrupt_mask_wr_o <= '1' when wr_en_bar1_s='1' and unsigned(wr_addr.addr(8 downto 0))=ADDR_IRQ_SET_MASK else '0';
    interrupt_irq_clear_o <= '1' when wr_en_bar1_s='1' and unsigned(wr_addr.addr(8 downto 0))=ADDR_IRQ_CLEAR else '0';
    interrupt_data_wr_o <= wr_data_s;                                                        

    -- write internal configuration register process (Bar 1)
    wr_bar1_proc : process(rst_n, clk)
    begin
        if (rst_n = '0') then

            config_register_s.command                   <= (others => '0');
            config_register_s.write_src_address         <= (others => '0');
            config_register_s.write_dst_address         <= (others => '0');
            config_register_s.write_length              <= (others => '0');
            config_register_s.debug                     <= (others => '0');
            config_register_s.write_nb_packet           <= (others => '0');
            config_register_s.read_src_address          <= (others => '0');
            config_register_s.read_dst_address          <= (others => '0');
            config_register_s.read_length               <= (others => '0');
            config_register_s.read_nb_packet            <= (others => '0');
            config_register_s.read_pipe_size            <= x"00000003"; --(0=>'1', others => '0');
            config_register_s.debug                     <= (others => '0');
            config_register_s.write_msg_header          <= (others => '0'); 
            interrupt_irq_o <= (others=>'0');

            lastFifoRead_s           <= '0';


            cluster_size_s  <= x"00000004";
            endOfParsing_s  <= '0';

        elsif rising_edge(clk) then

            interrupt_irq_o <= (others=>'0');      

            if (wr_en_bar1_s = '1') then
                case to_integer(unsigned(wr_addr.addr(8 downto 0))) is 
                    when ADDR_COMMAND           =>  config_register_s.command           <= wr_data_s;
                    when ADDR_WRITE_SRC_ADDRESS =>  config_register_s.write_src_address <= wr_data_s;
                    when ADDR_WRITE_DST_ADDRESS =>  config_register_s.write_dst_address <= wr_data_s;
                    when ADDR_WRITE_LENGTH      =>  config_register_s.write_length      <= wr_data_s;
                    when ADDR_WRITE_NB_PACKET   =>  config_register_s.write_nb_packet   <= wr_data_s;
                    when ADDR_READ_SRC_ADDRESS  =>  config_register_s.read_src_address  <= wr_data_s;
                    when ADDR_READ_DST_ADDRESS  =>  config_register_s.read_dst_address  <= wr_data_s;
                    when ADDR_READ_LENGTH       =>  config_register_s.read_length       <= wr_data_s;
                    when ADDR_READ_NB_PACKET    =>  config_register_s.read_nb_packet    <= wr_data_s;     
                    when ADDR_READ_PIPE_SIZE    =>  config_register_s.read_pipe_size    <= wr_data_s;
                    when ADDR_DEBUG             =>  config_register_s.debug             <= wr_data_s;    
                    when ADDR_WRITE_MSG_HEADER  =>  config_register_s.write_msg_header  <= wr_data_s;    
                    when ADDR_IRQ_GENERATE      =>  interrupt_irq_o                     <= wr_data_s;   

                    -- User config :
                    when ADDR_CLUSTER_SIZE      =>  cluster_size_s                      <= wr_data;        
                    when ADDR_PARSING_END       =>  endOfParsing_s                      <= wr_data(0);

                    when others => null;
                end case; 
            end if;

            if(param_tx_i.end_of_command(0) = '1') then
                interrupt_irq_o(0)<='1';
            end if;

            if(param_tx_i.mem_acq_cmd(0) = '1') then
                config_register_s.command(0) <= '0';
            end if;

            if (param_tx_i.end_of_command(1) = '1') then
                interrupt_irq_o(1)<='1';
            end if;

            if(param_tx_i.mem_acq_cmd(1) = '1') then
                config_register_s.command(COMMAND_BIT_DMA_WRITE) <= '0';
            end if;

            if (param_rx_i.end_of_read_cpld = '1') then
                interrupt_irq_o(2)<='1';
            end if;

            if (param_rx_i.new_poisoned_tlp = '1') then
                interrupt_irq_o(3) <= '1';
            end if;

            -- irq fifo out full    
--            if user2mem_i.irq_trigger(1) = '1' then 
--                --interrupt_irq_o(16) <= '1';
--                interrupt_irq_o(15) <= '1';
--            end if;

            -- irq one result saved  -- TODO remove this interrupt
--            if user2mem_i.irq_trigger(0) = '1' then 
--                interrupt_irq_o(17) <= '1';
--            end if;

            if (taskComleated_s = '1' and endOfParsing_s = '1') then 
                interrupt_irq_o(15)  <= '1';  
                endOfParsing_s       <= '0';    -- EPO plus valide lorsque on utilise IRQ partielle pour vider la fifo. 
                --ddr_out.readSeq     <= '1';   -- EPO debug 
                lastFifoRead_s       <= '1';    -- tag pour le soft, signale la dernière lecture de la fifo
            end if;    

            if(param_tx_i.mem_acq_cmd(2) = '1') then
                config_register_s.command(COMMAND_BIT_DMA_READ) <= '0';
            end if;

            if(config_register_s.command(COMMAND_BIT_LOG_RESET) = '1') then
                config_register_s.command(COMMAND_BIT_LOG_RESET) <= '0';
            end if;

        end if;
    end process;

    -- Read internal configuration register process (Bar 1)
    process(rst_n,clk)
    begin
        if (rst_n = '0') then
            rd_data_bar1_s <= (others => '0'); 
        elsif rising_edge(clk) then
            if (rd_en_bar1_s = '1') then
                case to_integer(unsigned(rd_addr.addr(MEMORY_SIZE-1 downto 0))) is
                    when ADDR_COMMAND                 => rd_data_bar1_s <= config_register_s.command;         
                    when ADDR_WRITE_SRC_ADDRESS       => rd_data_bar1_s <= config_register_s.write_src_address;
                    when ADDR_WRITE_DST_ADDRESS       => rd_data_bar1_s <= config_register_s.write_dst_address;
                    when ADDR_WRITE_LENGTH            => rd_data_bar1_s <= config_register_s.write_length;
                    when ADDR_WRITE_NB_PACKET         => rd_data_bar1_s <= config_register_s.write_nb_packet;
                    when ADDR_READ_SRC_ADDRESS        => rd_data_bar1_s <= config_register_s.read_src_address;
                    when ADDR_READ_DST_ADDRESS        => rd_data_bar1_s <= config_register_s.read_dst_address;
                    when ADDR_READ_LENGTH             => rd_data_bar1_s <= config_register_s.read_length;
                    when ADDR_READ_NB_PACKET          => rd_data_bar1_s <= config_register_s.read_nb_packet;
                    when ADDR_READ_PIPE_SIZE          => rd_data_bar1_s <= config_register_s.read_pipe_size;
                    when ADDR_DEBUG                   => rd_data_bar1_s <= config_register_s.debug;     
                    when ADDR_WRITE_MSG_HEADER        => rd_data_bar1_s <= config_register_s.write_msg_header;  
                    when ADDR_VERSION                 => rd_data_bar1_s <= VERSION_VEC;
                    -- address of the Log memory - byte aligned
                    when ADDR_LOG_MEMORY_ADDR         => rd_data_bar1_s <= std_logic_vector(to_unsigned(2**LOG_ADDR_SIZE,30))&"00";
                    -- size of the Log memory - byte aligned
                    when ADDR_LOG_MEMORY_SIZE         => rd_data_bar1_s <= std_logic_vector(to_unsigned(2**LOG_ADDR_SIZE,30))&"00";
                    when ADDR_IRQ_REGISTER            => rd_data_bar1_s <= interrupt_data_rd_i;


                    -- User config :
                    when ADDR_DESIGN_VERSION    =>  rd_data_bar1_s   <= VERSION_VEC;
                    when ADDR_NB_RESULTS        =>  rd_data_bar1_s   <= lastFifoRead_s & x"0000" & "00" & fifo_out_nb_available_s;
                    when ADDR_NB_SEQ            =>  rd_data_bar1_s   <= x"0000" & "000" & fifo_in_nb_available_s;
                    when ADDR_CLUSTER_SIZE      =>  rd_data_bar1_s   <= cluster_size_s;


                    when others => rd_data_bar1_s <= x"DEADBEEF"; -- fake data returned when the read address is unmapped.
                end case;

            end if; 
        end if;
    end process;


    --Tag table
    TagTable : entity work.tag_table                            
    port map (                                             
        clka  => clk,                                                     
        ena   => '1',                                                 
        wea   => tag_table_wen_s,                                              
        addra => tx_eng_tag_i,                                                     
        dina  => tx_eng_data_tag_i,                                                  
        clkb  => clk,                                                
        enb   => '1',      
        addrb => rx_eng_tag_i,      
        doutb => rx_eng_data_tag_o     
    );                                                                                   

    tag_table_wen_s(0) <= tx_eng_wr_tag_i;

    --| outputs processing     |-------------------------------------------------------------- 

    rd_data <= rd_data_s;  


    config_register_o <= config_register_s;


    --| Clustering FIFOs      |--------------------------------------------------------------

--    -- fifo output result                    
--    FIFO_OUT_MAP : entity work.fifo_std
--    generic map (
--        NB_BIT_DATA => FIFO_OUT_LOGSIZE,
--        FIFOSIZE    => FIFO_OUT_SIZE,
--        DATAWIDTH   => FIFO_OUT_DATA_WIDTH
--    )
--    port map (
--        Clock_i                 => clk,
--        Reset_i                 => rst_s,
--        Wr_Req_i                => user2mem_i.fifo_out_wr,
--        Rd_Req_i                => ren_out_s,
--        Data_i                  => user2mem_i.fifo_out_wr_data,
--        Fifo_Full_o             => mem2user_o.fifo_out_full,
--        Fifo_Empty_o            => mem2user_o.fifo_out_empty,
--        Data_o                  => data_out_rd_s,
--        Nb_Data_Available_o     => fifo_out_nb_available_s,
--        Nb_Data_Free_o          => mem2user_o.fifo_out_nb_free
--    );

--    -- fifo input sequences
--    FIFO_IN_MAP : entity work.fifo_std
--    generic map (
--        NB_BIT_DATA => FIFO_IN_LOGSIZE,
--        FIFOSIZE    => FIFO_IN_SIZE,
--        DATAWIDTH   => FIFO_IN_DATA_WIDTH
--    )
--    port map (
--        Clock_i                 => clk,
--        Reset_i                 => rst_s,
--        Wr_Req_i                => wen_in_s,
--        Rd_Req_i                => rd_en_s,
--        Data_i                  => data_in_wr_s,
--        Fifo_Full_o             => full_in_s,
--        Fifo_Empty_o            => fifo_rd_empty_s,
--        Data_o                  => rd_data_seq_s,
--        Nb_Data_Available_o     => fifo_in_nb_available_s,
--        Nb_Data_Free_o          => mem2user_o.fifo_in_nb_free
--    );
--
--    mem2user_o.fifo_in_full <= full_in_s;


--    ClusteringController : entity work.cluster_fsm
--    port map
--    (
--        clk_i                 => clk,
--        reset_i               => rst_s,
--        endOfParsing_i        => endOfParsing_s,
--        Cluster_idle_i        => user2mem_i.Cluster_idle, 
--        readFromDDR_o         => ddr_out.readSeq,
--        endSeqRead_i          => ddr_in.mem_empty,
--        readsInCache_i        => readsInCache_s,
--        seqFifoEmpty_i        => ddr3_rx_empty, 
--        dataFromPciEmpty_i    => fifo_rd_empty_s,
--        incClusterId_o        => incClusterId_s,
--        dataSourceMux_o       => dataSourceMux_s,
--        taskComleated_o       => taskComleated_s
--    );

--    mem2user_o.incClusterId <= incClusterId_s;


--    rd_en_s         <= user2mem_i.fifo_in_rd and not(dataSourceMux_s);
--    ddr3_rx_rd_en   <= user2mem_i.fifo_in_rd and dataSourceMux_s;
    
--    mem2user_o.fifo_out_nb_available <= fifo_out_nb_available_s;
--    mem2user_o.fifo_in_nb_available  <= fifo_in_nb_available_s when dataSourceMux_s = '0' else ddr3_rx_cnt;
--    mem2user_o.fifo_in_empty         <= fifo_rd_empty_s        when dataSourceMux_s = '0' else ddr3_rx_empty;
--    mem2user_o.fifo_in_rd_data       <= rd_data_seq_s          when dataSourceMux_s = '0' else ddr3_rx_dout;

--    --========================
--    --== FIFOS from/to DDR3 ==
--    --========================

--    ddr3_rx_cnt(12 downto 11) <= "00";
--    ddr3_tx_cnt(12 downto 11) <= "00"; 
--    ddr_out.data_cnt <= ddr3_rx_cnt(10 downto 0);


--    fifo_ddr3_rx_map : entity work.fifo_Std
--        generic map(
--          NB_BIT_DATA => 10,
--          FIFOSIZE    => 2**10,
--          DATAWIDTH   => FIFO_IN_DATA_WIDTH
--        )
--        port map(
--          Clock_i               => clk,
--          Reset_i               => rst_s,
--          Wr_Req_i              => ddr_in.wr_en,
--          Rd_Req_i              => ddr3_rx_rd_en,
--          Data_i                => ddr_in.data,
--          Fifo_Full_o           => ddr_out.full,
--          Fifo_Empty_o          => ddr3_rx_empty,
--          Data_o                => ddr3_rx_dout,
--          Nb_Data_Available_o   => ddr3_rx_cnt(10 downto 0),
--          Nb_Data_Free_o        => open
--        );


--    fifo_ddr3_tx_map : entity work.fifo_Std
--        generic map(
--          NB_BIT_DATA => 10,
--          FIFOSIZE    => 2**10,
--          DATAWIDTH   => FIFO_IN_DATA_WIDTH
--        )
--        port map(
--          Clock_i               => clk,
--          Reset_i               => rst_s,
--          Wr_Req_i              => user2mem_i.seq2ddr_save,
--          Rd_Req_i              => ddr_in.rd_en,
--          Data_i                => subSeq2DDR_s,
--          Fifo_Full_o           => ddr3_tx_full,
--          Fifo_Empty_o          => open,
--          Data_o                => ddr_out.data,
--          Nb_Data_Available_o   => ddr3_tx_cnt(10 downto 0),
--          Nb_Data_Free_o        => open
--        );


--      -- EPO DEBUG
--      -- on attend un moment avant de signaler le nonEmpty a la FMS_DDR
--  process(clk, rst_s)
--    begin
--      if rising_edge(clk) then
--        if rst_s = '1' then
--          ddr_out.empty <= '1'; 
--        else 
--          if (unsigned(ddr3_tx_cnt) > 4) then    -- test empirique 
--            ddr_out.empty <= '0';
--          else
--            ddr_out.empty <= '1';   
--          end if;
--        end if;
--      end if;
--    end process;
          
--      mem2user_o.seq2ddr_ready   <= not(ddr3_tx_full);      --EPO a controler
                                                        
--    ---- bete et mechant
--    -- subSeq2DDR_s <= "00" & user2mem_i.seq2ddr(9) & user2mem_i.seq2ddr(8) & user2mem_i.seq2ddr(7) & user2mem_i.seq2ddr(6) & user2mem_i.seq2ddr(5) & user2mem_i.seq2ddr(4) & user2mem_i.seq2ddr(3) & user2mem_i.seq2ddr(2) & user2mem_i.seq2ddr(1) & user2mem_i.seq2ddr(0);

--    flat_seq2ddr : for seqi in 0 to SUB_SEQUENCE_SIZE-1 generate
--        subSeq2DDR_s(((seqi+1)*NUCLEOTIDE_SIZE)-1 downto seqi*NUCLEOTIDE_SIZE) <= user2mem_i.seq2ddr(seqi);
--    end generate flat_seq2ddr;
--    subSeq2DDR_s(FIFO_IN_DATA_WIDTH-1 downto SUB_SEQUENCE_SIZE*NUCLEOTIDE_SIZE) <= (others => '0');


--    fifo_ddr3_ID_map : entity work.fifo_Std
--        generic map(
--          NB_BIT_DATA => 10,
--          FIFOSIZE    => 2**10,
--          DATAWIDTH   => ID_READ_SIZE
--        )
--        port map(
--          Clock_i               => clk,
--          Reset_i               => rst_s,
--          Wr_Req_i              => seq2ddr_wr_id_s,
--          Rd_Req_i              => ddr_in.IDrd_en,
--          Data_i                => user2mem_i.seq2ddr_id,
--          Fifo_Full_o           => open,
--          Fifo_Empty_o          => open,
--          Data_o                => ddr_out.ID,
--          Nb_Data_Available_o   => open,
--          Nb_Data_Free_o        => open
--        );


--    process(clk, rst_s)
--    begin
--        if rising_edge(clk) then
--            if rst_s = '1' then
--                readsInCache_s <= '0';
--            else 
--                if (user2mem_i.seq2ddr_save = '1') then -- un missmatch detect�
--                    readsInCache_s <= '1';
--                end if;

--                if (incClusterId_s = '1') then -- on passe au prochaine cluster 
--                    readsInCache_s <= '0';
--                end if;
--            end if;
--        end if;
--    end process;


--    process(clk, rst_s)
--    begin
--        if rising_edge(clk) then
--            if rst_s = '1' then
--                seq2ddr_wr_id_s   <= '0';  
--                seq2ddr_save_d1_s <= '0';
--            else 
--                if (seq2ddr_save_d1_s = '0' and user2mem_i.seq2ddr_save = '1') then 
--                    -- flanc montant
--                    seq2ddr_wr_id_s   <= '1';
--                else
--                    seq2ddr_wr_id_s   <= '0';  
--                end if;

--                seq2ddr_save_d1_s   <= user2mem_i.seq2ddr_save;

--            end if;
--        end if;
--    end process;

end Behavioral;
