
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


  entity tag_table is
  port (
    clka  : in  std_logic;
    ena   : in  std_logic;
    wea   : in  std_logic_vector( 0 downto 0);
    addra : in  std_logic_vector( 4 downto 0);
    dina  : in  std_logic_vector(31 downto 0);
    clkb  : in  std_logic;
    enb   : in  std_logic;
    addrb : in  std_logic_vector( 4 downto 0);
    doutb : out std_logic_vector(31 downto 0)
  );
  end entity tag_table;
  
architecture behave of tag_table is

	type mem_t is array(2**5-1 downto 0) of std_logic_vector(31 downto 0);
	signal mem : mem_t;
	
begin

	process(clka) is
	begin
		if rising_edge(clka) then
			if wea(0)='1' then
				mem(to_integer(unsigned(addra))) <= dina;
			end if;
		end if;
	end process;
	
	process(clkb) is
	begin
		if rising_edge(clkb) then
			doutb <= mem(to_integer(unsigned(addrb)));
		end if;
	end process;
	
end behave;
