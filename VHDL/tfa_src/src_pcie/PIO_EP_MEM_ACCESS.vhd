------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : PIO_EP_MEM_ACCESS.vhd
-- Author               : Convers Anthony (ACS)
-- Date                 : 21.04.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Endpoint Memory Access Unit. This module provides access functions
--               to the Endpoint memory aperture.
--   
------------------------------------------------------------------------------------------
-- Dependencies : pio_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version. Merge PCIe2FPGA and old PoSeNoGap
--
------------------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use work.pio_pkg.all;
--    use work.simsynth_pkg.all;
--    use work.clustering_pkg.all;
    use work.overlay_pkg.all;

entity PIO_EP_MEM_ACCESS is
    generic (
        TCQ                   : integer := 1
    );
    port(
        clk                   : in  std_logic;                                                
        rst_n                 : in  std_logic;                                            
        rd_addr               : in  memory_addr_t;
        rd_be                 : in  std_logic_vector( 3 downto 0);
        rd_data               : out std_logic_vector(31 downto 0);
        rd_en                 : in  std_logic;

        wr_addr               : in  memory_addr_t;  -- Memory Read Address;
        wr_be                 : in  std_logic_vector( 7 downto 0);
        wr_data               : in  std_logic_vector(31 downto 0);
        wr_en                 : in  std_logic;                    
        wr_busy               : out std_logic;

        interrupt_mask_wr_o   : out std_logic;
        interrupt_irq_clear_o : out std_logic;
        interrupt_data_wr_o   : out std_logic_vector(31 downto 0);
        interrupt_data_rd_i   : in  std_logic_vector(31 downto 0);
        interrupt_irq_o       : out std_logic_vector(31 downto 0);   

        mem128_wr_i           : in  mem128_wr_interface;
        mem128_rd_ctrl_i      : in  mem128_rd_ctrl_interface;
        mem128_rd_data_o      : out mem128_rd_data_interface;

        -- tag table interface                                  
        rx_eng_tag_i          : in  std_logic_vector( 4 downto 0);
        rx_eng_rd_tag_i       : in  std_logic;                    
        rx_eng_data_tag_o     : out std_logic_vector(31 downto 0);
        tx_eng_tag_i          : in  std_logic_vector( 4 downto 0);
        tx_eng_wr_tag_i       : in  std_logic;                    
        tx_eng_data_tag_i     : in  std_logic_vector(31 downto 0);

        -- internal register                               
        param_tx_i            : in  param_tx_t;
        param_rx_i            : in  param_rx_t;
        config_register_o     : out config_register_t;
        
        -- fifo interface info
        fifo_in_empty_i         : in  std_logic;
        fifo_in_full_i          : in  std_logic;
        fifo_in_count_i         : in  std_logic_vector(10 downto 0);
        fifo_out_empty_i        : in  std_logic;
        fifo_out_full_i         : in  std_logic;
        fifo_out_count_i        : in  std_logic_vector(10 downto 0);
        
        -- user reset
        user_rst_all_o          : out  std_logic;
        user_rst_data_o         : out  std_logic;
        user_data_loopbk_o      : out  std_logic;

        -- overlay interface
        overlay_ram_we_o        : out std_logic;
        overlay_ram_addr_o      : out std_logic_vector(10 downto 0);
        overlay_ram_din_o       : out std_logic_vector(31 downto 0);
        overlay_ram_dout_i      : in  std_logic_vector(31 downto 0);
        overlay_dat_prv_o       : out std_logic_vector(39 downto 0);
        overlay_val_prv_o       : out std_logic;
        overlay_acc_prv_i       : in  std_logic;
        overlay_dat_nxt_i       : in  std_logic_vector(39 downto 0);
        overlay_val_nxt_i       : in  std_logic;
        overlay_acc_nxt_o       : out std_logic
--        mem2user_o            : out mem2user_t;
--        user2mem_i            : in  user2mem_t;

--        ddr_in                : in  ddr2mem_t;
--        ddr_out               : out mem2ddr_t
    );                                                                     
end PIO_EP_MEM_ACCESS;                                                         

architecture Behavioral of PIO_EP_MEM_ACCESS is

    --| Types declarations     |--------------------------------------------------------------  
    -- 

    --| Constants declarations |--------------------------------------------------------------
    --

    --| Signals declarations   |--------------------------------------------------------------
    attribute keep : string;

    signal rst_s                : std_logic;


--    signal rd_data_seq_s        : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);


    signal rd_data_bar0_s       : std_logic_vector(31 downto 0);  
    signal rd_data_bar1_s       : std_logic_vector(31 downto 0);  

    signal wr_data_s            : std_logic_vector(31 downto 0);  
    signal rd_data_s            : std_logic_vector(31 downto 0);
    signal rd_addr_s            : memory_addr_t;
    signal rd_be_s              : std_logic_vector(3 downto 0);

    signal rd_en_bar0_s         : std_logic;
    signal rd_en_bar0_int_s     : std_logic; 
    signal rd_en_bar1_s         : std_logic;
    signal rd_en_bar1_int_s     : std_logic;

    signal wr_en_bar0_s         : std_logic;
    signal wr_en_bar0_int_s     : std_logic; 
    signal wr_en_bar1_s         : std_logic;
    signal wr_en_bar1_int_s     : std_logic;

    --signal mem_receive_header_data_out_s : std_logic_vector(31 downto 0);

    signal config_register_s    : config_register_t;
    signal tag_table_wen_s      : std_logic_vector(0 downto 0);

    signal reg_dword_wr_s : std_logic_vector(31 downto 0);

    signal wen_in_s            : std_logic;
    signal data_in_wr_s        : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);
    attribute keep of data_in_wr_s: signal is "true";
    signal full_in_s           : std_logic;

    signal ren_out_s            : std_logic;
    signal data_out_rd_s       : std_logic_vector(FIFO_OUT_DATA_WIDTH-1 downto 0);




    -- regarder s'il y a besoint de tout ces signaux  ??? :

    signal endOfParsing_s           : std_logic;  
--    signal seq2ddr_wr_id_s          : std_logic;
--    signal seq2ddr_save_d1_s        : std_logic;   
--    signal subSeq2DDR_s             : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);     
    
    signal taskComleated_s          : std_logic; 
--    signal dataSourceMux_s          : std_logic;
    
--    signal rd_en_s                  : std_logic;                       
--    signal fifo_rd_empty_s          : std_logic;                       
--    signal readsInCache_s           : std_logic;                --indique s'il y a eu des missmatch et des reads ont �t�s transfer�s dans la DDR3
--    signal incClusterId_s           : std_logic;

--    signal cluster_size_s           : std_logic_vector(31 downto 0);  
    signal lastFifoRead_s           : std_logic;

    signal fifo_in_nb_available_s   : std_logic_vector(FIFO_IN_LOGSIZE downto 0);  
    signal fifo_out_nb_available_s  : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);  


--    signal ddr3_rx_cnt              : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);
--    signal ddr3_tx_cnt              : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);
    
--    signal ddr3_rx_rd_en            : std_logic;
--    signal ddr3_rx_dout             : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);
--    signal ddr3_rx_empty            : std_logic;

--    signal ddr3_tx_full             : std_logic;  
    
    signal interrupt_mask_s         : std_logic_vector(31 downto 0);
    
    -- overlay interface
    signal overlay_ram_we_s     : std_logic;
    signal overlay_ram_addr_s   : std_logic_vector(10 downto 0);
    signal overlay_ram_din_s    : std_logic_vector(31 downto 0);
    signal overlay_ram_dout_s   : std_logic_vector(31 downto 0);
    signal overlay_dat_prv_s    : std_logic_vector(39 downto 0);
    signal overlay_val_prv_s    : std_logic;
    signal overlay_acc_prv_s    : std_logic;
    signal overlay_dat_nxt_s    : std_logic_vector(39 downto 0);
    signal overlay_val_nxt_s    : std_logic;
    signal overlay_acc_nxt_s    : std_logic;
    
    signal test_return_s        : std_logic_vector(31 downto 0);
    signal cfg_status_s         : std_logic_vector(31 downto 0);
    signal data_status_s        : std_logic_vector(31 downto 0);
    signal reg_msb_din_s        : std_logic_vector(7 downto 0);
    signal reg_ram_we_s         : std_logic;
    signal fifo_in_status_s     : std_logic_vector(31 downto 0);
    signal fifo_out_status_s    : std_logic_vector(31 downto 0);
    signal fifo_in_seuil_s      : std_logic_vector(31 downto 0);
    signal fifo_out_seuil_s     : std_logic_vector(31 downto 0);
    signal fifo_in_seuil_reg    : std_logic_vector(31 downto 0);
    signal fifo_out_seuil_reg   : std_logic_vector(31 downto 0);
    signal fifo_in_count_reg    : std_logic_vector(31 downto 0);
    signal fifo_out_count_reg   : std_logic_vector(31 downto 0);
    signal cmd_irq_fifo_in_seuil  : std_logic;
    signal cmd_irq_fifo_out_seuil : std_logic;
    
    signal reg_rst_all_s        : std_logic_vector(3 downto 0);
    signal reg_rst_data_s       : std_logic_vector(3 downto 0);
    signal cmd_rst_all_s        : std_logic;
    signal cmd_rst_data_s       : std_logic;
    signal cmd_data_loopbk_s    : std_logic;
    signal user_rst_all_s       : std_logic;
    signal user_rst_data_s      : std_logic;
    signal user_data_loopbk_s   : std_logic;
    
    signal data_count_128b_s    : std_logic_vector(10 downto 0);
    signal fifo128_dat_nxt_s    : std_logic_vector(31 downto 0);
    signal fifo128_val_nxt_s    : std_logic;
    signal fifo128_acc_nxt_s    : std_logic;
    signal rst_fifo128_s        : std_logic;
    
    signal debug_error_reg_s    : std_logic_vector(31 downto 0);
    signal debug_1_reg_s    : unsigned(7 downto 0);
    signal debug_2_reg_s    : unsigned(7 downto 0);
    signal debug_3_reg_s    : unsigned(7 downto 0);
    signal debug_4_reg_s    : unsigned(7 downto 0);

begin

--    ddr_out.cnumber <= std_logic_vector(to_unsigned(N_CLUSTER, 16)) ;

    -- user reset signal
    user_rst_all_o      <= user_rst_all_s;
    user_rst_data_o     <= user_rst_data_s;
    user_data_loopbk_o  <= user_data_loopbk_s;
    
    -- overlay signal
    overlay_ram_we_o    <= overlay_ram_we_s;
    overlay_ram_addr_o  <= overlay_ram_addr_s;
    overlay_ram_din_o   <= overlay_ram_din_s;
    overlay_ram_dout_s  <= overlay_ram_dout_i;
    --
    overlay_dat_prv_o   <= overlay_dat_prv_s;
    overlay_val_prv_o   <= overlay_val_prv_s;
    overlay_acc_prv_s   <= overlay_acc_prv_i;
    --
    overlay_dat_nxt_s   <= overlay_dat_nxt_i;
    overlay_val_nxt_s   <= overlay_val_nxt_i;
    overlay_acc_nxt_o   <= overlay_acc_nxt_s;
    
    --| Inputs processing      |--------------------------------------------------------------

    rst_s <= not rst_n;

    bar0_proc : process(clk) is

    begin
        if (rst_n = '0') then

            reg_dword_wr_s <= (others => '0');
            data_in_wr_s  <= (others => '0');
            wen_in_s <= '0';
            
            --mem128_rd_data_o.data <= (others => '0');
            ren_out_s <= '0';
            debug_error_reg_s <= (others => '0');
            debug_1_reg_s <= (others => '0');
            debug_2_reg_s <= (others => '0');
            debug_3_reg_s <= (others => '0');
            debug_4_reg_s <= (others => '0');

        elsif rising_edge(clk) then

            wen_in_s <= '0';
            ren_out_s <= '0';
            
            debug_error_reg_s(31 downto 24) <= std_logic_vector(debug_4_reg_s);
            debug_error_reg_s(23 downto 16) <= std_logic_vector(debug_3_reg_s);
            debug_error_reg_s(15 downto 08) <= std_logic_vector(debug_2_reg_s);
            debug_error_reg_s(07 downto 00) <= std_logic_vector(debug_1_reg_s);

            -- Burst write access -> Sequence FIFO
            if (mem128_wr_i.wr = '1') then

                -- TODO : test address

                if (mem128_wr_i.dword_en="1000") then -- start burst

                    reg_dword_wr_s  <=  mem128_wr_i.data(127 downto 96);
                    wen_in_s <= '0';

                elsif (mem128_wr_i.dword_en="1111") then -- middle burst

                    --data_in_wr_s   <=   reg_dword_wr_s & 
                    --                    mem128_wr_i.data( 31 downto 00) & 
                    --                    mem128_wr_i.data( 63 downto 32) & 
                    --                    mem128_wr_i.data( 95 downto 64);
                    
                    data_in_wr_s   <=   mem128_wr_i.data( 95 downto 64) & 
                                        mem128_wr_i.data( 63 downto 32) & 
                                        mem128_wr_i.data( 31 downto 00) & 
                                        reg_dword_wr_s;
                    

                    reg_dword_wr_s  <=  mem128_wr_i.data(127 downto 96);

                    if (overlay_acc_prv_s = '1') then
                         wen_in_s <= '1';
--                        note("[PIO_EP_MEM_ACCESS] data write to fifo : " &  to_hstring(reg_dword_wr_s & 
--                                                                            mem128_wr_i.data( 31 downto 00) & 
--                                                                            mem128_wr_i.data( 63 downto 32) & 
--                                                                            mem128_wr_i.data( 95 downto 64))); -- tmp FCC
                    else
                        wen_in_s <= '0';
                        debug_1_reg_s <= debug_1_reg_s + 1;
                        -- data ignored !!! TODO : Generate interrupt ?? FCC
--                        error("[PIO_EP_MEM_ACCESS] FIFO full ! Data ignored");
                    end if;

                elsif (mem128_wr_i.dword_en="0111") then -- end burst

                    --data_in_wr_s   <=   reg_dword_wr_s & 
                    --                    mem128_wr_i.data( 31 downto 00) & 
                    --                    mem128_wr_i.data( 63 downto 32) & 
                    --                    mem128_wr_i.data( 95 downto 64);
                    
                    data_in_wr_s   <=   mem128_wr_i.data( 95 downto 64) & 
                                        mem128_wr_i.data( 63 downto 32) & 
                                        mem128_wr_i.data( 31 downto 00) & 
                                        reg_dword_wr_s;
                    
                    if (overlay_acc_prv_s = '1') then
                        wen_in_s <= '1';
                        --note("[PIO_EP_MEM_ACCESS] final data write to fifo : " &  to_hstring(reg_dword_wr_s & 
                                                                            --mem128_wr_i.data( 31 downto 00) & 
                                                                            --mem128_wr_i.data( 63 downto 32) & 
                                                                            --mem128_wr_i.data( 95 downto 64))); -- tmp FCC
                    else
                        wen_in_s <= '0';
                        debug_1_reg_s <= debug_1_reg_s + 1;
                        -- data ignored !!! TODO : Generate interrupt before that !! FCC
--                        error("[PIO_EP_MEM_ACCESS] FIFO full ! Data ignored");
                    end if;

                elsif (mem128_wr_i.dword_en="0011") then -- end burst, this means that burst lenght isn't 4 multiple
--                    error("[PIO_EP_MEM_ACCESS] Burst lenght need to be 4 multiple");
                    wen_in_s <= '0';
                    debug_2_reg_s <= debug_2_reg_s + 1;
                    --null;
                elsif (mem128_wr_i.dword_en="0001") then -- end burst, this means that burst lenght isn't 4 multiple
--                    error("[PIO_EP_MEM_ACCESS] Burst lenght need to be 4 multiple");
                    wen_in_s <= '0';
                    debug_2_reg_s <= debug_2_reg_s + 1;
                    --null;
                else
--                    error("[PIO_EP_MEM_ACCESS] Dword enable not handled by the system");
                    wen_in_s <= '0';
                    debug_3_reg_s <= debug_3_reg_s + 1;
                    --null;
                end if;

            -- single/double dword write in bar 0 
            elsif wr_en_bar0_s = '1' then
--                error("[PIO_EP_MEM_ACCESS] Single or double dword write in bar 0 not used");
                debug_4_reg_s <= debug_4_reg_s + 1;
                --null;
            end if;


            -- pour l'instant lecture classique... 32bits par 32bits   
            if (rd_en_bar0_s = '1') then
                ren_out_s <= '1';
            end if;

            -- TODO : fifo de 128 : a verifier ....
            -- if mem128_rd_ctrl_i.rd_en = '1' then
            --     -- mem128_rd_data_o.data(31 downto 0) <= memory128(0)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            --     -- mem128_rd_data_o.data(63 downto 32) <= memory128(1)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            --     -- mem128_rd_data_o.data(95 downto 64) <= memory128(2)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            --     -- mem128_rd_data_o.data(127 downto 96) <= memory128(3)(to_integer(unsigned(mem128_rd_ctrl_i.addr(mem128_rd_ctrl_i.addr'high downto 2))));
            -- end if;

        end if;
    end process;

    -- test FCC
    --mem128_rd_data_o.data( 95 downto  0) <= (others => '0');
    --mem128_rd_data_o.data(127 downto 96) <= data_out_rd_s;
    --rd_data_bar0_s <= data_out_rd_s;
    
    -- test ACS
    -- write fifo in part (DMA PC->FPGA)
    -- Full 128 bit is used for data transfer
    U01: entity work.data_in_fifo128to32 port map (
           clk => clk,
           reset => rst_fifo128_s,
           wr_en_i => mem128_wr_i.wr,
           dword_en_i => mem128_wr_i.dword_en,
           data_i => mem128_wr_i.data,
           data_count_o => data_count_128b_s,
           dat_nxt_o => fifo128_dat_nxt_s,
           val_nxt_o => fifo128_val_nxt_s,
           acc_nxt_i => fifo128_acc_nxt_s
         );
    
    overlay_dat_prv_s(31 downto  0) <= fifo128_dat_nxt_s;
    overlay_dat_prv_s(39 downto 32) <= x"00";
    overlay_val_prv_s <= fifo128_val_nxt_s;
    fifo128_acc_nxt_s <= overlay_acc_prv_s;
    
--    -- version only 40bit used on 128bit
--    overlay_dat_prv_s(31 downto  0) <= data_in_wr_s(31 downto  0);
--    overlay_dat_prv_s(39 downto 32) <= data_in_wr_s(39 downto 32);
--    overlay_val_prv_s <= wen_in_s;

    
    -- read fifo out part (DMA FPGA->PC)
    data_out_rd_s <= x"DEADBEEF";
    mem128_rd_data_o.data( 31 downto  0) <= overlay_dat_nxt_s(31 downto 0);
    mem128_rd_data_o.data( 63 downto 32) <= x"000d00"&overlay_dat_nxt_s(39 downto 32);
    mem128_rd_data_o.data( 95 downto 64) <= x"000e000e";
    mem128_rd_data_o.data(127 downto 96) <= x"000f000f";
    rd_data_bar0_s <= data_out_rd_s;
    overlay_acc_nxt_s <= mem128_rd_ctrl_i.rd_fifo_en;


    -- assignment of interal write value
    wr_data_s  <= wr_data;

    -- memory Write Controller
    wr_en_bar0_int_s <= '1' when (wr_addr.region = "00") else '0';
    wr_en_bar1_int_s <= '1' when (wr_addr.region = "01") else '0';
    wr_en_bar0_s     <= wr_en and wr_en_bar0_int_s; 
    wr_en_bar1_s     <= wr_en and wr_en_bar1_int_s;

    -- memory Read Controller
    rd_en_bar0_int_s <= '1' when (rd_addr.region = "00") else '0';
    rd_en_bar1_int_s <= '1' when (rd_addr.region = "01") else '0';  
    rd_en_bar0_s     <= rd_en and rd_en_bar0_int_s; 
    rd_en_bar1_s     <= rd_en and rd_en_bar1_int_s; 

    -- write controller busy
    wr_busy <= wr_en;

    process(rst_n, clk)
    begin
        if (rst_n = '0') then
            rd_addr_s <= ((others=>'0'),(others=>'0'));
            rd_be_s   <= (others=>'0');
        elsif rising_edge(clk) then
            rd_addr_s <= rd_addr;
            rd_be_s   <= rd_be;
        end if;
    end process;


--    ddr_out.csize <= cluster_size_s;

    -- read bar selection
    rd_bar1_proc : process(rd_addr_s, rd_data_bar0_s, rd_data_bar1_s)--, mem_receive_header_data_out_s)
    begin
        case (rd_addr_s.region) is 
            when "00" => 
                rd_data_s <= rd_data_bar0_s;
            when "01" => 
                --if(rd_addr_s.addr(LOG_ADDR_SIZE) = '1') then    -- FCC ?? 
                --    rd_data_s  <= mem_receive_header_data_out_s;
                --else
                    rd_data_s <= rd_data_bar1_s;
                --end if;
            when others => rd_data_s <= rd_data_bar0_s;
        end case;
    end process;
                                                                                                                                  
    --interrupt_mask_wr_o <= '1' when wr_en_bar1_s='1' and unsigned(wr_addr.addr(8 downto 0))=ADDR_IRQ_SET_MASK else '0';
    --interrupt_irq_clear_o <= '1' when wr_en_bar1_s='1' and unsigned(wr_addr.addr(8 downto 0))=ADDR_IRQ_CLEAR else '0';
    --interrupt_data_wr_o <= wr_data_s;

    -- reset generation
    rst_gen_proc : process(rst_n, clk)
    begin
        if (rst_n = '0') then
            reg_rst_all_s     <= "1111";
            reg_rst_data_s    <= "1111";
            user_rst_all_s    <= '1';
            user_rst_data_s   <= '1';
            user_data_loopbk_s <= '0';
            rst_fifo128_s     <= '1';
        elsif rising_edge(clk) then
            
            if cmd_rst_all_s='1' then
                reg_rst_all_s <= "1111";
            else
                reg_rst_all_s     <= reg_rst_all_s(2 downto 0)& '0';
            end if;
            
            if cmd_rst_data_s='1' then
                reg_rst_data_s <= "1111";
            else
                reg_rst_data_s    <= reg_rst_data_s(2 downto 0)& '0';
            end if;
            
            user_rst_all_s      <= reg_rst_all_s(3);
            user_rst_data_s     <= reg_rst_data_s(3);
            
            if (user_rst_all_s='1' or user_rst_data_s='1') then
                rst_fifo128_s <= '1';
            else
                rst_fifo128_s <= '0';
            end if;
            
            user_data_loopbk_s  <= cmd_data_loopbk_s;
            
        end if;
    end process;
    
    -- fifo threshold irq generation
    fifo_irq_proc : process(rst_n, clk)
    begin
        if (rst_n = '0') then
            fifo_in_seuil_reg        <= x"00000200";    --default = 512 (default equal to fifo_in_seuil_s)
            fifo_out_seuil_reg       <= x"00000200";    --default = 512 (default equal to fifo_out_seuil_s)
            fifo_in_count_reg        <= x"00000000";
            fifo_out_count_reg       <= x"00000000";
            cmd_irq_fifo_in_seuil    <= '0';
            cmd_irq_fifo_out_seuil   <= '0';
        elsif rising_edge(clk) then
            fifo_in_seuil_reg  <= fifo_in_seuil_s;
            fifo_out_seuil_reg <= fifo_out_seuil_s;
            fifo_in_count_reg  <= x"00000" & '0' &fifo_in_count_i;
            fifo_out_count_reg <= x"00000" & '0' &fifo_out_count_i;
            
            if (fifo_in_count_reg <= fifo_in_seuil_reg) then
                cmd_irq_fifo_in_seuil <= '1';
            else
                cmd_irq_fifo_in_seuil <= '0';
            end if;
            
            if (fifo_out_count_reg >= fifo_out_seuil_reg) and (fifo_out_empty_i='0') then
                cmd_irq_fifo_out_seuil <= '1';
            else
                cmd_irq_fifo_out_seuil <= '0';
            end if;
            
            
        end if;
    end process;


    -- write internal configuration register process (Bar 1)
    wr_bar1_proc : process(rst_n, clk)
    begin
        if (rst_n = '0') then

            config_register_s.command                   <= (others => '0');
            config_register_s.write_src_address         <= (others => '0');
            config_register_s.write_dst_address         <= (others => '0');
            config_register_s.write_length              <= (others => '0');
            config_register_s.debug                     <= (others => '0');
            config_register_s.write_nb_packet           <= (others => '0');
            config_register_s.read_src_address          <= (others => '0');
            config_register_s.read_dst_address          <= (others => '0');
            config_register_s.read_length               <= (others => '0');
            config_register_s.read_nb_packet            <= (others => '0');
            config_register_s.read_pipe_size            <= x"00000003"; --(0=>'1', others => '0');
            config_register_s.debug                     <= (others => '0');
            config_register_s.write_msg_header          <= (others => '0'); 
            interrupt_irq_o <= (others=>'0');
            interrupt_mask_wr_o <= '0';
            interrupt_irq_clear_o <= '0';
            interrupt_data_wr_o <= (others => '0');
            interrupt_mask_s <= (others => '0');

            lastFifoRead_s           <= '0';

            --cluster_size_s  <= x"00000004";
            endOfParsing_s  <= '0';
            -- interface overlay
            overlay_ram_we_s        <= '0';
            overlay_ram_addr_s      <= (others => '0');
            overlay_ram_din_s       <= (others => '0');
            test_return_s           <= x"00000046";
            --reg_msb_din_s           <= (others => '0');
            --overlay_dat_prv_s       <= (others => '0');
            --overlay_val_prv_s       <= '0';
            reg_ram_we_s            <= '1';
            cfg_status_s            <= (others => '0');
            cmd_rst_all_s           <= '1';
            cmd_rst_data_s          <= '1';
            cmd_data_loopbk_s       <= '0';
            fifo_in_seuil_s         <= x"00000200";   --default = 512 (default equal to fifo_in_seuil_reg)
            fifo_out_seuil_s        <= x"00000200";   --default = 512 (default equal to fifo_out_seuil_reg)
        elsif rising_edge(clk) then

            interrupt_irq_o <= (others=>'0');
            interrupt_mask_wr_o     <= '0';                    
            interrupt_irq_clear_o   <= '0';  
            overlay_ram_we_s        <= '0';
            overlay_ram_addr_s      <= "000"&x"02"; --status address (read)
            --overlay_val_prv_s       <= '0';
            reg_ram_we_s            <= overlay_ram_we_s;
            cmd_rst_all_s           <= '0';
            cmd_rst_data_s          <= '0';

            if (wr_en_bar1_s = '1') then
                if wr_addr.addr(11) = '1' then   --wr_addr.addr(8) address from 256 to 511.
                    --write overlay config in ram
                    overlay_ram_we_s <= '1';
                    overlay_ram_addr_s <= wr_addr.addr(10 downto 0);
                    overlay_ram_din_s <= wr_data_s;
                    
                else                
                    case to_integer(unsigned(wr_addr.addr(8 downto 0))) is 
                        when ADDR_COMMAND            =>  config_register_s.command           <= wr_data_s;
                        when ADDR_WRITE_SRC_ADDRESS  =>  config_register_s.write_src_address <= wr_data_s;
                        when ADDR_WRITE_DST_ADDRESS  =>  config_register_s.write_dst_address <= wr_data_s;
                        when ADDR_WRITE_LENGTH       =>  config_register_s.write_length      <= wr_data_s;
                        when ADDR_WRITE_NB_PACKET    =>  config_register_s.write_nb_packet   <= wr_data_s;
                        when ADDR_READ_SRC_ADDRESS   =>  config_register_s.read_src_address  <= wr_data_s;
                        when ADDR_READ_DST_ADDRESS   =>  config_register_s.read_dst_address  <= wr_data_s;
                        when ADDR_READ_LENGTH        =>  config_register_s.read_length       <= wr_data_s;
                        when ADDR_READ_NB_PACKET     =>  config_register_s.read_nb_packet    <= wr_data_s;     
                        when ADDR_READ_PIPE_SIZE     =>  config_register_s.read_pipe_size    <= wr_data_s;
                        when ADDR_DEBUG              =>  config_register_s.debug             <= wr_data_s;    
                        when ADDR_WRITE_MSG_HEADER   =>  config_register_s.write_msg_header  <= wr_data_s;    
                        -- IRQ
                        when ADDR_IRQ_GENERATE       =>  interrupt_irq_o                     <= wr_data_s;
                        when ADDR_IRQ_SET_MASK       =>  interrupt_mask_wr_o                 <= '1';                    
                                                         interrupt_mask_s                    <= wr_data_s; -- this register is only use to read back the mask information
                                                         interrupt_data_wr_o                 <= wr_data_s; -- this values goes straight to the IRQ controller
                        when ADDR_IRQ_CLEAR          =>  interrupt_irq_clear_o               <= '1';     
                                                         interrupt_data_wr_o                 <= wr_data_s;                                                      

                        -- User config :
                        when ADDR_TEST_RETURN        =>  test_return_s                       <= wr_data_s;
                        --when ADDR_MSB_DATA_IN        =>  reg_msb_din_s                       <= wr_data_s(7 downto 0);
                        --when ADDR_LSB_DATA_IN        =>  overlay_dat_prv_s(31 downto 0)      <= wr_data_s;
                        --                                 overlay_dat_prv_s(39 downto 32)     <= reg_msb_din_s;
                        --                                 overlay_val_prv_s <= '1';
                        when ADDR_OVLY_ALL_RST       =>  cmd_rst_all_s                       <= wr_data_s(0);
                        when ADDR_OVLY_DATA_RST      =>  cmd_rst_data_s                      <= wr_data_s(0);
                        when ADDR_DATA_LOOPBACK      =>  cmd_data_loopbk_s                   <= wr_data_s(0);
                        when ADDR_FIFO_IN_THRESHOLD  =>  fifo_in_seuil_s                     <= wr_data_s;
                        when ADDR_FIFO_OUT_THRESHOLD =>  fifo_out_seuil_s                    <= wr_data_s;
                        
                        
                        --when ADDR_CLUSTER_SIZE      =>  cluster_size_s                      <= wr_data;        
                        --when ADDR_PARSING_END       =>  endOfParsing_s                      <= wr_data(0);

                        when others => null;
                    end case;
                end if;
            end if;

--            if(param_tx_i.end_of_command(0) = '1') then
--                interrupt_irq_o(0)<='1';
--            end if;

--            if(param_tx_i.mem_acq_cmd(0) = '1') then
--                config_register_s.command(0) <= '0';
--            end if;

           if (param_tx_i.end_of_command(1) = '1') then
               interrupt_irq_o(1)<='1';
           end if;

           if(param_tx_i.mem_acq_cmd(1) = '1') then
               config_register_s.command(COMMAND_BIT_DMA_WRITE) <= '0';
           end if;

           if (param_rx_i.end_of_read_cpld = '1') then
               interrupt_irq_o(2)<='1';
           end if;

--            if (param_rx_i.new_poisoned_tlp = '1') then
--                interrupt_irq_o(3) <= '1';
--            end if;

            -- irq fifo out full    
--            if user2mem_i.irq_trigger(1) = '1' then 
--                --interrupt_irq_o(16) <= '1';
--                interrupt_irq_o(15) <= '1';
--            end if;

            -- irq one result saved  -- TODO remove this interrupt
--            if user2mem_i.irq_trigger(0) = '1' then 
--                interrupt_irq_o(17) <= '1';
--            end if;
            
            if fifo_in_empty_i='1' then
                interrupt_irq_o(4) <= '1';
            end if;
            
            if fifo_in_full_i='1' then
                interrupt_irq_o(5) <= '1';
            end if;
            
            --if fifo_out_empty_i='0' then
            --    interrupt_irq_o(6) <= '1';
            --end if;
            
            if fifo_out_full_i='1' then
                interrupt_irq_o(7) <= '1';
            end if;
            
            if cfg_status_s(1)='1' then
                interrupt_irq_o(8) <= '1';
            end if;
            
            if cmd_irq_fifo_in_seuil='1' then
                interrupt_irq_o(10) <= '1';
            end if;
            
            if cmd_irq_fifo_out_seuil='1' then
                interrupt_irq_o(11) <= '1';
            end if;

            
            if reg_ram_we_s = '0' then
                --update config status when a read is done.
                cfg_status_s <= overlay_ram_dout_s;
            end if;

--            if (taskComleated_s = '1' and endOfParsing_s = '1') then 
--                interrupt_irq_o(15)  <= '1';  
--                endOfParsing_s       <= '0';    -- EPO plus valide lorsque on utilise IRQ partielle pour vider la fifo. 
--                --ddr_out.readSeq     <= '1';   -- EPO debug 
--                lastFifoRead_s       <= '1';    -- tag pour le soft, signale la dernière lecture de la fifo
--            end if;    

           if(param_tx_i.mem_acq_cmd(2) = '1') then
               config_register_s.command(COMMAND_BIT_DMA_READ) <= '0';
           end if;

           if(config_register_s.command(COMMAND_BIT_LOG_RESET) = '1') then
               config_register_s.command(COMMAND_BIT_LOG_RESET) <= '0';
           end if;

        end if;
    end process;

    -- Read internal configuration register process (Bar 1)
    process(rst_n,clk)
    begin
        if (rst_n = '0') then
            rd_data_bar1_s <= (others => '0'); 
            --overlay_acc_nxt_s   <= '0';
            data_status_s       <= (others => '0'); 
            fifo_in_status_s    <= (others => '0'); 
            fifo_out_status_s   <= (others => '0'); 
        elsif rising_edge(clk) then
            --overlay_acc_nxt_s             <= '0';
            data_status_s(31 downto 9)    <= (others => '0');
            data_status_s(8)              <= overlay_val_nxt_s;
            data_status_s( 7 downto 1)    <= (others => '0');
            data_status_s(0)              <= overlay_acc_prv_s;
            --fifo status
            fifo_in_status_s(31 downto 9)    <= (others => '0');
            fifo_in_status_s(8)              <= fifo_in_full_i;
            fifo_in_status_s( 7 downto 1)    <= (others => '0');
            fifo_in_status_s(0)              <= fifo_in_empty_i;
            fifo_out_status_s(31 downto 9)   <= (others => '0');
            fifo_out_status_s(8)             <= fifo_out_full_i;
            fifo_out_status_s( 7 downto 1)   <= (others => '0');
            fifo_out_status_s(0)             <= fifo_out_empty_i;
            
            if (rd_en_bar1_s = '1') then
                case to_integer(unsigned(rd_addr.addr(MEMORY_SIZE-1 downto 0))) is
                    when ADDR_COMMAND                 => rd_data_bar1_s <= config_register_s.command;         
                    when ADDR_WRITE_SRC_ADDRESS       => rd_data_bar1_s <= config_register_s.write_src_address;
                    when ADDR_WRITE_DST_ADDRESS       => rd_data_bar1_s <= config_register_s.write_dst_address;
                    when ADDR_WRITE_LENGTH            => rd_data_bar1_s <= config_register_s.write_length;
                    when ADDR_WRITE_NB_PACKET         => rd_data_bar1_s <= config_register_s.write_nb_packet;
                    when ADDR_READ_SRC_ADDRESS        => rd_data_bar1_s <= config_register_s.read_src_address;
                    when ADDR_READ_DST_ADDRESS        => rd_data_bar1_s <= config_register_s.read_dst_address;
                    when ADDR_READ_LENGTH             => rd_data_bar1_s <= config_register_s.read_length;
                    when ADDR_READ_NB_PACKET          => rd_data_bar1_s <= config_register_s.read_nb_packet;
                    when ADDR_READ_PIPE_SIZE          => rd_data_bar1_s <= config_register_s.read_pipe_size;
                    when ADDR_DEBUG                   => rd_data_bar1_s <= config_register_s.debug;     
                    when ADDR_WRITE_MSG_HEADER        => rd_data_bar1_s <= config_register_s.write_msg_header;  
                    -- address of the Log memory - byte aligned
                    when ADDR_LOG_MEMORY_ADDR         => rd_data_bar1_s <= std_logic_vector(to_unsigned(2**LOG_ADDR_SIZE,30))&"00";
                    -- size of the Log memory - byte aligned
                    when ADDR_LOG_MEMORY_SIZE         => rd_data_bar1_s <= std_logic_vector(to_unsigned(2**LOG_ADDR_SIZE,30))&"00";
                    when ADDR_VERSION                 => rd_data_bar1_s <= VERSION_VEC;
                    --IRQ
                    when ADDR_IRQ_REGISTER            => rd_data_bar1_s <= interrupt_data_rd_i;
                    when ADDR_IRQ_SET_MASK            => rd_data_bar1_s <= interrupt_mask_s;


                    -- User config :
                    when ADDR_DESIGN_VERSION     =>  rd_data_bar1_s      <= VERSION_VEC;
                    when ADDR_TEST_RETURN        =>  rd_data_bar1_s      <= test_return_s;
                    when ADDR_FIFO_IN_STATUS     =>  rd_data_bar1_s      <= fifo_in_status_s;
                    when ADDR_FIFO_IN_COUNT      =>  rd_data_bar1_s      <= x"00000" & '0' & fifo_in_count_i;
                    when ADDR_FIFO_OUT_STATUS    =>  rd_data_bar1_s      <= fifo_out_status_s;
                    when ADDR_FIFO_OUT_COUNT     =>  rd_data_bar1_s      <= x"00000" & '0' & fifo_out_count_i;
                    when ADDR_FIFO_IN_THRESHOLD  =>  rd_data_bar1_s      <= fifo_in_seuil_s;
                    when ADDR_FIFO_OUT_THRESHOLD =>  rd_data_bar1_s      <= fifo_out_seuil_s;
                    when ADDR_LINE_OVLY_STATUS   =>  rd_data_bar1_s      <= LINE_OVLY_VEC;
                    when ADDR_COL_OVLY_STATUS    =>  rd_data_bar1_s      <= COL_OVLY_VEC;
                    
                    --when ADDR_MSB_DATA_OUT       =>  rd_data_bar1_s      <= x"000000" & overlay_dat_nxt_s(39 downto 32);
                    --when ADDR_LSB_DATA_OUT       =>  rd_data_bar1_s      <= overlay_dat_nxt_s(31 downto 0);
                    --                                 overlay_acc_nxt_s   <= '1';
                    when ADDR_CFG_STATUS         =>  rd_data_bar1_s      <= cfg_status_s;
                    when ADDR_DATA_STATUS        =>  rd_data_bar1_s      <= data_status_s;
                    when ADDR_DATA_LOOPBACK      =>  rd_data_bar1_s      <=  x"0000000" & "000" & cmd_data_loopbk_s;
                    when ADDR_DEBUG_ERROR        =>  rd_data_bar1_s      <= debug_error_reg_s;
                    
                    
                    --when ADDR_NB_RESULTS        =>  rd_data_bar1_s   <= lastFifoRead_s & x"0000" & "00" & fifo_out_nb_available_s;
                    --when ADDR_NB_SEQ            =>  rd_data_bar1_s   <= x"0000" & "000" & fifo_in_nb_available_s;
                    --when ADDR_CLUSTER_SIZE      =>  rd_data_bar1_s   <= cluster_size_s;
                    
                    when others => rd_data_bar1_s <= x"DEADBEEF"; -- fake data returned when the read address is unmapped.
                end case;

            end if; 
        end if;
    end process;


    --Tag table
    TagTable : entity work.tag_table                            
    port map (                                             
        clka  => clk,                                                     
        ena   => '1',                                                 
        wea   => tag_table_wen_s,                                              
        addra => tx_eng_tag_i,                                                     
        dina  => tx_eng_data_tag_i,                                                  
        clkb  => clk,                                                
        enb   => '1',      
        addrb => rx_eng_tag_i,      
        doutb => rx_eng_data_tag_o     
    );                                                                                   

    tag_table_wen_s(0) <= tx_eng_wr_tag_i;

    --| outputs processing     |-------------------------------------------------------------- 

    rd_data <= rd_data_s;  


    config_register_o <= config_register_s;


    --| Clustering FIFOs      |--------------------------------------------------------------

--    -- fifo output result                    
--    FIFO_OUT_MAP : entity work.fifo_std
--    generic map (
--        NB_BIT_DATA => FIFO_OUT_LOGSIZE,
--        FIFOSIZE    => FIFO_OUT_SIZE,
--        DATAWIDTH   => FIFO_OUT_DATA_WIDTH
--    )
--    port map (
--        Clock_i                 => clk,
--        Reset_i                 => rst_s,
--        Wr_Req_i                => user2mem_i.fifo_out_wr,
--        Rd_Req_i                => ren_out_s,
--        Data_i                  => user2mem_i.fifo_out_wr_data,
--        Fifo_Full_o             => mem2user_o.fifo_out_full,
--        Fifo_Empty_o            => mem2user_o.fifo_out_empty,
--        Data_o                  => data_out_rd_s,
--        Nb_Data_Available_o     => fifo_out_nb_available_s,
--        Nb_Data_Free_o          => mem2user_o.fifo_out_nb_free
--    );

--    -- fifo input sequences
--    FIFO_IN_MAP : entity work.fifo_std
--    generic map (
--        NB_BIT_DATA => FIFO_IN_LOGSIZE,
--        FIFOSIZE    => FIFO_IN_SIZE,
--        DATAWIDTH   => FIFO_IN_DATA_WIDTH
--    )
--    port map (
--        Clock_i                 => clk,
--        Reset_i                 => rst_s,
--        Wr_Req_i                => wen_in_s,
--        Rd_Req_i                => rd_en_s,
--        Data_i                  => data_in_wr_s,
--        Fifo_Full_o             => full_in_s,
--        Fifo_Empty_o            => fifo_rd_empty_s,
--        Data_o                  => rd_data_seq_s,
--        Nb_Data_Available_o     => fifo_in_nb_available_s,
--        Nb_Data_Free_o          => mem2user_o.fifo_in_nb_free
--    );
--
--    mem2user_o.fifo_in_full <= full_in_s;


--    ClusteringController : entity work.cluster_fsm
--    port map
--    (
--        clk_i                 => clk,
--        reset_i               => rst_s,
--        endOfParsing_i        => endOfParsing_s,
--        Cluster_idle_i        => user2mem_i.Cluster_idle, 
--        readFromDDR_o         => ddr_out.readSeq,
--        endSeqRead_i          => ddr_in.mem_empty,
--        readsInCache_i        => readsInCache_s,
--        seqFifoEmpty_i        => ddr3_rx_empty, 
--        dataFromPciEmpty_i    => fifo_rd_empty_s,
--        incClusterId_o        => incClusterId_s,
--        dataSourceMux_o       => dataSourceMux_s,
--        taskComleated_o       => taskComleated_s
--    );

--    mem2user_o.incClusterId <= incClusterId_s;


--    rd_en_s         <= user2mem_i.fifo_in_rd and not(dataSourceMux_s);
--    ddr3_rx_rd_en   <= user2mem_i.fifo_in_rd and dataSourceMux_s;
    
--    mem2user_o.fifo_out_nb_available <= fifo_out_nb_available_s;
--    mem2user_o.fifo_in_nb_available  <= fifo_in_nb_available_s when dataSourceMux_s = '0' else ddr3_rx_cnt;
--    mem2user_o.fifo_in_empty         <= fifo_rd_empty_s        when dataSourceMux_s = '0' else ddr3_rx_empty;
--    mem2user_o.fifo_in_rd_data       <= rd_data_seq_s          when dataSourceMux_s = '0' else ddr3_rx_dout;

--    --========================
--    --== FIFOS from/to DDR3 ==
--    --========================

--    ddr3_rx_cnt(12 downto 11) <= "00";
--    ddr3_tx_cnt(12 downto 11) <= "00"; 
--    ddr_out.data_cnt <= ddr3_rx_cnt(10 downto 0);


--    fifo_ddr3_rx_map : entity work.fifo_Std
--        generic map(
--          NB_BIT_DATA => 10,
--          FIFOSIZE    => 2**10,
--          DATAWIDTH   => FIFO_IN_DATA_WIDTH
--        )
--        port map(
--          Clock_i               => clk,
--          Reset_i               => rst_s,
--          Wr_Req_i              => ddr_in.wr_en,
--          Rd_Req_i              => ddr3_rx_rd_en,
--          Data_i                => ddr_in.data,
--          Fifo_Full_o           => ddr_out.full,
--          Fifo_Empty_o          => ddr3_rx_empty,
--          Data_o                => ddr3_rx_dout,
--          Nb_Data_Available_o   => ddr3_rx_cnt(10 downto 0),
--          Nb_Data_Free_o        => open
--        );


--    fifo_ddr3_tx_map : entity work.fifo_Std
--        generic map(
--          NB_BIT_DATA => 10,
--          FIFOSIZE    => 2**10,
--          DATAWIDTH   => FIFO_IN_DATA_WIDTH
--        )
--        port map(
--          Clock_i               => clk,
--          Reset_i               => rst_s,
--          Wr_Req_i              => user2mem_i.seq2ddr_save,
--          Rd_Req_i              => ddr_in.rd_en,
--          Data_i                => subSeq2DDR_s,
--          Fifo_Full_o           => ddr3_tx_full,
--          Fifo_Empty_o          => open,
--          Data_o                => ddr_out.data,
--          Nb_Data_Available_o   => ddr3_tx_cnt(10 downto 0),
--          Nb_Data_Free_o        => open
--        );


--      -- EPO DEBUG
--      -- on attend un moment avant de signaler le nonEmpty a la FMS_DDR
--  process(clk, rst_s)
--    begin
--      if rising_edge(clk) then
--        if rst_s = '1' then
--          ddr_out.empty <= '1'; 
--        else 
--          if (unsigned(ddr3_tx_cnt) > 4) then    -- test empirique 
--            ddr_out.empty <= '0';
--          else
--            ddr_out.empty <= '1';   
--          end if;
--        end if;
--      end if;
--    end process;
          
--      mem2user_o.seq2ddr_ready   <= not(ddr3_tx_full);      --EPO a controler
                                                        
--    ---- bete et mechant
--    -- subSeq2DDR_s <= "00" & user2mem_i.seq2ddr(9) & user2mem_i.seq2ddr(8) & user2mem_i.seq2ddr(7) & user2mem_i.seq2ddr(6) & user2mem_i.seq2ddr(5) & user2mem_i.seq2ddr(4) & user2mem_i.seq2ddr(3) & user2mem_i.seq2ddr(2) & user2mem_i.seq2ddr(1) & user2mem_i.seq2ddr(0);

--    flat_seq2ddr : for seqi in 0 to SUB_SEQUENCE_SIZE-1 generate
--        subSeq2DDR_s(((seqi+1)*NUCLEOTIDE_SIZE)-1 downto seqi*NUCLEOTIDE_SIZE) <= user2mem_i.seq2ddr(seqi);
--    end generate flat_seq2ddr;
--    subSeq2DDR_s(FIFO_IN_DATA_WIDTH-1 downto SUB_SEQUENCE_SIZE*NUCLEOTIDE_SIZE) <= (others => '0');


--    fifo_ddr3_ID_map : entity work.fifo_Std
--        generic map(
--          NB_BIT_DATA => 10,
--          FIFOSIZE    => 2**10,
--          DATAWIDTH   => ID_READ_SIZE
--        )
--        port map(
--          Clock_i               => clk,
--          Reset_i               => rst_s,
--          Wr_Req_i              => seq2ddr_wr_id_s,
--          Rd_Req_i              => ddr_in.IDrd_en,
--          Data_i                => user2mem_i.seq2ddr_id,
--          Fifo_Full_o           => open,
--          Fifo_Empty_o          => open,
--          Data_o                => ddr_out.ID,
--          Nb_Data_Available_o   => open,
--          Nb_Data_Free_o        => open
--        );


--    process(clk, rst_s)
--    begin
--        if rising_edge(clk) then
--            if rst_s = '1' then
--                readsInCache_s <= '0';
--            else 
--                if (user2mem_i.seq2ddr_save = '1') then -- un missmatch detect�
--                    readsInCache_s <= '1';
--                end if;

--                if (incClusterId_s = '1') then -- on passe au prochaine cluster 
--                    readsInCache_s <= '0';
--                end if;
--            end if;
--        end if;
--    end process;


--    process(clk, rst_s)
--    begin
--        if rising_edge(clk) then
--            if rst_s = '1' then
--                seq2ddr_wr_id_s   <= '0';  
--                seq2ddr_save_d1_s <= '0';
--            else 
--                if (seq2ddr_save_d1_s = '0' and user2mem_i.seq2ddr_save = '1') then 
--                    -- flanc montant
--                    seq2ddr_wr_id_s   <= '1';
--                else
--                    seq2ddr_wr_id_s   <= '0';  
--                end if;

--                seq2ddr_save_d1_s   <= user2mem_i.seq2ddr_save;

--            end if;
--        end if;
--    end process;

end Behavioral;
