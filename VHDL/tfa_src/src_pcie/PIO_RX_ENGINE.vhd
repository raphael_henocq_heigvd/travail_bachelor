--------------------------------------------------------------------------------
--    PCIE2FPGA is a FPGA design that aims to demonstrate PCIe Gen2 x8
--    communication by implementing a DMA engine
--    
--    Copyright (C) 2014 HEIG-VD / REDS
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : PIO_RX_ENGINE.vhd
-- Author               : Enrico Petraglio
-- Date                 : 07.04.2014
--
-- Context              : PCIe2FPGA
--
------------------------------------------------------------------------------------------
-- Description : Xilinx PCI Express Endpoint sample application design.
--   
------------------------------------------------------------------------------------------
-- Dependencies : pio_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer     Comments
-- 0.0    See header  EPO          Initial version. Translate in VHDL
-- 1.0    09.04.2014  GHR          Can handle with 2 BARs and wr_addr return 
--                                  type memory_addr_t 
-- 2.0    10.02.2016  FCC          Added burst read and write for any size
-- 2.1    10.02.2016  FCC          Use tkeep instead of count remaining data for last 
--                                  packet valid data.
--
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.PIO_pkg.all;
--use work.simsynth_pkg.all;

entity PIO_RX_ENGINE is
    generic (
        TCQ                : integer := 1;
        C_DATA_WIDTH       : integer := 128
    );
    port (
        clk                : in    std_logic;
        rst_n              : in    std_logic;
        m_axis_rx_tdata    : in    std_logic_vector(C_DATA_WIDTH-1 downto 0);
        
        -- is_eof and is_data passed to user instead.
        m_axis_rx_tkeep    : in    std_logic_vector((C_DATA_WIDTH/8)-1 downto 0);
        m_axis_rx_tlast    : in    std_logic;
        
        m_axis_rx_tvalid   : in    std_logic;
        m_axis_rx_tready   : out   std_logic;
        
        m_axis_rx_tuser    : in    std_logic_vector(21 downto 0);    
      -- ------------------------------------------------------------------------------
      --  Create is_eof : tuser bits [21:17]                                         --
      --  -------------                                                              --
      --  is_eof is a signal to the user indicating the location of EOF in TDATA   . --
      --  Due to DWORD granularity of packets from the block, the only               --
      --  possible values are:                                                       --
      --                       Value                      Valid data widths          --
      --                       5'b11111 (eof @ byte 15)   128                        --
      --                       5'b11011 (eof @ byte 11)   128                        --
      --                       5'b10111 (eof @ byte 7)    128, 64                    --
      --                       5'b10011 (eof @ byte 3)`   128, 64, 32                --
      --                       5'b00011 (eof not present) 128, 64, 32                --
      -- ------------------------------------------------------------------------------
                
        req_compl          : out   std_logic;
        req_compl_wd       : out   std_logic;
        compl_done         : in    std_logic;
                
        req_tc             : out   std_logic_vector( 2 downto 0);
        req_td             : out   std_logic;
        req_ep             : out   std_logic;
        req_attr           : out   std_logic_vector( 1 downto 0);
        req_len            : out   std_logic_vector( 9 downto 0);
        req_rid            : out   std_logic_vector(15 downto 0);
        req_tag            : out   std_logic_vector( 7 downto 0);
        req_be             : out   std_logic_vector( 7 downto 0);
        req_addr           : out   memory_addr_t;  -- Memory Read Address
        
        wr_addr            : out   memory_addr_t;  -- Memory Read Address;
        -- wr_addr            : out   std_logic_vector(10 downto 0);
        wr_be              : out   std_logic_vector( 7 downto 0);
        wr_data            : out   std_logic_vector(31 downto 0);
        wr_en              : out   std_logic;
        wr_busy            : in    std_logic;
        
        rx_eng_tag_o       : out   std_logic_vector( 4 downto 0);     
        rx_eng_rd_tag_o    : out   std_logic;
        rx_eng_data_tag_i  : in    std_logic_vector(31 downto 0);
        
        mem128_wr_o        : out mem128_wr_interface;
        param_tx_i         : in   param_tx_t;
        param_rx_o         : out  param_rx_t;
        config_register_i  : in  config_register_t                   
    );
end PIO_RX_ENGINE;

architecture Behavioral of PIO_RX_ENGINE is

--| Constants declarations |--------------------------------------------------------------
    constant PIO_RX_CPLD_FMT_TYPE         : std_logic_vector(6 downto 0) := "1001010";
    constant PIO_RX_MEM_RD32_FMT_TYPE     : std_logic_vector(6 downto 0) := "0000000";       
    constant PIO_RX_MEM_WR32_FMT_TYPE     : std_logic_vector(6 downto 0) := "1000000";       
    constant PIO_RX_MEM_RD64_FMT_TYPE     : std_logic_vector(6 downto 0) := "0100000";       
    constant PIO_RX_MEM_WR64_FMT_TYPE     : std_logic_vector(6 downto 0) := "1100000";       
    constant PIO_RX_IO_RD32_FMT_TYPE      : std_logic_vector(6 downto 0) := "0000010";       
    constant PIO_RX_IO_WR32_FMT_TYPE      : std_logic_vector(6 downto 0) := "1000010";       
  
    type state_type is (
        PIO_RX_RST_STATE,
        PIO_RX_MEM_RD32_DW1DW2,
        PIO_RX_MEM_WR32_DW1DW2,
        PIO_RX_MEM_RD64_DW1DW2,
        PIO_RX_MEM_WR64_DW1DW2,
        PIO_RX_MEM_WR64_DW3,
        PIO_RX_WAIT_STATE,
        PIO_RX_IO_WR_DW1DW2,
        PIO_RX_IO_MEM_WR_WAIT_STATE,
        PIO_RX_MEM_WR32_2,
        PIO_RX_MEM_WR32_N,
        PIO_RX_MEM_CPLD1,
        PIO_RX_MEM_CPLD2,
        PIO_RX_MEM_CPLD3
    );
  
--| Signals declarations   |--------------------------------------------------------------  
    attribute keep : string;
    
    signal mem32_bar0_hit_n_s             : std_logic;
    signal mem32_bar1_hit_n_s             : std_logic;
    signal mem64_bar_hit_n_s              : std_logic;
    signal erom_bar_hit_n_s               : std_logic;
    signal m_axis_rx_tready_s             : std_logic;                    -- signal copy for internal utilisation
    signal region_select_s                : std_logic_vector(1 downto 0);     
    signal region_sel_vect_s              : std_logic_vector(3 downto 0);
    signal state_s                        : state_type;
    attribute keep of state_s: signal is "true";
    signal tlp_type_s                     : std_logic_vector(6 downto 0);
      
    signal sof_present : std_logic; 
    signal sof_right   : std_logic;  
    signal sof_mid     : std_logic;  
    
    alias rx_tdata_dw0 : std_logic_vector(31 downto 0) is m_axis_rx_tdata(31 downto 0);
    alias rx_tdata_dw1 : std_logic_vector(31 downto 0) is m_axis_rx_tdata(63 downto 32);
    alias rx_tdata_dw2 : std_logic_vector(31 downto 0) is m_axis_rx_tdata(95 downto 64);
    alias rx_tdata_dw3 : std_logic_vector(31 downto 0) is m_axis_rx_tdata(127 downto 96);
    
    
    alias rx_tlast  : std_logic is m_axis_rx_tuser(21);
    alias rx_eof_pos: std_logic_vector(3 downto 0) is m_axis_rx_tuser(20 downto 17);
    alias rx_is_eof : std_logic_vector(4 downto 0) is m_axis_rx_tuser(21 downto 17);
    alias rx_is_sof : std_logic_vector(4 downto 0) is m_axis_rx_tuser(14 downto 10);
    -- not handled by the testbench yet
    alias rx_err_fwd : std_logic is m_axis_rx_tuser(1);
    alias rx_ecrc_err : std_logic is m_axis_rx_tuser(0);
    alias rx_bar_hit : std_logic_vector(7 downto 0) is m_axis_rx_tuser(9 downto 2);    
    
    signal rx_tdata_dw0_s : std_logic_vector(31 downto 0); 
    signal rx_tdata_dw1_s : std_logic_vector(31 downto 0); 
    signal rx_tdata_dw2_s : std_logic_vector(31 downto 0); 
    signal rx_tdata_dw3_s : std_logic_vector(31 downto 0); 
    
    signal rx_cpld_lower_addr_s : std_logic_vector( 6 downto 0);  -- Lower Address information from completion header
    
    signal byte_count_s       : unsigned(11 downto 0);
    signal wr_be_s : std_logic_vector(3 downto 0);
    
    -- Length of the packet being processed within a full DMA completion
    signal current_packet_length_s            :   std_logic_vector(10 downto 0);
    
    -- Indicates if the component is ready to receive a new full DMA read completion.
    -- It stays at '0' during the entire DMA read receiving period, and goes to
    -- '1' when the last packet of the DMA is received
    signal ready_for_new_dma_read_s : std_logic;
    attribute keep of ready_for_new_dma_read_s: signal is "true";
    
    -- Tag of the packet currently under processing
    signal current_packet_tag_s : std_logic_vector(7 downto 0);
    
    -- Next address to be accessed during the full DMA read completion
    signal wr_rd_cpld_addr_s  : std_logic_vector(MEMORY_SIZE-1 downto 0);
    -- Next address to be accessed during a write 
    signal wr_next_addr_s     : std_logic_vector(MEMORY_SIZE-1 downto 0);
    
    -- This signal allow to manage read completions splitted into many subpackets
    -- It stores the offset of the next subpacket to be received
    -- 10 downto 0 in order to avoid overflow in case of 4096 bytes packets
    signal read_completion_subpacket_offset_s    : unsigned(10 downto 0);
    
    -- Number of read completion received within a full DMA read
    -- It will go up to the number of packets asked by the DMA engine
    signal nbr_completion_received_s  : unsigned(31 downto 0);
    attribute keep of nbr_completion_received_s: signal is "true";
    signal nbr_DW_received_s          : unsigned(31 downto 0);   
    
    signal param_rx_s         : param_rx_t;     
    signal dword_en_s         : std_logic_vector(3 downto 0);      
    signal cpld_write_last_s  : std_logic;
    
    --if a CPLD is splitted into several small packets, this signal shall be set to '1'
    --only when the first packet of the CPLD is received. 
    signal firstCPLD_s        : std_logic;


    signal words_remaining_s : integer range 0 to (2**9-1);--unsigned(9 downto 0); -- same size as length
  
begin
    
    --EPO ask for the read_src_address related to this specirfic tag
    rx_eng_tag_o         <= rx_tdata_dw2(12 downto 8);      
                                    
    param_rx_o        <= param_rx_s;
    m_axis_rx_tready  <= m_axis_rx_tready_s;
  
    sof_present       <= m_axis_rx_tuser(14);
    sof_right         <= not(m_axis_rx_tuser(13)) and sof_present;
    sof_mid           <= m_axis_rx_tuser(13) and sof_present;
 
    process (clk, rst_n)
        variable ready_for_new_dma_read_v   : std_logic;   
        variable nbr_DW_received_v          : unsigned(31 downto 0);
        variable current_packet_length_v    : std_logic_vector(10 downto 0);
        variable nbr_completion_received_v  : unsigned(31 downto 0);
    begin     
                
        if (rst_n = '0') then                                
                    
            m_axis_rx_tready_s <= '0';        
            req_compl        <= '0';                
            req_compl_wd     <= '1';            
            req_tc           <= "000";            
            req_td           <= '0';            
            req_ep           <= '0';            
            req_attr         <= "00";            
            req_len          <= "0000000000";           
            req_rid          <= x"0000";           
                    
            req_tag          <= x"00";            
            req_be           <= x"00";            
            req_addr         <= ((others => '0'),(others=>'0'));        
            wr_be            <= x"00";            
            wr_addr          <= ((others => '0'),(others=>'0'));           
            wr_data          <= x"00000000";           
            wr_en            <= '0';            
                                                      
            state_s          <= PIO_RX_RST_STATE;
            tlp_type_s       <= (others => '0');        
            wr_rd_cpld_addr_s <= ((others=>'0'));
            wr_next_addr_s    <= ((others=>'0'));
            wr_be_s       <= (others => '0'); 
            current_packet_tag_s         <= (others => '0'); 
            byte_count_s  <= (others => '0'); 
            current_packet_length_s      <= (others => '0');
            nbr_completion_received_s <= (others => '0');   
            nbr_DW_received_s         <= (others => '0');     
            read_completion_subpacket_offset_s <= (others => '0');
                    
            ready_for_new_dma_read_s   <= '1'; -- A read completion will always start with a first packet
                    
                    
            param_rx_s.tx_req_cpld                        <= '0';
            param_rx_s.tx_req_write_measure          <= '0';
            param_rx_s.tx_acq_write_measure          <= '0';
            param_rx_s.tx_acq_read_measure           <= '0';
            param_rx_s.tx_acq_read                  <= '0';
            
            param_rx_s.mem_wr_header                <= '0';
            param_rx_s.mem_debug                           <= (others => '0');   
            
            
            param_rx_s.nb_poisoned_tlps             <= (others => '0');
            param_rx_s.new_poisoned_tlp             <= '0';
            
            param_rx_s.end_of_read_cpld             <= '0';
            
            mem128_wr_o.addr  <= (others => '0');
            mem128_wr_o.data  <= (others => '0');
            mem128_wr_o.dword_en <= (others => '0');
            mem128_wr_o.wr    <= '0';

           -- rx_eng_tag_o <= (others => '0');  
            rx_eng_rd_tag_o <= '0';        
            dword_en_s   <= (others => '0');
            firstCPLD_s <= '1';
    
    
        elsif (rising_edge(clk)) then       
           
            mem128_wr_o.dword_en <= (others => '0');
            mem128_wr_o.wr    <= '0';
            wr_en        <= '0'; 
            req_compl    <= '0';                      
            
            
            param_rx_s.tx_acq_read                  <= '0';
            param_rx_s.mem_wr_header                <= '0';
            param_rx_s.mem_debug                    <= (others => '0');
            
            param_rx_s.tx_acq_write_measure          <= '0';   
            param_rx_s.tx_acq_read_measure           <= '0'; 
            
            
            param_rx_s.new_poisoned_tlp             <= '0';
    
            param_rx_s.end_of_read_cpld             <= '0';
                                       
            case state_s is
           
                when PIO_RX_RST_STATE =>
                
                    m_axis_rx_tready_s <= '1';
                    state_s           <= PIO_RX_RST_STATE;
                    req_compl_wd      <= '1';            

            
                    if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_s = '1')) then
                               
                        -- Packet starts in the middle of the 128-bit bus.
                        if (sof_mid = '1') then                                                                    
                                                                                                     
                            tlp_type_s         <= m_axis_rx_tdata(94 downto 88);                             
                            req_len            <= m_axis_rx_tdata(73 downto 64);                             
                            m_axis_rx_tready_s <= '0';                                               
                                                                                                          
                            -- Evaluate packet type                                                       
                            case m_axis_rx_tdata(94 downto 88) is                                                 
                                                                                                          
                                when PIO_RX_MEM_RD32_FMT_TYPE =>
                                    
                                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then
                                      
                                        req_tc       <= m_axis_rx_tdata( 86 downto  84);                            
                                        req_td       <= m_axis_rx_tdata( 79);                               
                                        req_ep       <= m_axis_rx_tdata( 78);                               
                                        req_attr     <= m_axis_rx_tdata( 77 downto  76);                            
                                        req_len      <= m_axis_rx_tdata( 73 downto  64);                            
                                        req_rid      <= m_axis_rx_tdata(127 downto 112);                          
                                        req_tag      <= m_axis_rx_tdata(111 downto 104);                          
                                        req_be       <= m_axis_rx_tdata(103 downto  96);                           
                                        state_s      <= PIO_RX_MEM_RD32_DW1DW2;
                                                                
                                    else                                                                      
                                                                                                         
                                        state_s      <= PIO_RX_RST_STATE;                                  
                                                                  
                                    end if; 
                                    
                                when PIO_RX_MEM_WR32_FMT_TYPE =>                                     
                                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then                          
                                                                                             
                                        wr_be        <= m_axis_rx_tdata(103 downto 96);                 
                                        state_s      <= PIO_RX_MEM_WR32_DW1DW2;                  
                                                   
                                    else                                                            
                                                                                               
                                        state_s      <= PIO_RX_RST_STATE;                        
                                    
                                    end if;
                                                                                                  
                                when PIO_RX_MEM_RD64_FMT_TYPE =>
                                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then                     
                                                                                         
                                        req_tc       <= m_axis_rx_tdata( 86 downto  84);                  
                                        req_td       <= m_axis_rx_tdata(79);                     
                                        req_ep       <= m_axis_rx_tdata(78);                     
                                        req_attr     <= m_axis_rx_tdata( 77 downto  76);                  
                                        req_len      <= m_axis_rx_tdata( 73 downto  64);                  
                                        req_rid      <= m_axis_rx_tdata(127 downto 112);                
                                        req_tag      <= m_axis_rx_tdata(111 downto 104);                
                                        req_be       <= m_axis_rx_tdata(103 downto  96);                 
                                        state_s      <= PIO_RX_MEM_RD64_DW1DW2;                  
                                  
                                    else                                                            
                                                                                             
                                        state_s      <= PIO_RX_RST_STATE;                        
                                  
                                    end if;
                                                                                                  
                                when PIO_RX_MEM_WR64_FMT_TYPE =>                                
                                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then                       
                                                                                           
                                        wr_be        <= m_axis_rx_tdata(103 downto 96);                 
                                        state_s      <= PIO_RX_MEM_WR64_DW1DW2;                  
                                               
                                    else                                                            
                                                                                             
                                        state_s      <= PIO_RX_RST_STATE;                        
                                 
                                    end if;
                                                                                                  
                                when PIO_RX_IO_RD32_FMT_TYPE =>
                                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then                        
                                                                                            
                                        req_tc       <=m_axis_rx_tdata( 86 downto  84);                  
                                        req_td       <=m_axis_rx_tdata(79);                     
                                        req_ep       <=m_axis_rx_tdata(78);                     
                                        req_attr     <=m_axis_rx_tdata( 77 downto  76);                  
                                        req_len      <=m_axis_rx_tdata( 73 downto  64);                  
                                        req_rid      <=m_axis_rx_tdata(127 downto 112);                
                                        req_tag      <=m_axis_rx_tdata(111 downto 104);                
                                        req_be       <=m_axis_rx_tdata(103 downto  96);                 
                                        state_s      <=PIO_RX_MEM_RD32_DW1DW2;                  
                                                     
                                    else                                                            
                                                                                             
                                        state_s      <= PIO_RX_RST_STATE;                        
                                                      
                                    end if;                                    
                                                                                                  
                                when PIO_RX_IO_WR32_FMT_TYPE =>                                   
                                    if (m_axis_rx_tdata(73 downto 64) = "0000000001") then                          
                                                                                    
                                        req_tc       <= m_axis_rx_tdata( 86 downto  84);                  
                                        req_td       <= m_axis_rx_tdata(79);                     
                                        req_ep       <= m_axis_rx_tdata(78);                     
                                        req_attr     <= m_axis_rx_tdata( 77 downto  76);                  
                                        req_len      <= m_axis_rx_tdata( 73 downto  64);                  
                                        req_rid      <= m_axis_rx_tdata(127 downto 112);                
                                        req_tag      <= m_axis_rx_tdata(111 downto 104);                
                                                                                                 
                                        wr_be        <= m_axis_rx_tdata(103 downto 96);                 
                                        state_s      <= PIO_RX_MEM_WR32_DW1DW2;                  
                                             
                                    else                                                            
                                                                                             
                                        state_s      <= PIO_RX_RST_STATE;                        
                                 
                                    end if;
                                                                                                  
                                when others =>
                                    
                                    state_s        <= PIO_RX_RST_STATE;                          
                                
                            end case;                                                         
                        
                        elsif (sof_right = '1') then                                  
                                                                      
                            tlp_type_s         <= m_axis_rx_tdata(30 downto 24);    
                            req_len            <= m_axis_rx_tdata( 9 downto  0);      
                            m_axis_rx_tready_s <= '0';
                            
                            case m_axis_rx_tdata(30 downto 24) is      
                                                                                                  
                                when PIO_RX_MEM_RD32_FMT_TYPE =>                                              
                                    req_tc       <= m_axis_rx_tdata(22 downto 20);                                   
                                    req_td       <= m_axis_rx_tdata(15);                                      
                                    req_ep       <= m_axis_rx_tdata(14);                                      
                                    req_attr     <= m_axis_rx_tdata(13 downto 12);                                   
                                    req_len      <= m_axis_rx_tdata( 9 downto  0);                                     
                                    req_rid      <= m_axis_rx_tdata(63 downto 48);                                   
                                    req_tag      <= m_axis_rx_tdata(47 downto 40);                                   
                                    req_be       <= m_axis_rx_tdata(39 downto 32);                                   
                                                                                                                   
                                    --lower qw                                                                     
                                    -- req_addr     <= region_select_s(1 downto 0) & m_axis_rx_tdata(74 downto 66) & "00";                       
                                    req_addr     <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(74 downto 66));                      
                                    req_compl    <= '1';                                                     
                                    req_compl_wd <= '1';                                                     
                                    state_s      <= PIO_RX_WAIT_STATE;                                        
                                                    
                                    m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                  
                                                                                                                   
                                when PIO_RX_MEM_WR32_FMT_TYPE =>                                                   
                                    m_axis_rx_tready_s  <= '1';
                                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then                                             
                                                                                                            
                                        wr_be        <= m_axis_rx_tdata(39 downto 32);                                   
                                                                                                                     
                                        --lower qw                                                                     
                                        wr_data      <= switch_data(m_axis_rx_tdata(127 downto 96));                                  
                                        wr_en        <= '1';                                                     
                                        wr_addr      <= (region_select_s(1 downto 0), m_axis_rx_tdata((MEMORY_SIZE+66-1) downto 66));             
                                        -- wr_addr      <= region_select_s(1 downto 0) & m_axis_rx_tdata(74 downto 66);             
                                        -- wr_en        <= '1';                                                     
                                        state_s      <= PIO_RX_WAIT_STATE; 

                                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                        
                                    
                                    elsif (m_axis_rx_tdata(9 downto 0) = "0000000010") then 
                                        wr_be        <= m_axis_rx_tdata(39 downto 32);                                   
                                                                                                                     
                                        --lower qw                                                                     
                                        wr_data      <= switch_data(m_axis_rx_tdata(127 downto 96));                                  
                                        wr_en        <= '1';                                                     
                                        wr_addr      <= (region_select_s(1 downto 0), m_axis_rx_tdata((MEMORY_SIZE+66-1) downto 66));             
                                        wr_next_addr_s   <= std_logic_vector(unsigned(m_axis_rx_tdata((MEMORY_SIZE+66-1) downto 66)) + 1);
                                        -- wr_addr      <= region_select_s(1 downto 0) & m_axis_rx_tdata(74 downto 66);             
                                        -- wr_en        <= '1';                                                     
                                        state_s      <= PIO_RX_MEM_WR32_2;
                                    
                                    else                           
                                        --write bigger than 2

                                        mem128_wr_o.addr <= "000000" & m_axis_rx_tdata(74 downto 66);
                                        wr_next_addr_s   <= std_logic_vector(unsigned(m_axis_rx_tdata((MEMORY_SIZE+66-1) downto 66)) + 1);
                                        
                                        mem128_wr_o.data(127 downto 96) <= switch_data(m_axis_rx_tdata(127 downto 96));                                        
                                       -- note("[PIO_RX_ENGINE] 1st  DW : " & integer'image(to_integer(unsigned(switch_data(m_axis_rx_tdata(127 downto 96))))));
                                        
                                        words_remaining_s <= to_integer(unsigned(m_axis_rx_tdata(9 downto 0)))-1;

                                        mem128_wr_o.dword_en <= "1000";
                                        mem128_wr_o.wr <= '1';                                       
                                        state_s      <= PIO_RX_MEM_WR32_N;                                       
                                    end if;
                                                                                                                                                                                                                                              
                                when PIO_RX_MEM_RD64_FMT_TYPE =>                                                   
                                  if (m_axis_rx_tdata(9 downto 0) = "0000000001") then                                             
                                                                                                              
                                        req_tc       <= m_axis_rx_tdata(22 downto 20);                                   
                                        req_td       <= m_axis_rx_tdata(15);                                      
                                        req_ep       <= m_axis_rx_tdata(14);                                      
                                        req_attr     <= m_axis_rx_tdata(13 downto 12);                                   
                                        req_len      <= m_axis_rx_tdata(9 downto 0);                                     
                                        req_rid      <= m_axis_rx_tdata(63 downto 48);                                   
                                        req_tag      <= m_axis_rx_tdata(47 downto 40);                                   
                                        req_be       <= m_axis_rx_tdata(39 downto 32);                                   
                                                                                                                       
                                        --lower qw                                                                     
                                        -- Upper 32-bits of 64-bit address not used, but would be captured             
                                        -- in this state if used.  Upper 32 address bits are on                        
                                        --m_axis_rx_tdata(127 downto 96)                                                      
                                        -- req_addr     <= region_select_s(1 downto 0) & m_axis_rx_tdata(74 downto 66) & "00";       
                                        req_addr     <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(74 downto 66));       
                                        req_compl    <= '1';                                                     
                                        req_compl_wd <= '1';                                                     
                                        state_s      <= PIO_RX_WAIT_STATE; 

                                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                        
                                                                     
                                    else                                                                             
                                                                                                              
                                        state_s      <= PIO_RX_RST_STATE;                                         
                                  
                                    end if;
                                                                                                                   
                                when PIO_RX_MEM_WR64_FMT_TYPE =>                                                   
                                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then                                             
                                                                                                                
                                        wr_be        <= m_axis_rx_tdata(39 downto 32);                                   
                                                                                                                       
                                        -- lower qw                                                                    
                                        -- wr_addr      <= region_select_s(1 downto 0) & m_axis_rx_tdata(74 downto 66);             
                                        wr_addr      <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(74 downto 66));             
                                        state_s      <= PIO_RX_MEM_WR64_DW3;                                      
                                        
                                    else                                                                             
                                                                                                                  
                                        state_s      <= PIO_RX_WAIT_STATE;  

                                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                       
                                    
                                    end if;
                                                                                                                                                                          
                                when PIO_RX_IO_RD32_FMT_TYPE =>                                                  
                                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then                                                
                                                                                                        
                                        req_tc       <= m_axis_rx_tdata(22 downto 20);                                   
                                        req_td       <= m_axis_rx_tdata(15);                                      
                                        req_ep       <= m_axis_rx_tdata(14);                                      
                                        req_attr     <= m_axis_rx_tdata(13 downto 12);                                   
                                        req_len      <= m_axis_rx_tdata(9 downto 0);                                     
                                        req_rid      <= m_axis_rx_tdata(63 downto 48);                                   
                                        req_tag      <= m_axis_rx_tdata(47 downto 40);                                   
                                        req_be       <= m_axis_rx_tdata(39 downto 32);                                   
                                                                                                                       
                                        --lower qw                                                                     
                                        -- req_addr     <= region_select_s(1 downto 0) & m_axis_rx_tdata(74 downto 66) & "00";       
                                        req_addr     <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(74 downto 66));       
                                        req_compl    <= '1';                                                     
                                        req_compl_wd <= '1';                                                     
                                        state_s      <= PIO_RX_WAIT_STATE; 

                                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                        
                                                                      
                                    else                                                                             
                                                                                                              
                                        state_s      <= PIO_RX_RST_STATE;                                         
                                                                    
                                    end if;                             
                                                                                                                                                                                        
                                when PIO_RX_IO_WR32_FMT_TYPE =>                                                    
                                    if (m_axis_rx_tdata(9 downto 0) = "0000000001") then                                               
                                                                                                           
                                        wr_be        <= m_axis_rx_tdata(39 downto 32);                                   
                                                                                                                  
                                        --lower qw                                                                
                                        req_tc       <= m_axis_rx_tdata(22 downto 20);                                   
                                        req_td       <= m_axis_rx_tdata(15);                                      
                                        req_ep       <= m_axis_rx_tdata(14);                                      
                                        req_attr     <= m_axis_rx_tdata(13 downto 12);                                   
                                        req_len      <= m_axis_rx_tdata(9 downto 0);                                     
                                        req_rid      <= m_axis_rx_tdata(63 downto 48);                                   
                                        req_tag      <= m_axis_rx_tdata(47 downto 40);                                   
                                                                                                                  
                                        wr_data      <= switch_data(m_axis_rx_tdata(127 downto 96));                                  
                                        wr_en        <= '1';                                                     
                                        wr_addr      <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(74 downto 66));             
                                        wr_en        <= '1';                                                     
                                        req_compl    <= '1';                                                     
                                        req_compl_wd <= '0';                                                     
                                        state_s      <= PIO_RX_WAIT_STATE; 

                                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                        
                                                                   
                                    else                                                                             
                                                                                                              
                                        state_s      <= PIO_RX_RST_STATE;                                         
                                 
                                    end if;   
                                    
                                -- Read completion
                                when PIO_RX_CPLD_FMT_TYPE =>
                                
                                      
                                    m_axis_rx_tready_s  <= '1';
                                      
                                    --EPO  wr_be_s  NOT USED?                        
                                    case rx_tdata_dw1(1 downto 0) is
                                    when "00" => wr_be_s(3 downto 0) <= "1111";
                                    when "01" => wr_be_s(3 downto 0) <= "0111";
                                    when "10" => wr_be_s(3 downto 0) <= "0011";
                                    when "11" => wr_be_s(3 downto 0) <= "0001";
                                    when others => null;
                                    end case;    
                                    
                                    ready_for_new_dma_read_v := ready_for_new_dma_read_s;
                                    nbr_DW_received_v  := nbr_DW_received_s + unsigned(rx_tdata_dw0(9 downto 0));
                                    -- if there are more Byte Count than the packet lenght can provide => the completion was split in more than one packet (more has to come) 
                                    if(unsigned(rx_tdata_dw1(11 downto 2)) > unsigned(rx_tdata_dw0(9 downto 0))) then
                                        ready_for_new_dma_read_v := '0';    
                                    else
                                        -- check if left number of bits is contained in the current CPLD
                                        if (nbr_DW_received_v < unsigned(config_register_i.read_length)) then
                                            ready_for_new_dma_read_v  := '0';
                                            param_rx_s.tx_acq_read    <= '0';                                                 
                                        else -- a full CPLD was received         
                                            nbr_DW_received_v         := (others => '0');
                                            param_rx_s.tx_acq_read    <= '1';         
                                            nbr_completion_received_s <= nbr_completion_received_s + 1;
                                            -- check if we are expecting more CPLDs
                                            if (nbr_completion_received_s = (unsigned(config_register_i.read_nb_packet) - 1)) then  
                                                ready_for_new_dma_read_v := '1';            
                                                nbr_completion_received_s <= (others => '0'); 
                                            else -- more CPLD has to come                                                                               
                                                ready_for_new_dma_read_v := '0';                                                    
                                            end if;                                                                                                    
                                        end if;   
                                    end if;

                                    nbr_DW_received_s        <= nbr_DW_received_v;
                                    ready_for_new_dma_read_s <= ready_for_new_dma_read_v;
                                    
                                    -- first completion 
                                    --if (nbr_completion_received_s = x"00000000") then
                                    --    nbr_completion_received_s <= to_unsigned(1,32);
                                    --else
                                    --    nbr_completion_received_s <= nbr_completion_received_s + 1;
                                    --end if;
                                                             
                                    --EPO ask for the read_src_address related to this specific tag
                                    --rx_eng_tag_o         <= rx_tdata_dw2(12 downto 8);      
                                    rx_eng_rd_tag_o      <= '1';
                                    current_packet_tag_s <= rx_tdata_dw2(15 downto 8);
                            
                                    -- note("[PIO_RX_ENGINE] Starting to receive a read completion. First dword : " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw3)))));
                                    -- note("[PIO_RX_ENGINE] Starting to receive a read completion. Tag : " & integer'image(to_integer(unsigned(rx_tdata_dw2(12 downto 8)))));
                                    
                                    -- if (ready_for_new_dma_read_v = '1' ) then
                                    --     note("[PIO_RX_ENGINE] Ready for new DMA read");
                                    -- else
                                    --     note("[PIO_RX_ENGINE] Not ready for new DMA read");
                                    -- end if;
                                                                    
                                    --store the information 1 clock cycle in ordert to wait for the read_src_address response
                                    rx_tdata_dw3_s <= switch_data(rx_tdata_dw3);
                                    rx_tdata_dw2_s <= (others => '0');
                                    rx_tdata_dw1_s <= (others => '0');
                                    rx_tdata_dw0_s <= (others => '0');
                                    dword_en_s     <= "1000";
                                    
                                    if (m_axis_rx_tlast = '1') OR (rx_tlast = '1') then  -- FCC
                                    --if(rx_tlast = '1') then  
                                        --param_rx_s.tx_acq_read <= '1';  
                                        cpld_write_last_s      <= '1';
                                        if(rx_tdata_dw2(15 downto 8) = TAG_READ_LAST_REQUEST) then -- EPO why TAG_READ_LAST_REQUEST := X"04" ????
                                            param_rx_s.tx_acq_read_measure <= '1';                        
                                        end if;                                                           
                                        if (ready_for_new_dma_read_v='1') then                            
                                            -- That the last of the last of this DMA                      
                                            -- note("[PIO_RX_ENGINE] ************************************");          
                                            -- note("[PIO_RX_ENGINE] Very end of the DMA transfer 1");                  
                                            -- note("[PIO_RX_ENGINE] ************************************");          
                                            -- generates the interrupt                                    
                                            param_rx_s.end_of_read_cpld <= '1';  
                                            --nbr_completion_received_s <= (others => '0');                        
                                        end if;
                                    else        
                                        cpld_write_last_s      <= '0';                                                                                                                                        
                                    end if;                                                               
                                    state_s <= PIO_RX_MEM_CPLD1;
                                                                                 

                                when others =>     
                                    
                                    state_s        <= PIO_RX_RST_STATE;
                                                                                                                              
                            end case;
                            
                        end if;
                    
                    else                        
                              
                        state_s <= PIO_RX_RST_STATE;           
                    
                    end if;
                    
                when PIO_RX_MEM_WR32_2 =>
                    if (m_axis_rx_tvalid = '1') then
                        wr_data      <= switch_data(m_axis_rx_tdata(31 downto 0));                                  
                        wr_en        <= '1';                                                     
                        wr_addr      <= (region_select_s(1 downto 0), wr_next_addr_s); 
                        state_s      <= PIO_RX_RST_STATE;
                    end if;
                
                when PIO_RX_MEM_WR32_N =>
                
                    if (m_axis_rx_tvalid = '1') then
                        mem128_wr_o.data(127 downto 96) <= switch_data(m_axis_rx_tdata(127 downto 96));
                        mem128_wr_o.data( 95 downto 64) <= switch_data(m_axis_rx_tdata( 95 downto 64));
                        mem128_wr_o.data( 63 downto 32) <= switch_data(m_axis_rx_tdata( 63 downto 32));
                        mem128_wr_o.data( 31 downto  0) <= switch_data(m_axis_rx_tdata( 31 downto  0));
                        mem128_wr_o.dword_en <= "1111";
                        mem128_wr_o.wr <= '1';
                        mem128_wr_o.addr <= wr_next_addr_s;
                        wr_next_addr_s  <= std_logic_vector(unsigned(wr_next_addr_s) + 4);
                        
                        if (m_axis_rx_tlast = '1') OR (rx_tlast = '1') then  -- FCC
                        --if (rx_tlast = '1') then
                            wr_next_addr_s  <= std_logic_vector(unsigned(wr_next_addr_s) + 3);
                            
                             case words_remaining_s is 
                                 when 1 =>  mem128_wr_o.dword_en <= "0001";
                                 when 2 =>  mem128_wr_o.dword_en <= "0011";
                                 when 3 =>  mem128_wr_o.dword_en <= "0111";
                                 when 4 =>  mem128_wr_o.dword_en <= "1111";
                                 when others => NULL; --failure("[PIO_RX_ENGINE] words_remaining_s wrong value !! words_remaining_s : " & integer'image(words_remaining_s) );
                             end case;


                            -- L'IP ne mets pas a jour m_axis_rx_tkeep !!!! FCC

                            --case m_axis_rx_tkeep(15 downto 0) is 
                            --    when x"000F" => mem128_wr_o.dword_en <= "0001";
                            --    when x"00FF" => mem128_wr_o.dword_en <= "0011";
                            --    when x"0FFF" => mem128_wr_o.dword_en <= "0111";
                            --    when x"FFFF" => mem128_wr_o.dword_en <= "1111";
                            --    when others =>  
                            --        failure("[PIO_RX_ENGINE] m_axis_rx_tkeep : invalid value !!");
                            --end case;
                          
                            state_s <= PIO_RX_RST_STATE;

                        else
                            words_remaining_s <= words_remaining_s-4;
                        end if;
                    end if;

                when PIO_RX_MEM_CPLD1 => 
                    
                    if(firstCPLD_s = '1') then    
                        mem128_wr_o.addr            <= std_logic_vector(unsigned(rx_eng_data_tag_i(mem128_wr_o.addr'range))); -- get base address from tag_fifo
                    else
                        mem128_wr_o.addr            <= wr_rd_cpld_addr_s;
                    end if; 
                    mem128_wr_o.data(127 downto 96) <= rx_tdata_dw3_s;
                    mem128_wr_o.data(95 downto 64)  <= rx_tdata_dw2_s;   
                    mem128_wr_o.data(63 downto 32)  <= rx_tdata_dw1_s;   
                    mem128_wr_o.data(31 downto 00)  <= rx_tdata_dw0_s;   
                    mem128_wr_o.dword_en            <= dword_en_s;                               
                    mem128_wr_o.wr                  <= '1';    
                    m_axis_rx_tready_s              <= '1'; 
                    
                    -- note("[PIO_RX_ENGINE] ................................................................... Address: " &  integer'image(to_integer(unsigned(rx_eng_data_tag_i(mem128_wr_o.addr'range)))));
                    
                    if (cpld_write_last_s = '1') then     
                        state_s                     <= PIO_RX_RST_STATE;                              
                    else   
                        if (m_axis_rx_tvalid = '1') then
                            if(firstCPLD_s = '1') then
                                wr_rd_cpld_addr_s           <= std_logic_vector(unsigned(rx_eng_data_tag_i(MEMORY_SIZE-1 downto 0)) + 1);
                            else
                                wr_rd_cpld_addr_s       <= std_logic_vector(unsigned(wr_rd_cpld_addr_s) + 1); 
                            end if;
                            rx_tdata_dw3_s              <= switch_data(rx_tdata_dw3); 
                            rx_tdata_dw2_s              <= switch_data(rx_tdata_dw2);                               
                            rx_tdata_dw1_s              <= switch_data(rx_tdata_dw1);                              
                            rx_tdata_dw0_s              <= switch_data(rx_tdata_dw0);           
                            
                            -- note("[PIO_RX_ENGINE] Ctd... Words : " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw0))))
                            --        & " , " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw1))))
                            --        & " , " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw2))))
                            --        & " , " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw3)))));
                            if (m_axis_rx_tlast = '1') OR (rx_tlast = '1') then  -- FCC
                            --if(rx_tlast = '1') then                                                                          
                                --param_rx_s.tx_acq_read <= '1';
                                cpld_write_last_s      <= '1';   
                                if(current_packet_tag_s = TAG_READ_LAST_REQUEST) then 
                                    param_rx_s.tx_acq_read_measure <= '1';                        
                                end if;
                                if (ready_for_new_dma_read_s='1') then                            
                                    -- That the last of the last of this DMA                      
                                    -- note("[PIO_RX_ENGINE] ************************************");          
                                    -- note("[PIO_RX_ENGINE] Very end of the DMA transfer N");                  
                                    -- note("[PIO_RX_ENGINE] ************************************");          
                                    -- generates the interrupt                                    
                                    param_rx_s.end_of_read_cpld <= '1';  
                                    --nbr_completion_received_s <= (others => '0');                        
                                end if;                         
                                                              
                                -- Only valid with packet sizes being 4n (in this case 1 + 3)
                                --dword_en_s             <= "0111";
                                case rx_eof_pos is -- ACS
                                    when "0011" => dword_en_s <= "0001";
                                    when "0111" => dword_en_s <= "0011";
                                    when "1011" => dword_en_s <= "0111";
                                    when "1111" => dword_en_s <= "1111";
                                    when others => dword_en_s <= "0000";
                                end case;
                                ---- L'IP ne mets pas a jour m_axis_rx_tkeep !!!!
                                --case m_axis_rx_tkeep(15 downto 0) is -- FCC
                                --    when x"000F" => dword_en_s <= "0001";
                                --    when x"00FF" => dword_en_s <= "0011";
                                --    when x"0FFF" => dword_en_s <= "0111";
                                --    when x"FFFF" => dword_en_s <= "1111";
                                --    when others =>  
                                --        NULL; --failure("[PIO_RX_ENGINE] m_axis_rx_tkeep : invalid value !!");
                                --end case; 
                                state_s                <= PIO_RX_MEM_CPLD3;
                            else                       
                                cpld_write_last_s      <= '0';  
                                -- Still not the end, the 4 DW are valid
                                dword_en_s             <= "1111";               
                                state_s                <= PIO_RX_MEM_CPLD2;
                            end if; 
                        else -- this else shall never been reach, but in case it prevents the FSM to block
                            state_s                    <= PIO_RX_RST_STATE;          
                        end if; 
                    end if;
             
             
                when PIO_RX_MEM_CPLD2 =>
                
                    if (m_axis_rx_tvalid = '1') then    
                        mem128_wr_o.addr                <= wr_rd_cpld_addr_s;
                        mem128_wr_o.data(127 downto 96) <= rx_tdata_dw3_s;   
                        mem128_wr_o.data(95 downto 64)  <= rx_tdata_dw2_s;   
                        mem128_wr_o.data(63 downto 32)  <= rx_tdata_dw1_s;   
                        mem128_wr_o.data(31 downto 00)  <= rx_tdata_dw0_s;   
                        mem128_wr_o.dword_en            <= dword_en_s;       
                        mem128_wr_o.wr                  <= '1';   
                        m_axis_rx_tready_s              <= '1'; 
                        
                        -- note("[PIO_RX_ENGINE] ................................................................... Address Wr: " &  integer'image(to_integer(unsigned(wr_rd_cpld_addr_s))));           
                     
                        wr_rd_cpld_addr_s    <= std_logic_vector(unsigned(wr_rd_cpld_addr_s) + 4);     
                        rx_tdata_dw3_s       <= switch_data(rx_tdata_dw3); 
                        rx_tdata_dw2_s       <= switch_data(rx_tdata_dw2); 
                        rx_tdata_dw1_s       <= switch_data(rx_tdata_dw1); 
                        rx_tdata_dw0_s       <= switch_data(rx_tdata_dw0); 
                        
                                        
                        -- note("[PIO_RX_ENGINE] Ctd... Words : " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw0))))
                        --        & " , " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw1))))
                        --        & " , " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw2))))
                        --        & " , " & integer'image(to_integer(unsigned(switch_data(rx_tdata_dw3)))));
                                                       
                        if (m_axis_rx_tlast = '1') OR (rx_tlast = '1') then -- FCC
                        --if (rx_tlast = '1') then      
                            --param_rx_s.tx_acq_read <= '1';                                        
                            cpld_write_last_s      <= '1';   
                            
                            if(current_packet_tag_s = TAG_READ_LAST_REQUEST) then 
                                param_rx_s.tx_acq_read_measure <= '1';                        
                            end if;
                            if (ready_for_new_dma_read_s='1') then                            
                                -- That the last of the last of this DMA                      
                                -- note("[PIO_RX_ENGINE] ************************************");          
                                -- note("[PIO_RX_ENGINE] Very end of the DMA transfer");                  
                                -- note("[PIO_RX_ENGINE] ************************************");          
                                -- generates the interrupt                                    
                                param_rx_s.end_of_read_cpld <= '1'; 
                                --nbr_completion_received_s <= (others => '0');                          
                            end if;  
                                                             
                            -- Only valid with packet sizes being 4n (in this case 1 + 3)
                            --dword_en_s             <= "0111";
                            case rx_eof_pos is -- ACS
                                when "0011" => dword_en_s <= "0001";
                                when "0111" => dword_en_s <= "0011";
                                when "1011" => dword_en_s <= "0111";
                                when "1111" => dword_en_s <= "1111";
                                when others => dword_en_s <= "0000";
                            end case;
                            ---- L'IP ne mets pas a jour m_axis_rx_tkeep !!!!
                            --case m_axis_rx_tkeep(15 downto 0) is -- FCC
                            --    when x"000F" => dword_en_s <= "0001";
                            --    when x"00FF" => dword_en_s <= "0011";
                            --    when x"0FFF" => dword_en_s <= "0111";
                            --    when x"FFFF" => dword_en_s <= "1111";
                            --    when others =>  
                            --        NULL; --failure("[PIO_RX_ENGINE] m_axis_rx_tkeep : invalid value !!");
                            --end case;

                            state_s                <= PIO_RX_MEM_CPLD3; 
                            m_axis_rx_tready_s              <= '0';
                        else                                                             
                            cpld_write_last_s <= '0';                                    
                            -- Still not the end, the 4 DW are valid                     
                            dword_en_s             <= "1111";                          
                            state_s                <= PIO_RX_MEM_CPLD2;                       
                        end if;  
                     else -- this else shall never been reach, but in case it prevents the FSM to block  
                          state_s                    <= PIO_RX_RST_STATE;             
                     end if;              
                             
                when PIO_RX_MEM_CPLD3 =>  
                    m_axis_rx_tready_s              <= '0';           
                    mem128_wr_o.addr                <= wr_rd_cpld_addr_s;
                    mem128_wr_o.data(127 downto 96) <= rx_tdata_dw3_s;   
                    mem128_wr_o.data(95 downto 64)  <= rx_tdata_dw2_s;   
                    mem128_wr_o.data(63 downto 32)  <= rx_tdata_dw1_s;   
                    mem128_wr_o.data(31 downto 00)  <= rx_tdata_dw0_s;   
                    mem128_wr_o.dword_en            <= dword_en_s;       
                    mem128_wr_o.wr                  <= '1';
                    -- Only valid with packet sizes being 4n. 
                    if (ready_for_new_dma_read_s='1') then -- this is the last part of a splitted CPLD
                        firstCPLD_s                     <= '1';
                    else -- the CPLD is not finished yet
                        firstCPLD_s                     <= '0';
                        -- Compute the next address value if there is another bit of CPLD incoming.
                        wr_rd_cpld_addr_s               <= std_logic_vector(unsigned(wr_rd_cpld_addr_s) + 3);
                    end if;
                    state_s                         <= PIO_RX_RST_STATE;        
                             
                when PIO_RX_MEM_WR64_DW3 =>                                                                                                             
                    if (m_axis_rx_tvalid = '1') then                                                                                  
                                                                                                                    
                        wr_data        <= switch_data(m_axis_rx_tdata(31 downto 0));                                                        
                        wr_en          <= '1';                                                                         
                        state_s        <= PIO_RX_WAIT_STATE;      

                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                                       
                                                                                               
                    else                                                                                                   
                                                                                                                      
                        state_s        <= PIO_RX_MEM_WR64_DW3;                                                          
                    
                    end if;
                                                                                                                         
                when PIO_RX_MEM_RD32_DW1DW2 =>                                                                           
                    if (m_axis_rx_tvalid = '1') then                                                                                 
                  
                        m_axis_rx_tready_s <= '0';                                                                      
                        -- req_addr          <= region_select_s(1 downto 0) & m_axis_rx_tdata(10 downto 2) & "00";                        
                        req_addr           <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(10 downto 2));                        
                        req_compl          <= '1';                                                                      
                        req_compl_wd       <= '1';                                                                      
                        state_s            <= PIO_RX_WAIT_STATE;  

                    else                                                                                                   
                                                                                                                    
                        state_s           <= PIO_RX_MEM_RD32_DW1DW2;                                                    
                 
                    end if;                                                                                             
                                                                                                                         
                when PIO_RX_MEM_WR32_DW1DW2 =>                                                                           
                    if (m_axis_rx_tvalid = '1') then                                                                                  
                                                                                                                    
                        wr_data            <= switch_data(m_axis_rx_tdata(63 downto 32));                                                    
                        wr_en              <= '1';                                                                      
                        m_axis_rx_tready_s <= '0';                                                                      
                        wr_addr            <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(10 downto 2));                               
                        -- wr_addr            <= region_select_s(1 downto 0) & m_axis_rx_tdata(10 downto 2);                               
                        state_s            <=  PIO_RX_WAIT_STATE; 

                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                                        
                                                                                      
                    else                                                                                                   
                                                                                                                    
                        state_s           <= PIO_RX_MEM_WR32_DW1DW2;                                                    
                  
                    end if;                                                                                                      
                                                                                                                         
                when PIO_RX_IO_WR_DW1DW2 =>                                                                              
                    if (m_axis_rx_tvalid = '1') then                                                                                 
                  
                        wr_data            <= switch_data(m_axis_rx_tdata(63 downto 32));                                                    
                        wr_en              <= '1';                                                                      
                        m_axis_rx_tready_s <= '0';                                                                      
                        wr_addr            <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(10 downto 2));                               
                        -- wr_addr            <= region_select_s(1 downto 0) & m_axis_rx_tdata(10 downto 2);
                        req_compl          <= '1';                                                                      
                        req_compl_wd       <= '0';                                                                      
                        state_s            <=  PIO_RX_WAIT_STATE;  

                        m_axis_rx_tready_s  <= '0'; -- FCC 16.04.2016                                                       
                                                                                             
                    else                                                                                                   
                                                                                                                    
                        state_s           <= PIO_RX_MEM_WR32_DW1DW2;                                                    
                 
                    end if;                                                                                                                                 
                                                                                                                         
                when PIO_RX_MEM_RD64_DW1DW2 =>                                                                           
                    if (m_axis_rx_tvalid = '1') then                                                                                 
                                                                                                                    
                        -- req_addr         <= region_select_s(1 downto 0) & m_axis_rx_tdata(10 downto 2) & "00";                         
                        req_addr           <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(10 downto 2));                        
                        req_compl          <= '1';                                                                       
                        req_compl_wd       <= '1';                                                                       
                        m_axis_rx_tready_s <= '0';                                                                       
                        state_s            <= PIO_RX_WAIT_STATE;                                                          
                  
                    else                                                                                                   
                  
                        state_s        <= PIO_RX_MEM_RD64_DW1DW2;                                                         
                  
                    end if;
                                                                                                                                                                                                                                  
                when PIO_RX_MEM_WR64_DW1DW2 =>                                                                           
                    if (m_axis_rx_tvalid = '1') then                                                                                  
                                                                                                                      
                        m_axis_rx_tready_s <= '0';                                                                      
                        -- wr_addr           <= region_select_s(1 downto 0) & m_axis_rx_tdata(10 downto 2);                               
                        wr_addr           <= (region_select_s(1 downto 0), "000000" & m_axis_rx_tdata(10 downto 2));                               
                        -- lower QW                                                                                          
                        wr_data           <= switch_data(m_axis_rx_tdata(95 downto 64));                                                    
                        wr_en             <= '1';                                                                      
                        state_s             <= PIO_RX_WAIT_STATE;                                                         
                    
                    else                                                                                                   
                                                                                                                      
                        state_s            <= PIO_RX_MEM_WR64_DW1DW2;                                                     
                    
                    end if;
                
                when PIO_RX_WAIT_STATE =>                                                                                                                                                             
                                                                                                     
                    wr_en      <= '0';                                                           
                    req_compl  <= '0';                                                           
                                                                                                     
                    if ((tlp_type_s = PIO_RX_MEM_WR32_FMT_TYPE) and (wr_busy = '0')) then                                                                                                       
                                                                                                       
                        m_axis_rx_tready_s  <= '1';                                                  
                        state_s             <= PIO_RX_RST_STATE;                                      
                                                                                                          
                    elsif ((tlp_type_s = PIO_RX_IO_WR32_FMT_TYPE) and (wr_busy = '0')) then                                                                                                    
                                                                                                       
                        m_axis_rx_tready_s  <= '1';                                                   
                        state_s             <= PIO_RX_RST_STATE;                                           
                                                                                                                      
                    elsif ((tlp_type_s = PIO_RX_MEM_WR64_FMT_TYPE) and (wr_busy = '0')) then                                                                                                
                                                                                                        
                        m_axis_rx_tready_s  <= '1';                                                  
                        state_s             <= PIO_RX_RST_STATE;                                           
                                                                                                         
                    elsif ((tlp_type_s = PIO_RX_MEM_RD32_FMT_TYPE) and (compl_done = '1')) then                                                                                             
                                                                                                       
                        m_axis_rx_tready_s  <= '1';                                                   
                        state_s             <= PIO_RX_RST_STATE;                                           
                                                                                                       
                    elsif ((tlp_type_s = PIO_RX_IO_RD32_FMT_TYPE) and (compl_done = '1')) then                                                                                                 
                                                                                                       
                        m_axis_rx_tready_s  <= '1';                                                   
                        state_s             <= PIO_RX_RST_STATE;                                           
                                                                                                           
                    elsif ((tlp_type_s = PIO_RX_MEM_RD64_FMT_TYPE) and (compl_done = '1')) then                                                                                              
                                                                                                       
                        m_axis_rx_tready_s  <= '1';                                                   
                        state_s             <= PIO_RX_RST_STATE;                                           
                                                                                                       
                    else                                                                               
                                                                                              
                        state_s             <= PIO_RX_WAIT_STATE;                                          
                    
                    end if;                                                                                                                                                                                                                                     
                                                                                                     
                when others =>
                                                                   
                    state_s        <= PIO_RX_RST_STATE;                                             
         
            end case;
          
        end if;
            
    end process;
            
    mem64_bar_hit_n_s  <= '1';                  
    mem32_bar0_hit_n_s <= not(m_axis_rx_tuser(2));                                                                                                            
    mem32_bar1_hit_n_s <= not(m_axis_rx_tuser(3)); 
    erom_bar_hit_n_s   <= not(m_axis_rx_tuser(8)); 
    
    region_sel_vect_s <= mem32_bar0_hit_n_s & mem32_bar1_hit_n_s & mem64_bar_hit_n_s & erom_bar_hit_n_s;
    
    with (region_sel_vect_s) select 
    
    region_select_s <= "00" when "0111",    
                       "01" when "1011",
                       "10" when "1101",
                       "11" when "1110",
                       "00" when others;
   
end Behavioral;
