--------------------------------------------------------------------------------
--    PCIE2FPGA is a FPGA design that aims to demonstrate PCIe Gen2 x8
--    communication by implementing a DMA engine
--    
--    Copyright (C) 2014 HEIG-VD / REDS
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : PIO_EP.vhd
-- Author               : Gilles Habegger
-- Date                 : 09.04.2014
--
-- Context              : PCIe2FPGA
--
------------------------------------------------------------------------------------------
-- Description : Xilinx PCI Express Endpoint sample application design.
--   
------------------------------------------------------------------------------------------
-- Dependencies : pio_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  GHR           Initial version. Translate in VHDL 
-- 1.0    11.04.2014  GHR           Port map of config_register between PIO_EP_MEM_ACCESS
--                                  PIO_TX_ENGINE  
--
------------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use work.pio_pkg.all;

entity PIO_EP is
    generic (
        C_DATA_WIDTH        : integer := 64;                -- TSTRB width
        TCQ                 : integer := 1
    );  
    port (  
        clk                 : in  std_logic;
        rst_n               : in  std_logic;

        -- AXIS TX
        s_axis_tx_tready    : in  std_logic;
        s_axis_tx_tdata     : out std_logic_vector(C_DATA_WIDTH-1 downto 0);
        s_axis_tx_tkeep     : out std_logic_vector((C_DATA_WIDTH/8)-1 downto 0);
        s_axis_tx_tlast     : out std_logic;
        s_axis_tx_tvalid    : out std_logic;
        tx_src_dsc          : out std_logic;

        -- AXIS RX  
        m_axis_rx_tdata     : in  std_logic_vector(C_DATA_WIDTH-1 downto 0);
        m_axis_rx_tkeep     : in  std_logic_vector((C_DATA_WIDTH/8)-1 downto 0);
        m_axis_rx_tlast     : in  std_logic;
        m_axis_rx_tvalid    : in  std_logic;
        m_axis_rx_tready    : out std_logic;
        m_axis_rx_tuser     : in  std_logic_vector(21 downto 0);

        req_compl           : out std_logic;
        compl_done          : out std_logic;

        cfg_completer_id    : in  std_logic_vector(15 downto 0);

        cfg_interrupt             : out std_logic; 
        cfg_interrupt_assert      : out std_logic;
        cfg_interrupt_di          : out std_logic_vector(7 downto 0);
        --  cfg_interrupt_do          : in  std_logic_vector(7 downto 0);
        --  cfg_interrupt_mmenable    : in  std_logic_vector(2 downto 0);
        cfg_interrupt_msienable   : in  std_logic;
        cfg_interrupt_rdy         : in  std_logic;

        cfg_interrupt_stat        : out std_logic;
        cfg_pciecap_interrupt_msgnum : out std_logic_vector(4 downto 0);
        
        -- fifo interface info
        fifo_in_empty_i         : in  std_logic;
        fifo_in_full_i          : in  std_logic;
        fifo_in_count_i         : in  std_logic_vector(10 downto 0);
        fifo_out_empty_i        : in  std_logic;
        fifo_out_full_i         : in  std_logic;
        fifo_out_count_i        : in  std_logic_vector(10 downto 0);
        
        -- user reset
        user_rst_all_o          : out  std_logic;
        user_rst_data_o         : out  std_logic;
        user_data_loopbk_o      : out  std_logic;
        
        -- overlay interface
        overlay_ram_we_o        : out std_logic;
        overlay_ram_addr_o      : out std_logic_vector(10 downto 0);
        overlay_ram_din_o       : out std_logic_vector(31 downto 0);
        overlay_ram_dout_i      : in  std_logic_vector(31 downto 0);
        overlay_dat_prv_o       : out std_logic_vector(39 downto 0);
        overlay_val_prv_o       : out std_logic;
        overlay_acc_prv_i       : in  std_logic;
        overlay_dat_nxt_i       : in  std_logic_vector(39 downto 0);
        overlay_val_nxt_i       : in  std_logic;
        overlay_acc_nxt_o       : out std_logic

--        ddr_in        : in  ddr2mem_t;
--        ddr_out       : out mem2ddr_t
    );
end PIO_EP;

architecture struct of PIO_EP is

    --| Signals declarations   |-------------------------------------------------------------- 
    signal rd_be          : std_logic_vector( 3 downto 0);
    signal rd_addr        : memory_addr_t;
    signal rd_data        : std_logic_vector(31 downto 0);
    signal rd_en          : std_logic;
    

    signal wr_addr        : memory_addr_t;
    signal wr_en          : std_logic;
    signal wr_busy        : std_logic;
    signal wr_be          : std_logic_vector( 7 downto 0);
    signal wr_data        : std_logic_vector(31 downto 0);

    signal mem128_wr_s      : mem128_wr_interface;
    signal mem128_rd_ctrl_s : mem128_rd_ctrl_interface;
    signal mem128_rd_data_s : mem128_rd_data_interface;

    signal req_compl_int  : std_logic;
    signal req_compl_wd   : std_logic;
    signal compl_done_int : std_logic;

    signal req_td         : std_logic;
    signal req_ep         : std_logic;
    signal req_attr       : std_logic_vector( 1 downto 0);
    signal req_tc         : std_logic_vector( 2 downto 0);
    signal req_tag        : std_logic_vector( 7 downto 0);
    signal req_be         : std_logic_vector( 7 downto 0);
    signal req_len        : std_logic_vector( 9 downto 0);
    signal req_rid        : std_logic_vector(15 downto 0);
    signal req_addr       : memory_addr_t;


    signal flag_irq_assert_s   : std_logic;
    signal interrupt_mask_wr   : std_logic;
    signal interrupt_irq_clear : std_logic;
    signal interrupt_data_wr   : std_logic_vector(31 downto 0);
    signal interrupt_data_rd   : std_logic_vector(31 downto 0);
    signal interrupt_irq       : std_logic_vector(31 downto 0);

    signal param_rx_s         : param_rx_t;
    signal param_tx_s         : param_tx_t;
    signal config_register_s  : config_register_t;   

    signal rx_eng_tag_s      : std_logic_vector( 4 downto 0);
    signal rx_eng_rd_tag_s   : std_logic;                    
    signal rx_eng_data_tag_s : std_logic_vector(31 downto 0);
    signal tx_eng_tag_s      : std_logic_vector( 4 downto 0);
    signal tx_eng_wr_tag_s   : std_logic;                    
    signal tx_eng_data_tag_s : std_logic_vector(31 downto 0); 

--    signal mem2user_s : mem2user_t;
--    signal user2mem_s : user2mem_t;


    signal rst_s                   : std_logic;


begin    



    rst_s <= not rst_n;



    --| Ports mapping          |--------------------------------------------------------------

--    FPGA2_USER_MAP : entity work.Fpga2_User

--    port map (
--        clk                      => clk,
--        rst                      => rst_s,

--        mem2user_i               => mem2user_s,
--        user2mem_o               => user2mem_s
--    );


    inter_manager : entity work.interrupt_manager
    port map(
        clk                             => clk,
        rst_n                           => rst_n,
        cfg_interrupt                   => cfg_interrupt,
        cfg_interrupt_rdy               => cfg_interrupt_rdy,
        cfg_interrupt_msienable         => cfg_interrupt_msienable,
        cfg_interrupt_assert            => cfg_interrupt_assert, 
        cfg_interrupt_di                => cfg_interrupt_di,
        cfg_interrupt_stat              => cfg_interrupt_stat,
        cfg_pciecap_interrupt_msgnum    => cfg_pciecap_interrupt_msgnum,
        flag_irq_assert_o               => flag_irq_assert_s,
        mask_wr_i                       => interrupt_mask_wr,
        irq_clear_i                     => interrupt_irq_clear,
        data_wr_i                       => interrupt_data_wr,
        data_rd_o                       => interrupt_data_rd,
        irq_i                           => interrupt_irq
    );

    EP_MEM_inst : entity work.PIO_EP_MEM_ACCESS
    generic map (
        TCQ                   => TCQ
    )                     
    port map (              
        clk                   => clk,
        rst_n                 => rst_n,
        -- Read Port          
        rd_addr               => rd_addr,
        rd_be                 => rd_be,
        rd_data               => rd_data,
        rd_en                 => rd_en,

        -- Write Port         
        wr_addr               => wr_addr,
        wr_be                 => wr_be,
        wr_data               => wr_data, 
        wr_en                 => wr_en,
        wr_busy               => wr_busy,

        interrupt_mask_wr_o   => interrupt_mask_wr,
        interrupt_irq_clear_o => interrupt_irq_clear,
        interrupt_data_wr_o   => interrupt_data_wr,
        interrupt_data_rd_i   => interrupt_data_rd,
        interrupt_irq_o       => interrupt_irq,   

        mem128_rd_ctrl_i      => mem128_rd_ctrl_s,
        mem128_rd_data_o      => mem128_rd_data_s,

        rx_eng_tag_i          => rx_eng_tag_s,     
        rx_eng_rd_tag_i       => rx_eng_rd_tag_s,  
        rx_eng_data_tag_o     => rx_eng_data_tag_s,
        tx_eng_tag_i          => tx_eng_tag_s,     
        tx_eng_wr_tag_i       => tx_eng_wr_tag_s,  
        tx_eng_data_tag_i     => tx_eng_data_tag_s,

        mem128_wr_i           => mem128_wr_s,

        --irq_register_i     => (others => '0'),  -- EPO TBC irq_register_i,
        param_tx_i            => param_tx_s,
        param_rx_i            => param_rx_s,
        config_register_o     => config_register_s,
        
        -- fifo interface info
        fifo_in_empty_i         => fifo_in_empty_i,
        fifo_in_full_i          => fifo_in_full_i,
        fifo_in_count_i         => fifo_in_count_i,
        fifo_out_empty_i        => fifo_out_empty_i,
        fifo_out_full_i         => fifo_out_full_i,
        fifo_out_count_i        => fifo_out_count_i,
        
        -- user reset
        user_rst_all_o          => user_rst_all_o,
        user_rst_data_o         => user_rst_data_o,
        user_data_loopbk_o      => user_data_loopbk_o,
        
        -- overlay interface
        overlay_ram_we_o        => overlay_ram_we_o,
        overlay_ram_addr_o      => overlay_ram_addr_o,
        overlay_ram_din_o       => overlay_ram_din_o,
        overlay_ram_dout_i      => overlay_ram_dout_i,
        overlay_dat_prv_o       => overlay_dat_prv_o,
        overlay_val_prv_o       => overlay_val_prv_o,
        overlay_acc_prv_i       => overlay_acc_prv_i,
        overlay_dat_nxt_i       => overlay_dat_nxt_i,
        overlay_val_nxt_i       => overlay_val_nxt_i,
        overlay_acc_nxt_o       => overlay_acc_nxt_o

--        mem2user_o            => mem2user_s,
--        user2mem_i            => user2mem_s,

--        ddr_in                => ddr_in,
--        ddr_out               => ddr_out
    );


    -- Local-Link Receive Controller

    EP_RX_inst : entity work.PIO_RX_ENGINE
    generic map (
        C_DATA_WIDTH        => C_DATA_WIDTH,
        TCQ                 => TCQ 
    )
    port map (
        clk                 => clk,   
        rst_n               => rst_n,  
        -- AXIS RX    
        m_axis_rx_tdata     => m_axis_rx_tdata,   
        m_axis_rx_tkeep     => m_axis_rx_tkeep,   
        m_axis_rx_tlast     => m_axis_rx_tlast,   
        m_axis_rx_tvalid    => m_axis_rx_tvalid,   
        m_axis_rx_tready    => m_axis_rx_tready,   
        m_axis_rx_tuser     => m_axis_rx_tuser,   
        -- Handshake with Tx engine
        req_compl           => req_compl_int,   
        req_compl_wd        => req_compl_wd,   
        compl_done          => compl_done_int,   

        req_tc              => req_tc,   
        req_td              => req_td,   
        req_ep              => req_ep,   
        req_attr            => req_attr,   
        req_len             => req_len,   
        req_rid             => req_rid,   
        req_tag             => req_tag,   
        req_be              => req_be,   
        req_addr            => req_addr,   
        -- Memory Write Port
        wr_addr             => wr_addr,   
        wr_be               => wr_be,   
        wr_data             => wr_data,   
        wr_en               => wr_en,   
        wr_busy             => wr_busy,   

        rx_eng_tag_o        => rx_eng_tag_s,     
        rx_eng_rd_tag_o     => rx_eng_rd_tag_s,  
        rx_eng_data_tag_i   => rx_eng_data_tag_s,

        mem128_wr_o         => mem128_wr_s,
        param_rx_o          => param_rx_s,
        param_tx_i          => param_tx_s,
        config_register_i   => config_register_s
    );

    -- Local-Link Transmit Controller

    EP_TX_inst : entity work.PIO_TX_ENGINE
    generic map (
        C_DATA_WIDTH        => C_DATA_WIDTH ,
        TCQ                 => TCQ 
    )
    port map (
        clk                 => clk, 
        rst_n               => rst_n, 
        -- AXIS Tx
        s_axis_tx_tready    => s_axis_tx_tready, 
        s_axis_tx_tdata     => s_axis_tx_tdata, 
        s_axis_tx_tkeep     => s_axis_tx_tkeep, 
        s_axis_tx_tlast     => s_axis_tx_tlast, 
        s_axis_tx_tvalid    => s_axis_tx_tvalid, 
        tx_src_dsc          => tx_src_dsc, 
        -- Handshake with Rx engine
        req_compl           => req_compl_int, 
        req_compl_wd        => req_compl_wd, 
        compl_done          => compl_done_int, 
        -- Read Port  
        req_tc              => req_tc, 
        req_td              => req_td, 
        req_ep              => req_ep, 
        req_attr            => req_attr, 
        req_len             => req_len, 
        req_rid             => req_rid, 
        req_tag             => req_tag, 
        req_be              => req_be, 
        req_addr            => req_addr, 

        rd_addr             => rd_addr, 
        rd_be               => rd_be, 
        rd_data             => rd_data, 
        rd_en               => rd_en,

        completer_id        => cfg_completer_id,


        mem128_rd_ctrl_o    => mem128_rd_ctrl_s,
        mem128_rd_data_i    => mem128_rd_data_s, 

        tx_eng_tag_o        => tx_eng_tag_s,     
        tx_eng_wr_tag_o     => tx_eng_wr_tag_s,  
        tx_eng_data_tag_o   => tx_eng_data_tag_s,
        
        flag_irq_assert_i   => flag_irq_assert_s,

        -- added to test registers
        param_tx_o          => param_tx_s,
        param_rx_i          => param_rx_s,
        config_register_i   => config_register_s
    );

    req_compl  <= req_compl_int;
    compl_done <= compl_done_int;

end struct;

