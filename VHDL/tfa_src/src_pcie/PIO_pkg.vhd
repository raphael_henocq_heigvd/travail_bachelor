--------------------------------------------------------------------------------
--    PCIE2FPGA is a FPGA design that aims to demonstrate PCIe Gen2 x8
--    communication by implementing a DMA engine
--    
--    Copyright (C) 2014 HEIG-VD / REDS
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

library IEEE;
    use IEEE.STD_LOGIC_1164.all;
    use IEEE.Numeric_Std.all;
    use work.ilog_pkg.all;
--    use work.clustering_pkg.all;

package PIO_pkg is

    constant REGION_SIZE : integer := 2;
    constant MEMORY_SIZE : integer := 15; -- in DWORDS
    constant TOT_MEMORY_SIZE : integer := REGION_SIZE + MEMORY_SIZE;

    constant VERSION : integer := 12;
    constant VERSION_VEC : std_logic_vector(31 downto 0) := 
                   std_logic_vector(to_unsigned(VERSION,32));


    constant LOG_ADDR_SIZE : integer := 8; --[PJD] adresse pour log en implémentation

    constant FIFO_IN_LOGSIZE      : integer := 12;
    constant FIFO_OUT_LOGSIZE     : integer := 12;
    constant FIFO_IN_SIZE         : integer := 2**FIFO_IN_LOGSIZE;
    constant FIFO_OUT_SIZE        : integer := 2**FIFO_OUT_LOGSIZE;
    
    -- FIFO in/out du PCIe
    constant FIFO_IN_DATA_WIDTH   : integer := 128; 
    constant FIFO_OUT_DATA_WIDTH  : integer := 32; 


    --------------------- Address bar 0 ---------------------

    -- Sequence in and resul out : only by burst multiple of 4
    -- All these addresses are in DWORDS, not in bytes.
    constant FIFO_IN_START_ADDR   : integer := 2**13; -- Write
    constant FIFO_OUT_START_ADDR  : integer := 2**13; -- Read
    

    --------------------- Address bar 1 ---------------------

    -- These constants describe the address space of bar1, in which are all
    -- the control registers.
    -- All these addresses are in DWORDS, not in bytes.
    constant ADDR_COMMAND                : integer := 0;
    constant ADDR_WRITE_SRC_ADDRESS      : integer := 1;
    constant ADDR_WRITE_DST_ADDRESS      : integer := 2;
    constant ADDR_WRITE_LENGTH           : integer := 3;
    constant ADDR_WRITE_NB_PACKET        : integer := 4;
    constant ADDR_READ_SRC_ADDRESS       : integer := 5;
    constant ADDR_READ_DST_ADDRESS       : integer := 6;
    constant ADDR_READ_LENGTH            : integer := 7;
    constant ADDR_READ_NB_PACKET         : integer := 8;
    constant ADDR_DEBUG                  : integer := 9;
    constant ADDR_WRITE_MSG_HEADER       : integer := 10;
    constant ADDR_READ_PIPE_SIZE         : integer := 13;
    constant ADDR_TX_MEM_CNT_WRITE_CYCLE : integer := 33;
    constant ADDR_TX_MEM_CNT_READ_CYCLE  : integer := 34;
    constant ADDR_TX_MEM_DEBUG           : integer := 35;
    constant ADDR_RX_NB_POISONED_TLPS    : integer := 36;
    constant ADDR_VERSION                : integer := 37;
    constant ADDR_LOG_MEMORY_ADDR        : integer := 38;
    constant ADDR_LOG_MEMORY_SIZE        : integer := 39;
    constant ADDR_LOG_MEMORY_AVAILABLE   : integer := 40;


    -- -- start address of the Fifo_in (used for GPU->FPGA transfers), in bytes
    -- constant ADDR_FIFO_IN_START_ADDR      : integer := 50;
    -- -- size of the Fifo_in (used for GPU->FPGA transfers), in bytes
    -- constant ADDR_FIFO_IN_SIZE            : integer := 51;
    -- -- number of free slots in the Fifo_in (used for GPU->FPGA transfers), in bytes
    -- constant ADDR_FIFO_IN_DATA_FREE       : integer := 52;
    -- -- start address of the Fifo_out (used for FPGA->GPU transfers), in bytes
    -- constant ADDR_FIFO_OUT_START_ADDR     : integer := 53;
    -- -- size of the Fifo_out (used for FPGA->GPU transfers), in bytes
    -- constant ADDR_FIFO_OUT_SIZE           : integer := 54;
    -- -- number of available dwords in the Fifo_out (used for FPGA->GPU transfers), in bytes
    -- constant ADDR_FIFO_OUT_DATA_AVAILABLE : integer := 55;


    constant ADDR_IRQ_SET_MASK     : integer := 64;
    constant ADDR_IRQ_CLEAR        : integer := 65;
    constant ADDR_IRQ_REGISTER     : integer := 66;
    constant ADDR_IRQ_GENERATE     : integer := 67;
    constant ADDR_IRQ_DEBUG_LEGACY : integer := 68;
    constant ADDR_IRQ_DEBUG_MSI    : integer := 69;


    -- User address :

    constant ADDR_DESIGN_VERSION    : integer := 80;

    --constant ADDR_NB_SEQ            : integer := 128;
    --constant ADDR_NB_RESULTS        : integer := 129;
    --constant ADDR_PARSING_END       : integer := 130;
    --constant ADDR_CLUSTER_SIZE      : integer := 131;

    constant ADDR_TEST_RETURN        : integer := 81;
    
    constant ADDR_FIFO_IN_STATUS     : integer := 82;
    constant ADDR_FIFO_IN_COUNT      : integer := 83;
    constant ADDR_FIFO_OUT_STATUS    : integer := 84;
    constant ADDR_FIFO_OUT_COUNT     : integer := 85;
    constant ADDR_FIFO_IN_THRESHOLD  : integer := 86;
    constant ADDR_FIFO_OUT_THRESHOLD : integer := 87;
    constant ADDR_LINE_OVLY_STATUS   : integer := 88;
    constant ADDR_COL_OVLY_STATUS    : integer := 89;
    
    constant ADDR_MSB_DATA_IN       : integer := 128;
    constant ADDR_LSB_DATA_IN       : integer := 129;
    constant ADDR_MSB_DATA_OUT      : integer := 130;
    constant ADDR_LSB_DATA_OUT      : integer := 131;
    constant ADDR_CFG_STATUS        : integer := 132;
    constant ADDR_DATA_STATUS       : integer := 133;
    
    constant ADDR_OVLY_ALL_RST      : integer := 144;
    constant ADDR_OVLY_DATA_RST     : integer := 145;
    constant ADDR_DATA_LOOPBACK     : integer := 146;
    constant ADDR_DEBUG_ERROR       : integer := 147;

    --!!!!!!!!!!!!!! Warning !!!!!!!!!!!!!!!!!!!!!!!!
    --address from 256 to 511 --> Reserved for RAM overlay config

    ---------------------------------------------------------------------
    -- Detail of command number
    --    0. start single write request
    --    1. start multiple write request
    --    2. start multiple read request
    --    3. reset log tag
    --    4. start interrupt
    --   31. reset 
    ---------------------------------------------------------------------
    constant COMMAND_BIT_DMA_WRITE         : integer := 1;
    constant COMMAND_BIT_DMA_READ          : integer := 2;
    constant COMMAND_BIT_LOG_RESET         : integer := 3;
    constant COMMAND_BIT_INTERRUPT_REQUEST : integer := 4;
    constant COMMAND_BIT_RESET             : integer := 31;


    -- tag used to recognize tranfers
    constant TAG_WRITE_MEASURE_REQUEST   : std_logic_vector(07 downto 0) := X"01";
    constant TAG_WRITE_MEASURE_RESPONSE  : std_logic_vector(07 downto 0) := X"02";
    constant TAG_READ_REQUEST            : std_logic_vector(07 downto 0) := X"03";
    constant TAG_READ_LAST_REQUEST       : std_logic_vector(07 downto 0) := X"04";

    -- used to identify the previous header
    constant TAG_READ_RECEIVE_HEADER     : std_logic_vector(31 downto 0) := X"00112233";


--    type mem2user_t is
--    record
--        fifo_out_nb_free        : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);
--        fifo_out_nb_available   : std_logic_vector(FIFO_OUT_LOGSIZE downto 0);
--        fifo_out_empty          : std_logic;
--        fifo_out_full           : std_logic;
--        fifo_in_rd_data         : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);
--        fifo_in_nb_free         : std_logic_vector(FIFO_IN_LOGSIZE downto 0);
--        fifo_in_nb_available    : std_logic_vector(FIFO_IN_LOGSIZE downto 0);
--        fifo_in_empty           : std_logic;
--        fifo_in_full            : std_logic;
--        incClusterId            : std_logic;
--        seq2ddr_ready           : std_logic; -- ready to save seq
--    end record;

--    type user2mem_t is
--    record
--        fifo_out_wr         : std_logic;
--        fifo_out_wr_data    : std_logic_vector(FIFO_OUT_DATA_WIDTH-1 downto 0);
--        fifo_in_rd          : std_logic;
--        irq_trigger         : std_logic_vector(15 downto 0);
--        Cluster_idle        : std_logic; 
--        seq2ddr             : sequence_type(SUB_SEQUENCE_SIZE-1 downto 0);
--        seq2ddr_id          : std_logic_vector(ID_READ_SIZE-1 downto 0);
--        seq2ddr_save        : std_logic;
--    end record;

--    -- cluster fifo -> ddr3
--    type mem2ddr_t is
--    record
--        data    : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);
--        ID      : std_logic_vector(ID_READ_SIZE-1 downto 0);
--        full    : std_logic;
--        empty   : std_logic;
--        readSeq : std_logic;              -- EPO debug 
--        data_cnt: std_logic_vector(10 downto 0);
--        csize   : std_logic_vector(31 downto 0);
--        cnumber : std_logic_vector(15 downto 0);
--    end record;

--    -- ddr3 -> cluster fifo
--    type ddr2mem_t is
--    record
--        data      : std_logic_vector(FIFO_IN_DATA_WIDTH-1 downto 0);
--        reads_cnt : unsigned(31 downto 0);
--        mem_empty : std_logic;
--        wr_en     : std_logic;
--        rd_en     : std_logic;
--        IDrd_en   : std_logic;   
--        clk       : std_logic;
--    end record;

    type memory_addr_t is
    record
        region : std_logic_vector(REGION_SIZE-1 downto 0);
        addr   : std_logic_vector(MEMORY_SIZE-1 downto 0);
    end record;

    type config_register_t is
    record
        command                     : std_logic_vector(31 downto 00);
        write_src_address           : std_logic_vector(31 downto 00);
        write_dst_address           : std_logic_vector(31 downto 00);
        write_length                : std_logic_vector(31 downto 00);
        write_nb_packet             : std_logic_vector(31 downto 00);
        read_src_address            : std_logic_vector(31 downto 00);
        read_dst_address            : std_logic_vector(31 downto 00);
        read_length                 : std_logic_vector(31 downto 00);
        read_nb_packet              : std_logic_vector(31 downto 00);
        read_pipe_size              : std_logic_vector(31 downto 00);
        debug                       : std_logic_vector(31 downto 00);
        write_msg_header            : std_logic_vector(31 downto 00);
    end record;

    type param_tx_t is
    record
        -- signals to rx interface
        rx_acq_cpld                     : std_logic;                      -- acknowlegde of completion request to the tx interface 
        rx_acq_write_measure            : std_logic;                      -- acknowlegde of write mesure request to the tx interface 
        -- signals to memory interface  
        mem_acq_cmd                     : std_logic_vector(03 downto 00); -- command acknowledge to the memory interface
        mem_cnt_write_cycle             : std_logic_vector(31 downto 00); -- write mesure counter value to the memory interface 
        mem_cnt_read_cycle              : std_logic_vector(31 downto 00);   -- read mesure counter value to the memory interface
        mem_debug                       : std_logic_vector(31 downto 00); -- used to debug the tx interface
        end_of_command                  : std_logic_vector(2 downto 0);
    end record;

    type param_rx_t is
    record
        -- singals to memory interface
        mem_wr_header                 : std_logic;                        -- header of packet for logs
        mem_debug                     : std_logic_vector(31 downto 00);   -- used to debug the rx interface
        -- signals to tx interface      
        tx_req_cpld                   : std_logic;                        -- completion request to the tx interface
        tx_acq_write_measure          : std_logic;                        -- acknowlegde of a write request to the tx interface
        tx_req_write_measure          : std_logic;                        -- write mesure request to the tx interface
        tx_acq_read_measure           : std_logic;                        -- acknowlegde of a read mesure request to the tx interface
        tx_acq_read                   : std_logic;                        -- acknowlegde of a read request to the tx interface

        nb_poisoned_tlps              : std_logic_vector(31 downto 00);   -- Number of poisoned TLPs received
        new_poisoned_tlp              : std_logic;                        -- Indicates that a new poisoned is received
        end_of_read_cpld              : std_logic;                        -- Indicates the end of a read completion
    end record;

    type irq_register_t is
    record
        mem_acq_irq   : std_logic;                                        -- acknowlegde of an irq mesure request to the memory interface
        mem_debug     : std_logic_vector(31 downto 00);                   -- used to debug the irq interface
        debug_legacy  : std_logic_vector(31 downto 00);
        debug_msi     : std_logic_vector(31 downto 00);
    end record;

    type mem128_wr_interface is
    record
        addr : std_logic_vector(MEMORY_SIZE-1 downto 0);
        data : std_logic_vector(127 downto 0);
        dword_en : std_logic_vector(3 downto 0);
        wr : std_logic;
    end record;

    type mem128_rd_ctrl_interface is
    record
        addr  : std_logic_vector(MEMORY_SIZE-1 downto 0);
        rd_en : std_logic;
        rd_fifo_en : std_logic;
    end record;

    type mem128_rd_data_interface is
    record
        data : std_logic_vector(127 downto 0);
    end record;

    function switch_data(src: std_logic_vector(31 downto 0)) return std_logic_vector;

end package PIO_pkg; 

package body PIO_pkg is

    function switch_data(src: std_logic_vector(31 downto 0)) return std_logic_vector is
    begin
        return src(07 downto 0)  &
               src(15 downto 8)  &
               src(23 downto 16) &
               src(31 downto 24);
    end switch_data;

    -- integer logarithm (rounded up) [MR version]
    function ilogup (x : natural; base : natural := 2) return natural is
        variable y : natural := 1;
    begin
        y:= 1;  --Mod EMI 26.03.2009
        while x > base ** y loop
            y := y + 1;
        end loop;
        return y;
    end ilogup;

    -- integer logarithm (rounded down) [MR version]
    function ilog (x : natural; base : natural := 2) return natural is
        variable y : natural := 1;
    begin
        y:= 1;  --Mod EMI 26.03.2009
        while x > base ** y loop
            y := y + 1;
        end loop;
        if x<base**y then
            y:=y-1;
        end if;
        return y;
    end ilog;

end package body PIO_pkg;
