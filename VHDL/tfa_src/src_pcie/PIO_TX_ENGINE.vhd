--EPO : copie de TX Engine pour travailler sur la gestion des tag et dest addr


--------------------------------------------------------------------------------
--    PCIE2FPGA is a FPGA design that aims to demonstrate PCIe Gen2 x8
--    communication by implementing a DMA engine
--    
--    Copyright (C) 2014 HEIG-VD / REDS
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : PIO_RX_ENGINE.vhd
-- Author               : Enrico Petraglio
-- Date                 : 07.04.2014
--
-- Context              : PCIe2FPGA
--
------------------------------------------------------------------------------------------
-- Description : Xilinx PCI Express Endpoint sample application design.
--   
------------------------------------------------------------------------------------------
-- Dependencies : pio_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer     Comments
-- 0.0    See header  EPO          Initial version. Translate in VHDL
-- 1.0    10.04.2014  GHR          req_addr and rd_addr are now type memory_addr_t 
-- 2.0    10.02.2016  FCC          Changed burst read and write to allow any size
-- 2.1    11.02.2016  FCC          Changed DMA write to allow any size
--
------------------------------------------------------------------------------------------

library ieee;                    
use ieee.std_logic_1164.all;     
use ieee.numeric_std.all;
use work.PIO_pkg.all;
--use work.simsynth_pkg.all;

entity PIO_TX_ENGINE is
  generic ( 
    TCQ               : integer := 1;
    C_DATA_WIDTH      : integer :=128
  );
  port(
    clk               :  in std_logic;                                                            
    rst_n             :  in std_logic;   
    
    s_axis_tx_tready  :  in std_logic;                                 
    s_axis_tx_tdata   : out std_logic_vector(C_DATA_WIDTH-1 downto 0);    
    s_axis_tx_tkeep   : out std_logic_vector((C_DATA_WIDTH/8)-1 downto 0);    
    s_axis_tx_tlast   : out std_logic;                                   
    s_axis_tx_tvalid  : out std_logic;                                  
    tx_src_dsc        : out std_logic;                                  

    req_compl         :  in std_logic;                                  
    req_compl_wd      :  in std_logic;                                
    compl_done        : out std_logic;                                  

    req_tc            :  in std_logic_vector( 2 downto 0);              
    req_td            :  in std_logic;                                
    req_ep            :  in std_logic;                                  
    req_attr          :  in std_logic_vector( 1 downto 0);              
    req_len           :  in std_logic_vector( 9 downto 0);              
    req_rid           :  in std_logic_vector(15 downto 0);              
    req_tag           :  in std_logic_vector( 7 downto 0);              
    req_be            :  in std_logic_vector( 7 downto 0);              
    req_addr          :  in memory_addr_t;              
  
    rd_addr           : out memory_addr_t;              
    rd_be             : out std_logic_vector( 3 downto 0);            
    rd_data           :  in std_logic_vector(31 downto 0); 
    rd_en             : out std_logic;             

    completer_id      :  in std_logic_vector(15 downto 0);
    
    mem128_rd_ctrl_o  : out mem128_rd_ctrl_interface;
    mem128_rd_data_i  : in  mem128_rd_data_interface;
    
    tx_eng_tag_o      : out std_logic_vector( 4 downto 0);
    tx_eng_wr_tag_o   : out std_logic;
    tx_eng_data_tag_o : out std_logic_vector(31 downto 0);
    
    flag_irq_assert_i :  in std_logic;
   
    param_tx_o        : out param_tx_t;
    param_rx_i        : in  param_rx_t;
    config_register_i : in  config_register_t
  );                                                   

end PIO_TX_ENGINE;

architecture Behavioral of PIO_TX_ENGINE is

--| Types declarations     |--------------------------------------------------------------   
  type state_type is (TX_RST_STATE,        
                      TX_MEM32_RD_QW1, -- prepare the request
                      TX_MEM32_RD_QW2, -- send the request
                      TX_MEM32_RD_QW3, -- check (or wait) tready is available (request is send)
                      TX_DMA_WR_0,     -- fetch data during the DMA write
                      TX_DMA_WR_1,     -- send the DMA write requests first four dwords
                      TX_DMA_WR_2,     -- send the DMA write requests next dwords
                      TX_DMA_WR_3,     -- wait for the command register update
                      TX_CPLD_QW0,     -- prepare compl        
                      TX_CPLD_QW1,     -- prepare and send compl        
                      TX_CPLD_N_QW1,   -- send read compl bigger that 1DW        
                      TX_CPLD_QW2,     -- send read second DW        
                      TX_CPLD_QWD);    -- dessert req_compl_q2_hold
--| Constants declarations |--------------------------------------------------------------
  constant PIO_CPLD_FMT_TYPE    : std_logic_vector(6 downto 0) := "1001010";
  constant PIO_CPL_FMT_TYPE     : std_logic_vector(6 downto 0) := "0001010";
  constant TX_MEM_WR32_FMT_TYPE : std_logic_vector(6 downto 0) := "1000000";   
  constant TX_MEM_RD32_FMT_TYPE : std_logic_vector(6 downto 0) := "0000000";
  constant PIO_TX_RST_STATE     : std_logic := '0';       
  constant PIO_TX_CPLD_QW1      : std_logic := '1';   
  constant MAX_RD_REQ           : unsigned(3 downto 0) := "0011";
  
--| Signals declarations   |--------------------------------------------------------------
  attribute keep : string;
  attribute keep of s_axis_tx_tdata: signal is "true";
  attribute keep of config_register_i: signal is "true";
  
  signal state_s              : state_type;
  attribute keep of state_s: signal is "true";
  signal next_state           : state_type;
    
  signal byte_count           : std_logic_vector(11 downto 0); 
  signal lower_addr           : std_logic_vector( 6 downto 0);
  signal req_compl_q          : std_logic;
  signal req_compl_wd_q       : std_logic;
  signal compl_wd             : std_logic;  
  signal rd_be_copy           : std_logic_vector( 3 downto 0);     -- signal copy fot internal utilisation

  --signal hold_state           : std_logic;            
  signal req_compl_q2         : std_logic;        
  signal req_compl_wd_q2      : std_logic;      
  signal req_compl_q2_hold    : std_logic;
  signal req_compl_wd_q2_hold : std_logic;  
  
  signal nbr_packet_read_s    : unsigned(31 downto 0);
  attribute keep of nbr_packet_read_s: signal is "true";
  signal tag_s                : unsigned(7 downto 0);   
  signal dst_address_s        : unsigned(31 downto 0);   
  signal src_address_s        : unsigned(31 downto 0);
  signal compl_done_s         : std_logic;
  signal last_packet_s        : std_logic;
  signal param_tx_s           : param_tx_t;        
  signal req_read_s           : std_logic;
  signal send_rd_rq_s         : std_logic;        
  signal write_tag_s          : std_logic;

	signal dma_write_s          : std_logic;
	signal dma_write_addr_s     : unsigned(31 downto 0);
	signal dma_write_nb_sent    : unsigned(9 downto 0);
	signal read_nb_sent_s       : unsigned(9 downto 0);
	signal nbr_packet_write_s   : unsigned(31 downto 0);
	
  signal cfg_reg_write_length_s : std_logic_vector(31 downto 0);
  
  signal rd_128_addr_s        : unsigned(MEMORY_SIZE-1 downto 0);
  signal next_packet_rd_128_addr_s  : unsigned(MEMORY_SIZE-1 downto 0);
  
  signal mem128_rd_data_reg   : std_logic_vector(127 downto 0);
  signal DW_BE                : std_logic_vector( 7 downto 0);

  signal req_addr_s           : memory_addr_t;   
  signal req_read_cnt_s       : unsigned(3 downto 0);
  attribute keep of req_read_cnt_s: signal is "true";
  
  signal rd_fifo_128_en_s          : std_logic;
  signal rd_one_words_only_s       : std_logic;
  
begin

  mem128_rd_ctrl_o.addr <= std_logic_vector(rd_128_addr_s);
  mem128_rd_ctrl_o.rd_fifo_en <= rd_fifo_128_en_s and s_axis_tx_tready;
                      
  compl_done <= compl_done_s;
  param_tx_o <= param_tx_s;   
  
  tx_eng_tag_o       <= std_logic_vector(tag_s(4 downto 0));     
  tx_eng_wr_tag_o    <= write_tag_s;
  tx_eng_data_tag_o  <= std_logic_vector("00" & src_address_s(31 downto 2));
                      
  -- Unused discontinue                                        
  tx_src_dsc <= '0';                                    

  -- Present address and byte enable to memory module           
  
  rd_addr     <= req_addr_s;

  
  process(clk, rst_n) begin                 
    if (rst_n = '0') then                 
      rd_be       <= "0000";                                      
      rd_be_copy  <= "0000";
    elsif (rising_edge(clk)) then         
      rd_be       <= req_be(3 downto 0);   
      rd_be_copy  <= req_be(3 downto 0);                                                                
    end if;                               
  end process;                                                                                                                                                                                                                                                                  

  -- Calculate byte count based on byte enable                 

  process (rd_be_copy) begin 
    case rd_be_copy(3 downto 0) is
      when "1001" | "1011" | "1101" | "1111" => byte_count <= x"004";
      when "0101" | "0111"                   => byte_count <= x"003";
      when "1010" | "1110"                   => byte_count <= x"003";
      when "0011"                            => byte_count <= x"002";
      when "0110"                            => byte_count <= x"002";
      when "1100"                            => byte_count <= x"002";
      when "0001"                            => byte_count <= x"001";
      when "0010"                            => byte_count <= x"001";
      when "0100"                            => byte_count <= x"001";
      when "1000"                            => byte_count <= x"001";
      when "0000"                            => byte_count <= x"001";
      when others                            => byte_count <= x"000";
    end case;  
  
  end process;

  process(clk, rst_n) begin             
    if (rst_n = '0') then
      req_compl_q      <= '0';
      req_compl_wd_q   <= '1';                               
    elsif (rising_edge(clk)) then     
      req_compl_q      <= req_compl;                                  
      req_compl_wd_q   <= req_compl_wd;                              
    end if;                                         
  end process;                                                

   -- EPO bug fix : std_logic_vector(unsigned(req_addr_s.addr(4 downto 0)) - 1) instead of req_addr_s.addr(4 downto 0)
   -- prevent a wrong incrementation of lower_address before to create the read completion
  process(rd_be_copy, req_addr_s, compl_wd) begin
    if (compl_wd = '1') then
      case (rd_be_copy(3 downto 0)) is 
        when "0000" => lower_addr <= std_logic_vector(unsigned(req_addr_s.addr(4 downto 0)) - 1) & "00";
        when "0001" | "0011" | "0101" | "0111" => lower_addr <= std_logic_vector(unsigned(req_addr_s.addr(4 downto 0)) - 1) & "00";
        when "1001" | "1011" | "1101" | "1111" => lower_addr <= std_logic_vector(unsigned(req_addr_s.addr(4 downto 0)) - 1) & "00";
        when "0010" | "0110" | "1010" | "1110" => lower_addr <= std_logic_vector(unsigned(req_addr_s.addr(4 downto 0)) - 1) & "01";
        when "0100" | "1100" => lower_addr <= std_logic_vector(unsigned(req_addr_s.addr(4 downto 0)) - 1) & "10";
        when "1000" => lower_addr <= std_logic_vector(unsigned(req_addr_s.addr(4 downto 0)) - 1) & "11";
        when others => lower_addr <= (others => '0');
      end case;
    else
      lower_addr <= "0000000";
    end if;
  end process;

                        
    compl_wd <= req_compl_wd_q2;
    
    -- process(clk, rst_n) begin        
      -- if (rst_n = '0') then        
        -- req_compl_q2      <= '0';
        -- req_compl_wd_q2   <= '0';                         
      -- elsif (rising_edge(clk)) then    
        -- req_compl_q2      <= req_compl_q;                                 
        -- req_compl_wd_q2   <= req_compl_wd_q;                                                                                                                                                                                 
      -- end if;                          
    -- end process;   
    
    -- State Machine
        process(clk, rst_n) begin        
            if (rst_n = '0') then        
--              s_axis_tx_tlast   <= '0';                
--              s_axis_tx_tvalid  <= '0';                
--              s_axis_tx_tdata   <= (others => '0');
--              s_axis_tx_tkeep   <= (others => '0');
--              compl_done        <= '0';                
--              hold_state        <= '0';  
                req_compl_q2           <= '0';
                req_compl_wd_q2        <= '0'; 
                req_compl_q2_hold      <= '0'; 
                req_compl_wd_q2_hold   <= '0'; 
                -- req_read_s             <= '0';    
                req_read_cnt_s         <= (others => '0');
              
              
--              state             <= TX_RST_STATE;
            elsif (rising_edge(clk)) then    
--              s_axis_tx_tlast   <= next_s_axis_tx_tlast;                
--              s_axis_tx_tvalid  <= next_s_axis_tx_tvalid;                
--              s_axis_tx_tdata   <= next_s_axis_tx_tdata;
--              s_axis_tx_tkeep   <= next_s_axis_tx_tkeep;
--              compl_done        <= next_compl_done;                
--              hold_state        <= next_hold_state;  
                req_compl_q2      <= req_compl_q;                                 
                req_compl_wd_q2   <= req_compl_wd_q;  
                
                if (req_compl_q = '1') then
                    req_compl_q2_hold <= '1';
                elsif (compl_done_s = '1') then
                    req_compl_q2_hold <= '0';
                end if;
                
                if (req_compl_wd_q = '1') then
                    req_compl_wd_q2_hold <= '1';  
                elsif (compl_done_s = '1') then 
                    req_compl_wd_q2_hold <= '0';  
                end if;                       
                
                -- Indicate that an another read request can be received
                --if(param_rx_i.tx_acq_read = '1') then
                --    req_read_s <= '0';
                --elsif(send_rd_rq_s = '1') then
                --    req_read_s <= '1';            
                --end if;                         
                if(param_rx_i.tx_acq_read = '1') and (send_rd_rq_s = '0') then  
                    req_read_cnt_s <= req_read_cnt_s - 1;
                elsif(send_rd_rq_s = '1') and (param_rx_i.tx_acq_read = '0') then
                    req_read_cnt_s <= req_read_cnt_s + 1;  
                end if;
                
                
              
            end if;
        end process;
        
    
        process(req_read_cnt_s, config_register_i) is
        begin
                -- allow MAX_RD_REQ to be sent. 
                if (req_read_cnt_s >= unsigned(config_register_i.read_pipe_size)) then --MAX_RD_REQ) then
                    req_read_s  <= '1';
                else
                    req_read_s  <= '0';      
                end if;
        end process;
        
    process(clk, rst_n) 
        variable data_v : integer; 
    begin
        if (rst_n = '0') then        
            s_axis_tx_tlast     <= '0';                
            s_axis_tx_tvalid    <= '0';                
            s_axis_tx_tdata     <= (others => '0');
            s_axis_tx_tkeep     <= (others => '0');
            compl_done_s          <= '0';                  
            
            tag_s               <= (others => '0');
            nbr_packet_read_s   <= (others => '0');    
            last_packet_s       <= '0';       
            send_rd_rq_s        <= '0';
            req_addr_s          <= ((others => '0'),(others=>'0'));
            
            
            state_s           <= TX_RST_STATE;
			
			      dma_write_s <= '0';
			      nbr_packet_write_s <= (others => '0');
            rd_128_addr_s          <= (others => '0');
            read_nb_sent_s          <= (others => '0');
            mem128_rd_data_reg <= (others => '0');
            next_packet_rd_128_addr_s <= (others => '0');
            cfg_reg_write_length_s <= (others => '0');
            
            write_tag_s <= '0';
            mem128_rd_ctrl_o.rd_en <= '0';
            rd_fifo_128_en_s <= '0';

            rd_en <= '0';
            rd_one_words_only_s <= '0';
        
        elsif (rising_edge(clk)) then
            --mem128_rd_data_reg <= mem128_rd_data_i.data;
            req_addr_s         <= req_addr;    
            
      			param_tx_s.end_of_command(1) <= '0';
      			param_tx_s.mem_acq_cmd(1)    <= '0';
      			
      			param_tx_s.end_of_command(2) <= '0';
      			param_tx_s.mem_acq_cmd(2)    <= '0';

            rd_en <= '0';
            
            --rd_fifo_128_en_s <= '0';
            --if rd_fifo_128_en_s = '1' then
            --    mem128_rd_data_reg <= mem128_rd_data_i.data;
            --end if;
        
            case (state_s) is              
        
            when TX_RST_STATE =>   
              rd_fifo_128_en_s  <= '0';
              rd_one_words_only_s <= '0';
              mem128_rd_ctrl_o.rd_en <= '0';
              if (s_axis_tx_tready = '1') then
                if(req_compl_q2_hold = '1') and (flag_irq_assert_i='0') then --ACS : wait irq assert is done before sending new message(data or cmd); otherwise the message could be lost.
                  rd_128_addr_s  <= unsigned(req_addr.addr);
                  state_s        <= TX_CPLD_QW0;
                  mem128_rd_ctrl_o.rd_en <= '1';
                  rd_en <= '1';
                elsif((config_register_i.command(2) = '1') and (req_read_s = '0')) and (flag_irq_assert_i='0')  then --ACS : wait irq assert is done before sending new message(data or cmd); otherwise the message could be lost.                    
                  state_s      <= TX_MEM32_RD_QW1;
                elsif (config_register_i.command(1) = '1') and (flag_irq_assert_i='0') then --ACS : wait irq assert is done before sending new message(data or cmd); otherwise the message could be lost.
                  mem128_rd_ctrl_o.rd_en <= '1';
                  state_s      <= TX_DMA_WR_0;
                  dma_write_s <= '1';
                  cfg_reg_write_length_s <= config_register_i.write_length;
                    if (dma_write_s = '0') then						
                      data_v := 3;
                      nbr_packet_write_s <= to_unsigned(1,nbr_packet_write_s'length);
                      dma_write_addr_s <= unsigned(config_register_i.write_dst_address);
                      next_packet_rd_128_addr_s <= unsigned(config_register_i.write_src_address(rd_128_addr_s'high+2 downto 2)) + unsigned(config_register_i.write_length(rd_128_addr_s'range)) ;
                      rd_128_addr_s <= unsigned(config_register_i.write_src_address(rd_128_addr_s'high+2 downto 2));
                    else
                      dma_write_addr_s <= dma_write_addr_s + unsigned(config_register_i.write_length(29 downto 0) & "00");
                      rd_128_addr_s <= next_packet_rd_128_addr_s;
                      next_packet_rd_128_addr_s <= next_packet_rd_128_addr_s + unsigned(config_register_i.write_length(rd_128_addr_s'range)) ;
                    end if;
                else
                  state_s <= TX_RST_STATE;
                end if;
              else
                state_s <= TX_RST_STATE;    
              end if;
              s_axis_tx_tlast   <= '0';            
              s_axis_tx_tvalid  <= '0';            
              s_axis_tx_tdata   <= (others => '0');
              s_axis_tx_tkeep   <= (others => '1');
              send_rd_rq_s      <= '0';
              compl_done_s      <= '0';        

            when TX_DMA_WR_0 =>
                rd_128_addr_s <= rd_128_addr_s + 4;
                if(cfg_reg_write_length_s = X"00000001") then   --ACS replace: config_register_i.write_length
                    DW_BE <= X"0F";      
                else                                             
                    DW_BE <= X"FF";      
                end if;                  
                rd_fifo_128_en_s  <= '1';
                state_s <= TX_DMA_WR_1;
                if (4 >= unsigned(cfg_reg_write_length_s)) then     --ACS replace: config_register_i.write_length
                    -- test if we have only one words to send (one words to read in fifo).
                    rd_one_words_only_s  <= '1';
                else
                    rd_one_words_only_s  <= '0';
                end if;
                
            when TX_DMA_WR_1 =>
              s_axis_tx_tlast   <= '0';    
              s_axis_tx_tvalid  <= '1';
              --rd_fifo_128_en_s  <= '1';
              --if SIMULATE then
              if true then
                s_axis_tx_tdata     <= switch_data(mem128_rd_data_i.data(31 downto 0))  &    -- 32
                                       std_logic_vector(dma_write_addr_s)               &    -- 32
                                       completer_id                                     &    -- 16  
                                       "00000000"                                       &    --  8  Tag  
                                       DW_BE                                            &    --  8 DW BE
                                       '0'                                              &    --  1
                                       TX_MEM_WR32_FMT_TYPE                             &    --  7  
                                       '0'                                              &    --  1  
                                       req_tc                                           &    --  3  
                                       "0000"                                           &    --  4  
                                       req_td                                           &    --  1  
                                       req_ep                                           &    --  1  
                                       req_attr                                         &    --  2  
                                       "00"                                             &    --  2  
                                       cfg_reg_write_length_s(9 downto 0);                   -- 10      --ACS replace: config_register_i.write_length(9 downto 0)
                                            
                data_v := data_v + 1;
              else
                s_axis_tx_tdata     <= switch_data(x"CAFECAFE")                     &    -- 32  -- YTA: TO be modified
                                       std_logic_vector(dma_write_addr_s)           &    -- 32
                                       completer_id                                 &    -- 16  
                                       "00000000"                                   &    --  8  Tag  
                                       DW_BE                                        &    --  8 DW BE
                                       '0'                                          &    --  1
                                       TX_MEM_WR32_FMT_TYPE                         &    --  7  
                                       '0'                                          &    --  1  
                                       req_tc                                       &    --  3  
                                       "0000"                                       &    --  4  
                                       req_td                                       &    --  1  
                                       req_ep                                       &    --  1  
                                       req_attr                                     &    --  2  
                                       "00"                                         &    --  2  
                                       cfg_reg_write_length_s(9 downto 0);               -- 10          --ACS replace: config_register_i.write_length(9 downto 0)
              end if;
              dma_write_nb_sent <= to_unsigned(1,dma_write_nb_sent'length);
              if (s_axis_tx_tready = '1') then
                rd_128_addr_s <= rd_128_addr_s + 4;
                --rd_fifo_128_en_s <= '1';
                mem128_rd_data_reg <= mem128_rd_data_i.data;
                state_s <= TX_DMA_WR_2;
                if rd_one_words_only_s='1' then
                    -- Data is already read from fifo. Stop to read from fifo.
                    rd_fifo_128_en_s  <= '0';
                else
                    rd_fifo_128_en_s  <= '1';
                end if;
              else
                --rd_fifo_128_en_s  <= '0';
                rd_fifo_128_en_s  <= '1';
                state_s <= TX_DMA_WR_1;
              end if;
				
            when TX_DMA_WR_2 =>
              
              s_axis_tx_tlast   <= '0';    
              s_axis_tx_tvalid  <= '1';
              --if SIMULATE then
              if true then
                s_axis_tx_tdata     <= switch_data(mem128_rd_data_i.data(31 downto 0)) &    -- 32  -- YTA: TO be modified
                                       switch_data(mem128_rd_data_reg(127 downto 96)) &     -- 32  -- YTA: TO be modified
                                       switch_data(mem128_rd_data_reg(95 downto 64)) &      -- 32  -- YTA: TO be modified
                                       switch_data(mem128_rd_data_reg(63 downto 32)) ;      -- 32  -- YTA: TO be modified
                data_v := data_v + 4;
              else
                s_axis_tx_tdata     <= switch_data(x"CAFECAFE") &    -- 32  -- YTA: TO be modified
                                       switch_data(x"CAFECAFE") &    -- 32  -- YTA: TO be modified
                                       switch_data(x"CAFECAFE") &    -- 32  -- YTA: TO be modified
                                       switch_data(x"CAFECAFE") ;    -- 32  -- YTA: TO be modified
              end if;
              
              if (s_axis_tx_tready = '1') then
                rd_128_addr_s <= rd_128_addr_s + 4;
                dma_write_nb_sent <= dma_write_nb_sent + 4;
                if (dma_write_nb_sent + 4 >= unsigned(cfg_reg_write_length_s)) then     --ACS replace: config_register_i.write_length
                  -- The last 128-bit of the packet
                  
                  data_v := data_v - 1 ;
                  
                  s_axis_tx_tlast <= '1';
                  --s_axis_tx_tkeep <= x"0FFF";
                  
                  -- FCC -- change data bus ???
                  if (dma_write_nb_sent = unsigned(cfg_reg_write_length_s)-4) then      --ACS replace: config_register_i.write_length
                    s_axis_tx_tkeep <= x"FFFF";
                    --rd_fifo_128_en_s <= '1';
                    mem128_rd_data_reg <= mem128_rd_data_i.data;
                  elsif (dma_write_nb_sent = unsigned(cfg_reg_write_length_s)-3) then   --ACS replace: config_register_i.write_length
                    s_axis_tx_tkeep <= x"0FFF";
                    --rd_fifo_128_en_s  <= '0';
                  elsif (dma_write_nb_sent = unsigned(cfg_reg_write_length_s)-2) then   --ACS replace: config_register_i.write_length
                    s_axis_tx_tkeep <= x"00FF";
                    --rd_fifo_128_en_s  <= '0';
                  elsif (dma_write_nb_sent = unsigned(cfg_reg_write_length_s)-1) then   --ACS replace: config_register_i.write_length
                    s_axis_tx_tkeep <= x"000F";
                    --rd_fifo_128_en_s  <= '0';
                  end if;


                  -- it's the last packet to send
                  if(nbr_packet_write_s = unsigned(config_register_i.write_nb_packet)) then
                    param_tx_s.end_of_command(1) <= '1';
                    param_tx_s.mem_acq_cmd(1) <= '1';  
                    dma_write_s <= '0';
                  else
                    nbr_packet_write_s <= nbr_packet_write_s + 1;
                  end if;
                  
                  state_s <= TX_DMA_WR_3;
                  rd_fifo_128_en_s  <= '0';
                else
                    if (dma_write_nb_sent + 8 >= unsigned(cfg_reg_write_length_s)) then     --ACS replace: config_register_i.write_length
                        -- next data to send is already present in dmaDataHigh_s
                        -- stop to fech because next data are the last 32bits to be sent.
                        rd_fifo_128_en_s <= '0';
                    else
                        rd_fifo_128_en_s <= '1';
                    end if;
                  state_s <= TX_DMA_WR_2;
                  mem128_rd_data_reg <= mem128_rd_data_i.data;
                end if;
              else
                rd_fifo_128_en_s <= '1';
                state_s <= TX_DMA_WR_2;
              end if;
              
            when TX_DMA_WR_3 =>
              rd_fifo_128_en_s  <= '0';
              -- we wait for the command register update
              if (s_axis_tx_tready = '1') then
                  s_axis_tx_tlast   <= '0';            
                  s_axis_tx_tvalid  <= '0';            
                  s_axis_tx_tdata   <= (others => '0');
                  s_axis_tx_tkeep   <= (others => '1');
                  state_s <= TX_RST_STATE;
              else
                  s_axis_tx_tlast   <= '1';            
                  s_axis_tx_tvalid  <= '1';
              end if;
              
              
                  
            when TX_CPLD_QW0 =>
              rd_fifo_128_en_s  <= '0';
              state_s <= TX_CPLD_QW1;
            
            when TX_CPLD_QW1 =>
              s_axis_tx_tvalid  <= '1';
              rd_fifo_128_en_s  <= '0';
              if (req_compl_wd_q2_hold = '1') then                         
                s_axis_tx_tdata   <= switch_data(rd_data) &    -- 32  
                                    req_rid              &    -- 16  
                                    req_tag              &    --  8  
                                    '0'                  &    --  1  
                                    lower_addr           &    --  7  
                                    completer_id         &    -- 16  
                                    "000"                &    --  3  
                                    '0'                  &    --  1  
                                    byte_count           &    -- 12  
                                    '0'                  &    --  1  
                                    PIO_CPLD_FMT_TYPE    &    --  7  
                                    '0'                  &    --  1  
                                    req_tc               &    --  3  
                                    "0000"               &    --  4  
                                    req_td               &    --  1  
                                    req_ep               &    --  1  
                                    req_attr             &    --  2  
                                    "00"                 &    --  2  
                                    req_len;                   -- 10 
                                                                      
              else                                                    
                s_axis_tx_tdata   <= switch_data(rd_data) &    -- 32  
                                    req_rid              &    -- 16  
                                    req_tag              &    --  8  
                                    '0'                  &    --  1  
                                    lower_addr           &    --  7  
                                    completer_id       &    -- 16  
                                    "000"                &    --  3  
                                    '0'                  &    --  1  
                                    byte_count           &    -- 12  
                                    '0'                  &    --  1  
                                    PIO_CPL_FMT_TYPE     &    --  7  
                                    '0'                  &    --  1  
                                    req_tc               &    --  3  
                                    "0000"               &    --  4  
                                    req_td               &    --  1  
                                    req_ep               &    --  1  
                                    req_attr             &    --  2  
                                    "00"                 &    --  2  
                                    req_len;                   -- 10 
              end if; 
        
              -- Here we select if the packet has data or                                      
              -- not.  The strobe signal will mask data                                        
              -- when it is not needed.  No reason to change                                   
              -- the data bus.                   
              if (req_compl_wd_q2 = '1') then                                                             
                  s_axis_tx_tkeep   <= x"FFFF";                                            
              else                                                                             
                  s_axis_tx_tkeep   <= x"0FFF";                                     
              end if;                              
              
              -- ghr
              send_rd_rq_s        <= '0';
              if req_len = "0000000001" then
                s_axis_tx_tlast     <= '1';    
                compl_done_s        <= '1';
                state_s             <= TX_CPLD_QWD;    
              elsif req_len = "0000000010" then
                 req_addr_s.addr    <= std_logic_vector(unsigned(req_addr_s.addr) + 1); -- 2 read 
                 state_s            <= TX_CPLD_QW2;
                 rd_en <= '1';    
              else
                 read_nb_sent_s     <= to_unsigned(1,read_nb_sent_s'length);
                 rd_128_addr_s      <= rd_128_addr_s + 4;
                 state_s            <= TX_CPLD_N_QW1; 
                 rd_en <= '1'; 
              end if;
                
            when TX_CPLD_QW2   =>
              s_axis_tx_tlast               <= '1';
              s_axis_tx_tdata(31 downto 0)  <= switch_data(rd_data);
              s_axis_tx_tdata(C_DATA_WIDTH-1 downto 32)  <= (others => '0');
              s_axis_tx_tkeep               <= x"000F";
              compl_done_s                  <= '1';   
              send_rd_rq_s                  <= '0';
              state_s                       <= TX_CPLD_QWD;
              rd_fifo_128_en_s  <= '0';
              
            when TX_CPLD_N_QW1 =>  
              rd_fifo_128_en_s  <= '0';
              s_axis_tx_tlast   <= '0';    
              s_axis_tx_tvalid  <= '1';
              s_axis_tx_tdata     <= switch_data(mem128_rd_data_i.data(31 downto 0)) &    
                                     switch_data(mem128_rd_data_reg(127 downto 96)) &     
                                     switch_data(mem128_rd_data_reg(95 downto 64)) &      
                                     switch_data(mem128_rd_data_reg(63 downto 32)) ;      
              data_v := data_v + 4;
              
              if (s_axis_tx_tready = '1') then
                rd_128_addr_s <= rd_128_addr_s + 4;
                read_nb_sent_s <= read_nb_sent_s + 4;
                if (read_nb_sent_s + 4 >= unsigned(req_len)) then
                  -- The last 128-bit of the packet
                  
                  s_axis_tx_tlast <= '1';
                  --s_axis_tx_tkeep <= x"0FFF";
                  compl_done_s    <= '1';   
                                  
                  state_s <= TX_CPLD_QWD;

                  -- FCC -- change data bus ???
                  if (read_nb_sent_s = unsigned(req_len)-3) then
                    s_axis_tx_tkeep <= x"0FFF";
                  elsif (read_nb_sent_s = unsigned(req_len)-2) then
                    s_axis_tx_tkeep <= x"00FF";
                  elsif (read_nb_sent_s = unsigned(req_len)-1) then
                    s_axis_tx_tkeep <= x"000F";
                  end if;

                else
                  state_s <= TX_CPLD_N_QW1;
                  rd_en <= '1';
                end if;
              else
                state_s <= TX_CPLD_N_QW1;
              end if;

            when TX_CPLD_QWD =>
                rd_fifo_128_en_s  <= '0';
                send_rd_rq_s      <= '0';
                compl_done_s      <= '0';  
                if (s_axis_tx_tready = '1') then
                    s_axis_tx_tlast   <= '0';            
                    s_axis_tx_tvalid  <= '0';            
                    s_axis_tx_tdata   <= (others => '0');
                    s_axis_tx_tkeep   <= (others => '1');
                    state_s           <= TX_RST_STATE;
                 else
                    s_axis_tx_tlast   <= '1';            
                    s_axis_tx_tvalid  <= '1';
                 end if;                                          
            
            when TX_MEM32_RD_QW1 =>  
              -- it's the first packet to send  
              rd_fifo_128_en_s  <= '0';
              if(nbr_packet_read_s = X"00000000") then                                                                           
                  dst_address_s      <= unsigned(config_register_i.read_dst_address);     
                  src_address_s      <= unsigned(config_register_i.read_src_address);                                              
                  nbr_packet_read_s  <= (0 => '1', others => '0');                                                               
                  tag_s              <= (others => '0');                                                                         
                  last_packet_s      <= '0';                                                                                     
                  --packet_offset_s    <= (others => '0');                                                                       
                  --data_length_s      <= (others => '0');                                                                       
              else                                                                                                               
                  dst_address_s      <= dst_address_s + unsigned((config_register_i.read_length(dst_address_s'left-3 downto 0) & "00"));      
                  src_address_s      <= src_address_s + unsigned((config_register_i.read_length(dst_address_s'left-3 downto 0) & "00"));                                                            
                  nbr_packet_read_s  <= nbr_packet_read_s + 1;                                               
                  if (tag_s(5) = '0') then        -- Despite the 8 bits allocated, only the 5 LSBs are allowed for use, and the rest must be zero by default.                    
                    tag_s              <= tag_s + 1;
                  else
                    tag_s              <= (others => '0'); 
                  end if;                          
                  --data_length_s      <= (others => '0'); 
                  --packet_offset_s    <= packet_offset_s + unsigned(config_register_i.read_length);                             
              end if;                                                                                                            
              
              -- it's the last packet to send     
              if(nbr_packet_read_s = unsigned(config_register_i.read_nb_packet) - 1) then  
                  param_tx_s.mem_acq_cmd(2)    <= '1'; 
                  param_tx_s.end_of_command(2) <= '1';                           
                  nbr_packet_read_s         <= (others => '0');                          
                  last_packet_s             <= '1';                                          
              end if;                                                            
            
              s_axis_tx_tlast    <= '0';            
              s_axis_tx_tvalid   <= '0';            
              s_axis_tx_tdata    <= (others => '0');
              s_axis_tx_tkeep    <= (others => '1');
              send_rd_rq_s       <= '1';
              compl_done_s       <= '0';  
              write_tag_s        <= '1';       -- write src_address_s into MEM_ACCESS.src_address_mem(tag_s) this value shall be use when a CLPD is received
               
              state_s <= TX_MEM32_RD_QW2;
      
            when TX_MEM32_RD_QW2 =>
                rd_fifo_128_en_s   <= '0';
                s_axis_tx_tlast    <= '1';            
                s_axis_tx_tvalid   <= '1';     
                send_rd_rq_s       <= '0';    
                write_tag_s        <= '0';                                 
                                                                                                                                                                                                                             
                s_axis_tx_tdata(127 downto 64) <= X"00000000" &
                                                  std_logic_vector(dst_address_s(31 downto 02)) & 
                                                  "00";            
    
                s_axis_tx_tdata(63 downto 40) <= completer_id &
                                                 std_logic_vector(tag_s);
                                                           
                if(config_register_i.read_length = X"00000001") then 
                    s_axis_tx_tdata(39 downto 32) <= X"0F";      
                else                                             
                    s_axis_tx_tdata(39 downto 32) <= X"FF";      
                end if;                                          
    
                s_axis_tx_tdata(31 downto  0) <= '0' &                                                                                                                 
                                                TX_MEM_RD32_FMT_TYPE &                                                                                                     
                                                '0' &                                                                                                                      
                                                "000" &                                                                                                                    
                                                "0000" &                                                                                                                   
                                                '0' &                                                                                                                      
                                                '0' &                                                                                                                      
                                                "00" &                                                                                                                     
                                                "00" &                                                                                                                     
                                                config_register_i.read_length(9 downto 0);
                                                        
                s_axis_tx_tkeep   <= x"0FFF";
                compl_done_s        <= '0';
                state_s <= TX_MEM32_RD_QW3;     
                
            when TX_MEM32_RD_QW3 =>
                rd_fifo_128_en_s  <= '0';
                send_rd_rq_s      <= '0';
                compl_done_s      <= '0';  
                if (s_axis_tx_tready = '1') then
                    state_s           <= TX_RST_STATE;
                    s_axis_tx_tlast   <= '0';            
                    s_axis_tx_tvalid  <= '0';
                    s_axis_tx_tdata   <= (others => '0');
                    s_axis_tx_tkeep   <= (others => '1');
                else
                    s_axis_tx_tlast   <= '1';            
                    s_axis_tx_tvalid  <= '1';
                end if;
              
            when others => 
                
                state_s               <= TX_RST_STATE;    

            end case;
            
        end if;
        
    end process;    
    
end Behavioral;                                                                                              
