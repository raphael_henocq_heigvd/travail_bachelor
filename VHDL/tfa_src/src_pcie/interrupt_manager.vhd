--------------------------------------------------------------------------------
--    PCIE2FPGA is a FPGA design that aims to demonstrate PCIe Gen2 x8
--    communication by implementing a DMA engine
--    
--    Copyright (C) 2014 HEIG-VD / REDS
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:10:18 10/04/2012 
-- Design Name: 
-- Module Name:    interrupt_manager - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use WORK.PIO_pkg.all;

entity interrupt_manager is
port(
  clk                       : in std_logic;
  rst_n                     : in std_logic;
  cfg_interrupt             : out std_logic; 
  cfg_interrupt_assert      : out std_logic;
  cfg_interrupt_di          : out std_logic_vector(7 downto 0);
--  cfg_interrupt_do          : in  std_logic_vector(7 downto 0);
--  cfg_interrupt_mmenable    : in  std_logic_vector(2 downto 0);
  cfg_interrupt_msienable   : in  std_logic;
  cfg_interrupt_rdy         : in  std_logic;
  
  cfg_interrupt_stat        : out std_logic;
  cfg_pciecap_interrupt_msgnum : out std_logic_vector(4 downto 0);

  flag_irq_assert_o : out std_logic;
  mask_wr_i : in std_logic;
  irq_clear_i : in std_logic;
  -- for the mask or for a clear
  data_wr_i : in std_logic_vector(31 downto 0);
  
  
  data_rd_o : out std_logic_vector(31 downto 0);

  irq_i : in std_logic_vector(31 downto 0)
);
end interrupt_manager;

architecture behave of interrupt_manager is
    attribute keep : string;
    
	signal irq_reg_s : std_logic_vector(31 downto 0);
	signal mask_s : std_logic_vector(31 downto 0);
	attribute keep of mask_s: signal is "true";
	
	signal could_start_s: std_logic;
	attribute keep of could_start_s: signal is "true";
	
	
  signal irq_register_s           : irq_register_t;
  
  signal flag_irq_assert_s : std_logic;
	
  
  -- Types use for the interrupt machine state
  type interrupt_legacy_state_type is (INT_RST_STATE, INT_WAIT_STATE, INT_ASSERT_STATE_1, INT_ASSERT_STATE_2, INT_DEASSERT_STATE_1, INT_DEASSERT_STATE_2);
  type interrupt_msi_state_type    is (INT_RST_STATE, INT_WAIT_STATE, INT_ASSERT_STATE_1, INT_ASSERT_STATE_2, INT_WAITCLEAR_STATE);
   
  signal interrupt_legacy_state_s,
         interrupt_legacy_nstate_s: interrupt_legacy_state_type;
  attribute keep of interrupt_legacy_state_s: signal is "true";
  
  signal interrupt_msi_state_s,
         interrupt_msi_nstate_s   : interrupt_msi_state_type;
  attribute keep of interrupt_msi_state_s: signal is "true";
  
begin



  cfg_interrupt_stat <= '0';                -- Never set the Interrupt Status bit
  cfg_pciecap_interrupt_msgnum <= "00000";  -- Zero out Interrupt Message Number




	data_rd_o <= irq_reg_s;
	
	flag_irq_assert_o <= flag_irq_assert_s;
	
	-- could start is set to '1' if there is an incomming interrupt
	-- or if there is at least one active bit in the interrupt register
	process(irq_i,irq_reg_s,mask_s)
	begin
		could_start_s <= '0';
		for i in 31 downto 0 loop
			if ((irq_i(i)='1') or (irq_reg_s(i)='1'))
				and (mask_s(i)='1') then
				could_start_s <= '1';
			end if;
		end loop;
	end process;
	
	process(clk,rst_n)
		variable irq_reg_v : std_logic_vector(31 downto 0);
	begin
		if rst_n='0' then
			irq_reg_s <= (others=>'0');
			mask_s <= (others=>'0');
			flag_irq_assert_s <= '0';
		elsif rising_edge(clk) then
			
			-- a write access to the mask
			if (mask_wr_i='1') then
				mask_s <= data_wr_i;
			end if;
			
			irq_reg_v:=irq_reg_s;
			
			-- If a clear request occurs, then clear the bits
			if (irq_clear_i='1') then
				irq_reg_v:=irq_reg_v and (not(data_wr_i));
			end if;
			
			-- If there is an incoming interrupt, then set the corresponding
			-- bits
			irq_reg_v:=irq_reg_v or irq_i;			
			
			irq_reg_s<=irq_reg_v;
			
            -- If an interrupt is assert, then set a flag (for Tx engine)
            if cfg_interrupt_msienable='0' then
                if ((could_start_s='1') and (interrupt_legacy_state_s=INT_WAIT_STATE)) then
                    flag_irq_assert_s <= '1';
                elsif (interrupt_legacy_state_s=INT_ASSERT_STATE_1)then
                    flag_irq_assert_s <= '1';
                elsif (interrupt_legacy_state_s=INT_DEASSERT_STATE_1)then
                    flag_irq_assert_s <= '1';
                else
                    flag_irq_assert_s <= '0';
                end if;
            else
                if ((could_start_s='1') and (interrupt_msi_state_s=INT_WAIT_STATE)) then
                    flag_irq_assert_s <= '1';
                elsif (interrupt_msi_state_s=INT_ASSERT_STATE_1)then
                    flag_irq_assert_s <= '1';
                else
                    flag_irq_assert_s <= '0';
                end if;
            end if;
		end if;
	end process;


  Interrupt_Management_Asynch_Process : process(cfg_interrupt_msienable,
                                                interrupt_legacy_state_s,
																interrupt_msi_state_s,
																cfg_interrupt_rdy,
																could_start_s,
																irq_clear_i)
  begin
    interrupt_legacy_nstate_s <= interrupt_legacy_state_s;
	 interrupt_msi_nstate_s <= interrupt_msi_state_s;

    irq_register_s.debug_legacy <= (others => '0');
    irq_register_s.debug_msi    <= (others => '0');
    cfg_interrupt            <= '0';  
    cfg_interrupt_assert     <= '0';  
    cfg_interrupt_di         <= X"00";
      
      if cfg_interrupt_msienable = '0' then
     
        case (interrupt_legacy_state_s) is    
        
          -- wait until the interrupt command is '0'
          when INT_RST_STATE => 
            irq_register_s.debug_legacy(5 downto 0) <= "000001";
            --if config_register_s.command(command_interrupt_request_c) = '0' then
              interrupt_legacy_nstate_s <= INT_WAIT_STATE;
            --end if;
           
          -- wait for an interrupt
          when INT_WAIT_STATE =>
            irq_register_s.debug_legacy(5 downto 0) <= "000010";
            if could_start_s='1' then
              interrupt_legacy_nstate_s <= INT_ASSERT_STATE_1;
            end if;
         
          -- first assert state
          when INT_ASSERT_STATE_1 => 
            irq_register_s.debug_legacy(5 downto 0) <= "000100";
            cfg_interrupt_assert <= '1';
				cfg_interrupt <= '1';
            if cfg_interrupt_rdy = '1' then
				  interrupt_legacy_nstate_s <= INT_ASSERT_STATE_2;
            end if;
          
          -- second assert state        
          when INT_ASSERT_STATE_2 =>    
            irq_register_s.debug_legacy(5 downto 0) <= "001000";
				cfg_interrupt <= '0';
				cfg_interrupt_assert <= '1';
				-- The CPU acknowledge the interrupt by writing '0' into the 
				-- command register
            if irq_clear_i='1' then
				  interrupt_legacy_nstate_s <= INT_DEASSERT_STATE_1;
            end if;
          
          -- first deassert state
          when INT_DEASSERT_STATE_1 => 
            irq_register_s.debug_legacy(5 downto 0) <= "010000";
				cfg_interrupt <= '1';
				cfg_interrupt_assert <= '0';
            if cfg_interrupt_rdy = '1' then
              interrupt_legacy_nstate_s <= INT_WAIT_STATE;
            end if;
                    
          when others =>  
            irq_register_s.debug_legacy(5 downto 0) <= "111111";
            interrupt_legacy_nstate_s <= INT_RST_STATE;

        end case;
        
      else
      
        case (interrupt_msi_state_s) is    
        
          -- wait until the interrupt command is '0'
          when INT_RST_STATE => 
            irq_register_s.debug_msi(3 downto 0) <= "0001";
           -- if could_start='1' then
              interrupt_msi_nstate_s <= INT_WAIT_STATE;
           -- else
           --   interrupt_msi_nstate_s <= INT_RST_STATE;
           -- end if;
           
          -- wait for an interrupt
          when INT_WAIT_STATE =>
            irq_register_s.debug_msi(3 downto 0) <= "0010";
            if could_start_s='1' then
              interrupt_msi_nstate_s <= INT_ASSERT_STATE_1;
            else
              interrupt_msi_nstate_s <= INT_WAIT_STATE;
            end if;
         
          -- first assert state
          when INT_ASSERT_STATE_1 =>   
            irq_register_s.debug_msi(3 downto 0) <= "0100";
            cfg_interrupt <= '1';
            if cfg_interrupt_rdy = '1' then
              interrupt_msi_nstate_s <= INT_WAITCLEAR_STATE;
            end if;
            
          when INT_WAITCLEAR_STATE =>
            if irq_clear_i='1' then
              interrupt_msi_nstate_s <= INT_RST_STATE;
            end if;
            
          when others =>  
            irq_register_s.debug_msi(3 downto 0) <= "1111";
            interrupt_msi_nstate_s <= INT_RST_STATE;

        end case;      
      end if;
  end process;
  
  -- Interrupt management process
  Interrupt_Management_Sync_Process : process (rst_n, clk)
  begin
    if rst_n = '0' then
		interrupt_legacy_state_s <= INT_RST_STATE;
      interrupt_msi_state_s    <= INT_WAIT_STATE;
    elsif rising_edge(clk) then
		interrupt_legacy_state_s <= interrupt_legacy_nstate_s;
		interrupt_msi_state_s    <= interrupt_msi_nstate_s;
    end if;
  end process;			

end behave;
