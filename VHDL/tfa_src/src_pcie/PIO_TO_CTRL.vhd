--------------------------------------------------------------------------------
--    PCIE2FPGA is a FPGA design that aims to demonstrate PCIe Gen2 x8
--    communication by implementing a DMA engine
--    
--    Copyright (C) 2014 HEIG-VD / REDS
--
--    This program is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    This program is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;


entity PIO_TO_CTRL is
    generic(
        TCQ          : integer := 1
        );
        
    port(
            clk               :  in std_logic;                                         
            rst_n             :  in std_logic;                                         
            req_compl         :  in std_logic; 
            compl_done        :  in std_logic; 
                                               
            cfg_to_turnoff    :  in std_logic; 
            cfg_turnoff_ok    : out std_logic                                                                                                                          
        );                                                   
                                                                                                                 
end PIO_TO_CTRL;

architecture Behavioral of PIO_TO_CTRL is

    signal   trn_pending : std_logic;

    begin
    
        -- Check if completion is pending 
        process(clk, rst_n) begin              
            if (rst_n = '0') then              
                trn_pending <= '0';                
            elsif (rising_edge(clk)) then      
                if ((trn_pending = '0') and (req_compl = '1')) then                                             
                    trn_pending <= '1';   
                                              
                elsif (compl_done = '1') then                                                       
                    trn_pending <= '0';   
                    
                end if;        
                 
            end if;     
                                   
        end process;                           

        -- Turn-off OK if requested and no transaction is pending 
        process(clk, rst_n) begin           
            if (rst_n = '0') then           
                cfg_turnoff_ok <= '0';                                
            elsif (rising_edge(clk)) then   
                if ((cfg_to_turnoff = '1') and (trn_pending = '0')) then 
                    cfg_turnoff_ok <= '1';
                            
                else                                  
                    cfg_turnoff_ok <= '0';
                            
                end if;
                                            
            end if; 
                                    
        end process;                        

end Behavioral;
