------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : demux_1toline.vhd
-- Author               : Convers Anthony
-- Date                 : 22.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity demux_1toline is
    Port ( sel : in  STD_LOGIC_VECTOR (bit_sel_cel_io-1 downto 0);
           x : in  STD_LOGIC_VECTOR (size_bus_ext-1 downto 0);
           y_all : out dat_line_ovly);
end demux_1toline;

architecture demux_1toline_arch of demux_1toline is

begin

gen_1 : for i_gen in y_all'range generate
begin
	y_all(i_gen)<= x when sel=i_gen else (others => '0');
end generate;


end demux_1toline_arch;

