------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : overlay_cell.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use work.overlay_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity overlay_cell is
    generic (size: integer:= size_bus);
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           cfg_mux_sel_i : in  STD_LOGIC_VECTOR (31 downto 0);
			  --North
           N_dat_prv_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           N_val_prv_i : in  STD_LOGIC;
           N_acc_prv_o : out  STD_LOGIC;
           N_dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           N_val_nxt_o : out  STD_LOGIC;
           N_acc_nxt_i : in  STD_LOGIC;
			  --West
           W_dat_prv_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           W_val_prv_i : in  STD_LOGIC;
           W_acc_prv_o : out  STD_LOGIC;
           W_dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           W_val_nxt_o : out  STD_LOGIC;
           W_acc_nxt_i : in  STD_LOGIC;
			  --East
           E_dat_prv_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           E_val_prv_i : in  STD_LOGIC;
           E_acc_prv_o : out  STD_LOGIC;
           E_dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           E_val_nxt_o : out  STD_LOGIC;
           E_acc_nxt_i : in  STD_LOGIC;
			  --South
           S_dat_prv_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           S_val_prv_i : in  STD_LOGIC;
           S_acc_prv_o : out  STD_LOGIC;
           S_dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           S_val_nxt_o : out  STD_LOGIC;
           S_acc_nxt_i : in  STD_LOGIC;

           data_size_i : in std_logic_vector(data_size_length-1 downto 0));
end overlay_cell;

architecture overlay_cell_arch of overlay_cell is

--North Signal
signal Nin_dat_00_s : std_logic_vector(size-1 downto 0);
signal Nin_val_00_s : std_logic;
signal Nin_acc_00_s : std_logic;
signal Nin_dat_01_s : std_logic_vector(size-1 downto 0);
signal Nin_val_01_s : std_logic;
signal Nin_acc_01_s : std_logic;
signal Nin_dat_02_s : std_logic_vector(size-1 downto 0);
signal Nin_val_02_s : std_logic;
signal Nin_acc0_02_s : std_logic;
signal Nin_acc1_02_s : std_logic;
signal Nin_acc2_02_s : std_logic;
signal Nin_acc3_02_s : std_logic;
signal Nin_acc4_02_s : std_logic;
signal Nin_acc5_02_s : std_logic;

signal Nout_acc_00_s : std_logic;
signal Nout_dat_01_s : std_logic_vector(size-1 downto 0);
signal Nout_val_01_s : std_logic;
signal Nout_acc_01_s : std_logic;
signal Nout_dat_02_s : std_logic_vector(size-1 downto 0);
signal Nout_val_02_s : std_logic;
signal Nout_acc_02_s : std_logic;

signal Nin_fs_mask_s : std_logic_vector(5 downto 0);
signal Nout_mux_sel_s : std_logic_vector(2 downto 0);

--West  Signal
signal Win_dat_00_s : std_logic_vector(size-1 downto 0);
signal Win_val_00_s : std_logic;
signal Win_acc_00_s : std_logic;
signal Win_dat_01_s : std_logic_vector(size-1 downto 0);
signal Win_val_01_s : std_logic;
signal Win_acc_01_s : std_logic;
signal Win_dat_02_s : std_logic_vector(size-1 downto 0);
signal Win_val_02_s : std_logic;
signal Win_acc0_02_s : std_logic;
signal Win_acc1_02_s : std_logic;
signal Win_acc2_02_s : std_logic;
signal Win_acc3_02_s : std_logic;
signal Win_acc4_02_s : std_logic;
signal Win_acc5_02_s : std_logic;

signal Wout_acc_00_s : std_logic;
signal Wout_dat_01_s : std_logic_vector(size-1 downto 0);
signal Wout_val_01_s : std_logic;
signal Wout_acc_01_s : std_logic;
signal Wout_dat_02_s : std_logic_vector(size-1 downto 0);
signal Wout_val_02_s : std_logic;
signal Wout_acc_02_s : std_logic;

signal Win_fs_mask_s : std_logic_vector(5 downto 0);
signal Wout_mux_sel_s : std_logic_vector(2 downto 0);


--East  Signal
signal Ein_dat_00_s : std_logic_vector(size-1 downto 0);
signal Ein_val_00_s : std_logic;
signal Ein_acc_00_s : std_logic;
signal Ein_dat_01_s : std_logic_vector(size-1 downto 0);
signal Ein_val_01_s : std_logic;
signal Ein_acc_01_s : std_logic;
signal Ein_dat_02_s : std_logic_vector(size-1 downto 0);
signal Ein_val_02_s : std_logic;
signal Ein_acc0_02_s : std_logic;
signal Ein_acc1_02_s : std_logic;
signal Ein_acc2_02_s : std_logic;
signal Ein_acc3_02_s : std_logic;
signal Ein_acc4_02_s : std_logic;
signal Ein_acc5_02_s : std_logic;

signal Eout_acc_00_s : std_logic;
signal Eout_dat_01_s : std_logic_vector(size-1 downto 0);
signal Eout_val_01_s : std_logic;
signal Eout_acc_01_s : std_logic;
signal Eout_dat_02_s : std_logic_vector(size-1 downto 0);
signal Eout_val_02_s : std_logic;
signal Eout_acc_02_s : std_logic;

signal Ein_fs_mask_s : std_logic_vector(5 downto 0);
signal Eout_mux_sel_s : std_logic_vector(2 downto 0);

--South Signal
signal Sin_dat_00_s : std_logic_vector(size-1 downto 0);
signal Sin_val_00_s : std_logic;
signal Sin_acc_00_s : std_logic;
signal Sin_dat_01_s : std_logic_vector(size-1 downto 0);
signal Sin_val_01_s : std_logic;
signal Sin_acc_01_s : std_logic;
signal Sin_dat_02_s : std_logic_vector(size-1 downto 0);
signal Sin_val_02_s : std_logic;
signal Sin_acc0_02_s : std_logic;
signal Sin_acc1_02_s : std_logic;
signal Sin_acc2_02_s : std_logic;
signal Sin_acc3_02_s : std_logic;
signal Sin_acc4_02_s : std_logic;
signal Sin_acc5_02_s : std_logic;

signal Sout_acc_00_s : std_logic;
signal Sout_dat_01_s : std_logic_vector(size-1 downto 0);
signal Sout_val_01_s : std_logic;
signal Sout_acc_01_s : std_logic;
signal Sout_dat_02_s : std_logic_vector(size-1 downto 0);
signal Sout_val_02_s : std_logic;
signal Sout_acc_02_s : std_logic;

signal Sin_fs_mask_s : std_logic_vector(5 downto 0);
signal Sout_mux_sel_s : std_logic_vector(2 downto 0);

--FU Signal
--in1
signal Fin1_acc_00_s : std_logic;
signal Fin1_dat_01_s : std_logic_vector(size-1 downto 0);
signal Fin1_val_01_s : std_logic;
signal Fin1_acc_01_s : std_logic;
signal Fin1_dat_02_s : std_logic_vector(size-1 downto 0);
signal Fin1_val_02_s : std_logic;
signal Fin1_acc_02_s : std_logic;

signal Fin1_mux_sel_s : std_logic_vector(2 downto 0);

--in2
signal Fin2_acc_00_s : std_logic;
signal Fin2_dat_01_s : std_logic_vector(size-1 downto 0);
signal Fin2_val_01_s : std_logic;
signal Fin2_acc_01_s : std_logic;
signal Fin2_dat_02_s : std_logic_vector(size-1 downto 0);
signal Fin2_val_02_s : std_logic;
signal Fin2_acc_02_s : std_logic;

signal Fin2_mux_sel_s : std_logic_vector(2 downto 0);

--sel
signal Fsel_acc_00_s : std_logic;
signal Fsel_dat_01_s : std_logic_vector(size-1 downto 0);
signal Fsel_val_01_s : std_logic;
signal Fsel_acc_01_s : std_logic;
signal Fsel_dat_02_s : std_logic_vector(size-1 downto 0);
signal Fsel_val_02_s : std_logic;
signal Fsel_acc_02_s : std_logic;

signal Fsel_mux_sel_s : std_logic_vector(2 downto 0);

--fu
--signal FU_acc_00_s : std_logic;
signal FU_dat_01_s : std_logic_vector(size-1 downto 0);
signal FU_val_01_s : std_logic;
signal FU_acc_01_s : std_logic;

signal Fu_cfg_const_s : std_logic_vector(1 downto 0);
signal Fu_cfg_op_s : std_logic_vector(7 downto 0);

--out
signal Fout_dat_01_s : std_logic_vector(size-1 downto 0);
signal Fout_val_01_s : std_logic;
signal Fout_acc_01_s : std_logic;
signal Fout_dat_02_s : std_logic_vector(size-1 downto 0);
signal Fout_val_02_s : std_logic;
signal Fout_acc0_02_s : std_logic;
signal Fout_acc1_02_s : std_logic;
signal Fout_acc2_02_s : std_logic;
signal Fout_acc3_02_s : std_logic;

signal Fout_fs_mask_s : std_logic_vector(3 downto 0);


--temp
--signal bus_0 : std_logic_vector(size-1 downto 0) := (others =>'0');

begin
----------------------------------------------------------------------------------------------------
--Configuration Cell (routing)
Cfg_01: entity work.cfg_cell_mux2fs port map (clk,reset,cfg_mux_sel_i,
                                              Nout_mux_sel_s,Wout_mux_sel_s,Eout_mux_sel_s,Sout_mux_sel_s,Fin1_mux_sel_s,Fin2_mux_sel_s,Fsel_mux_sel_s,
                                              Nin_fs_mask_s,Win_fs_mask_s,Ein_fs_mask_s,Sin_fs_mask_s,Fout_fs_mask_s,Fu_cfg_const_s,Fu_cfg_op_s);

----------------------------------------------------------------------------------------------------
--North input
Nin_dat_00_s <= N_dat_prv_i;
Nin_val_00_s <= N_val_prv_i;
N_acc_prv_o <= Nin_acc_00_s;

Nin_01: entity work.d_fifo generic map(size) port map (clk,reset,Nin_dat_00_s,Nin_val_00_s,Nin_acc_00_s,Nin_dat_01_s,Nin_val_01_s,Nin_acc_01_s);
Nin_02: entity work.d_fs_6 generic map(size) port map (Nin_fs_mask_s,Nin_dat_01_s,Nin_val_01_s,Nin_acc_01_s,Nin_dat_02_s,Nin_val_02_s,
                                                       Nin_acc0_02_s,Nin_acc1_02_s,Nin_acc2_02_s,Nin_acc3_02_s,Nin_acc4_02_s,Nin_acc5_02_s);
Nin_acc0_02_s <= Wout_acc_00_s;
Nin_acc1_02_s <= Eout_acc_00_s;
Nin_acc2_02_s <= Sout_acc_00_s;
Nin_acc3_02_s <= Fin1_acc_00_s;
Nin_acc4_02_s <= Fin2_acc_00_s;
Nin_acc5_02_s <= Fsel_acc_00_s;

--North output
Nout_01: entity work.d_mux generic map(size) port map (Nout_mux_sel_s,Win_dat_02_s,Win_val_02_s,Ein_dat_02_s,Ein_val_02_s,
                                                       Sin_dat_02_s,Sin_val_02_s,Fout_dat_02_s,Fout_val_02_s,Nout_acc_00_s,
                                                       Nout_dat_01_s,Nout_val_01_s,Nout_acc_01_s);
Nout_02: entity work.d_reg generic map(size) port map (clk,reset,Nout_dat_01_s,Nout_val_01_s,Nout_acc_01_s,Nout_dat_02_s,Nout_val_02_s,Nout_acc_02_s);

N_dat_nxt_o <= Nout_dat_02_s;
N_val_nxt_o <= Nout_val_02_s;
Nout_acc_02_s <= N_acc_nxt_i;

----------------------------------------------------------------------------------------------------
--West input
Win_dat_00_s <= W_dat_prv_i;
Win_val_00_s <= W_val_prv_i;
W_acc_prv_o <= Win_acc_00_s;

Win_01: entity work.d_fifo generic map(size) port map (clk,reset,Win_dat_00_s,Win_val_00_s,Win_acc_00_s,Win_dat_01_s,Win_val_01_s,Win_acc_01_s);
Win_02: entity work.d_fs_6 generic map(size) port map (Win_fs_mask_s,Win_dat_01_s,Win_val_01_s,Win_acc_01_s,Win_dat_02_s,Win_val_02_s,
                                                       Win_acc0_02_s,Win_acc1_02_s,Win_acc2_02_s,Win_acc3_02_s,Win_acc4_02_s,Win_acc5_02_s);
Win_acc0_02_s <= Nout_acc_00_s;
Win_acc1_02_s <= Eout_acc_00_s;
Win_acc2_02_s <= Sout_acc_00_s;
Win_acc3_02_s <= Fin1_acc_00_s;
Win_acc4_02_s <= Fin2_acc_00_s;
Win_acc5_02_s <= Fsel_acc_00_s;

--West output
Wout_01: entity work.d_mux generic map(size) port map (Wout_mux_sel_s,Nin_dat_02_s,Nin_val_02_s,Ein_dat_02_s,Ein_val_02_s,
                                                       Sin_dat_02_s,Sin_val_02_s,Fout_dat_02_s,Fout_val_02_s,Wout_acc_00_s,
                                                       Wout_dat_01_s,Wout_val_01_s,Wout_acc_01_s);
Wout_02: entity work.d_reg generic map(size) port map (clk,reset,Wout_dat_01_s,Wout_val_01_s,Wout_acc_01_s,Wout_dat_02_s,Wout_val_02_s,Wout_acc_02_s);

W_dat_nxt_o <= Wout_dat_02_s;
W_val_nxt_o <= Wout_val_02_s;
Wout_acc_02_s <= W_acc_nxt_i;

----------------------------------------------------------------------------------------------------
--East input
Ein_dat_00_s <= E_dat_prv_i;
Ein_val_00_s <= E_val_prv_i;
E_acc_prv_o <= Ein_acc_00_s;

Ein_01: entity work.d_fifo generic map(size) port map (clk,reset,Ein_dat_00_s,Ein_val_00_s,Ein_acc_00_s,Ein_dat_01_s,Ein_val_01_s,Ein_acc_01_s);
Ein_02: entity work.d_fs_6 generic map(size) port map (Ein_fs_mask_s,Ein_dat_01_s,Ein_val_01_s,Ein_acc_01_s,Ein_dat_02_s,Ein_val_02_s,
                                                       Ein_acc0_02_s,Ein_acc1_02_s,Ein_acc2_02_s,Ein_acc3_02_s,Ein_acc4_02_s,Ein_acc5_02_s);
Ein_acc0_02_s <= Nout_acc_00_s;
Ein_acc1_02_s <= Wout_acc_00_s;
Ein_acc2_02_s <= Sout_acc_00_s;
Ein_acc3_02_s <= Fin1_acc_00_s;
Ein_acc4_02_s <= Fin2_acc_00_s;
Ein_acc5_02_s <= Fsel_acc_00_s;

--East output
Eout_01: entity work.d_mux generic map(size) port map (Eout_mux_sel_s,Nin_dat_02_s,Nin_val_02_s,Win_dat_02_s,Win_val_02_s,
                                                       Sin_dat_02_s,Sin_val_02_s,Fout_dat_02_s,Fout_val_02_s,Eout_acc_00_s,
                                                       Eout_dat_01_s,Eout_val_01_s,Eout_acc_01_s);
Eout_02: entity work.d_reg generic map(size) port map (clk,reset,Eout_dat_01_s,Eout_val_01_s,Eout_acc_01_s,Eout_dat_02_s,Eout_val_02_s,Eout_acc_02_s);

E_dat_nxt_o <= Eout_dat_02_s;
E_val_nxt_o <= Eout_val_02_s;
Eout_acc_02_s <= E_acc_nxt_i;

----------------------------------------------------------------------------------------------------
--South input
Sin_dat_00_s <= S_dat_prv_i;
Sin_val_00_s <= S_val_prv_i;
S_acc_prv_o <= Sin_acc_00_s;

Sin_01: entity work.d_fifo generic map(size) port map (clk,reset,Sin_dat_00_s,Sin_val_00_s,Sin_acc_00_s,Sin_dat_01_s,Sin_val_01_s,Sin_acc_01_s);
Sin_02: entity work.d_fs_6 generic map(size) port map (Sin_fs_mask_s,Sin_dat_01_s,Sin_val_01_s,Sin_acc_01_s,Sin_dat_02_s,Sin_val_02_s,
                                                       Sin_acc0_02_s,Sin_acc1_02_s,Sin_acc2_02_s,Sin_acc3_02_s,Sin_acc4_02_s,Sin_acc5_02_s);
Sin_acc0_02_s <= Nout_acc_00_s;
Sin_acc1_02_s <= Wout_acc_00_s;
Sin_acc2_02_s <= Eout_acc_00_s;
Sin_acc3_02_s <= Fin1_acc_00_s;
Sin_acc4_02_s <= Fin2_acc_00_s;
Sin_acc5_02_s <= Fsel_acc_00_s;

--South output
Sout_01: entity work.d_mux generic map(size) port map (Sout_mux_sel_s,Nin_dat_02_s,Nin_val_02_s,Win_dat_02_s,Win_val_02_s,
                                                       Ein_dat_02_s,Ein_val_02_s,Fout_dat_02_s,Fout_val_02_s,Sout_acc_00_s,
                                                       Sout_dat_01_s,Sout_val_01_s,Sout_acc_01_s);
Sout_02: entity work.d_reg generic map(size) port map (clk,reset,Sout_dat_01_s,Sout_val_01_s,Sout_acc_01_s,Sout_dat_02_s,Sout_val_02_s,Sout_acc_02_s);

S_dat_nxt_o <= Sout_dat_02_s;
S_val_nxt_o <= Sout_val_02_s;
Sout_acc_02_s <= S_acc_nxt_i;

----------------------------------------------------------------------------------------------------
--Functional Unit (FU)
-- FU in1
FU_in1_01: entity work.d_mux generic map(size) port map (Fin1_mux_sel_s,Nin_dat_02_s,Nin_val_02_s,Win_dat_02_s,Win_val_02_s,
                                                         Ein_dat_02_s,Ein_val_02_s,Sin_dat_02_s,Sin_val_02_s,Fin1_acc_00_s,
                                                         Fin1_dat_01_s,Fin1_val_01_s,Fin1_acc_01_s);
FU_in1_02: entity work.d_eb generic map(size) port map (clk,reset,Fin1_dat_01_s,Fin1_val_01_s,Fin1_acc_01_s,Fin1_dat_02_s,Fin1_val_02_s,Fin1_acc_02_s);
--Fin1_acc_02_s <= FU_acc_00_s;

-- FU in2
FU_in2_01: entity work.d_mux generic map(size) port map (Fin2_mux_sel_s,Nin_dat_02_s,Nin_val_02_s,Win_dat_02_s,Win_val_02_s,
                                                         Ein_dat_02_s,Ein_val_02_s,Sin_dat_02_s,Sin_val_02_s,Fin2_acc_00_s,
                                                         Fin2_dat_01_s,Fin2_val_01_s,Fin2_acc_01_s);
FU_in2_02: entity work.d_eb generic map(size) port map (clk,reset,Fin2_dat_01_s,Fin2_val_01_s,Fin2_acc_01_s,Fin2_dat_02_s,Fin2_val_02_s,Fin2_acc_02_s);
--Fin2_acc_02_s <= FU_acc_00_s;

-- FU sel
FU_sel_01: entity work.d_mux generic map(size) port map (Fsel_mux_sel_s,Nin_dat_02_s,Nin_val_02_s,Win_dat_02_s,Win_val_02_s,
                                                         Ein_dat_02_s,Ein_val_02_s,Sin_dat_02_s,Sin_val_02_s,Fsel_acc_00_s,
                                                         Fsel_dat_01_s,Fsel_val_01_s,Fsel_acc_01_s);
FU_sel_02: entity work.d_eb generic map(size) port map (clk,reset,Fsel_dat_01_s,Fsel_val_01_s,Fsel_acc_01_s,Fsel_dat_02_s,Fsel_val_02_s,Fsel_acc_02_s);
--Fsel_acc_02_s <= FU_acc_00_s;

-- FU
--FU_01: entity work.fu_add generic map(size) port map (clk,reset,Fin1_dat_02_s,Fin1_val_02_s,Fin2_dat_02_s,Fin2_val_02_s,FU_acc_00_s,FU_dat_01_s,FU_val_01_s,FU_acc_01_s);
FU_01: entity work.fu_all generic map(size) port map (clk,reset,Fu_cfg_const_s,Fu_cfg_op_s,
                                                      Fin1_dat_02_s,Fin1_val_02_s,Fin1_acc_02_s,
                                                      Fin2_dat_02_s,Fin2_val_02_s,Fin2_acc_02_s,
																		Fsel_dat_02_s,Fsel_val_02_s,Fsel_acc_02_s,
																		FU_dat_01_s,FU_val_01_s,FU_acc_01_s, data_size_i);

-- FU out
FU_out_01: entity work.d_buf generic map(size) port map (clk,reset,FU_dat_01_s,FU_val_01_s,FU_acc_01_s,Fout_dat_01_s,Fout_val_01_s,Fout_acc_01_s);
FU_out_02: entity work.d_fs_4 generic map(size) port map (Fout_fs_mask_s,Fout_dat_01_s,Fout_val_01_s,Fout_acc_01_s,Fout_dat_02_s,Fout_val_02_s,
                                                          Fout_acc0_02_s,Fout_acc1_02_s,Fout_acc2_02_s,Fout_acc3_02_s);
Fout_acc0_02_s <= Nout_acc_00_s;
Fout_acc1_02_s <= Wout_acc_00_s;
Fout_acc2_02_s <= Eout_acc_00_s;
Fout_acc3_02_s <= Sout_acc_00_s;

end overlay_cell_arch;
