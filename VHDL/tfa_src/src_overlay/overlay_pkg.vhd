------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : overlay_pkg.vhd
-- Author               : Convers Anthony
-- Date                 : 19.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_Std.all;

package overlay_pkg is

constant size_bus_ext     : integer := 32;        -- Modif by RHE
constant size_bus         : integer := 2;         -- Modif by RHE
constant data_size_length : integer := 32;        --Taille du bus contenant la taille des données envoyés à l'overlay
constant bit_sel_reg_io : integer := 2;
constant bit_sel_cel_io : integer := 6;
constant bit_cmd_mux_io : integer := bit_sel_reg_io + bit_sel_cel_io;

constant line_ovly : integer := 19;	--3 15 24
constant col_ovly : integer := 18;	--3 15 16
constant nbr_all_cell : integer := line_ovly*col_ovly;
constant nbr_io_ovly : integer := line_ovly*2 + col_ovly*2;
constant nbr_region : integer := 4;

constant LINE_OVLY_VEC : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(line_ovly,32));
constant COL_OVLY_VEC : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(col_ovly,32));

type dat_reg_ovly is array (nbr_region-1 downto 0) of std_logic_vector (size_bus_ext+bit_sel_cel_io-1 downto 0);
type bit_reg_ovly is array (nbr_region-1 downto 0) of std_logic;
type dat_line_ovly is array (line_ovly-1 downto 0) of std_logic_vector (size_bus_ext-1 downto 0);
type bit_line_ovly is array (line_ovly-1 downto 0) of std_logic;
type dat_col_ovly is array (col_ovly-1 downto 0) of std_logic_vector (size_bus_ext-1 downto 0);
type bit_col_ovly is array (col_ovly-1 downto 0) of std_logic;

type dat_io_in_ovly is array (nbr_io_ovly-1 downto 0) of std_logic_vector (size_bus-1 downto 0);
type dat_io_ovly is array (nbr_io_ovly-1 downto 0) of std_logic_vector (size_bus_ext-1 downto 0);
type bit_io_ovly is array (nbr_io_ovly-1 downto 0) of std_logic;

type cfg_mux_ovly_type is array (nbr_all_cell-1 downto 0) of std_logic_vector (31 downto 0);
--type cfg_fs_ovly_type is array (nbr_all_cell-1 downto 0) of std_logic_vector (23 downto 0);

type dat_nxm_ovly is array (line_ovly downto 0,col_ovly downto 0) of std_logic_vector (size_bus-1 downto 0);
type bit_nxm_ovly is array (line_ovly downto 0,col_ovly downto 0) of std_logic;

------------------------------------------------------------------
--FU operation opcodes
constant FU_OPCODE_SUM   : std_logic_vector(07 downto 0) := X"01";	--in1 + in2
constant FU_OPCODE_SUB   : std_logic_vector(07 downto 0) := X"02";	--in1 - in2
constant FU_OPCODE_MUL   : std_logic_vector(07 downto 0) := X"03";	--in1 * in2
constant FU_OPCODE_DIV   : std_logic_vector(07 downto 0) := X"04";	--in1 / in2
constant FU_OPCODE_MOD   : std_logic_vector(07 downto 0) := X"05";	--in1 % in2
--phy
constant FU_OPCODE_EQU   : std_logic_vector(07 downto 0) := X"10";	--in1 == in2
constant FU_OPCODE_NEQ   : std_logic_vector(07 downto 0) := X"11";	--in1 != in2
constant FU_OPCODE_GEQ   : std_logic_vector(07 downto 0) := X"12";	--in1 >= in2
constant FU_OPCODE_GRE   : std_logic_vector(07 downto 0) := X"13";	--in1 >  in2
constant FU_OPCODE_SEL   : std_logic_vector(07 downto 0) := X"14";	--if(sel) in1 else in2

end overlay_pkg;

--package body overlay_pkg is
--
--end overlay_pkg;
