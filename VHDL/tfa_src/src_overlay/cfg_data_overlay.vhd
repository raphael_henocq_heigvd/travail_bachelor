------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : cfg_data_overlay.vhd
-- Author               : Convers Anthony
-- Date                 : 16.02.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity cfg_data_overlay is
    Port ( clk : in  STD_LOGIC;
           rst_config_i : in  STD_LOGIC;
           rst_overlay_i : in  STD_LOGIC;
           clk_ram : in  STD_LOGIC;
           ram_we_i : in  STD_LOGIC_VECTOR (0 downto 0);
           ram_addr_i : in  STD_LOGIC_VECTOR (7 downto 0);
           ram_din_i : in  STD_LOGIC_VECTOR (31 downto 0);
           ram_dout_o : out  STD_LOGIC_VECTOR (31 downto 0);
           dat_prv_i : in  STD_LOGIC_VECTOR (size_bus+bit_cmd_mux_io-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size_bus+bit_cmd_mux_io-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end cfg_data_overlay;

architecture cfg_data_overlay_arch of cfg_data_overlay is

-- signal intern
--cfg mux
signal cfg_x_mux_s : cfg_mux_ovly_type;

--ram_32b
signal ram_web_s : std_logic_vector(0 downto 0);
signal ram_addrb_s : std_logic_vector(7 downto 0);
signal ram_dinb_s : std_logic_vector(31 downto 0);
signal ram_doutb_s : std_logic_vector(31 downto 0);

begin

-------------------- configuration interfaces
U01: entity work.ram_32b port map (
          clka => clk_ram,
          --rsta => reset,
          wea => ram_we_i,
          addra => ram_addr_i,
          dina => ram_din_i,
          douta => ram_dout_o,
          clkb => clk,
          --rstb => reset,
          web => ram_web_s,
          addrb => ram_addrb_s,
          dinb => ram_dinb_s,
          doutb => ram_doutb_s);

U02: entity work.config_overlay port map (clk,rst_config_i,cfg_x_mux_s,ram_web_s,ram_addrb_s,ram_dinb_s,ram_doutb_s);

-------------------- overlay with data in and out interfaces
U03: entity work.data_in_out_overlay port map (clk,rst_overlay_i,cfg_x_mux_s,dat_prv_i,val_prv_i,acc_prv_o,dat_nxt_o,val_nxt_o,acc_nxt_i);

end cfg_data_overlay_arch;

