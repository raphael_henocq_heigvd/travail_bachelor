------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : compteur.vhd
-- Author               : Convers Anthony
-- Date                 : 18.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity compteur is
    generic (size: integer:= 8);
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           enable_i : in  STD_LOGIC;
           load_i : in  STD_LOGIC;
           D_load_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           Q : out  STD_LOGIC_VECTOR (size-1 downto 0));
end compteur;

architecture compteur_arch of compteur is

signal cpt_s : std_logic_vector(size-1 downto 0);

begin

process (clk,reset)
begin
	if reset='1' then 
		cpt_s <= (others =>'0');
	elsif (clk'event and clk = '1') then 
		if load_i='1' then
			cpt_s <= D_load_i;
		elsif enable_i='1' then
			cpt_s <= cpt_s + 1;
		end if;
	end if;
end process;

Q <= cpt_s;

end compteur_arch;

