------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : d_eb.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity d_eb is
    generic (size: integer:= 8);
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           dat_prv_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end d_eb;

architecture d_eb_arch of d_eb is

--signal data
signal dat_01_s : std_logic_vector(size-1 downto 0);
signal dat_02_s : std_logic_vector(size-1 downto 0);
signal dat_nxt_s : std_logic_vector(size-1 downto 0);

--signal valid
signal val_01_s : std_logic;
signal val_02_s : std_logic;
signal val_nxt_s : std_logic;

--signal accept
signal acc_00_s : std_logic;
signal acc_01_s : std_logic;

begin

--data part
D01: entity work.d_flipflop generic map(size) port map (clk,reset,acc_01_s,dat_prv_i,dat_01_s);
D02: entity work.d_flipflop generic map(size) port map (clk,reset,acc_01_s,dat_01_s,dat_02_s);
MD01: entity work.mux2to1_bus generic map(size) port map (dat_01_s,dat_02_s,acc_01_s,dat_nxt_s);

--valid part
V01: entity work.d_flipflop_bit port map (clk,reset,acc_01_s,val_prv_i,val_01_s);
V02: entity work.d_flipflop_bit port map (clk,reset,acc_01_s,val_01_s,val_02_s);
MV01: entity work.mux2to1_bit port map (val_01_s,val_02_s,acc_01_s,val_nxt_s);

--accept part
acc_00_s <= (not val_nxt_s) or acc_nxt_i;
A01: entity work.d_flipflop_bit port map (clk,reset,'1',acc_00_s,acc_01_s);

--------------------
--signal output
dat_nxt_o <= dat_nxt_s;
val_nxt_o <= val_nxt_s;
acc_prv_o <= acc_01_s;

end d_eb_arch;

