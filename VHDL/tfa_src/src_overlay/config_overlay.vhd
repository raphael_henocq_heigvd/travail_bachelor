------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : config_overlay.vhd
-- Author               : Convers Anthony
-- Date                 : 09.02.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity config_overlay is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           cfg_x_mux_o : out  cfg_mux_ovly_type;
           ram_we_o : out  STD_LOGIC_VECTOR (0 downto 0);
           ram_addr_o : out  STD_LOGIC_VECTOR (7 downto 0);
           ram_din_o : out  STD_LOGIC_VECTOR (31 downto 0);
           ram_dout_i : in  STD_LOGIC_VECTOR (31 downto 0));
end config_overlay;

architecture config_overlay_arch of config_overlay is

--Signal intern

signal cfg_x_mux_s : cfg_mux_ovly_type;
signal cfg_sel : integer range nbr_all_cell-1 downto 0;
signal cfg_start : std_logic;

--ram
signal ram_we_s : std_logic_vector(0 downto 0);
signal ram_addr_s : std_logic_vector(7 downto 0);
signal ram_din_s : std_logic_vector(31 downto 0);
signal ram_addr_reg : std_logic_vector(7 downto 0);

--compteur
signal en_cpt_1 : std_logic;
signal load_cpt_1 : std_logic;
signal D_load_cpt_1 : std_logic_vector(ram_addr_s'length-1 downto 0);
signal Q_cpt_1 : std_logic_vector(ram_addr_s'length-1 downto 0);

signal ram_addr_fsm : std_logic_vector(7 downto 0);
signal ram_addr_cpt : std_logic_vector(7 downto 0);
signal enable_from_cpt : std_logic;

--FSM
type FSM_state_type is (s0_idle, s1_read_offset, s2_load_offset, s3_read_config, s4_end_config, s5_wait_clean); 
signal current_state : FSM_state_type;
signal next_state : FSM_state_type;

signal op_prog : std_logic;
signal cpt_max : std_logic;
signal offset_cfg : std_logic_vector(ram_addr_s'length-1 downto 0);
signal length_cfg : std_logic_vector(ram_addr_s'length-1 downto 0);

begin

------------------------------------------------------------
-- output signal

cfg_x_mux_o <= cfg_x_mux_s;

ram_we_o <= ram_we_s;
ram_addr_o <= ram_addr_s;
ram_din_o <= ram_din_s;

------------------------------------------------------------
-- compteur
C01: entity work.compteur generic map(ram_addr_s'length) port map (
		 clk => clk,
		 reset => reset,
		 enable_i => en_cpt_1,
		 load_i => load_cpt_1,
		 D_load_i => D_load_cpt_1,
		 Q => Q_cpt_1);

D_load_cpt_1 <= ram_dout_i(ram_addr_s'length-1 downto 0);
en_cpt_1 <= enable_from_cpt;
ram_addr_cpt <= Q_cpt_1;
cpt_max <= '1' when Q_cpt_1=(length_cfg+offset_cfg-1) else '0';

ram_addr_s <= ram_addr_cpt when enable_from_cpt='1' else ram_addr_fsm;

------------------------------------------------------------
-- FSM
------------------------------------------------------------
FSM_SYNC_PROC : process (clk,reset)
begin
	if reset='1' then 
		current_state <= s0_idle;
	elsif (clk'event and clk = '1') then 
		current_state <= next_state;
	end if;
end process;


FSM_NEXT_STATE_DECODE : process (current_state,op_prog,cpt_max)
begin
	next_state <= current_state;
	
	case current_state is
		when s0_idle =>
			if op_prog='1' then
				next_state <= s1_read_offset;
			end if;
		
		when s1_read_offset =>
			next_state <= s2_load_offset;
		
		when s2_load_offset =>
			next_state <= s3_read_config;
		
		when s3_read_config =>
			if cpt_max='1' then
				next_state <= s4_end_config;
			end if;
		
		when s4_end_config =>
			next_state <= s5_wait_clean;
		
		when s5_wait_clean =>
			if op_prog='0' then
				next_state <= s0_idle;
			end if;
		
		when others =>  --idle
			next_state <= s0_idle;
	
	end case;

end process;


FSM_OUTPUT_DECODE : process (current_state)
begin
	case current_state is
		when s0_idle =>
			--read cmd operation (addr = x"00")
			ram_we_s <= "0";
			ram_addr_fsm <= x"00";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
		
		when s1_read_offset =>
			--read offset addr (addr = x"01")
			ram_we_s <= "0";
			ram_addr_fsm <= x"01";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
		
		when s2_load_offset =>
			--load offset addr to compteur
			ram_we_s <= "0";
			ram_addr_fsm <= x"01";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '1';
		
		when s3_read_config =>
			--reading the config (enable compteur)
			ram_we_s <= "0";
			ram_addr_fsm <= x"01";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '1';
			load_cpt_1 <= '0';
		
		when s4_end_config =>
			-- write status (end configuration)
			ram_we_s <= "1";
			ram_addr_fsm <= x"02";
			ram_din_s <= x"00000002";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
			
		when s5_wait_clean =>
			-- wait clean cmd operation
			ram_we_s <= "0";
			ram_addr_fsm <= x"00";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
		
		when others =>  --idle
			ram_we_s <= "0";
			ram_addr_fsm <= x"00";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';

	end case;
end process;


FSM_OUTPUT_SYNC : process (clk,reset)
begin
	if reset='1' then 
		op_prog <= '0';
		offset_cfg <= (others => '0');
		length_cfg <= (others => '0');
		ram_addr_reg <= (others => '0');
		
		cfg_x_mux_s <= (others => x"00000000");
		cfg_sel <= 0;
		cfg_start <='0';

	elsif (clk'event and clk = '1') then 
		ram_addr_reg <= ram_addr_s;
		
		--detect program operation
		if (current_state=s0_idle or current_state=s5_wait_clean) and (ram_addr_reg=x"00") then
			op_prog <= ram_dout_i(0);
		else
			op_prog <= op_prog;
		end if;
		
		--load offset addr
		if current_state=s2_load_offset then
			offset_cfg <= ram_dout_i(ram_addr_s'length-1 downto 0);
			length_cfg <= ram_dout_i(ram_addr_s'length-1+16 downto 16);
		else
			offset_cfg <= offset_cfg;
			length_cfg <= length_cfg;
		end if;
		
		--write config on the output signal (demux)
		if current_state=s2_load_offset then
			cfg_sel <= 0;
			cfg_start <='0';
		elsif (current_state=s3_read_config or current_state=s4_end_config) then
			if cfg_start='1' then
				cfg_x_mux_s(cfg_sel) <= ram_dout_i;
				
				if cfg_sel = nbr_all_cell-1 then
					cfg_sel <= 0;
				else
					cfg_sel <= cfg_sel + 1;
				end if;
			else
				cfg_sel <= cfg_sel;
			end if;
			
			--wait first read
			cfg_start <= '1';
		else
			cfg_sel <= cfg_sel;
			cfg_start <= cfg_start;
		end if;
	
	end if;
end process;


end config_overlay_arch;

