------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : overlay_3x3.vhd
-- Author               : Convers Anthony
-- Date                 : 19.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_3x3_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_3x3_pkg.all;

entity overlay_3x3 is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
			  cfg_x_mux_i : in  cfg_mux_ovly_type;
			  --North
           Nx_dat_prv_i : in  dat_ovly_type;
           Nx_val_prv_i : in  bit_ovly_type;
           Nx_acc_prv_o : out  bit_ovly_type;
           Nx_dat_nxt_o : out  dat_ovly_type;
           Nx_val_nxt_o : out  bit_ovly_type;
           Nx_acc_nxt_i : in  bit_ovly_type;
			  --West
           Wx_dat_prv_i : in  dat_ovly_type;
           Wx_val_prv_i : in  bit_ovly_type;
           Wx_acc_prv_o : out  bit_ovly_type;
           Wx_dat_nxt_o : out  dat_ovly_type;
           Wx_val_nxt_o : out  bit_ovly_type;
           Wx_acc_nxt_i : in  bit_ovly_type;
			  --East
           Ex_dat_prv_i : in  dat_ovly_type;
           Ex_val_prv_i : in  bit_ovly_type;
           Ex_acc_prv_o : out  bit_ovly_type;
           Ex_dat_nxt_o : out  dat_ovly_type;
           Ex_val_nxt_o : out  bit_ovly_type;
           Ex_acc_nxt_i : in  bit_ovly_type;
			  --South
           Sx_dat_prv_i : in  dat_ovly_type;
           Sx_val_prv_i : in  bit_ovly_type;
           Sx_acc_prv_o : out  bit_ovly_type;
           Sx_dat_nxt_o : out  dat_ovly_type;
           Sx_val_nxt_o : out  bit_ovly_type;
           Sx_acc_nxt_i : in  bit_ovly_type);
end overlay_3x3;

architecture overlay_3x3_arch of overlay_3x3 is

--Configuration Signal
constant size : integer := 3;

--Signal intern East
signal E00_dat_prv_s : std_logic_vector(size-1 downto 0);
signal E00_val_prv_s : std_logic;
signal E00_acc_prv_s : std_logic;
signal E00_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal E00_val_nxt_s : std_logic;
signal E00_acc_nxt_s : std_logic;

signal E01_dat_prv_s : std_logic_vector(size-1 downto 0);
signal E01_val_prv_s : std_logic;
signal E01_acc_prv_s : std_logic;
signal E01_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal E01_val_nxt_s : std_logic;
signal E01_acc_nxt_s : std_logic;

signal E10_dat_prv_s : std_logic_vector(size-1 downto 0);
signal E10_val_prv_s : std_logic;
signal E10_acc_prv_s : std_logic;
signal E10_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal E10_val_nxt_s : std_logic;
signal E10_acc_nxt_s : std_logic;

signal E11_dat_prv_s : std_logic_vector(size-1 downto 0);
signal E11_val_prv_s : std_logic;
signal E11_acc_prv_s : std_logic;
signal E11_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal E11_val_nxt_s : std_logic;
signal E11_acc_nxt_s : std_logic;

signal E20_dat_prv_s : std_logic_vector(size-1 downto 0);
signal E20_val_prv_s : std_logic;
signal E20_acc_prv_s : std_logic;
signal E20_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal E20_val_nxt_s : std_logic;
signal E20_acc_nxt_s : std_logic;

signal E21_dat_prv_s : std_logic_vector(size-1 downto 0);
signal E21_val_prv_s : std_logic;
signal E21_acc_prv_s : std_logic;
signal E21_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal E21_val_nxt_s : std_logic;
signal E21_acc_nxt_s : std_logic;

--Signal intern South
signal S00_dat_prv_s : std_logic_vector(size-1 downto 0);
signal S00_val_prv_s : std_logic;
signal S00_acc_prv_s : std_logic;
signal S00_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal S00_val_nxt_s : std_logic;
signal S00_acc_nxt_s : std_logic;

signal S01_dat_prv_s : std_logic_vector(size-1 downto 0);
signal S01_val_prv_s : std_logic;
signal S01_acc_prv_s : std_logic;
signal S01_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal S01_val_nxt_s : std_logic;
signal S01_acc_nxt_s : std_logic;

signal S02_dat_prv_s : std_logic_vector(size-1 downto 0);
signal S02_val_prv_s : std_logic;
signal S02_acc_prv_s : std_logic;
signal S02_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal S02_val_nxt_s : std_logic;
signal S02_acc_nxt_s : std_logic;

signal S10_dat_prv_s : std_logic_vector(size-1 downto 0);
signal S10_val_prv_s : std_logic;
signal S10_acc_prv_s : std_logic;
signal S10_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal S10_val_nxt_s : std_logic;
signal S10_acc_nxt_s : std_logic;

signal S11_dat_prv_s : std_logic_vector(size-1 downto 0);
signal S11_val_prv_s : std_logic;
signal S11_acc_prv_s : std_logic;
signal S11_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal S11_val_nxt_s : std_logic;
signal S11_acc_nxt_s : std_logic;

signal S12_dat_prv_s : std_logic_vector(size-1 downto 0);
signal S12_val_prv_s : std_logic;
signal S12_acc_prv_s : std_logic;
signal S12_dat_nxt_s : std_logic_vector(size-1 downto 0);
signal S12_val_nxt_s : std_logic;
signal S12_acc_nxt_s : std_logic;



begin


----------------------------------------------------------------------------------------------------
-- Cell of line 0
-------------------
C_00: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(0),
                  Nx_dat_prv_i(0),Nx_val_prv_i(0),Nx_acc_prv_o(0),Nx_dat_nxt_o(0),Nx_val_nxt_o(0),Nx_acc_nxt_i(0),
                  Wx_dat_prv_i(2),Wx_val_prv_i(2),Wx_acc_prv_o(2),Wx_dat_nxt_o(2),Wx_val_nxt_o(2),Wx_acc_nxt_i(2),
                  E00_dat_prv_s,E00_val_prv_s,E00_acc_prv_s,E00_dat_nxt_s,E00_val_nxt_s,E00_acc_nxt_s,
                  S00_dat_prv_s,S00_val_prv_s,S00_acc_prv_s,S00_dat_nxt_s,S00_val_nxt_s,S00_acc_nxt_s);

C_01: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(1),
                  Nx_dat_prv_i(1),Nx_val_prv_i(1),Nx_acc_prv_o(1),Nx_dat_nxt_o(1),Nx_val_nxt_o(1),Nx_acc_nxt_i(1),
                  E00_dat_nxt_s,E00_val_nxt_s,E00_acc_nxt_s,E00_dat_prv_s,E00_val_prv_s,E00_acc_prv_s,
                  E01_dat_prv_s,E01_val_prv_s,E01_acc_prv_s,E01_dat_nxt_s,E01_val_nxt_s,E01_acc_nxt_s,
                  S01_dat_prv_s,S01_val_prv_s,S01_acc_prv_s,S01_dat_nxt_s,S01_val_nxt_s,S01_acc_nxt_s);

C_02: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(2),
                  Nx_dat_prv_i(2),Nx_val_prv_i(2),Nx_acc_prv_o(2),Nx_dat_nxt_o(2),Nx_val_nxt_o(2),Nx_acc_nxt_i(2),
                  E01_dat_nxt_s,E01_val_nxt_s,E01_acc_nxt_s,E01_dat_prv_s,E01_val_prv_s,E01_acc_prv_s,
                  Ex_dat_prv_i(0),Ex_val_prv_i(0),Ex_acc_prv_o(0),Ex_dat_nxt_o(0),Ex_val_nxt_o(0),Ex_acc_nxt_i(0),
                  S02_dat_prv_s,S02_val_prv_s,S02_acc_prv_s,S02_dat_nxt_s,S02_val_nxt_s,S02_acc_nxt_s);

----------------------------------------------------------------------------------------------------
-- Cell of line 1
-------------------
C_10: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(3),
                  S00_dat_nxt_s,S00_val_nxt_s,S00_acc_nxt_s,S00_dat_prv_s,S00_val_prv_s,S00_acc_prv_s,
                  Wx_dat_prv_i(1),Wx_val_prv_i(1),Wx_acc_prv_o(1),Wx_dat_nxt_o(1),Wx_val_nxt_o(1),Wx_acc_nxt_i(1),
                  E10_dat_prv_s,E10_val_prv_s,E10_acc_prv_s,E10_dat_nxt_s,E10_val_nxt_s,E10_acc_nxt_s,
                  S10_dat_prv_s,S10_val_prv_s,S10_acc_prv_s,S10_dat_nxt_s,S10_val_nxt_s,S10_acc_nxt_s);

C_11: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(4),
                  S01_dat_nxt_s,S01_val_nxt_s,S01_acc_nxt_s,S01_dat_prv_s,S01_val_prv_s,S01_acc_prv_s,
                  E10_dat_nxt_s,E10_val_nxt_s,E10_acc_nxt_s,E10_dat_prv_s,E10_val_prv_s,E10_acc_prv_s,
                  E11_dat_prv_s,E11_val_prv_s,E11_acc_prv_s,E11_dat_nxt_s,E11_val_nxt_s,E11_acc_nxt_s,
                  S11_dat_prv_s,S11_val_prv_s,S11_acc_prv_s,S11_dat_nxt_s,S11_val_nxt_s,S11_acc_nxt_s);

C_12: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(5),
                  S02_dat_nxt_s,S02_val_nxt_s,S02_acc_nxt_s,S02_dat_prv_s,S02_val_prv_s,S02_acc_prv_s,
                  E11_dat_nxt_s,E11_val_nxt_s,E11_acc_nxt_s,E11_dat_prv_s,E11_val_prv_s,E11_acc_prv_s,
                  Ex_dat_prv_i(1),Ex_val_prv_i(1),Ex_acc_prv_o(1),Ex_dat_nxt_o(1),Ex_val_nxt_o(1),Ex_acc_nxt_i(1),
                  S12_dat_prv_s,S12_val_prv_s,S12_acc_prv_s,S12_dat_nxt_s,S12_val_nxt_s,S12_acc_nxt_s);

----------------------------------------------------------------------------------------------------
-- Cell of line 2
-------------------
C_20: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(6),
                  S10_dat_nxt_s,S10_val_nxt_s,S10_acc_nxt_s,S10_dat_prv_s,S10_val_prv_s,S10_acc_prv_s,
                  Wx_dat_prv_i(0),Wx_val_prv_i(0),Wx_acc_prv_o(0),Wx_dat_nxt_o(0),Wx_val_nxt_o(0),Wx_acc_nxt_i(0),
                  E20_dat_prv_s,E20_val_prv_s,E20_acc_prv_s,E20_dat_nxt_s,E20_val_nxt_s,E20_acc_nxt_s,
                  Sx_dat_prv_i(2),Sx_val_prv_i(2),Sx_acc_prv_o(2),Sx_dat_nxt_o(2),Sx_val_nxt_o(2),Sx_acc_nxt_i(2));

C_21: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(7),
                  S11_dat_nxt_s,S11_val_nxt_s,S11_acc_nxt_s,S11_dat_prv_s,S11_val_prv_s,S11_acc_prv_s,
                  E20_dat_nxt_s,E20_val_nxt_s,E20_acc_nxt_s,E20_dat_prv_s,E20_val_prv_s,E20_acc_prv_s,
                  E21_dat_prv_s,E21_val_prv_s,E21_acc_prv_s,E21_dat_nxt_s,E21_val_nxt_s,E21_acc_nxt_s,
                  Sx_dat_prv_i(1),Sx_val_prv_i(1),Sx_acc_prv_o(1),Sx_dat_nxt_o(1),Sx_val_nxt_o(1),Sx_acc_nxt_i(1));

C_22: entity work.overlay_cell generic map(size) 
        port map (clk,reset,cfg_x_mux_i(8),
                  S12_dat_nxt_s,S12_val_nxt_s,S12_acc_nxt_s,S12_dat_prv_s,S12_val_prv_s,S12_acc_prv_s,
                  E21_dat_nxt_s,E21_val_nxt_s,E21_acc_nxt_s,E21_dat_prv_s,E21_val_prv_s,E21_acc_prv_s,
                  Ex_dat_prv_i(2),Ex_val_prv_i(2),Ex_acc_prv_o(2),Ex_dat_nxt_o(2),Ex_val_nxt_o(2),Ex_acc_nxt_i(2),
                  Sx_dat_prv_i(0),Sx_val_prv_i(0),Sx_acc_prv_o(0),Sx_dat_nxt_o(0),Sx_val_nxt_o(0),Sx_acc_nxt_i(0));


end overlay_3x3_arch;

