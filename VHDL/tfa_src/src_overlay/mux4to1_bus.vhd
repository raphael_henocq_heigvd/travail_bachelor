------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : mux4to1_bus.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux4to1_bus is
    generic (size: integer:= 8);
    Port ( a : in  STD_LOGIC_VECTOR (size-1 downto 0);
           b : in  STD_LOGIC_VECTOR (size-1 downto 0);
			  c : in  STD_LOGIC_VECTOR (size-1 downto 0);
			  d : in  STD_LOGIC_VECTOR (size-1 downto 0);
           sel : in  STD_LOGIC_VECTOR (1 downto 0);
           y : out  STD_LOGIC_VECTOR (size-1 downto 0));
end mux4to1_bus;

architecture mux4to1_bus_arch of mux4to1_bus is

begin

y <= a when (sel="00") else
     b when (sel="01") else
     c when (sel="10") else
     d;

end mux4to1_bus_arch;

