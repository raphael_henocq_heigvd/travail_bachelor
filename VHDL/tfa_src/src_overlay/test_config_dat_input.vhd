------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : test_config_dat_input.vhd
-- Author               : Convers Anthony
-- Date                 : 12.09.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity test_config_dat_input is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           cfg_available_i : in  STD_LOGIC;
           cfg_length_i : in  STD_LOGIC_VECTOR (7 downto 0);
           ram_we_i : in  STD_LOGIC_VECTOR (0 downto 0);
           ram_addr_i : in  STD_LOGIC_VECTOR (7 downto 0);
           ram_din_i : in  STD_LOGIC_VECTOR (31 downto 0);
           ram_dout_o : out  STD_LOGIC_VECTOR (31 downto 0);
           dat_prv_i : in  STD_LOGIC_VECTOR (size_bus-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size_bus+bit_cmd_mux_io-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end test_config_dat_input;

architecture test_config_dat_input_arch of test_config_dat_input is

--Signal intern
signal dat_01_s : std_logic_vector(size_bus-1 downto 0);
signal val_01_s : std_logic;
signal acc_01_s : std_logic;

signal dat_02_s : std_logic_vector(size_bus+bit_cmd_mux_io-1 downto 0);
signal val_02_s : std_logic;
signal acc_02_s : std_logic;

--ram
signal ram_web_s : std_logic_vector(0 downto 0);
signal ram_addrb_s : std_logic_vector(7 downto 0);
signal ram_dinb_s : std_logic_vector(31 downto 0);
signal ram_doutb_s : std_logic_vector(31 downto 0);


begin


U01: entity work.ram_32b port map (
          clka => clk,
          --rsta => reset,
          wea => ram_we_i,
          addra => ram_addr_i,
          dina => ram_din_i,
          douta => ram_dout_o,
          clkb => clk,
          --rstb => reset,
          web => ram_web_s,
          addrb => ram_addrb_s,
          dinb => ram_dinb_s,
          doutb => ram_doutb_s);

U02: entity work.d_fifo generic map(size_bus) port map (clk,reset,dat_prv_i,val_prv_i,acc_prv_o,dat_01_s,val_01_s,acc_01_s);


U03: entity work.config_dat_input port map (
          clk => clk,
          reset => reset,
          cfg_available_i => cfg_available_i,
          cfg_length_i => cfg_length_i,
          ram_we_o => ram_web_s,
          ram_addr_o => ram_addrb_s,
          ram_din_o => ram_dinb_s,
          ram_dout_i => ram_doutb_s,
          dat_prv_i => dat_01_s,
          val_prv_i => val_01_s,
          acc_prv_o => acc_01_s,
          dat_nxt_o => dat_02_s,
          val_nxt_o => val_02_s,
          acc_nxt_i => acc_02_s);

U04: entity work.d_fifo_40 generic map(size_bus+bit_cmd_mux_io) port map (clk,reset,dat_02_s,val_02_s,acc_02_s,dat_nxt_o,val_nxt_o,acc_nxt_i);


end test_config_dat_input_arch;

