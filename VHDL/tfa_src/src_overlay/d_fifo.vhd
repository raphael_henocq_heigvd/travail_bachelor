------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : d_fifo.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity d_fifo is
    generic (size: integer:= 2);
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           dat_prv_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end d_fifo;

architecture d_fifo_arch of d_fifo is

-- signal fifo
signal wr_en_s : std_logic;
signal rd_en_s : std_logic;
signal full_s : std_logic;
signal empty_s : std_logic;

-- signal intern
signal val_nxt_s : std_logic;
signal acc_prv_s : std_logic;

begin

D01: entity work.fifo_data
  port map (
    clk => clk,
    rst => reset,
    din => dat_prv_i,
    wr_en => wr_en_s,
    rd_en => rd_en_s,
    dout => dat_nxt_o,
    full => full_s,
    empty => empty_s
  );

val_nxt_s <= not empty_s;
acc_prv_s <= not full_s;

wr_en_s <= val_prv_i and acc_prv_s;
rd_en_s <= val_nxt_s and acc_nxt_i;


--------------------
--signal output
val_nxt_o <= val_nxt_s;
acc_prv_o <= acc_prv_s;

end d_fifo_arch;
