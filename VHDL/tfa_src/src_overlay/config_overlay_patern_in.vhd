------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : config_overlay_patern_in.vhd
-- Author               : Convers Anthony
-- Date                 : 15.09.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity config_overlay_patern_in is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           cfg_x_mux_o : out  cfg_mux_ovly_type;
           ram_we_o : out  STD_LOGIC_VECTOR (0 downto 0);
           ram_addr_o : out  STD_LOGIC_VECTOR (10 downto 0);
           ram_din_o : out  STD_LOGIC_VECTOR (31 downto 0);
           ram_dout_i : in  STD_LOGIC_VECTOR (31 downto 0);
           patern_available_o : out  STD_LOGIC;
           patern_length_o : out  STD_LOGIC_VECTOR (7 downto 0);
           ram_patern_we_o : out  STD_LOGIC_VECTOR (0 downto 0);
           ram_patern_addr_o : out  STD_LOGIC_VECTOR (7 downto 0);
           ram_patern_din_o : out  STD_LOGIC_VECTOR (31 downto 0);
           ram_patern_dout_i : in  STD_LOGIC_VECTOR (31 downto 0));
end config_overlay_patern_in;

architecture config_overlay_patern_in_arch of config_overlay_patern_in is

--Signal intern

signal cfg_x_mux_s : cfg_mux_ovly_type;
signal cfg_sel : integer range nbr_all_cell-1 downto 0;
signal cfg_start : std_logic;

--ram
signal ram_we_s : std_logic_vector(0 downto 0);
signal ram_addr_s : std_logic_vector(10 downto 0);
signal ram_din_s : std_logic_vector(31 downto 0);
signal ram_addr_reg : std_logic_vector(10 downto 0);

--ram patern
signal ram_patern_we_s : std_logic_vector(0 downto 0);
signal ram_patern_addr_s : std_logic_vector(7 downto 0);
signal ram_patern_din_s : std_logic_vector(31 downto 0);

--compteur
signal en_cpt_1 : std_logic;
signal load_cpt_1 : std_logic;
signal D_load_cpt_1 : std_logic_vector(ram_addr_s'length-1 downto 0);
signal Q_cpt_1 : std_logic_vector(ram_addr_s'length-1 downto 0);

signal ram_addr_fsm : std_logic_vector(10 downto 0);
signal ram_addr_cpt : std_logic_vector(10 downto 0);
signal enable_from_cpt : std_logic;

--FSM
type FSM_state_type is (s0_idle, s1_read_offset, s2_load_offset, s3_read_config, s4_end_config, s5_read_offset, s6_load_offset, s7_read_config, s8_end_config, s9_wait_clean); 
signal current_state : FSM_state_type;
signal next_state : FSM_state_type;

signal op_prog : std_logic;
signal cpt_max_cfg : std_logic;
signal offset_cfg : std_logic_vector(ram_addr_s'length-1 downto 0);
signal length_cfg : std_logic_vector(ram_addr_s'length-1 downto 0);
signal s3_flag : std_logic;

signal cpt_max_pat : std_logic;
signal offset_pat : std_logic_vector(ram_patern_addr_s'length-1 downto 0);
signal length_pat : std_logic_vector(ram_patern_addr_s'length-1 downto 0);
signal s7_flag : std_logic;

signal cfg_start_pat : std_logic;
signal cpt_pat_s : std_logic_vector(ram_patern_addr_s'length-1 downto 0);

signal patern_available_s : std_logic;
signal patern_length_s : std_logic_vector(7 downto 0);

begin

------------------------------------------------------------
-- output signal

cfg_x_mux_o <= cfg_x_mux_s;

ram_we_o <= ram_we_s;
ram_addr_o <= ram_addr_s;
ram_din_o <= ram_din_s;

patern_available_o <= patern_available_s;
patern_length_o <= patern_length_s;

ram_patern_we_o <= ram_patern_we_s;
ram_patern_addr_o <= ram_patern_addr_s;
ram_patern_din_o <= ram_patern_din_s;

------------------------------------------------------------
-- compteur
C01: entity work.compteur generic map(ram_addr_s'length) port map (
		 clk => clk,
		 reset => reset,
		 enable_i => en_cpt_1,
		 load_i => load_cpt_1,
		 D_load_i => D_load_cpt_1,
		 Q => Q_cpt_1);

D_load_cpt_1 <= ram_dout_i(ram_addr_s'length-1 downto 0);
en_cpt_1 <= enable_from_cpt;
ram_addr_cpt <= Q_cpt_1;

cpt_max_cfg <= '1' when ((Q_cpt_1=(length_cfg+offset_cfg-1))and (s3_flag='1')) else '0';

cpt_max_pat <= '1' when ((Q_cpt_1=(length_pat+offset_pat-1))and (s7_flag='1')) else '0';

ram_addr_s <= ram_addr_cpt when enable_from_cpt='1' else ram_addr_fsm;

------------------------------------------------------------
-- FSM
------------------------------------------------------------
FSM_SYNC_PROC : process (clk,reset)
begin
	if reset='1' then 
		current_state <= s0_idle;
	elsif (clk'event and clk = '1') then 
		current_state <= next_state;
	end if;
end process;


FSM_NEXT_STATE_DECODE : process (current_state,op_prog,cpt_max_cfg,cpt_max_pat)
begin
	next_state <= current_state;
	
	case current_state is
		when s0_idle =>
			if op_prog='1' then
				next_state <= s1_read_offset;
			end if;
		
		when s1_read_offset =>
			next_state <= s2_load_offset;
		
		when s2_load_offset =>
			next_state <= s3_read_config;
		
		when s3_read_config =>
			if cpt_max_cfg='1' then
				next_state <= s4_end_config;
			end if;
		
		when s4_end_config =>
			next_state <= s5_read_offset;
            
        when s5_read_offset =>
			next_state <= s6_load_offset;
		
		when s6_load_offset =>
			next_state <= s7_read_config;
		
		when s7_read_config =>
			if cpt_max_pat='1' then
				next_state <= s8_end_config;
			end if;
		
		when s8_end_config =>
			next_state <= s9_wait_clean;
		
		when s9_wait_clean =>
			if op_prog='0' then
				next_state <= s0_idle;
			end if;
		
		when others =>  --idle
			next_state <= s0_idle;
	
	end case;

end process;


FSM_OUTPUT_DECODE : process (current_state)
begin
	case current_state is
		when s0_idle =>
			--read cmd operation (addr = x"00")
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"00";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '0';
		
		when s1_read_offset =>
			--read offset addr (addr = x"01") ; config overlay
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"01";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '0';
		
		when s2_load_offset =>
			--load offset addr to compteur ; config overlay
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"01";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '1';
            s3_flag <= '0';
            s7_flag <= '0';
		
		when s3_read_config =>
			--reading the config (enable compteur) ; config overlay
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"01";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '1';
			load_cpt_1 <= '0';
            s3_flag <= '1';
            s7_flag <= '0';
		
		when s4_end_config =>
			-- (wait end read last config) ; config overlay
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"03";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '0';
        
        when s5_read_offset =>
			--read offset addr (addr = x"03") ; patern input
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"03";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '0';
		
		when s6_load_offset =>
			--load offset addr to compteur ; patern input
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"03";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '1';
            s3_flag <= '0';
            s7_flag <= '0';
		
		when s7_read_config =>
			--reading the config (enable compteur) ; patern input
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"03";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '1';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '1';
		
		when s8_end_config =>
			-- write status (end configuration) ; patern input
			ram_we_s <= "1";
			ram_addr_fsm <= "000"&x"02";
			ram_din_s <= x"00000002";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '0';
			
		when s9_wait_clean =>
			-- wait clean cmd operation
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"00";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '0';
		
		when others =>  --idle
			ram_we_s <= "0";
			ram_addr_fsm <= "000"&x"00";
			ram_din_s <= x"00000000";
			enable_from_cpt <= '0';
			load_cpt_1 <= '0';
            s3_flag <= '0';
            s7_flag <= '0';

	end case;
end process;


FSM_OUTPUT_SYNC : process (clk,reset)
begin
	if reset='1' then 
		op_prog <= '0';
		offset_cfg <= (others => '0');
		length_cfg <= (others => '0');
        offset_pat <= (others => '0');
		length_pat <= (others => '0');
		ram_addr_reg <= (others => '0');
		
        cpt_pat_s <= (others => '0');
		cfg_x_mux_s <= (others => x"00000000");
		cfg_sel <= 0;
		cfg_start <='0';
        cfg_start_pat <='0';
        
        ram_patern_we_s <= "0";
        ram_patern_addr_s <= (others => '0');
        ram_patern_din_s <= (others => '0');
        
        patern_available_s <= '0';
        patern_length_s <= (others => '0');

	elsif (clk'event and clk = '1') then 
		ram_addr_reg <= ram_addr_s;
		
		--detect program operation
		if (current_state=s0_idle or current_state=s9_wait_clean) and (ram_addr_reg="000"&x"00") then
			op_prog <= ram_dout_i(0);
		else
			op_prog <= op_prog;
		end if;
		
		--load offset addr config
		if current_state=s2_load_offset then
			offset_cfg <= ram_dout_i(ram_addr_s'length-1 downto 0);
			length_cfg <= ram_dout_i(ram_addr_s'length-1+16 downto 16);
		else
			offset_cfg <= offset_cfg;
			length_cfg <= length_cfg;
		end if;
		
		--write config on the output signal (demux)
		if current_state=s2_load_offset then
			cfg_sel <= 0;
			cfg_start <='0';
		elsif (current_state=s3_read_config or current_state=s4_end_config) then
			if cfg_start='1' then
				cfg_x_mux_s(cfg_sel) <= ram_dout_i;
				
				if cfg_sel = nbr_all_cell-1 then
					cfg_sel <= 0;
				else
					cfg_sel <= cfg_sel + 1;
				end if;
			else
				cfg_sel <= cfg_sel;
			end if;
			
			--wait first read
			cfg_start <= '1';
		else
			cfg_sel <= cfg_sel;
			cfg_start <= cfg_start;
		end if;
        
        ---------------------------------------------------------------
        --load offset addr patern
        if current_state=s6_load_offset then
			offset_pat <= ram_dout_i(ram_patern_addr_s'length-1 downto 0);
			length_pat <= ram_dout_i(ram_patern_addr_s'length-1+16 downto 16);
		else
			offset_pat <= offset_pat;
			length_pat <= length_pat;
		end if;
        
        --write patern on the output signal (demux)
		if current_state=s6_load_offset then
			cpt_pat_s <= x"00";
			cfg_start_pat <='0';
            ram_patern_we_s <= "0";
		elsif (current_state=s7_read_config or current_state=s8_end_config) then
			if cfg_start_pat='1' then
                ram_patern_we_s <= "1";
				ram_patern_addr_s <= cpt_pat_s;
				ram_patern_din_s <= ram_dout_i;
				
                cpt_pat_s <= cpt_pat_s + 1;
			else
                ram_patern_we_s <= "0";
				cpt_pat_s <= cpt_pat_s;
			end if;
			
			--wait first read
			cfg_start_pat <= '1';
		else
            ram_patern_we_s <= "0";
			cpt_pat_s <= cpt_pat_s;
			cfg_start_pat <= cfg_start_pat;
		end if;
        
        --control patern available
        if current_state=s8_end_config then
			patern_length_s <= length_pat;
		else
			patern_length_s <= patern_length_s;
		end if;
        
        if current_state=s1_read_offset then
			patern_available_s <= '0';
        elsif current_state=s9_wait_clean then
			patern_available_s <= '1';
		else
			patern_available_s <= patern_available_s;
		end if;
	

	end if;
end process;


end config_overlay_patern_in_arch;

