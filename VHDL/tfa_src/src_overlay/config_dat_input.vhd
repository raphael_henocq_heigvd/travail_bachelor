------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : config_dat_input.vhd
-- Author               : Convers Anthony
-- Date                 : 12.09.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity config_dat_input is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           cfg_available_i : in  STD_LOGIC;
           cfg_length_i : in  STD_LOGIC_VECTOR (7 downto 0);
           ram_we_o : out  STD_LOGIC_VECTOR (0 downto 0);
           ram_addr_o : out  STD_LOGIC_VECTOR (7 downto 0);
           ram_din_o : out  STD_LOGIC_VECTOR (31 downto 0);
           ram_dout_i : in  STD_LOGIC_VECTOR (31 downto 0);
           dat_prv_i : in  STD_LOGIC_VECTOR (size_bus_ext-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size_bus_ext+bit_cmd_mux_io-1 downto 0); --Modif by RHE
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end config_dat_input;

architecture config_dat_input_arch of config_dat_input is

--Signal intern
--data part
signal dat_d_00_s : std_logic_vector(size_bus_ext-1 downto 0);
signal val_d_00_s : std_logic;
signal acc_d_00_s : std_logic;
signal dat_d_01_s : std_logic_vector(size_bus_ext-1 downto 0);
signal val_d_01_s : std_logic;
signal acc_d_01_s : std_logic;

--number input part
signal dat_n_00_s : std_logic_vector(bit_cmd_mux_io-1 downto 0);
signal val_n_00_s : std_logic;
signal acc_n_00_s : std_logic;
signal dat_n_01_s : std_logic_vector(bit_cmd_mux_io-1 downto 0);
signal val_n_01_s : std_logic;
signal acc_n_01_s : std_logic;

--combine part
signal acc_j_01_s : std_logic;
signal dat_j_02_s : std_logic_vector(size_bus_ext + bit_cmd_mux_io-1 downto 0);
signal val_j_02_s : std_logic;
signal acc_j_02_s : std_logic;

--ram
signal ram_we_s : std_logic_vector(0 downto 0);
signal ram_addr_s : std_logic_vector(7 downto 0);
signal ram_din_s : std_logic_vector(31 downto 0);
signal ram_dout_s : std_logic_vector(31 downto 0);

--FSM
type FSM_state_type is (S0_idle, S0toS1, S1, S1_wait, S2, S2_wait, S3, S3_wait, S4_rst); 
signal state : FSM_state_type;

signal end_config_s : std_logic_vector(7 downto 0);
signal dat_ipt_00_s : std_logic_vector(7 downto 0);
signal dat_ipt_00_temp : std_logic_vector(7 downto 0);
signal val_ipt_00_s : std_logic;
signal val_ipt_00_temp : std_logic;
signal en_wait : std_logic;
signal jump_load : std_logic;
signal inf_jump : std_logic;
signal inf_bcle : std_logic;
signal end_jump : std_logic_vector(7 downto 0);
signal cpt_jump : std_logic_vector(7 downto 0);
signal end_bcle : std_logic_vector(7 downto 0);
signal cpt_bcle : std_logic_vector(7 downto 0);
signal en_jump : std_logic;
signal en_bcle : std_logic;
signal en_inf : std_logic;
signal nbr_ins : std_logic_vector(7 downto 0);
signal num_ipt : std_logic_vector(7 downto 0);

signal reg1_cfg_available_s : std_logic;
signal reg2_cfg_available_s : std_logic;
signal reg1_cfg_length_s : std_logic_vector(7 downto 0);
signal reg2_cfg_length_s : std_logic_vector(7 downto 0);

signal rst_intern : std_logic;


begin

------------------------------------------------------------
-- data signal (input)
dat_d_00_s <= dat_prv_i;
val_d_00_s <= val_prv_i;
acc_prv_o <= acc_d_00_s;

-- data signal (output)
dat_nxt_o <= dat_j_02_s;
val_nxt_o <= val_j_02_s;
acc_j_02_s <= acc_nxt_i;

------------------------------------------------------------
-- ram signal

ram_we_o <= ram_we_s;
ram_addr_o <= ram_addr_s;
ram_din_o <= ram_din_s;

ram_dout_s <= ram_dout_i;


------------------------------------------------------------
-- FSM
------------------------------------------------------------
ram_we_s <= "0";
ram_din_s <= (others => '0');

en_jump <= ram_dout_s(29);
en_bcle <= ram_dout_s(28);

en_inf <= ram_dout_s(26);
nbr_ins <= ram_dout_s(23 downto 16);
num_ipt <= ram_dout_s(7 downto 0);

dat_n_00_s <= dat_ipt_00_temp when en_wait='1' else dat_ipt_00_s;
val_n_00_s <= val_ipt_00_temp when en_wait='1' else val_ipt_00_s;

FSM_P01 : process (clk,reset)
begin
	if reset='1' then 
		state <= S0_idle;
        ram_addr_s <= (others => '0');
        end_config_s <= (others => '0');
        dat_ipt_00_s <= (others => '0');
        dat_ipt_00_temp <= (others => '0');
        val_ipt_00_s <= '0';
        val_ipt_00_temp <= '0';
        en_wait <= '0';
        jump_load <= '0';
        inf_jump <= '0';
        end_jump <= (others => '0');
        cpt_jump <= (others => '0');
        inf_bcle <= '0';
        end_bcle <= (others => '0');
        cpt_bcle <= (others => '0');
        rst_intern <= '1';
        reg1_cfg_available_s <= '1';
        reg2_cfg_available_s <= '1';
        reg1_cfg_length_s <= (others => '0');
        reg2_cfg_length_s <= (others => '0');

	elsif (clk'event and clk = '1') then
	   reg1_cfg_available_s <= cfg_available_i;
	   reg2_cfg_available_s <= reg1_cfg_available_s;
	   reg1_cfg_length_s <= cfg_length_i;
       reg2_cfg_length_s <= reg1_cfg_length_s;

        case state is
            when S0_idle =>
                rst_intern <= '0';
                ram_addr_s <= (others => '0');
                dat_ipt_00_s <= (others => '0');
                dat_ipt_00_temp <= (others => '0');
                val_ipt_00_s <= '0';
                val_ipt_00_temp <= '0';
                en_wait <= '0';
                jump_load <= '0';
                inf_jump <= '0';
                end_jump <= (others => '0');
                cpt_jump <= (others => '0');
                inf_bcle <= '0';
                end_bcle <= (others => '0');
                cpt_bcle <= (others => '0');
                if reg2_cfg_available_s='1' then
                    state <= S0toS1;
                    end_config_s <= reg2_cfg_length_s - 1;
                end if;
            
            when S0toS1 =>
                rst_intern <= '0';
                if reg2_cfg_available_s='0' then
                    state <= S4_rst;
                elsif acc_n_00_s='1' then
                    if end_config_s=ram_addr_s then
                        ram_addr_s <= (others => '0');
                    else
                        ram_addr_s <= ram_addr_s + 1;
                    end if;
                    state <= S1;
                end if;
                
            when S1 =>  --normal
                rst_intern <= '0';
                if reg2_cfg_available_s='0' then
                    state <= S4_rst;
                elsif en_jump='1' then
                    if jump_load='1' then
                        if (inf_jump='0') and (cpt_jump >= end_jump) then
                            jump_load <= '0';
                            val_ipt_00_s <= '0';
                            if acc_n_00_s='1' then
                                if end_config_s=ram_addr_s then
                                    ram_addr_s <= (others => '0');
                                else
                                    ram_addr_s <= ram_addr_s + 1;
                                end if;
                            else
                                state <= S1_wait;
                                en_wait <= '1';
                                dat_ipt_00_temp <= dat_ipt_00_s;
                                val_ipt_00_temp <= val_ipt_00_s;
                            end if;
                        else
                            ram_addr_s <= num_ipt;
                            val_ipt_00_s <= '0';
                            if acc_n_00_s='1' then
                                state <= S3;
                            else
                                state <= S3_wait;
                                en_wait <= '1';
                                dat_ipt_00_temp <= dat_ipt_00_s;
                                val_ipt_00_temp <= val_ipt_00_s;
                            end if;
                        end if;
                    else
                        ram_addr_s <= num_ipt;
                        jump_load <= '1';
                        inf_jump <= en_inf;
                        end_jump <= nbr_ins;
                        cpt_jump <= (others => '0');
                        val_ipt_00_s <= '0';
                        if acc_n_00_s='1' then
                            state <= S3;
                        else
                            state <= S3_wait;
                            en_wait <= '1';
                            dat_ipt_00_temp <= dat_ipt_00_s;
                            val_ipt_00_temp <= val_ipt_00_s;
                        end if;
                    end if;
                elsif en_bcle='1' then
                    dat_ipt_00_s <= num_ipt;
                    val_ipt_00_s <= '1';
                    end_bcle <= nbr_ins;
                    inf_bcle <= en_inf;
                    cpt_bcle <= x"01";
                    if acc_n_00_s='1' then
                        state <= S2;
                    else
                        dat_ipt_00_temp <= dat_ipt_00_s;
                        val_ipt_00_temp <= val_ipt_00_s;
                        state <= S2_wait;
                        en_wait <= '1';
                    end if;
                elsif acc_n_00_s='1' then
                    if end_config_s=ram_addr_s then
                        ram_addr_s <= (others => '0');
                    else
                        ram_addr_s <= ram_addr_s + 1;
                    end if;
                    dat_ipt_00_s <= num_ipt;
                    val_ipt_00_s <= '1';
                else
                    dat_ipt_00_s <= num_ipt;
                    val_ipt_00_s <= '1';
                    dat_ipt_00_temp <= dat_ipt_00_s;
                    val_ipt_00_temp <= val_ipt_00_s;
                    state <= S1_wait;
                    en_wait <= '1';
                end if;
            
            when S1_wait =>
                rst_intern <= '0';
                en_wait <= '1';
                if reg2_cfg_available_s='0' then
                    state <= S4_rst;
                elsif acc_n_00_s='1'then
                    en_wait <= '0';
                    state <= S1;
                    if end_config_s=ram_addr_s then
                        ram_addr_s <= (others => '0');
                    else
                        ram_addr_s <= ram_addr_s + 1;
                    end if;
                end if;
            
            when S2 =>  --boucle
                rst_intern <= '0';
                if reg2_cfg_available_s='0' then
                    state <= S4_rst;
                elsif acc_n_00_s='1'then
                    if (inf_bcle='0') and (cpt_bcle >= (end_bcle - 1)) then
                        if end_config_s=ram_addr_s then
                            ram_addr_s <= (others => '0');
                        else
                            ram_addr_s <= ram_addr_s + 1;
                        end if;
                        state <= S1;
                        cpt_bcle <= x"00";
                    else
                        if inf_bcle='0' then
                            cpt_bcle <= cpt_bcle + 1;
                        end if;
                    end if;
                end if;
            
            when S2_wait =>
                rst_intern <= '0';
                en_wait <= '1';
                if reg2_cfg_available_s='0' then
                    state <= S4_rst;
                elsif acc_n_00_s='1'then
                    state <= S2;
                    en_wait <= '0';
                end if;
            
            when S3 =>  --jump
                rst_intern <= '0';
                val_ipt_00_s <= '0';
                if reg2_cfg_available_s='0' then
                    state <= S4_rst;
                elsif acc_n_00_s='1'then
                    if inf_jump='0' then
                        cpt_jump <= cpt_jump + 1;
                    end if;
                    ram_addr_s <= ram_addr_s + 1;
                    state <= S1;
                end if;
            
            when S3_wait =>
                rst_intern <= '0';
                en_wait <= '1';
                if reg2_cfg_available_s='0' then
                    state <= S4_rst;
                elsif acc_n_00_s='1'then
                    state <= S3;
                    en_wait <= '0';
                    val_ipt_00_s <= '0';
                end if;
                
            when S4_rst => --reset before new patern
                rst_intern <= '1';
                ram_addr_s <= (others => '0');
                dat_ipt_00_s <= (others => '0');
                dat_ipt_00_temp <= (others => '0');
                val_ipt_00_s <= '0';
                val_ipt_00_temp <= '0';
                en_wait <= '0';
                jump_load <= '0';
                inf_jump <= '0';
                end_jump <= (others => '0');
                cpt_jump <= (others => '0');
                inf_bcle <= '0';
                end_bcle <= (others => '0');
                cpt_bcle <= (others => '0');
                state <= S0_idle;
            
            when others =>  --idle
                state <= S0_idle;
        end case;
            
	end if;
end process;

------------------------------------------------------------
-- combine number input + data

dat_01: entity work.d_eb generic map(size_bus_ext) port map (clk,rst_intern,dat_d_00_s,val_d_00_s,acc_d_00_s,dat_d_01_s,val_d_01_s,acc_d_01_s);

num_01: entity work.d_eb generic map(bit_cmd_mux_io) port map (clk,rst_intern,dat_n_00_s,val_n_00_s,acc_n_00_s,dat_n_01_s,val_n_01_s,acc_n_01_s);

acc_d_01_s <= acc_j_01_s;
acc_n_01_s <= acc_j_01_s;
comb_01: entity work.join_num_dat port map (clk,rst_intern,dat_d_01_s,val_d_01_s,dat_n_01_s,val_n_01_s,acc_j_01_s,dat_j_02_s,val_j_02_s,acc_j_02_s);

end config_dat_input_arch;

