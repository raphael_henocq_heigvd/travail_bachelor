------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : data_in_fifo128to32.vhd
-- Author               : Convers Anthony
-- Date                 : 20.09.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity data_in_fifo128to32 is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           wr_en_i : in  STD_LOGIC;
           dword_en_i : in  STD_LOGIC_VECTOR (3 downto 0);
           data_i : in  STD_LOGIC_VECTOR (127 downto 0);
           data_count_o : out  STD_LOGIC_VECTOR (10 downto 0);
           dat_nxt_o : out  STD_LOGIC_VECTOR (31 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end data_in_fifo128to32;

architecture data_in_fifo128to32_arch of data_in_fifo128to32 is

-- signal fifo
signal din_fifo : std_logic_vector(131 downto 0);
signal dout_fifo : std_logic_vector(131 downto 0);
signal rd_en_s : std_logic;
signal full_s : std_logic;
signal empty_s : std_logic;

--FSM
type FSM_state_type is (S0_idle, S1, S2, S3, S4, S5); 
signal state : FSM_state_type;

-- signal intern
signal valid_s : std_logic;
--signal acc_prv_s : std_logic;
signal read_128_s : std_logic;

signal data_reg_s : std_logic_vector(127 downto 0);
signal dword_en_reg_s : std_logic_vector(3 downto 0);
signal dat_nxt_s : std_logic_vector(31 downto 0);
signal val_nxt_s : std_logic;


begin

din_fifo(131 downto 128) <= dword_en_i;
din_fifo(127 downto 0) <= data_i;

F01: entity work.fifo_128b
  port map (
    clk => clk,
    rst => reset,
    din => din_fifo,
    wr_en => wr_en_i,
    rd_en => rd_en_s,
    dout => dout_fifo,
    full => full_s,
    empty => empty_s,
    data_count => data_count_o
  );

valid_s <= not empty_s;
--acc_prv_s <= not full_s;

rd_en_s <= read_128_s;

FSM_P01 : process (clk,reset)
begin
	if reset='1' then 
		state <= S0_idle;
        data_reg_s <= (others => '0');
        dword_en_reg_s <= (others => '0');
        dat_nxt_s <= (others => '0');
        val_nxt_s <= '0';
        read_128_s <= '0';

	elsif (clk'event and clk = '1') then 
        case state is
            when S0_idle =>
                dat_nxt_s <= (others => '0');
                val_nxt_s <= '0';
                if valid_s='1' and acc_nxt_i='1' then
                    state <= S1;
                    data_reg_s <= dout_fifo(127 downto 0);
                    dword_en_reg_s <= dout_fifo(131 downto 128);
                    read_128_s <= '1';
                else
                    read_128_s <= '0';
                end if;
                
            when S1 =>
                read_128_s <= '0';
                if acc_nxt_i='1' then
                    if dword_en_reg_s(0)='1' then
                        dat_nxt_s <= data_reg_s(31 downto 0);
                        val_nxt_s <= '1';
                    else
                        val_nxt_s <= '0';
                    end if;
                    state <= S2;
                end if;
                
            when S2 =>
                read_128_s <= '0';
                if acc_nxt_i='1' then
                    if dword_en_reg_s(1)='1' then
                        dat_nxt_s <= data_reg_s(63 downto 32);
                        val_nxt_s <= '1';
                    else
                        val_nxt_s <= '0';
                    end if;
                    state <= S3;
                end if;
                
            when S3 =>
                read_128_s <= '0';
                if acc_nxt_i='1' then
                    if dword_en_reg_s(2)='1' then
                        dat_nxt_s <= data_reg_s(95 downto 64);
                        val_nxt_s <= '1';
                    else
                        val_nxt_s <= '0';
                    end if;
                    state <= S4;
                end if;
                
            when S4 =>
                if acc_nxt_i='1' then
                    if dword_en_reg_s(3)='1' then
                        dat_nxt_s <= data_reg_s(127 downto 96);
                        val_nxt_s <= '1';
                    else
                        val_nxt_s <= '0';
                    end if;
                    if valid_s='1' then
                        data_reg_s <= dout_fifo(127 downto 0);
                        dword_en_reg_s <= dout_fifo(131 downto 128);
                        read_128_s <= '1';
                        state <= S1;
                    else
                        state <= S5;
                        read_128_s <= '0';
                    end if;
                else
                    read_128_s <= '0';
                end if;
            
            when S5 =>
                read_128_s <= '0';
                if acc_nxt_i='1' then
                    state <= S0_idle;
                    dat_nxt_s <= (others => '0');
                    val_nxt_s <= '0';
                end if;
            
            when others =>  --idle
                state <= S0_idle;
        end case;
            
	end if;
end process;


--------------------
--signal output
dat_nxt_o <= dat_nxt_s;
val_nxt_o <= val_nxt_s;

end data_in_fifo128to32_arch;

