------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : overlay_NxM.vhd
-- Author               : Convers Anthony
-- Date                 : 19.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity overlay_NxM is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           cfg_x_mux_i : in  cfg_mux_ovly_type;
           --signal input
           dat_x_prv_i : in  dat_io_in_ovly;
           val_x_prv_i : in  bit_io_ovly;
           acc_x_prv_o : out  bit_io_ovly;
           --signal input
           dat_x_nxt_o : out  dat_io_in_ovly;
           val_x_nxt_o : out  bit_io_ovly;
           acc_x_nxt_i : in  bit_io_ovly;
           data_size_i : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0));
end overlay_NxM;

architecture overlay_NxM_arch of overlay_NxM is

--Signal intern

signal dat_prv_vert_s : dat_nxm_ovly;
signal val_prv_vert_s : bit_nxm_ovly;
signal acc_prv_vert_s : bit_nxm_ovly;
signal dat_nxt_vert_s : dat_nxm_ovly;
signal val_nxt_vert_s : bit_nxm_ovly;
signal acc_nxt_vert_s : bit_nxm_ovly;

signal dat_prv_hori_s : dat_nxm_ovly;
signal val_prv_hori_s : bit_nxm_ovly;
signal acc_prv_hori_s : bit_nxm_ovly;
signal dat_nxt_hori_s : dat_nxm_ovly;
signal val_nxt_hori_s : bit_nxm_ovly;
signal acc_nxt_hori_s : bit_nxm_ovly;

begin

----------------------------------------------------------------------------------------------------
--Overlay cell
-------------------

gen_line : for j in 0 to line_ovly-1 generate
begin
   gen_col : for i in 0 to col_ovly-1 generate
   begin
      Cell: entity work.overlay_cell generic map(size_bus)
           port map (clk,reset,cfg_x_mux_i(j*col_ovly+i),
                     dat_prv_vert_s(j,i),val_prv_vert_s(j,i),acc_prv_vert_s(j,i),dat_nxt_vert_s(j,i),val_nxt_vert_s(j,i),acc_nxt_vert_s(j,i),
                     dat_prv_hori_s(j,i),val_prv_hori_s(j,i),acc_prv_hori_s(j,i),dat_nxt_hori_s(j,i),val_nxt_hori_s(j,i),acc_nxt_hori_s(j,i),
                     dat_nxt_hori_s(j,i+1),val_nxt_hori_s(j,i+1),acc_nxt_hori_s(j,i+1),dat_prv_hori_s(j,i+1),val_prv_hori_s(j,i+1),acc_prv_hori_s(j,i+1),
                     dat_nxt_vert_s(j+1,i),val_nxt_vert_s(j+1,i),acc_nxt_vert_s(j+1,i),dat_prv_vert_s(j+1,i),val_prv_vert_s(j+1,i),acc_prv_vert_s(j+1,i), data_size_i);   
   end generate;
end generate;

----------------------------------------------------------------------------------------------------
--Overlay connection (extern side)
-------------------
--West connection of overlay (extern side)
gen_w : for w in line_ovly-1 downto 0 generate
begin
   --input
	dat_prv_hori_s(w,0) <= dat_x_prv_i(line_ovly-1-w);
	val_prv_hori_s(w,0) <= val_x_prv_i(line_ovly-1-w);
	acc_x_prv_o(line_ovly-1-w) <= acc_prv_hori_s(w,0);

   --output
	dat_x_nxt_o(line_ovly*2+col_ovly-1-w) <= dat_nxt_hori_s(w,0);
	val_x_nxt_o(line_ovly*2+col_ovly-1-w) <= val_nxt_hori_s(w,0);
	acc_nxt_hori_s(w,0) <= acc_x_nxt_i(line_ovly*2+col_ovly-1-w);

end generate;

--North connection of overlay (extern side)
gen_n : for n in 0 to col_ovly-1 generate
begin
   --input
	dat_prv_vert_s(0,n) <= dat_x_prv_i(line_ovly+n);
	val_prv_vert_s(0,n) <= val_x_prv_i(line_ovly+n);
	acc_x_prv_o(line_ovly+n) <= acc_prv_vert_s(0,n);

   --output
	dat_x_nxt_o(line_ovly*2+col_ovly+n) <= dat_nxt_vert_s(0,n);
	val_x_nxt_o(line_ovly*2+col_ovly+n) <= val_nxt_vert_s(0,n);
	acc_nxt_vert_s(0,n) <= acc_x_nxt_i(line_ovly*2+col_ovly+n);

end generate;

--East connection of overlay (extern side)
gen_e : for e in 0 to line_ovly-1 generate
begin
   --input
	dat_nxt_hori_s(e,col_ovly) <= dat_x_prv_i(line_ovly+col_ovly+e);
	val_nxt_hori_s(e,col_ovly) <= val_x_prv_i(line_ovly+col_ovly+e);
	acc_x_prv_o(line_ovly+col_ovly+e) <= acc_nxt_hori_s(e,col_ovly);

   --output
	dat_x_nxt_o(e) <= dat_prv_hori_s(e,col_ovly);
	val_x_nxt_o(e) <= val_prv_hori_s(e,col_ovly);
	acc_prv_hori_s(e,col_ovly) <= acc_x_nxt_i(e);

end generate;

--South connection of overlay (extern side)
gen_s : for s in col_ovly-1 downto 0 generate
begin
   --input
	dat_nxt_vert_s(line_ovly,s) <= dat_x_prv_i(line_ovly*2+col_ovly*2-1-s);
	val_nxt_vert_s(line_ovly,s) <= val_x_prv_i(line_ovly*2+col_ovly*2-1-s);
	acc_x_prv_o(line_ovly*2+col_ovly*2-1-s) <= acc_nxt_vert_s(line_ovly,s);

   --output
	dat_x_nxt_o(line_ovly+col_ovly-1-s) <= dat_prv_vert_s(line_ovly,s);
	val_x_nxt_o(line_ovly+col_ovly-1-s) <= val_prv_vert_s(line_ovly,s);
	acc_prv_vert_s(line_ovly,s) <= acc_x_nxt_i(line_ovly+col_ovly-1-s);

end generate;


end overlay_NxM_arch;
