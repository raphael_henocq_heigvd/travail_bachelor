------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : d_mux.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity d_mux is
    generic (size: integer:= 8);
    Port ( sel_i : in  STD_LOGIC_VECTOR (2 downto 0);
           dat_prv_0_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_0_i : in  STD_LOGIC;
	   dat_prv_1_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_1_i : in  STD_LOGIC;
	   dat_prv_2_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_2_i : in  STD_LOGIC;
	   dat_prv_3_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_3_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end d_mux;

architecture d_mux_arch of d_mux is

--Configuration sel
signal enable_mux_s : std_logic;
signal sel_mux_s : std_logic_vector(1 downto 0);

--signal valid
signal val_temp_s : std_logic;

begin

enable_mux_s <= sel_i(2);
sel_mux_s <= sel_i(1 downto 0);

--data mux
DM01: entity work.mux4to1_bus generic map(size)
  port map (
    a => dat_prv_0_i,
    b => dat_prv_1_i,
    c => dat_prv_2_i,
    d => dat_prv_3_i,
    sel => sel_mux_s,
    y => dat_nxt_o
  );
  
--valid mux
DM02: entity work.mux4to1_bit
  port map (
    a => val_prv_0_i,
    b => val_prv_1_i,
    c => val_prv_2_i,
    d => val_prv_3_i,
    sel => sel_mux_s,
    y => val_temp_s
  );

val_nxt_o <= val_temp_s and enable_mux_s;

--accept
acc_prv_o <=acc_nxt_i;

end d_mux_arch;

