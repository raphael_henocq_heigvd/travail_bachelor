------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : join_num_dat.vhd
-- Author               : Convers Anthony
-- Date                 : 12.09.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity join_num_dat is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           dat_prv_1_i : in  STD_LOGIC_VECTOR (size_bus_ext-1 downto 0);
           val_prv_1_i : in  STD_LOGIC;
           dat_prv_2_i : in  STD_LOGIC_VECTOR (bit_cmd_mux_io-1 downto 0);
           val_prv_2_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size_bus_ext + bit_cmd_mux_io-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end join_num_dat;

architecture join_num_dat_arch of join_num_dat is

--signal data
signal dat_01_s : std_logic_vector(size_bus_ext + bit_cmd_mux_io-1 downto 0);

--signal valid
signal val_01_s : std_logic;

--signal accept
signal acc_nxt_s : std_logic;

begin

--data part
dat_01_s <= dat_prv_2_i & dat_prv_1_i;

D01: entity work.d_flipflop generic map(size_bus_ext + bit_cmd_mux_io) port map (clk,reset,acc_nxt_s,dat_01_s,dat_nxt_o);

--valid part
val_01_s <= val_prv_1_i and val_prv_2_i and acc_nxt_s;

V01: entity work.d_flipflop_bit port map (clk,reset,acc_nxt_s,val_01_s,val_nxt_o);

--accept part
acc_nxt_s <=acc_nxt_i;
acc_prv_o <=val_01_s;


end join_num_dat_arch;

