------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : fu_add.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fu_add is
    generic (size: integer:= 8);
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           dat_prv_1_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_1_i : in  STD_LOGIC;
           dat_prv_2_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_2_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end fu_add;

architecture fu_add_arch of fu_add is

--signal data
signal dat_01_s : std_logic_vector(size-1 downto 0);

--signal valid
signal val_01_s : std_logic;

--signal accept
signal acc_nxt_s : std_logic;

begin

--data part
dat_01_s <= dat_prv_1_i + dat_prv_2_i;

D01: entity work.d_flipflop generic map(size) port map (clk,reset,acc_nxt_s,dat_01_s,dat_nxt_o);

--valid part
val_01_s <= val_prv_1_i and val_prv_2_i and acc_nxt_s;

V01: entity work.d_flipflop_bit port map (clk,reset,acc_nxt_s,val_01_s,val_nxt_o);

--accept part
acc_nxt_s <=acc_nxt_i;
acc_prv_o <=val_01_s;


end fu_add_arch;

