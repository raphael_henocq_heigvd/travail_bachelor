------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : data_in_out_overlay.vhd
-- Author               : Convers Anthony
-- Date                 : 27.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity data_in_out_overlay is
    Port ( clk_usr     : in  STD_LOGIC;
           clk_ovly    : in  STD_LOGIC;
           reset       : in  STD_LOGIC;
           cfg_x_mux_i : in  cfg_mux_ovly_type;
           dat_prv_i   : in  STD_LOGIC_VECTOR (size_bus_ext+bit_cmd_mux_io-1 downto 0);
           val_prv_i   : in  STD_LOGIC;
           acc_prv_o   : out  STD_LOGIC;
           dat_nxt_o   : out  STD_LOGIC_VECTOR (size_bus_ext+bit_cmd_mux_io-1 downto 0);
           val_nxt_o   : out  STD_LOGIC;
           acc_nxt_i   : in  STD_LOGIC;
           data_size_i : in std_logic_vector (data_size_length -1 downto 0)
           );
end data_in_out_overlay;

architecture data_in_out_overlay_arch of data_in_out_overlay is

-- signal intern
signal dat_x_01_s     : dat_io_ovly;
signal val_x_01_s     : bit_io_ovly;
signal acc_x_01_s     : bit_io_ovly;
signal dat_x_01_ser_s : dat_io_in_ovly;
signal val_x_01_ser_s : bit_io_ovly;
signal acc_x_01_ser_s : bit_io_ovly;

signal dat_x_02_s     : dat_io_ovly;
signal val_x_02_s     : bit_io_ovly;
signal acc_x_02_s     : bit_io_ovly;
signal dat_x_02_ser_s : dat_io_in_ovly;
signal val_x_02_ser_s : bit_io_ovly;
signal acc_x_02_ser_s : bit_io_ovly;


begin

-------------------- demux data input
U01: entity work.demux_data_in port map (clk_usr,clk_ovly,reset,dat_prv_i,val_prv_i,acc_prv_o,dat_x_01_s,val_x_01_s,acc_x_01_s);

--------------------- Serialisation
U02: entity work.signed_digit_serializer
generic map(size_bus_ext,data_size_length)
port map (clk_ovly,reset,dat_x_01_s,val_x_01_s,acc_x_01_ser_s,data_size_i,acc_x_01_s,val_x_01_ser_s,dat_x_01_ser_s );

-------------------- overlay
U03: entity work.overlay_NxM port map (clk_ovly,reset,cfg_x_mux_i,dat_x_01_ser_s,val_x_01_ser_s,acc_x_01_ser_s,dat_x_02_ser_s,val_x_02_ser_s,acc_x_02_ser_s,data_size_i);

U04: entity work.signed_digit_deserializer
generic map(size_bus_ext)
port map (clk_ovly,reset,dat_x_02_ser_s,val_x_02_ser_s,acc_x_02_s,acc_x_02_ser_s,val_x_02_s,dat_x_02_s);
-------------------- mux data output
U05: entity work.mux_data_out port map (clk_ovly,reset,dat_x_02_s,val_x_02_s,acc_x_02_s,dat_nxt_o,val_nxt_o,acc_nxt_i);


end data_in_out_overlay_arch;
