------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : mux_data_out.vhd
-- Author               : Convers Anthony
-- Date                 : 22.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity mux_data_out is
    Port ( clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           dat_prv_i : in  dat_io_ovly; --dat_io_ovly;
           val_prv_i : in  bit_io_ovly;
           acc_prv_o : out  bit_io_ovly;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size_bus_ext+bit_cmd_mux_io-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC);
end mux_data_out;

architecture mux_data_out_arch of mux_data_out is

-- signal fifo
signal wr_en_s : std_logic;
signal rd_en_s : std_logic;
signal full_s : std_logic;
signal empty_s : std_logic;

-- signal intern
signal val_x_std_01_s : std_logic_vector(nbr_io_ovly-1 downto 0);
signal sel_01_s : std_logic_vector(bit_cmd_mux_io-1 downto 0);
signal dat_02_s : std_logic_vector(size_bus_ext-1 downto 0);
signal val_02_s : std_logic;
signal acc_02_s : std_logic;
signal dat_sel_02_s : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);

signal dat_sel_03_s : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
signal val_03_s     : std_logic;
signal acc_03_s     : std_logic;
signal dat_sel_04_s : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
signal val_04_s     : std_logic;
signal acc_04_s     : std_logic;

signal dat_x_01_s : dat_io_ovly;
signal val_x_01_s : bit_io_ovly;
signal acc_x_01_s : bit_io_ovly;

-- signal demux

begin

-------------------- d_reg
gen_fifo : for i_gen in 0 to nbr_io_ovly-1 generate
begin
	F_X: entity work.d_fifo_ext generic map(size_bus_ext) port map (clk,reset,dat_prv_i(i_gen),val_prv_i(i_gen),acc_prv_o(i_gen),dat_x_01_s(i_gen),val_x_01_s(i_gen),acc_x_01_s(i_gen));
end generate;

-------------------- priority encodeur and mux
--conversion bit_in_type --> std_logic_vector
process(val_x_01_s)
begin
	for i in 0 to nbr_io_ovly-1 loop
		val_x_std_01_s(i) <= val_x_01_s(i);
	end loop;
end process;

P01: entity work.priority_enc port map (val_x_std_01_s,sel_01_s);

MUX01: entity work.mux_xto1 port map (sel_01_s,dat_x_01_s,dat_02_s);
MUX02: entity work.mux_xto1_bit port map (sel_01_s,val_x_01_s,val_02_s);
DMX03: entity work.demux_1tox_bit port map (sel_01_s,acc_02_s,acc_x_01_s);

-------------------- d_eb 40 and fifo out
dat_sel_02_s(size_bus_ext+bit_cmd_mux_io-1 downto size_bus_ext) <= sel_01_s;
dat_sel_02_s(size_bus_ext-1 downto 0) <= dat_02_s;

F03: entity work.d_eb generic map(size_bus_ext+bit_cmd_mux_io) port map (clk,reset,dat_sel_02_s,val_02_s,acc_02_s,dat_sel_03_s,val_03_s,acc_03_s);

F04: entity work.fifo_40b 
  port map (
    clk => clk,
    rst => reset,
    din => dat_sel_03_s,
    wr_en => wr_en_s,
    rd_en => rd_en_s,
    dout => dat_sel_04_s,
    full => full_s,
    empty => empty_s
  );

val_04_s <= not empty_s;
acc_03_s <= not full_s;

wr_en_s <= val_03_s and acc_03_s;
rd_en_s <= val_04_s and acc_04_s;


-------------------- signal output
dat_nxt_o <= dat_sel_04_s;
val_nxt_o <= val_04_s;
acc_04_s <= acc_nxt_i;


end mux_data_out_arch;

