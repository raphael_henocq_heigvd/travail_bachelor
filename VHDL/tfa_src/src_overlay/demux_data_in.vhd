------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : demux_data_in.vhd
-- Author               : Convers Anthony
-- Date                 : 22.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity demux_data_in is
    Port ( clk_usr : in  STD_LOGIC;
           clk_ovly : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           dat_prv_i : in   STD_LOGIC_VECTOR (size_bus_ext+bit_cmd_mux_io-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  dat_io_ovly;
           val_nxt_o : out  bit_io_ovly;
           acc_nxt_i : in  bit_io_ovly);
end demux_data_in;

architecture demux_data_in_arch of demux_data_in is

-- signal fifo
signal wr_en_s : std_logic;
signal rd_en_s : std_logic;
signal full_s : std_logic;
signal empty_s : std_logic;

-- signal intern
signal acc_00_s : std_logic;
signal dat_sel_01_s : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
signal val_01_s : std_logic;
signal acc_01_s : std_logic;
signal dat_sel_02_s : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
signal val_02_s : std_logic;
signal acc_02_s : std_logic;
signal dat_02_s : std_logic_vector(size_bus_ext+bit_sel_cel_io-1 downto 0);
signal sel_02_s : std_logic_vector(bit_sel_reg_io-1 downto 0);

signal dat_x_03_s : dat_reg_ovly;
signal val_x_03_s : bit_reg_ovly;
signal acc_x_03_s : bit_reg_ovly;

signal dat_x_04_s : dat_reg_ovly;
signal val_x_04_s : bit_reg_ovly;
signal acc_x_04_s : bit_reg_ovly;

signal dat_x_05_s : dat_reg_ovly;
signal val_x_05_s : bit_reg_ovly;
signal acc_x_05_s : bit_reg_ovly;

--------------------Signal North
signal N_dat_sel_11_s : std_logic_vector(size_bus_ext+bit_sel_cel_io-1 downto 0);
signal N_sel_11_s : std_logic_vector(bit_sel_cel_io-1 downto 0);
signal N_dat_11_s : std_logic_vector(size_bus_ext-1 downto 0);
signal N_val_11_s : std_logic;
signal N_acc_11_s : std_logic;
signal N_dat_x_12_s : dat_col_ovly;
signal N_val_x_12_s : bit_col_ovly;
signal N_acc_x_12_s : bit_col_ovly;
signal N_dat_x_13_s : dat_col_ovly;
signal N_val_x_13_s : bit_col_ovly;
signal N_acc_x_13_s : bit_col_ovly;

--------------------Signal West
signal W_dat_sel_11_s : std_logic_vector(size_bus_ext+bit_sel_cel_io-1 downto 0);
signal W_sel_11_s : std_logic_vector(bit_sel_cel_io-1 downto 0);
signal W_dat_11_s : std_logic_vector(size_bus_ext-1 downto 0);
signal W_val_11_s : std_logic;
signal W_acc_11_s : std_logic;
signal W_dat_x_12_s : dat_line_ovly;
signal W_val_x_12_s : bit_line_ovly;
signal W_acc_x_12_s : bit_line_ovly;
signal W_dat_x_13_s : dat_line_ovly;
signal W_val_x_13_s : bit_line_ovly;
signal W_acc_x_13_s : bit_line_ovly;

--------------------Signal East
signal E_dat_sel_11_s : std_logic_vector(size_bus_ext+bit_sel_cel_io-1 downto 0);
signal E_sel_11_s : std_logic_vector(bit_sel_cel_io-1 downto 0);
signal E_dat_11_s : std_logic_vector(size_bus_ext-1 downto 0);
signal E_val_11_s : std_logic;
signal E_acc_11_s : std_logic;
signal E_dat_x_12_s : dat_line_ovly;
signal E_val_x_12_s : bit_line_ovly;
signal E_acc_x_12_s : bit_line_ovly;
signal E_dat_x_13_s : dat_line_ovly;
signal E_val_x_13_s : bit_line_ovly;
signal E_acc_x_13_s : bit_line_ovly;

--------------------Signal South
signal S_dat_sel_11_s : std_logic_vector(size_bus_ext+bit_sel_cel_io-1 downto 0);
signal S_sel_11_s : std_logic_vector(bit_sel_cel_io-1 downto 0);
signal S_dat_11_s : std_logic_vector(size_bus_ext-1 downto 0);
signal S_val_11_s : std_logic;
signal S_acc_11_s : std_logic;
signal S_dat_x_12_s : dat_col_ovly;
signal S_val_x_12_s : bit_col_ovly;
signal S_acc_x_12_s : bit_col_ovly;
signal S_dat_x_13_s : dat_col_ovly;
signal S_val_x_13_s : bit_col_ovly;
signal S_acc_x_13_s : bit_col_ovly;

-- signal intern
signal dat_x_20_s : dat_io_ovly;
signal val_x_20_s : bit_io_ovly;
signal acc_x_20_s : bit_io_ovly;

begin

-------------------- fifo in
F01: entity work.fifo_40b 
  port map (
    clk => clk_usr,
    rst => reset,
    din => dat_prv_i,
    wr_en => wr_en_s,
    rd_en => rd_en_s,
    dout => dat_sel_01_s,
    full => full_s,
    empty => empty_s
  );

val_01_s <= not empty_s;
acc_00_s <= not full_s;

wr_en_s <= val_prv_i and acc_00_s;
rd_en_s <= val_01_s and acc_01_s;

--signal output
acc_prv_o <= acc_00_s;

-------------------- d_eb 40
F02: entity work.d_eb generic map(size_bus_ext+bit_cmd_mux_io) port map (clk_usr,reset,dat_sel_01_s,val_01_s,acc_01_s,dat_sel_02_s,val_02_s,acc_02_s);

-------------------- demux
sel_02_s <= dat_sel_02_s(size_bus_ext+bit_cmd_mux_io-1 downto size_bus_ext+bit_sel_cel_io);
dat_02_s <= dat_sel_02_s(size_bus_ext+bit_sel_cel_io-1 downto 0);

DMX01: entity work.demux_1to4 port map (sel_02_s,dat_02_s,dat_x_03_s);
DMX02: entity work.demux_1to4_bit port map (sel_02_s,val_02_s,val_x_03_s);
MUX03: entity work.mux_4to1_bit port map (sel_02_s,acc_x_03_s,acc_02_s);

-------------------- d_reg
gen_reg : for i_gen in 0 to nbr_region-1 generate
begin
	R_X: entity work.d_reg generic map(size_bus_ext+bit_sel_cel_io) port map (clk_usr,reset,dat_x_03_s(i_gen),val_x_03_s(i_gen),acc_x_03_s(i_gen),dat_x_04_s(i_gen),val_x_04_s(i_gen),acc_x_04_s(i_gen));
	F_X: entity work.d_fifo_38_io generic map(size_bus_ext+bit_sel_cel_io) port map (clk_usr,clk_ovly,reset,dat_x_04_s(i_gen),val_x_04_s(i_gen),acc_x_04_s(i_gen),dat_x_05_s(i_gen),val_x_05_s(i_gen),acc_x_05_s(i_gen));
end generate;


--------------------North(0)
NF11: entity work.d_eb generic map(size_bus_ext+bit_sel_cel_io) port map (clk_ovly,reset,dat_x_05_s(0),val_x_05_s(0),acc_x_05_s(0),N_dat_sel_11_s,N_val_11_s,N_acc_11_s);

N_sel_11_s <= N_dat_sel_11_s(size_bus_ext+bit_sel_cel_io-1 downto size_bus_ext);
N_dat_11_s <= N_dat_sel_11_s(size_bus_ext-1 downto 0);

NDMX11: entity work.demux_1tocol port map (N_sel_11_s,N_dat_11_s,N_dat_x_12_s);
NDMX12: entity work.demux_1tocol_bit port map (N_sel_11_s,N_val_11_s,N_val_x_12_s);
NMUX13: entity work.mux_colto1_bit port map (N_sel_11_s,N_acc_x_12_s,N_acc_11_s);

N_gen_reg : for n_i_gen in 0 to col_ovly-1 generate
begin
	N_R_X: entity work.d_reg generic map(size_bus_ext) port map (clk_ovly,reset,N_dat_x_12_s(n_i_gen),N_val_x_12_s(n_i_gen),N_acc_x_12_s(n_i_gen),N_dat_x_13_s(n_i_gen),N_val_x_13_s(n_i_gen),N_acc_x_13_s(n_i_gen));
	dat_x_20_s(line_ovly+n_i_gen) <= N_dat_x_13_s(n_i_gen);
    val_x_20_s(line_ovly+n_i_gen) <= N_val_x_13_s(n_i_gen);
    N_acc_x_13_s(n_i_gen) <= acc_x_20_s(line_ovly+n_i_gen);
end generate;

--------------------West(1)
WF11: entity work.d_eb generic map(size_bus_ext+bit_sel_cel_io) port map (clk_ovly,reset,dat_x_05_s(1),val_x_05_s(1),acc_x_05_s(1),W_dat_sel_11_s,W_val_11_s,W_acc_11_s);

W_sel_11_s <= W_dat_sel_11_s(size_bus_ext+bit_sel_cel_io-1 downto size_bus_ext);
W_dat_11_s <= W_dat_sel_11_s(size_bus_ext-1 downto 0);

WDMX11: entity work.demux_1toline port map (W_sel_11_s,W_dat_11_s,W_dat_x_12_s);
WDMX12: entity work.demux_1toline_bit port map (W_sel_11_s,W_val_11_s,W_val_x_12_s);
WMUX13: entity work.mux_lineto1_bit port map (W_sel_11_s,W_acc_x_12_s,W_acc_11_s);

W_gen_reg : for w_i_gen in 0 to line_ovly-1 generate
begin
	W_R_X: entity work.d_reg generic map(size_bus_ext) port map (clk_ovly,reset,W_dat_x_12_s(w_i_gen),W_val_x_12_s(w_i_gen),W_acc_x_12_s(w_i_gen),W_dat_x_13_s(w_i_gen),W_val_x_13_s(w_i_gen),W_acc_x_13_s(w_i_gen));
	dat_x_20_s(w_i_gen) <= W_dat_x_13_s(w_i_gen);
    val_x_20_s(w_i_gen) <= W_val_x_13_s(w_i_gen);
    W_acc_x_13_s(w_i_gen) <= acc_x_20_s(w_i_gen);
end generate;

--------------------East(2)
EF11: entity work.d_eb generic map(size_bus_ext+bit_sel_cel_io) port map (clk_ovly,reset,dat_x_05_s(2),val_x_05_s(2),acc_x_05_s(2),E_dat_sel_11_s,E_val_11_s,E_acc_11_s);

E_sel_11_s <= E_dat_sel_11_s(size_bus_ext+bit_sel_cel_io-1 downto size_bus_ext);
E_dat_11_s <= E_dat_sel_11_s(size_bus_ext-1 downto 0);

EDMX11: entity work.demux_1toline port map (E_sel_11_s,E_dat_11_s,E_dat_x_12_s);
EDMX12: entity work.demux_1toline_bit port map (E_sel_11_s,E_val_11_s,E_val_x_12_s);
EMUX13: entity work.mux_lineto1_bit port map (E_sel_11_s,E_acc_x_12_s,E_acc_11_s);

E_gen_reg : for e_i_gen in 0 to line_ovly-1 generate
begin
	E_R_X: entity work.d_reg generic map(size_bus_ext) port map (clk_ovly,reset,E_dat_x_12_s(e_i_gen),E_val_x_12_s(e_i_gen),E_acc_x_12_s(e_i_gen),E_dat_x_13_s(e_i_gen),E_val_x_13_s(e_i_gen),E_acc_x_13_s(e_i_gen));
	dat_x_20_s(line_ovly+col_ovly+e_i_gen) <= E_dat_x_13_s(e_i_gen);
    val_x_20_s(line_ovly+col_ovly+e_i_gen) <= E_val_x_13_s(e_i_gen);
    E_acc_x_13_s(e_i_gen) <= acc_x_20_s(line_ovly+col_ovly+e_i_gen);
end generate;

--------------------South(3)
SF11: entity work.d_eb generic map(size_bus_ext+bit_sel_cel_io) port map (clk_ovly,reset,dat_x_05_s(3),val_x_05_s(3),acc_x_05_s(3),S_dat_sel_11_s,S_val_11_s,S_acc_11_s);

S_sel_11_s <= S_dat_sel_11_s(size_bus_ext+bit_sel_cel_io-1 downto size_bus_ext);
S_dat_11_s <= S_dat_sel_11_s(size_bus_ext-1 downto 0);

SDMX11: entity work.demux_1tocol port map (S_sel_11_s,S_dat_11_s,S_dat_x_12_s);
SDMX12: entity work.demux_1tocol_bit port map (S_sel_11_s,S_val_11_s,S_val_x_12_s);
SMUX13: entity work.mux_colto1_bit port map (S_sel_11_s,S_acc_x_12_s,S_acc_11_s);

S_gen_reg : for s_i_gen in 0 to col_ovly-1 generate
begin
	S_R_X: entity work.d_reg generic map(size_bus_ext) port map (clk_ovly,reset,S_dat_x_12_s(s_i_gen),S_val_x_12_s(s_i_gen),S_acc_x_12_s(s_i_gen),S_dat_x_13_s(s_i_gen),S_val_x_13_s(s_i_gen),S_acc_x_13_s(s_i_gen));
	dat_x_20_s(line_ovly+col_ovly+line_ovly+s_i_gen) <= S_dat_x_13_s(s_i_gen);
    val_x_20_s(line_ovly+col_ovly+line_ovly+s_i_gen) <= S_val_x_13_s(s_i_gen);
    S_acc_x_13_s(s_i_gen) <= acc_x_20_s(line_ovly+col_ovly+line_ovly+s_i_gen);
end generate;

-------------------- output
dat_nxt_o <= dat_x_20_s;
val_nxt_o <= val_x_20_s;
acc_x_20_s <= acc_nxt_i;


end demux_data_in_arch;

