------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : priority_enc.vhd
-- Author               : Convers Anthony
-- Date                 : 25.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity priority_enc is
    Port ( sel_i : in  STD_LOGIC_VECTOR (nbr_io_ovly-1 downto 0);
           cmd_o : out  STD_LOGIC_VECTOR (bit_cmd_mux_io-1 downto 0));
end priority_enc;

architecture priority_enc_arch of priority_enc is

signal temp_s : STD_LOGIC_VECTOR (bit_cmd_mux_io-1 downto 0);

begin

process(sel_i)
begin
	temp_s <= (others => '0');
	for i in 0 to nbr_io_ovly-1 loop
		if sel_i(i)='1' then
			temp_s <= conv_std_logic_vector(i,bit_cmd_mux_io);
		end if;
	end loop;

end process;

cmd_o <= temp_s;

end priority_enc_arch;

