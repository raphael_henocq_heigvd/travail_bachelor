------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : fu_all.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
-- 1.0    03.05.17    RHE           Ajout online arithmetics adder
-- 2.0    03.07.17    RHE           Ajout du multiplicateur/comparateur
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.overlay_pkg.all;

entity fu_all is
    generic (size: integer:= size_bus);
    Port ( clk         : in  STD_LOGIC;
           reset       : in  STD_LOGIC;
           cfg_const_i : in STD_LOGIC_VECTOR (1 downto 0);
           cfg_op_i    : in STD_LOGIC_VECTOR (7 downto 0);
           dat_prv_1_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_1_i : in  STD_LOGIC;
           acc_prv_1_o : out  STD_LOGIC;
           dat_prv_2_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_2_i : in  STD_LOGIC;
	         acc_prv_2_o : out  STD_LOGIC;
	         dat_prv_sel_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_sel_i : in  STD_LOGIC;
           acc_prv_sel_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC;
           data_size_i : in std_logic_vector(DATA_SIZE_LENGTH -1 downto 0));
end fu_all;

architecture fu_all_arch of fu_all is
signal data_prv_1_s : std_logic_vector(size-1 downto 0);
signal data_prv_2_s : std_logic_vector(size-1 downto 0);
signal dat_prv_1_s  : std_logic_vector (size-1 downto 0);
signal dat_prv_2_s  : std_logic_vector (size-1 downto 0);

--signal config
signal en_const_in1 : std_logic;
signal en_const_in2 : std_logic;
signal mask_fsel_in : std_logic;

--signal data
signal dat_00_s : std_logic_vector(size-1 downto 0);
signal dat_01_s : std_logic_vector(size-1 downto 0);

--signal valid
signal val_00_s : std_logic;
signal val_01_s : std_logic;

--signal online adder
signal data_nxt_add_s   : std_logic_vector(size-1 downto 0);
signal dat_prv_add_1_s : std_logic_vector(size-1 downto 0);
signal dat_prv_add_2_s : std_logic_vector(size-1 downto 0);
signal add_nSub_s       : std_logic;

--Signal UC
signal end_of_data_s   : std_logic;
signal end_of_data_o_s : std_logic;
signal en_fu_s         : std_logic;
signal clear_fu_s      : std_logic;
signal in_fu_sel_s     : std_logic;
signal delta_s         : std_logic_vector(2 downto 0);
signal acc_prv_1_s     : std_logic;
signal acc_prv_2_s     : std_logic;
signal acc_prv_sel_s   : std_logic;
signal val_nxt_uc_s    : std_logic;
signal en_sel_s        : std_logic;
signal sel_out_s       : std_logic;
signal val_prv_1_s     : std_logic;
signal val_prv_2_s     : std_logic;

-- Signal multiplicator
signal val_nxt_mult_s   : std_logic;
signal mult_ready_s     : std_logic;
signal dat_prv_mult_cmp_1_s : std_logic_vector(size-1 downto 0);
signal dat_prv_mult_cmp_2_s : std_logic_vector(size-1 downto 0);
signal dat_nxt_mult_s  : std_logic_vector(size-1 downto 0);

-- Signal constant
type state_t is (sInit, sSetCst, sGetCst);
signal state         : state_t;
signal next_state    : state_t;
signal dat_prv_cst_s : std_logic_vector(size_bus-1 downto 0);
signal val_prv_cst_s : std_logic;
signal val_nxt_cst_s : std_logic;
signal en_cst_s      : std_logic;
signal acc_prv_cst_s : std_logic;
signal acc_nxt_cst_s : std_logic;
signal dat_nxt_cst   : std_logic_vector(size_bus-1 downto 0);

signal sel_reg_s : std_logic;
signal det_cmp_s : std_logic;

begin

--config part
en_const_in1 <= cfg_const_i(0);
en_const_in2 <= cfg_const_i(1);

dat_prv_cst_s <= dat_prv_1_i when en_const_in1 = '1' else
                 dat_prv_2_i;
val_prv_cst_s <= val_prv_1_i when en_const_in1 = '1' else
                 val_prv_2_i;
acc_nxt_cst_s <= acc_prv_1_s when en_const_in1 = '1' else
                 acc_prv_2_s;
data_prv_1_s <= dat_nxt_cst when en_const_in1 = '1' else
                dat_prv_1_i;
data_prv_2_s <= dat_nxt_cst when en_const_in2 = '1' else
                dat_prv_2_i;
val_prv_1_s  <= val_nxt_cst_s when en_const_in1 = '1' else
                val_prv_1_i;
val_prv_2_s  <= val_nxt_cst_s when en_const_in2 = '1' else
                val_prv_2_i;

mask_fsel_in <= '0' when (cfg_op_i = FU_OPCODE_SEL) else '1';

CONST : entity work.constants
  port map (clk,reset,en_cst_s,dat_prv_cst_s,val_prv_cst_s, acc_nxt_cst_s, dat_nxt_cst
            , val_nxt_cst_s,acc_prv_cst_s);

UC: entity work.uc_fu_all
port map(clk,reset,val_prv_1_s,
 val_prv_2_s,val_prv_sel_i,acc_nxt_i,end_of_data_s
 ,delta_s,mult_ready_s,cfg_op_i,acc_prv_1_s,acc_prv_2_s,acc_prv_sel_s,
 val_nxt_uc_s,end_of_data_o_s,
 en_fu_s,clear_fu_s,in_fu_sel_s,en_sel_s, sel_out_s);

 ADDER: entity work.online_adder
 port map (clk,reset,en_fu_s,clear_fu_s,dat_prv_add_1_s, dat_prv_add_2_s, data_nxt_add_s);

 MULT: entity work.fu_mult_comp
 port map(clk, reset, en_fu_s, clear_fu_s, dat_prv_mult_cmp_1_s, dat_prv_mult_cmp_2_s, end_of_data_s,acc_nxt_i,data_size_i, cfg_op_i,
          mult_ready_s, val_nxt_mult_s,dat_nxt_mult_s);

end_of_data_s <= '1' when data_prv_1_s = "11" and data_prv_2_s = "11" else
                 '1' when data_prv_1_s = "11" and dat_prv_2_s = "11" else
                 '1' when dat_prv_1_s = "11" and data_prv_2_s = "11" else
                 '0';

process (clk, reset)
begin
  if reset = '1' then
    dat_prv_1_s <= (others => '0');
    dat_prv_2_s <= (others => '0');
    dat_01_s    <= (others => '0');
    sel_reg_s    <= '0';
    state <= sInit;
  elsif rising_edge(clk) then
    state <= next_state;
    if acc_prv_1_s = '1' then
      dat_prv_1_s <= data_prv_1_s;
    end if;
    if acc_prv_2_s = '1' then
      dat_prv_2_s <= data_prv_2_s;
    end if;
    if en_sel_s = '1' then
      sel_reg_s <= dat_prv_sel_i(0);
    end if;
    if sel_out_s = '0' then
      dat_01_s <= dat_00_s;
    end if;
  end if;
end process;

-- Preparing data for add or sub
process (dat_prv_1_s,dat_prv_2_s, add_nSub_s, in_fu_sel_s)
begin
  if in_fu_sel_s = '1' then
      dat_prv_add_1_s <= "00";
      dat_prv_add_2_s <= "00";
  elsif add_nSub_s = '1' then
      dat_prv_add_1_s <= dat_prv_1_s;
      dat_prv_add_2_s <= dat_prv_2_s;
  else  -- Sub so we need to inverse data_b
      dat_prv_add_1_s <= dat_prv_1_s(1 downto 0);
      if dat_prv_2_s(1 downto 0) = "01" then
          dat_prv_add_2_s <= "10";
      elsif dat_prv_2_s(1 downto 0) = "10" then
          dat_prv_add_2_s <= "01";
      else
          dat_prv_add_2_s <= "00";
      end if;
  end if;
end process;

 -- Preparing data for mult_ready_s
 dat_prv_mult_cmp_1_s <= --dat_prv_1_s when in_fu_sel_s = '0'  else -- for online mult
                         dat_prv_1_s when in_fu_sel_s = '0' else -- for dsp mult
                        "11";
 dat_prv_mult_cmp_2_s <= --dat_prv_2_s when in_fu_sel_s = '0'  else -- for online mult
                         dat_prv_2_s when in_fu_sel_s = '0' else  -- for dsp mult
                         --dat_prv_2_s when in_fu_sel_s = '0' else -- comparaison
                         "11";

process (cfg_op_i, data_nxt_add_s, dat_prv_1_s, dat_prv_2_s, dat_nxt_mult_s,sel_reg_s)
begin

      --default values
      dat_00_s   <= (others => '0');
      add_nSub_s <= '0';
      delta_s    <= "000";

			case cfg_op_i is
				when FU_OPCODE_SUM =>
				  add_nSub_s <= '1';
					dat_00_s <= data_nxt_add_s;
          delta_s <= "011";  -- delta = 3 for addition

				when FU_OPCODE_SUB =>
				  add_nSub_s <= '0';
					dat_00_s <= data_nxt_add_s;
					delta_s <= "011";  -- delta = 3 for substraction

				when FU_OPCODE_MUL =>
          dat_00_s <= dat_nxt_mult_s;
          delta_s <= "000";  -- for mult with dsp
          --delta_s <= "100";    -- for online mult

				when FU_OPCODE_DIV =>	--in1 / in2		-- TO DO!!!!!!!!!!!!


				when FU_OPCODE_MOD =>	--in1 % in2		-- TO DO!!!!!!!!!!!!

				when FU_OPCODE_EQU =>	--in1 == in2
          delta_s <= "000";
          dat_00_s <= dat_nxt_mult_s;

				when FU_OPCODE_NEQ =>	--in1 != in2
          delta_s <= "000";
          dat_00_s <= dat_nxt_mult_s;

				when FU_OPCODE_GEQ =>	--in1 >= in2
          delta_s <= "000";
          dat_00_s <= dat_nxt_mult_s;

				when FU_OPCODE_GRE =>	--in1 >  in2
          delta_s <= "000";
          dat_00_s <= dat_nxt_mult_s;

				when FU_OPCODE_SEL =>
          delta_s <= "000";
          if sel_reg_s = '1' then
            dat_00_s <= dat_prv_1_s ;
          else
            dat_00_s <= dat_prv_2_s ;
          end if;
				when others =>
					null;
			end case;

end process;

process (en_const_in1, en_const_in2, dat_prv_cst_s, val_prv_cst_s,state)
begin
  en_cst_s <= '0';
  next_state <= state;
  case state is
    when sInit =>
      if en_const_in1 = '1' or en_const_in2 = '1' then
        next_state <= sSetCst;
      end if;
    when sSetCst =>
      en_cst_s <= '1';
      if dat_prv_cst_s = "11" and val_prv_cst_s = '1' then
        next_state  <= sGetCst;
      end if;
    when sGetCst =>
  end case;
end process;


  det_cmp_s <= '1' when cfg_op_i = FU_OPCODE_EQU or cfg_op_i = FU_OPCODE_NEQ or
                        cfg_op_i = FU_OPCODE_GEQ or cfg_op_i = FU_OPCODE_GRE else
               '0';

  val_00_s <= val_prv_1_i and val_prv_2_i and acc_nxt_i and (val_prv_sel_i or mask_fsel_in);

  acc_prv_sel_o <= acc_prv_sel_s;
  acc_prv_1_o <=  acc_prv_cst_s when en_const_in1 = '1' else
                  acc_prv_1_s;
  acc_prv_2_o <=  acc_prv_cst_s when en_const_in2 = '1' else
                  acc_prv_2_s;
  acc_prv_sel_o <= acc_prv_sel_s;
  val_nxt_o <= val_nxt_mult_s when cfg_op_i = FU_OPCODE_MUL or cfg_op_i = FU_OPCODE_DIV  or det_cmp_s = '1' else
               val_nxt_uc_s;
  dat_nxt_o <= "11" when end_of_data_o_s = '1'  and (cfg_op_i = FU_OPCODE_SUM or cfg_op_i = FU_OPCODE_SUB)  else
               dat_00_s when sel_out_s = '0' else
               dat_01_s;


end fu_all_arch;
