-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : uc_adder.vhd
-- Description  : Machine séquentiel permettant la réception des données sériel,
--                l'activation de la FU, et la génération du bit valide pour l'addition
-- Author       : Raphael Henocq
-- Date         : 16.04.17
-- Version      : 1.1
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    16.04.17           Creation
-- 1.0       RHE    20.04.17           Séparation de l'architecture pour machine de moore et de mealy
-- 1.1       RHE    24.04.17           Amélioration machine de moore
-- 1.2       RHE    27.06.17           Modification pour l'ajout du multiplicateur
-- 1.3       THE    0..07.17           Modification pour la config sel
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity uc_fu_all is
    port (
        -- Entrées
        clk_i         : in std_logic;
        rst_i         : in std_logic;
        val_prv_a_i   : in std_logic;
        val_prv_b_i   : in std_logic;
        val_prv_sel_i : in std_logic;
        acc_nxt_i     : in std_logic;
        end_of_data_i : in std_logic;
        delta_i       : in std_logic_vector(2 downto 0);
        mult_ready_i  : in std_logic;
        cfg_op_i      : in std_logic_vector(7 downto 0);

        -- Sorties
        acc_prv_a_o   : out std_logic;
        acc_prv_b_o   : out std_logic;
        acc_prv_sel_o : out std_logic;
        val_nxt_o     : out std_logic;
        end_of_data_o : out std_logic;
        en_fu_o      : out std_logic;
        clear_fu_o   : out std_logic;
        in_fu_sel_o  : out std_logic;
        en_sel_o     : out std_logic;
        sel_out_o    : out std_logic
    );
end uc_fu_all;

architecture moore of uc_fu_all is
-- Signals

-- Decompteur
signal cpt_fut    : unsigned(2 downto 0);
signal cpt_pres   : unsigned(2 downto 0);
signal en_cpt_s   : std_logic;
signal load_cpt_s : std_logic;

-- State machine
type state_t is (sInit,sWait_val_prv0, sWait_val_a_prv0, sWait_val_b_prv0, sStep0,
                 sWait_val_acc_1, sWait_val_1, sWait_valA_acc_1,sWait_valA_1, sWait_valB_acc_1, sWait_valB_1, sWait_acc_1 , sStep1,
                 sWait_acc_nxt2, sStep2,
                 sWait_acc_end, sStepEnd, sWait_val_init, sWait_val_sel, sWait_val_sel1);

signal state_s      : state_t;
signal next_state_s : state_t;

begin



-------------- Decompteur
cpt_fut <= cpt_pres when en_cpt_s = '0' else
           unsigned(delta_i) when load_cpt_s = '1' or cpt_pres = 0 else
           cpt_pres - 1 ;

process (val_prv_a_i, val_prv_b_i, end_of_data_i,acc_nxt_i, state_s, cpt_pres, cfg_op_i,val_prv_sel_i,delta_i,mult_ready_i)
begin

    next_state_s  <= state_s;
    acc_prv_a_o   <= '0';
    acc_prv_b_o   <= '0';
    acc_prv_sel_o <= '0';
    val_nxt_o     <= '0';
    end_of_data_o <= '0';
    en_fu_o       <= '0';
    clear_fu_o    <= '0';
    in_fu_sel_o   <= '0';
    en_cpt_s      <= '0';
    load_cpt_s    <= '0';
    en_sel_o      <= '0';
    sel_out_o     <= '0';

--  Case for next_state_s only
    case state_s is
        when sInit =>    -- Attente de la configuration de la cellule
          if cfg_op_i = FU_OPCODE_SEL then
            next_state_s <= sWait_val_sel;
          elsif cfg_op_i /= "00000000" then
            next_state_s <= sWait_val_init;
          end if;

        when sWait_val_sel =>  -- Attente de récéption du bit de séléction
          if val_prv_sel_i = '1' then
            next_state_s <= sWait_val_sel1;
          end if;

        when sWait_val_sel1 =>
        if val_prv_sel_i = '1' then
          next_state_s <= sWait_val_init;
        end if;

        when sWait_val_init =>  -- Attente d'une donnée valide en entrée
          if val_prv_a_i = '1' and val_prv_b_i = '1'  and cpt_pres <= 2 then
            next_state_s <= sStep1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' then
            next_state_s <= sStep0;
          elsif val_prv_a_i = '1' then
            next_state_s <= sWait_val_b_prv0;
          elsif val_prv_b_i = '1' then
            next_state_s <= sWait_val_a_prv0;
          end if;

        when sWait_val_prv0 => -- Attente d'une donnée valide en entrée
          if val_prv_a_i = '1' and val_prv_b_i = '1'  and cpt_pres <= 1 then
            next_state_s <= sStep1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' then
            next_state_s <= sStep0;
          elsif val_prv_a_i = '1' then
            next_state_s <= sWait_val_b_prv0;
          elsif val_prv_b_i = '1' then
            next_state_s <= sWait_val_a_prv0;
          end if;

        when sWait_val_a_prv0 => -- Attente d'une donnée A valide
          if val_prv_a_i = '1' and cpt_pres <= 1 then
            next_state_s <= sStep1;
          elsif val_prv_a_i = '1' then
            next_state_s <= sStep0;
          end if;

        when sWait_val_b_prv0 =>  -- Attente d'une donnée B valide
          if val_prv_b_i = '1' and cpt_pres <= 1 then
            next_state_s <= sStep1;
          elsif val_prv_b_i = '1' then
            next_state_s <= sStep0;
          end if;

        when sStep0 => -- Gestion du délais d'entrée de l'opérateur
          if val_prv_a_i = '1' and val_prv_b_i = '1' and cpt_pres <= 2 then
            next_state_s <= sStep1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' then
            next_state_s <= sStep0;
          elsif val_prv_a_i = '1' then
            next_state_s <= sWait_val_b_prv0;
          elsif val_prv_b_i = '1' then
            next_state_s <= sWait_val_a_prv0;
          else
            next_state_s <= sWait_val_prv0;
          end if;

        when sWait_val_acc_1 => -- Attente d'accéptation du résultat
          if val_prv_a_i = '1' and val_prv_b_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif val_prv_a_i = '0' and val_prv_b_i = '0' and acc_nxt_i = '1' then
            next_state_s <= sWait_val_1;
          elsif val_prv_a_i = '0' and val_prv_b_i = '1' and acc_nxt_i = '0' then
            next_state_s <= sWait_valA_acc_1;
          elsif val_prv_a_i = '0' and val_prv_b_i = '1' and acc_nxt_i = '1' then
            next_state_s <= sWait_valA_1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '0' and acc_nxt_i = '0' then
            next_state_s <= sWait_valB_acc_1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '0' and acc_nxt_i = '1' then
            next_state_s <= sWait_valB_1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' and acc_nxt_i = '0' then
            next_state_s <= sWait_acc_1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' and acc_nxt_i = '1' then
            next_state_s <= sStep1;
          end if;

        when sWait_val_1 =>  -- Attente d'une donnée valide en entrée
          if val_prv_a_i = '1' and val_prv_b_i = '1'  and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1'  and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' then
            next_state_s <= sStep1;
          elsif val_prv_a_i = '1' then
            next_state_s <= sWait_valB_1;
          elsif val_prv_b_i = '1' then
            next_state_s <= sWait_valA_1;
          end if;

        when sWait_valA_acc_1 =>   -- Attente d'une donnée A valide, et de l'accéptation de la donnée en sortie
          if val_prv_a_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif val_prv_a_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif val_prv_a_i = '1' and acc_nxt_i = '1' then
            next_state_s <= sStep1;
          elsif val_prv_a_i = '1' then
            next_state_s <= sWait_acc_1;
          elsif acc_nxt_i = '1' then
            next_state_s <= sWait_valA_1;
          end if;

        when sWait_valA_1 =>  -- Attente d'une donnée A valide
          if val_prv_a_i = '1' and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif val_prv_a_i = '1' and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif val_prv_a_i = '1' then
            next_state_s <= sStep1;
          end if;

        when sWait_valB_acc_1 => -- Attente d'une donnée B valide, et de l'accéptation de la donnée en sortie
          if val_prv_b_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif val_prv_b_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif val_prv_b_i = '1' and acc_nxt_i = '1' then
            next_state_s <= sStep1;
          elsif val_prv_b_i = '1' then
            next_state_s <= sWait_acc_1;
          elsif acc_nxt_i = '1' then
            next_state_s <= sWait_valB_1;
          end if;

        when sWait_valB_1 => -- Attente d'une donnée B valide
          if val_prv_b_i = '1' and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif val_prv_b_i = '1' and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif val_prv_b_i = '1' then
            next_state_s <= sStep1;
          end if;

        when sWait_acc_1 => -- Attente de l'accéptation de la donnée en sortie
          if acc_nxt_i = '1' and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif acc_nxt_i = '1' and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif acc_nxt_i = '1' then
            next_state_s <= sStep1;
          end if;

        when sStep1 => -- Réalisation de l'opération sériel et production d'un résultat valide
          if val_prv_a_i = '1' and val_prv_b_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' and delta_i = "000" then
            next_state_s <= sStepEnd;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' and acc_nxt_i = '1' and end_of_data_i = '1' then
            next_state_s <= sStep2;
          elsif val_prv_a_i = '0' and val_prv_b_i = '0' and acc_nxt_i = '0' then
            next_state_s <= sWait_val_acc_1;
          elsif val_prv_a_i = '0' and val_prv_b_i = '0' and acc_nxt_i = '1' then
            next_state_s <= sWait_val_1;
          elsif val_prv_a_i = '0' and val_prv_b_i = '1' and acc_nxt_i = '0' then
            next_state_s <= sWait_valA_acc_1;
          elsif val_prv_a_i = '0' and val_prv_b_i = '1' and acc_nxt_i = '1' then
            next_state_s <= sWait_valA_1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '0' and acc_nxt_i = '0' then
            next_state_s <= sWait_valB_acc_1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '0' and acc_nxt_i = '1' then
            next_state_s <= sWait_valB_1;
          elsif val_prv_a_i = '1' and val_prv_b_i = '1' and acc_nxt_i = '0' then
            next_state_s <= sWait_acc_1;
          end if;

        when sWait_acc_nxt2 => -- Attente l'accéptation de la donnée en sortie
          if acc_nxt_i = '1' and cpt_pres = 1 then
            next_state_s <= sStepEnd;
          elsif acc_nxt_i = '1' then
            next_state_s <= sStep2;
          end if;

        when sStep2 =>  -- Production des derniers résultats de l'opérateur (d'u au délais de l'opérateur)
          if acc_nxt_i = '1' and cpt_pres = 2 then
            next_state_s <= sStepEnd;
          elsif acc_nxt_i = '0' then
            next_state_s <= sWait_acc_nxt2;
          end if;

        when sWait_acc_end => -- Attente de l'accéptation de la donnée en sortie
          if acc_nxt_i = '1' then
            next_state_s <= sInit;
          end if;

        when sStepEnd =>  -- Fin de l'opération
          if cfg_op_i = FU_OPCODE_MUL then
            if mult_ready_i = '1'  then
              next_state_s <= sInit;
            end if;
          else
            if acc_nxt_i = '1'  then
              next_state_s <= sInit;
            else
              next_state_s <= sWait_acc_end;
            end if;
          end if;
    end case;

--   Case for output only
    case state_s is
        when sInit =>
          clear_fu_o    <= '1';
          en_cpt_s      <= '1';
          load_cpt_s    <= '1';

        when sWait_val_sel =>
          clear_fu_o    <= '1';
          en_cpt_s      <= '1';
          load_cpt_s    <= '1';
          acc_prv_sel_o <= '1';
          en_sel_o      <= '1';

        when sWait_val_sel1 =>
          acc_prv_sel_o <= '1';

        when sWait_val_init =>
          acc_prv_a_o   <= '1';
          acc_prv_b_o   <= '1';
          clear_fu_o    <= '1';
          en_cpt_s      <= '1';
          load_cpt_s    <= '1';

        when sWait_val_prv0 =>
          acc_prv_a_o   <= '1';
          acc_prv_b_o   <= '1';

        when sWait_val_a_prv0 =>
          acc_prv_a_o <= '1';

        when sWait_val_b_prv0 =>
          acc_prv_b_o <= '1';

        when sStep0 =>
          acc_prv_a_o   <= '1';
          acc_prv_b_o   <= '1';
          en_fu_o       <= '1';
          in_fu_sel_o   <= '0';
          en_cpt_s      <= '1';

        when sWait_val_acc_1 =>
          sel_out_o     <= '1';
          acc_prv_a_o   <= '1';
          acc_prv_b_o   <= '1';
          val_nxt_o     <= '1';

        when sWait_val_1 =>
          acc_prv_a_o <= '1';
          acc_prv_b_o <= '1';

        when sWait_valA_acc_1 =>
          acc_prv_a_o <= '1';
          val_nxt_o   <= '1';
          sel_out_o   <= '1';

        when sWait_valA_1 =>
          acc_prv_a_o <= '1';

        when sWait_valB_acc_1 =>
          acc_prv_b_o <= '1';
          val_nxt_o <= '1';
          sel_out_o     <= '1';

        when sWait_valB_1 =>
          acc_prv_b_o <= '1';

        when sWait_acc_1 =>
          val_nxt_o <= '1';
          sel_out_o     <= '1';

        when sStep1 =>
          acc_prv_a_o   <= '1';
          acc_prv_b_o   <= '1';
          val_nxt_o     <= '1';
          en_fu_o       <= '1';
          in_fu_sel_o   <= '0';
          en_cpt_s      <= '1';
          load_cpt_s    <= '1';

        when sWait_acc_nxt2 =>
          sel_out_o     <= '1';
          val_nxt_o <= '1';

        when sStep2 =>
          val_nxt_o     <= '1';
          en_fu_o       <= '1';
          in_fu_sel_o   <= '1';
          en_cpt_s      <= '1';

        when sWait_acc_end =>
          val_nxt_o <= '1';
          end_of_data_o <= '1';

        when sStepEnd =>
          val_nxt_o     <= '1';
          end_of_data_o <= '1';

    end case;

end process;


process(clk_i, rst_i) is
begin
    if rst_i = '1' then
        state_s <= sInit;
        cpt_pres <= (others => '0');
    elsif rising_edge(clk_i) then
        state_s <= next_state_s;
        cpt_pres <= cpt_fut;
    end if;
end process;

end moore;
