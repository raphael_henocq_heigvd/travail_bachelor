-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : signed_digit_deserializer.vhd
-- Description  : Récupère des données sérialisées de type  signed digit
--                et les converti en entier signé.
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

use work.overlay_pkg.all;

entity signed_digit_deserializer is
    generic (
            FIFOSIZE : integer :=size_bus_ext
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in dat_io_in_ovly; --donnée sérialisée
        val_prv_i      : in bit_io_ovly;
        acc_nxt_i      : in bit_io_ovly;

        -- Sorties
        acc_prv_o      : out bit_io_ovly;
        val_nxt_o      : out bit_io_ovly;
        data_nxt_o     : out dat_io_ovly
    );
end signed_digit_deserializer;

architecture struct of signed_digit_deserializer is

type sign_digit_data is array (nbr_io_ovly -1 downto 0) of std_logic_vector(1 downto 0);
-- Signals
signal en_conv_s     : bit_io_ovly;
signal clear_conv_s  : bit_io_ovly;
signal data_s        : sign_digit_data;
signal data1_s       : dat_io_in_ovly;
signal end_of_data_s : bit_io_ovly;


begin

gen_deserial : for i_gen in 0 to nbr_io_ovly-1 generate
begin
    uc_X : entity  work.uc_signed_digit_deserializer(moore)
    port map(
        -- Entrées
        clk_i          =>clk_i,
        rst_i          =>rst_i,
        val_prv_i      =>val_prv_i(i_gen),
        acc_nxt_i      =>acc_nxt_i(i_gen),
        end_of_data_i  =>end_of_data_s(i_gen),
        -- Sorties
        acc_prv_o      =>acc_prv_o(i_gen),
        val_nxt_o      =>val_nxt_o(i_gen),
        en_conv_o      =>en_conv_s(i_gen),
        clear_conv_o   =>clear_conv_s(i_gen)
    );

    on_fly_conv_X : entity  work.on_fly_conv
    generic map(
            FIFOSIZE => FIFOSIZE
    )
    port map(
        -- Entrées
        clk_i          =>clk_i,
        rst_i          =>rst_i,
        data_prv_i     =>data_s(i_gen),
        en_conv_i      =>en_conv_s(i_gen),
        clear_conv_i   =>clear_conv_s(i_gen),
        -- Sorties
        data_nxt_o     =>data_nxt_o(i_gen)
    );
end generate;

process (data1_s)
begin
    for i in 0 to nbr_io_ovly-1 loop
        data_s(i) <= data1_s(i);
    end loop;
end process;

process (data_prv_i)
begin
    for i in 0 to nbr_io_ovly-1 loop
      if data_prv_i(i) = "11" then
        end_of_data_s(i) <= '1' ;
      else
        end_of_data_s(i) <= '0';
      end if;
    end loop;
end process;

process (clk_i, rst_i,data_prv_i)
begin
    for i in 0 to nbr_io_ovly-1 loop
        if rst_i = '1' then
            data1_s(i) <= (others => '0');
        elsif Rising_Edge(clk_i) then
            data1_s(i) <= data_prv_i(i);
        end if;
    end loop;

end process;
end struct;
