-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : get_in_MSB_order.vhd
-- Description  : Sérialise un nombre donné en sa décomposition bit par bit
--                en commancant par le MSB (bit de signe exclus). Fournit aussi
--                le bit de signe en sortie.
--
-- Author       : Raphael Henocq
-- Date         : 19.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    19.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;
use work.overlay_pkg.all;

entity get_in_MSB_order is
    generic (
            FIFOSIZE: integer:= size_bus_ext;
            DATA_SIZE_LENGTH: integer := size_bus_ext
    );
    port ( clk_i         : in  std_logic;
           rst_i         : in  std_logic;
           enable_i      : in  std_logic;
           nLoad_get_i   : in  std_logic;
           data_size_i   : in  std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
           D_load_i      : in  std_logic_vector (FIFOSIZE-1 downto 0);
           D_o           : out std_logic;
           signe_bit_o   : out std_logic
    );
end get_in_MSB_order;

architecture behave of get_in_MSB_order is

signal data_size_s   : std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
signal reg_pres      : std_logic_vector(FIFOSIZE-1 downto 0);
signal reg_fut       : std_logic_vector(FIFOSIZE-1 downto 0);
signal sign_bit_pres : std_logic;
signal sign_bit_fut  : std_logic;
signal cpt_pres      : unsigned(5 downto 0);
signal cpt_fut       : unsigned(5 downto 0);

begin

data_size_s <= (others => '0') when data_size_i(0) = 'X' else  -- for simulation
               std_logic_vector(unsigned(data_size_i)-1);
signe_bit_o <= sign_bit_pres;
D_o         <= reg_pres(to_integer(unsigned(cpt_pres)));
cpt_fut <= unsigned(data_size_s(cpt_fut'length-1 downto 0)) when nLoad_get_i = '0' else
           cpt_pres when enable_i = '1'  and cpt_pres = 0 else
           cpt_pres-1 when enable_i = '1' else
           cpt_pres;

sign_bit_fut <= D_load_i(to_integer(unsigned(data_size_s)));
reg_fut <= reg_pres when nLoad_get_i = '1' else
           D_load_i when sign_bit_fut = '0' else                               -- positive number
           std_logic_vector(unsigned (not D_load_i(FIFOSIZE-1 downto 0)) + 1); -- negative number, chargement de sa valeur absolue

process (clk_i, rst_i)
begin
    if rst_i = '1' then
        reg_pres <= (others => '0');
        sign_bit_pres <= '0';
        cpt_pres <= (others =>'0');
    elsif rising_edge(clk_i) then
        cpt_pres <= cpt_fut;
        if nLoad_get_i = '0' then
          sign_bit_pres <= sign_bit_fut;
          reg_pres <= reg_fut;
        elsif enable_i = '1' then
          reg_pres <= reg_fut;
        end if;
    end if;
end process;


end behave;
