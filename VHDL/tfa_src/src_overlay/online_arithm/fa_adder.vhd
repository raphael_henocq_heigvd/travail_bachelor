-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : fa_adder.vhd
-- Description  : Effectue une addition entre les trois entrées a, b et c. Transmet
--                le carry de l'opération et le résultat en sortie
--
-- Author       : Raphael Henocq
-- Date         : 19.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    19.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.Numeric_Std.all;

entity fa_adder is
    port (
        a_in  : in std_logic;
        b_in  : in std_logic;
        c_in  : in std_logic;
        c_o   : out std_logic;
        res_o : out std_logic
    );
end fa_adder;

architecture data_flow of fa_adder is
signal resultat_s : std_logic_vector (1 downto 0);
signal in_s       : std_logic_vector (2 downto 0);

begin

in_s <= a_in & b_in & c_in;

with in_s select resultat_s <=
    "00" when "000",
    "01" when "001",
    "01" when "010",
    "10" when "011",
    "01" when "100",
    "10" when "101",
    "10" when "110",
    "11" when "111",
    "XX" when others;

c_o   <= resultat_s(1);
res_o <= resultat_s(0);

end data_flow;
