-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : signed_digit_serializer.vhd
-- Description  : Converti une donnée entière signée en sa représentation sériel en signed digit.
--                Signed Digit représentation : 00 = 0
--                                              01 = 1
--                                              10 = -1
--                                              11 = Fin de la donnée
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.overlay_pkg.all;

entity signed_digit_serializer is
    generic (
            FIFOSIZE : integer :=32;
            DATA_SIZE_LENGTH : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in dat_io_ovly;
        val_prv_i      : in bit_io_ovly;
        acc_nxt_i      : in bit_io_ovly;
        data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);

        -- Sorties
        acc_prv_o      : out bit_io_ovly;
        val_nxt_o      : out bit_io_ovly;
        data_ser_o     : out dat_io_in_ovly
    );
end signed_digit_serializer;

architecture struct of signed_digit_serializer is
type cpt_t is array(nbr_io_ovly-1 downto 0) of unsigned(6 downto 0);
-- Signals
signal en_get_msb_s   : bit_io_ovly;
signal nLoad_get_s    : bit_io_ovly;
signal end_of_data_s  : bit_io_ovly;
signal get_out_s      : bit_io_ovly;
signal data_ser0_s    : dat_io_in_ovly;
signal data_ser1_s    : dat_io_in_ovly;
signal sign_bit_s     : bit_io_ovly;
signal cpt_pres       : cpt_t;
signal cpt_fut        : cpt_t;
signal cpt_det0_s     : bit_io_ovly;
signal cpt_det1_s     : bit_io_ovly;


begin

gen_serial : for i_gen in 0 to nbr_io_ovly-1 generate
begin
    get_in_MSB_x : entity work.get_in_MSB_order
    generic map(
            FIFOSIZE => FIFOSIZE,
            DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
    )
    port map(
           clk_i         => clk_i,
           rst_i         => rst_i,
           enable_i      => en_get_msb_s(i_gen),
           nLoad_get_i   => nLoad_get_s(i_gen),
           data_size_i   => data_size_i,
           D_load_i      => data_prv_i(i_gen),
           D_o           => get_out_s(i_gen),
           signe_bit_o   => sign_bit_s(i_gen)
    );
end generate;

process(val_prv_i,acc_nxt_i,cpt_pres,data_size_i)
begin
  for i in 0 to nbr_io_ovly-1 loop
    if cpt_pres(i) = 0 and val_prv_i(i)='1' then -- initialisation du compteur lorsque donnée valide en entrée et acc_prv = '1'
      cpt_fut(i) <= unsigned(data_size_i(cpt_fut(i)'length-1 downto 0))+1;
    elsif cpt_pres(i) = 0  then
      cpt_fut(i) <= cpt_pres(i);
    elsif acc_nxt_i(i)='1' then
      cpt_fut(i) <= cpt_pres(i)-1;
    else
      cpt_fut(i) <= cpt_pres(i);
    end if;
  end loop;
end process;

process(cpt_pres)
begin
  for i in 0 to nbr_io_ovly-1 loop
    if cpt_pres(i) = 0 then
      cpt_det0_s(i) <= '1';
    else
      cpt_det0_s(i) <= '0';
    end if;
    if cpt_pres(i) = 1 then
      cpt_det1_s(i) <= '1';
    else
      cpt_det1_s(i) <= '0';
    end if;
  end loop;
end process;

process(cpt_det0_s,cpt_det1_s,acc_nxt_i)
begin
  for i in 0 to nbr_io_ovly-1 loop

    acc_prv_o(i)    <= cpt_det0_s(i);
    val_nxt_o(i)    <= not cpt_det0_s(i);
    en_get_msb_s(i) <= not cpt_det0_s(i) and acc_nxt_i(i);
    nLoad_get_s(i)  <= not cpt_det0_s(i);
    end_of_data_s(i)   <= cpt_det1_s(i);
  end loop;
end process;

-- Création de la donnée sérialisé avec la SD représentation
process (end_of_data_s, get_out_s, sign_bit_s)
begin
    for i in 0 to nbr_io_ovly -1 loop
      if (sign_bit_s(i) = '0' and end_of_data_s(i) = '0') then
        data_ser0_s(i) <= '0' & get_out_s(i);
      elsif (sign_bit_s(i) = '1' and end_of_data_s(i) = '0') then
        data_ser0_s(i) <= get_out_s(i) & '0';
      else
        data_ser0_s(i) <= "11";  -- Fin de la donnée
      end if;
    end loop;
end process;

process (clk_i, rst_i,cpt_fut)
begin
  for i in 0 to nbr_io_ovly -1 loop
    if rst_i = '1' then
      cpt_pres(i) <= (others => '0');
    elsif rising_edge(clk_i) then
      cpt_pres(i) <= cpt_fut(i);
    end if;
  end loop;
end process;

process(data_ser0_s)
begin
    for i in 0 to nbr_io_ovly -1 loop
        data_ser_o(i) <= data_ser0_s(i);
    end loop;
end process;
end struct;
