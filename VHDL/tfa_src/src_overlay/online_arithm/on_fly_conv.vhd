-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : on_fly_conv.vhd
-- Description  : Converti des données seriel en signed digit representation en
--                entier signé. Voir Digital Arithmetic - D.Ercegovac - p256-p258
--
-- Author       : Raphael Henocq
-- Date         : 05.04.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    05.04.17           Creation
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.overlay_pkg.all;

entity on_fly_conv is
    generic (
            FIFOSIZE : integer :=size_bus_ext
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        en_conv_i      : in std_logic;
        clear_conv_i   : in std_logic;

        -- Sorties
        data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end on_fly_conv;

architecture flow of on_fly_conv is
-- Signals
signal q_reg_pres     : std_logic_vector (FIFOSIZE-1 downto 0);
signal q_reg_fut      : std_logic_vector (FIFOSIZE-1 downto 0);
signal qm_reg_pres    : std_logic_vector (FIFOSIZE-1 downto 0);
signal qm_reg_fut     : std_logic_vector (FIFOSIZE-1 downto 0);
signal q_in_s         : std_logic;
signal qm_in_s        : std_logic;
signal sel_q_s         : std_logic;
signal sel_qm_s        : std_logic;

begin
q_in_s   <= '0' when data_prv_i = "11" or data_prv_i = "00" else
            '1';
qm_in_s  <= '1' when data_prv_i = "11" or data_prv_i = "00" else
            '0';
sel_q_s  <= '0' when data_prv_i = "10" else
            '1';
sel_qm_s <= '0' when data_prv_i = "01" else
            '1';

q_reg_fut <= (others => '0') when clear_conv_i = '1' else
             q_reg_pres when en_conv_i = '0' else
             q_reg_pres(FIFOSIZE-2 downto 0)&q_in_s when sel_q_s = '1' else
             qm_reg_pres(FIFOSIZE-2 downto 0)&q_in_s;

qm_reg_fut <= (others => '1') when clear_conv_i = '1' else
              qm_reg_pres when en_conv_i = '0' else
              qm_reg_pres(FIFOSIZE-2 downto 0)&qm_in_s when sel_qm_s = '1' else
              q_reg_pres(FIFOSIZE-2 downto 0)&qm_in_s;

process(clk_i, rst_i)
begin
    if rst_i = '1' then
        q_reg_pres <= (others => '0');
    elsif rising_edge(clk_i) then
        q_reg_pres <= q_reg_fut;
    end if;
end process;

process(clk_i, rst_i)
begin
    if rst_i = '1' then
        qm_reg_pres <= (others => '0');
    elsif rising_edge(clk_i) then
        qm_reg_pres <= qm_reg_fut;
    end if;
end process;

data_nxt_o <= q_reg_pres when data_prv_i="11" or sel_q_s = '1' else
              qm_reg_pres;

end flow;
