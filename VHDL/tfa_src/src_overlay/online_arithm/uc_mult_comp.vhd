-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : uc_mult_comp.vhd
-- Description  : Gestion de la validité du résultat produit ar la multiplication
--                et la comparaison.
--
-- Author       : Raphael Henocq
-- Date         : 23.06.17
-- Version      : 1.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    23.06.17           Creation
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity uc_mult_comp is
   port (
      clk_i          : in std_logic;
      rst_i          : in std_logic;
      end_of_data_i  : in std_logic;
      acc_nxt_i      : in std_logic;
      data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
      cmp_res_i      : in std_logic;
      cfg_op_i       : in std_logic_vector(7 downto 0);
      en_fu_i        : in std_logic;
      en_get_msb_o   : out std_logic;
      val_nxt_o      : out std_logic;
      ready_o        : out std_logic;
      nLoad_get_o    : out std_logic;
      end_of_data_o  : out std_logic
   );

end uc_mult_comp;

architecture dsp_mult of uc_mult_comp is

  type state_t is  (sInit, sConv, sWaitAcc, sEnd, sComp, sValidComp, sComp1, sWaitAccCmp, sCompWaitEn0, sCompWaitEn1,sCompWaitEn2);
  signal state           : state_t;
  signal state_fut       : state_t;
  signal cpt_pres        : unsigned(DATA_SIZE_LENGTH-1 downto 0);
  signal cpt_fut         : unsigned(DATA_SIZE_LENGTH-1 downto 0);
  signal en_cpt          : std_logic;
  signal en_cpt_s          : std_logic;
  signal load_nCount_cpt : std_logic;
  signal end_of_data_s1  : std_logic;
  signal end_of_data_s2  : std_logic;
  signal det_cmp_s       : std_logic;
  signal det_rising_end_of_data_s : std_logic;


begin

  det_cmp_s <= '1' when (cfg_op_i = FU_OPCODE_EQU or cfg_op_i = FU_OPCODE_NEQ or
                        cfg_op_i = FU_OPCODE_GEQ or cfg_op_i = FU_OPCODE_GRE) else
               '0';

  det_rising_end_of_data_s <= '1' when end_of_data_s2 = '0' and end_of_data_s1 = '1' else
                              '0';

  en_cpt <= en_fu_i when det_cmp_s = '1' else
            en_cpt_s;

  process(state, det_rising_end_of_data_s, acc_nxt_i,det_cmp_s,en_fu_i,cpt_pres,cmp_res_i)
  begin
    state_fut <= state;
    en_get_msb_o <= '0';
    val_nxt_o <= '0';
    ready_o <= '0';
    nLoad_get_o <= '1';
    load_nCount_cpt <= '0';
    end_of_data_o <= '0';
    en_cpt_s <= '0';

    case state is
      when sInit =>
        nLoad_get_o <= '0';
        en_cpt_s <= '1';
        load_nCount_cpt <= '1';
        ready_o <= '1';
        if det_cmp_s = '1' then
          if en_fu_i = '1' then
            state_fut <= sComp;
          end if;
        elsif det_rising_end_of_data_s = '1' then
          state_fut <= sConv;
        end if;

      when sComp => -- Effectue une comparaison
      if cmp_res_i = '1' then    -- Résultat de la comparaison est vrai, il faut transmettre le résultat dès que possible
        state_fut <= sValidComp;
      elsif en_fu_i = '0' then
        state_fut <= sCompWaitEn0;
      elsif cpt_pres = 2 then    -- Il ne reste plus qu'un seul digit à recevoir, le résultat de la comparaison doit être transmis au prochain cycle
        state_fut <= sValidComp;
      end if;

      when sCompWaitEn0 =>
      if en_fu_i = '1' and  cpt_pres = 1 then
        state_fut <= sValidComp;
      elsif en_fu_i = '1' then
        state_fut <= sComp;
      end if;

      when sValidComp =>         -- Transmission  du résultat de la comparaison
        val_nxt_o <= '1';
        if acc_nxt_i = '1' and cpt_pres <= 2 then
          state_fut <= sEnd;
        elsif acc_nxt_i = '1' then
          state_fut <= sEnd;
        else
          state_fut <= sWaitAccCmp;
        end if;

      when sComp1 =>  --Attente de la récéption des derniers digits après avoir produit un résultat valide
        if cpt_pres <= 1 then
          state_fut <= sInit;
        elsif en_fu_i = '0' then
          state_fut <= sCompWaitEn2;
        end if;

      when sCompWaitEn1 =>
        if en_fu_i = '1' and  cpt_pres <= 1 then
          state_fut <= sValidComp;
        elsif en_fu_i = '1' then
          state_fut <= sComp1;
        end if;

      when sCompWaitEn2 =>
        if cpt_pres <= 1 then
          state_fut <= sInit;
        elsif en_fu_i = '1' then
          state_fut <= sComp1;
        end if;

      when sWaitAccCmp => -- Attente d'accéptation du résultat de la comparaison
        val_nxt_o <= '1';
        if acc_nxt_i = '1' and cpt_pres <= 1 then
          state_fut <= sInit;
        elsif  acc_nxt_i = '1' then
          state_fut <= sComp1;
        end if;

      when sConv =>   -- Conversion du résultat parallèle de la multiplication en résultat sériel, et transmission du bit de valide
        en_cpt_s <= '1';
        en_get_msb_o <= '1';
        nLoad_get_o <= '1';
        val_nxt_o <= '1';
        if cpt_pres = 1 and acc_nxt_i = '1' then
          state_fut <= sEnd;
        elsif acc_nxt_i = '0' then
          state_fut <= sWaitAcc;
        end if;

      when sWaitAcc => -- Attente d'accéptation du résultat
        val_nxt_o <= '1';
        if acc_nxt_i = '1' and cpt_pres = 1 then
          state_fut <= sEnd;
        elsif acc_nxt_i = '1' then
          state_fut <= sConv;
        end if;

      when sEnd =>  -- Fin de la donnée à transmettre, permet de générer le digit 11 correspondant à la fin de la donnée
        val_nxt_o <= '1';
        end_of_data_o <= '1';
        if acc_nxt_i = '1' and det_cmp_s = '1' and cpt_pres > 1 and en_fu_i = '1'then
          state_fut <= sComp1;
        elsif acc_nxt_i = '1' and det_cmp_s = '1' and cpt_pres > 1 and en_fu_i = '0'then
          state_fut <= sCompWaitEn2;
        elsif acc_nxt_i = '1' then
          state_fut <= sInit;
        end if;
      end case;
  end process;

  cpt_fut <= cpt_pres when en_cpt = '0' else
             unsigned(data_size_i) when load_nCount_cpt = '1' else
             to_unsigned(0,cpt_fut'length) when cpt_pres = 0 else
             cpt_pres-1;

    process (clk_i, rst_i)
    begin
      if rst_i = '1' then
        state <= sInit;
        cpt_pres <= (others => '0');
        end_of_data_s1  <= '0';
        end_of_data_s2 <= '0';
      elsif rising_edge(clk_i) then
        state <= state_fut ;
        cpt_pres <= cpt_fut;

        end_of_data_s2 <= end_of_data_s1;
        end_of_data_s1 <= end_of_data_i;
      end if;
    end process;

end dsp_mult;

architecture online_mult of uc_mult_comp is

  type state_t is  (sInit,sWait0,sWait1, sConv, sWaitAcc, sEnd, sComp, sValidComp, sComp1, sWaitAccCmp, sCompWaitEn0, sCompWaitEn1,sCompWaitEn2);
  signal state           : state_t;
  signal state_fut       : state_t;
  signal cpt_pres        : unsigned(DATA_SIZE_LENGTH-1 downto 0);
  signal cpt_fut         : unsigned(DATA_SIZE_LENGTH-1 downto 0);
  signal en_cpt          : std_logic;
  signal en_cpt_s          : std_logic;
  signal load_nCount_cpt : std_logic;
  signal end_of_data_s1  : std_logic;
  signal end_of_data_s2  : std_logic;
  signal det_cmp_s       : std_logic;
  signal det_rising_end_of_data_s : std_logic;


begin

  det_cmp_s <= '1' when (cfg_op_i = FU_OPCODE_EQU or cfg_op_i = FU_OPCODE_NEQ or
                        cfg_op_i = FU_OPCODE_GEQ or cfg_op_i = FU_OPCODE_GRE) else
               '0';

  det_rising_end_of_data_s <= '1' when end_of_data_s2 = '0' and end_of_data_s1 = '1' else
                              '0';

  en_cpt <= en_fu_i when det_cmp_s = '1' else
            en_cpt_s;

  process(state, acc_nxt_i,det_cmp_s,en_fu_i,cpt_pres,cmp_res_i,det_rising_end_of_data_s)
  begin
    state_fut <= state;
    en_get_msb_o <= '0';
    val_nxt_o <= '0';
    ready_o <= '0';
    nLoad_get_o <= '0';
    load_nCount_cpt <= '0';
    end_of_data_o <= '0';
    en_cpt_s <= '0';

    case state is
      when sInit =>
        en_get_msb_o <= '1';
        en_cpt_s <= '1';
        load_nCount_cpt <= '1';
        ready_o <= '1';
        if det_cmp_s = '1' then
          if en_fu_i = '1' then
            state_fut <= sComp;
          end if;
        elsif det_rising_end_of_data_s = '1' then
          state_fut <= sWait0;
        end if;

      when sComp => -- Effectue une comparaison
      if cmp_res_i = '1' then    -- Résultat de la comparaison est vrai, il faut transmettre le résultat dès que possible
        state_fut <= sValidComp;
      elsif en_fu_i = '0' then
        state_fut <= sCompWaitEn0;
      elsif cpt_pres = 2 then    -- Il ne reste plus qu'un seul digit à recevoir, le résultat de la comparaison doit être transmis au prochain cycle
        state_fut <= sValidComp;
      end if;

      when sCompWaitEn0 =>
      if en_fu_i = '1' and  cpt_pres = 1 then
        state_fut <= sValidComp;
      elsif en_fu_i = '1' then
        state_fut <= sComp;
      end if;

      when sValidComp =>         -- Transmission  du résultat de la comparaison
        val_nxt_o <= '1';
        if acc_nxt_i = '1' and cpt_pres <= 2 then
          state_fut <= sEnd;
        elsif acc_nxt_i = '1' then
          state_fut <= sEnd;
        else
          state_fut <= sWaitAccCmp;
        end if;

      when sComp1 =>  --Attente de la récéption des derniers digits après avoir produit un résultat valide
        if cpt_pres <= 1 then
          state_fut <= sInit;
        elsif en_fu_i = '0' then
          state_fut <= sCompWaitEn2;
        end if;

      when sCompWaitEn1 =>
        if en_fu_i = '1' and  cpt_pres <= 1 then
          state_fut <= sValidComp;
        elsif en_fu_i = '1' then
          state_fut <= sComp1;
        end if;

      when sCompWaitEn2 =>
        if cpt_pres <= 1 then
          state_fut <= sInit;
        elsif en_fu_i = '1' then
          state_fut <= sComp1;
        end if;

      when sWaitAccCmp => -- Attente d'accéptation du résultat de la comparaison
        val_nxt_o <= '1';
        if acc_nxt_i = '1' and cpt_pres <= 1 then
          state_fut <= sInit;
        elsif  acc_nxt_i = '1' then
          state_fut <= sComp1;
        end if;

      when sWait0 =>
        en_get_msb_o <= '1';
        en_cpt_s <= '1';
        ready_o <= '1';
        load_nCount_cpt <= '1';
        state_fut <= sWait1;

      when sWait1 =>
        en_get_msb_o <= '1';
        en_cpt_s <= '1';
        ready_o <= '1';
        load_nCount_cpt <= '1';
        state_fut <= sConv;

      when sConv =>   -- Conversion du résultat parallèle de la multiplication en résultat sériel, et transmission du bit de valide
        en_cpt_s <= '1';
        en_get_msb_o <= '1';
        nLoad_get_o <= '1';
        val_nxt_o <= '1';
        if cpt_pres = 1 and acc_nxt_i = '1' then
          state_fut <= sEnd;
        elsif acc_nxt_i = '0' then
          state_fut <= sWaitAcc;
        end if;

      when sWaitAcc => -- Attente d'accéptation du résultat
        val_nxt_o <= '1';
        if acc_nxt_i = '1' and cpt_pres = 1 then
          state_fut <= sEnd;
        elsif acc_nxt_i = '1' then
          state_fut <= sConv;
        end if;

      when sEnd =>  -- Fin de la donnée à transmettre, permet de générer le digit 11 correspondant à la fin de la donnée
        val_nxt_o <= '1';
        end_of_data_o <= '1';
        if acc_nxt_i = '1' and det_cmp_s = '1' and cpt_pres > 1 and en_fu_i = '1'then
          state_fut <= sComp1;
        elsif acc_nxt_i = '1' and det_cmp_s = '1' and cpt_pres > 1 and en_fu_i = '0'then
          state_fut <= sCompWaitEn2;
        elsif acc_nxt_i = '1' then
          state_fut <= sInit;
        end if;
      end case;
  end process;

  cpt_fut <= cpt_pres when en_cpt = '0' else
             unsigned(data_size_i) when load_nCount_cpt = '1' else
             to_unsigned(0,cpt_fut'length) when cpt_pres = 0 else
             cpt_pres-1;

    process (clk_i, rst_i)
    begin
      if rst_i = '1' then
        state <= sInit;
        cpt_pres <= (others => '0');
        end_of_data_s1  <= '0';
        end_of_data_s2 <= '0';
      elsif rising_edge(clk_i) then
        state <= state_fut ;
        cpt_pres <= cpt_fut;
        end_of_data_s2 <= end_of_data_s1;
        end_of_data_s1 <= end_of_data_i;
      end if;
    end process;

end online_mult;
