-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : fu_mult_comp.vhd
-- Description  : Calcul le résultat de la multplication des données sériel
--                data_a * data_b et transmet le résultat de manière sériel.
--                Génère le signal de validité du résultat, et le SD de fin de donnée.
--                L'opération est calculé une fois que tous les bits seriel sont réceptionné.
--                Puis le résultat est sérialisé et envoyé grâce à un sérialisateur. Pendant cette
--                étape, le multiplicateur peux commencé à effectuer la multiplication suivante
--                Effectue aussi les comparaison >, =>, = et /= .
--                Seul le résultat de l'opération choisis par cfg_op_i est transmis.
--
--
-- Author       : Raphael Henocq
-- Date         : 23.06.17
-- Version      : 1.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    23.06.17           Creation, algorithme online pour la multiplication
-- 1.0       RHE    26.06.17           Changement d'algorithme pour une multiplication parallèle
-- 2.0       RHE    03.07.17           Ajout de la comparaion
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity fu_mult_comp is
   port (
      clk_i         : in std_logic;
      rst_i         : in std_logic;
      en_i          : in std_logic;
      clear_i       : in std_logic;
      data_a_i      : in std_logic_vector(1 downto 0);   -- |a+,a-|
      data_b_i      : in std_logic_vector(1 downto 0);   -- |b+,b-|
      end_of_data_i : in std_logic;
      acc_nxt_i     : in std_logic;
      data_size_i   : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
      cfg_op_i      : in std_logic_vector(7 downto 0);

      ready_o        : out std_logic;
      val_nxt_mult_o : out std_logic;
      result_o       : out std_logic_vector(1 downto 0)  -- Résultat sérialisé de l'opération
   );

end fu_mult_comp;

architecture struct of fu_mult_comp is

  component mult_comp is
     port (
        clk_i       : in std_logic;
        rst_i       : in std_logic;
        en_i        : in std_logic;
        clear_i     : in std_logic;
        data_a_i    : in std_logic_vector(1 downto 0);
        data_b_i    : in std_logic_vector(1 downto 0);
        cfg_op_i    : in std_logic_vector(7 downto 0);
      end_of_data_i : in std_logic;

        result_o  : out std_logic_vector(size_bus_ext - 1 downto 0);  -- Résultat de la multiplication parallèle
        cmp_res_o : out std_logic;
        ncmp_o : out std_logic
     );
  end component;

    component get_in_MSB_order is                                    -- Sérialisateur
        generic (
                FIFOSIZE: integer:= 8;
                DATA_SIZE_LENGTH: integer := 32
        );
        port ( clk_i         : in  std_logic;
               rst_i         : in  std_logic;
               enable_i      : in  std_logic;
               nLoad_get_i   : in  std_logic;
               data_size_i   : in  std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
               D_load_i      : in  std_logic_vector (size_bus_ext-1 downto 0);
               D_o           : out std_logic;
               signe_bit_o   : out std_logic
        );
    end component;

    component uc_mult_comp is
       port (
          clk_i          : in std_logic;
          rst_i          : in std_logic;
          end_of_data_i  : in std_logic;
          acc_nxt_i      : in std_logic;
          data_size_i    : in std_logic_vector(DATA_SIZE_LENGTH-1 downto 0);
          cmp_res_i      : in std_logic;
          cfg_op_i       : in std_logic_vector(7 downto 0);
          en_fu_i        : in std_logic;
          en_get_msb_o   : out std_logic;
          val_nxt_o      : out std_logic;
          ready_o        : out std_logic;
          nLoad_get_o    : out std_logic;
          end_of_data_o  : out std_logic
       );
    end component;

    signal result_s        : std_logic_vector(size_bus_ext - 1 downto 0); -- Résultat de la multiplication parallèle
    signal sign_bit_s      : std_logic;
    signal en_get_msb      : std_logic;
    signal out_get_msb_s   : std_logic;
    signal end_of_data_o_s : std_logic;
    signal nLoad_get_s     : std_logic;
    signal cmp_res_s       : std_logic;  -- Résultat de la comparaison
    signal val_cmp_res_s   : std_logic;  -- Validité de la comparaison
    signal det_cmp_s       : std_logic;  -- Test d'opérateur de comparaison
    signal ncmp_s          : std_logic;
    signal en1_s           : std_logic;
    signal end_of_data1_s  : std_logic;

begin

  det_cmp_s <= '1' when cfg_op_i = FU_OPCODE_EQU or cfg_op_i = FU_OPCODE_NEQ or
                        cfg_op_i = FU_OPCODE_GEQ or cfg_op_i = FU_OPCODE_GRE else
               '0';

  mult : entity work.mult_comp(dsp_mult) --  online_mult or dsp_mult
  port map (
     clk_i    => clk_i,
     rst_i    => rst_i,
     en_i     => en1_s,
     clear_i  => clear_i,
     data_a_i => data_a_i,
     data_b_i => data_b_i,
     cfg_op_i => cfg_op_i,
     end_of_data_i => end_of_data1_s,
     result_o => result_s,
     cmp_res_o => cmp_res_s,
     ncmp_o => ncmp_s
  );

  get_in_msb : get_in_MSB_order
  generic map(
          FIFOSIZE => size_bus_ext,
          DATA_SIZE_LENGTH => DATA_SIZE_LENGTH
  )
  port map( clk_i         => clk_i,
         rst_i         => rst_i,
         enable_i      => en_get_msb,
         nLoad_get_i   => nLoad_get_s,
         data_size_i   => data_size_i,
         D_load_i      => result_s,
         D_o           => out_get_msb_s,
         signe_bit_o   => sign_bit_s
  );

  uc: entity work.uc_mult_comp(dsp_mult) -- online_mult or dsp_mult
     port map(
        clk_i          => clk_i,
        rst_i          => rst_i,
        end_of_data_i  => end_of_data1_s,
        acc_nxt_i      => acc_nxt_i,
        data_size_i    => data_size_i,
        cmp_res_i      => val_cmp_res_s,
        cfg_op_i       => cfg_op_i,
        en_fu_i        => en1_s,
        en_get_msb_o   => en_get_msb,
        val_nxt_o      => val_nxt_mult_o,
        ready_o        => ready_o,
        nLoad_get_o    => nLoad_get_s,
        end_of_data_o  => end_of_data_o_s
     );

  val_cmp_res_s <= cmp_res_s or ncmp_s;
  result_o <=  "11" when end_of_data_o_s = '1' else
               '0' & cmp_res_s when det_cmp_s = '1' else
               ('0' & out_get_msb_s) when sign_bit_s = '0' else  -- positif number
               out_get_msb_s & '0';                              -- negativ number

process (clk_i, rst_i)
begin
  if rst_i = '1' then
    en1_s <= '0';
    end_of_data1_s <= '0';
  elsif rising_edge(clk_i) then
    en1_s <= en_i;
  end_of_data1_s <= end_of_data_i;
  end if;
end process;
end struct;
