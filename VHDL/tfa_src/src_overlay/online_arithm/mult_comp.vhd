-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : mult_comp.vhd
-- Description  : Effectue une multiplication  et les comparaisons
--                >, =>, /= et =. Récéptionne des données Signed Digit sériel, les
--                désérialises, et produit le résultat de la multiplication, ainsi
--                que celui de la comparaison sélectionné par cfg_op_i
--
-- Author       : Raphael Henocq
-- Date         : 23.06.17
-- Version      : 1.0
--
-- Remarques :
--       data_a_full_s contiens la valeur de A dans la multiplication, et celle de Q pour la division
--       data_b_full_s contiens la valeur de B dans la multiplication, et celle du diviseur pour la division
--       Afin d'utlisier l'une ou l'autre des architectures, il faut procéder à plusieurs modification :
--              - dans fu_mult_comp.vhd : Choisir la même architecture pour mult_comp et uc_mult_comp
--              - dans fu_all.vhd : modifier les signaaux dat_prv_mult_cmp_1_s et dat_prv_mult_cmp_2_s (commenter/décommenter selon l'architecture)
--                                  modifier la valeur de delta pour le cas de la multplication (0 pour dsp, 4 pour online)
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    23.06.17           Creation
-- 1.0       RHE    03.07.17           Ajout de la comparaison
-- 2.0       RHE    11.07.17           Création d'une architecture pour la multiplication dsp, et d'une pour la multiplication online
-------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use IEEE.Numeric_Std.all;

use work.overlay_pkg.all;

entity mult_comp is
   port (
     clk_i    : in std_logic;
     rst_i    : in std_logic;
     en_i     : in std_logic;
     clear_i  : in std_logic;
     data_a_i : in std_logic_vector(1 downto 0);
     data_b_i : in std_logic_vector(1 downto 0);
     cfg_op_i : in std_logic_vector(7 downto 0);
     end_of_data_i : in std_logic;

     result_o  : out std_logic_vector(size_bus_ext - 1 downto 0);
     cmp_res_o : out std_logic;
     ncmp_o : out std_logic
   );

end mult_comp;

architecture dsp_mult of mult_comp is
component on_fly_conv is          -- Sérialisateur
    generic (
            FIFOSIZE : integer :=32
    );
    port (
      -- Entrées
      clk_i          : in std_logic;
      rst_i          : in std_logic;
      data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
      en_conv_i      : in std_logic;
      clear_conv_i   : in std_logic;
      -- Sorties
      data_nxt_o     : out std_logic_vector(FIFOSIZE-1 downto 0)
    );
end component;


signal data_a_full_s : std_logic_vector(size_bus_ext-1 downto 0);
signal data_b_full_s : std_logic_vector(size_bus_ext-1 downto 0);
signal data_a_cmp_s  : std_logic_vector(2 downto 0);
signal data_b_cmp_s  : std_logic_vector(2 downto 0);
signal sign_mult_a   : signed(size_bus_ext/2-1 downto 0);
signal sign_mult_b   : signed(size_bus_ext/2-1 downto 0);
signal cmp_grt_s     : std_logic;
signal cmp_equ_s     : std_logic;
signal cmp_neq_s     : std_logic;
signal cmp_let_s     : std_logic;

begin

  convA : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_a_i,
      en_conv_i      => en_i,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_a_full_s
    );

  convB : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_b_i,
      en_conv_i      => en_i,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_b_full_s
    );

    sign_mult_a <= resize(signed(data_a_full_s), sign_mult_a'length);
    sign_mult_b <= resize(signed(data_b_full_s), sign_mult_b'length);

    data_a_cmp_s <= data_a_full_s(data_a_full_s'length-1) & data_a_full_s(1) & '0' when data_a_i /= "11" or data_b_i /="11" else
                    data_a_full_s(data_a_full_s'length-1) & data_a_full_s(1 downto 0);
    data_b_cmp_s <= data_b_full_s(data_b_full_s'length-1) & data_b_full_s(1) & '0' when data_a_i /= "11" or data_b_i /="11" else
                    data_b_full_s(data_b_full_s'length-1) & data_b_full_s(1 downto 0);

    cmp_grt_s <= '1' when signed(data_a_cmp_s) > signed(data_b_cmp_s) else '0';
    cmp_let_s <= '1' when signed(data_a_cmp_s) < signed(data_b_cmp_s) else '0';
    cmp_equ_s <= '1' when data_a_cmp_s = data_a_cmp_s else
                 '0';
    cmp_neq_s <= '1' when data_a_cmp_s /= data_b_cmp_s else
                 '0';

    cmp_res_o <= cmp_grt_s when cfg_op_i = FU_OPCODE_GRE else
                 (cmp_grt_s  or cmp_equ_s) when cfg_op_i = FU_OPCODE_GEQ and (data_a_i="11" or data_b_i = "11") else
                 cmp_grt_s when cfg_op_i = FU_OPCODE_GEQ else
                 cmp_equ_s when cfg_op_i = FU_OPCODE_EQU and (data_a_i="11" or data_b_i = "11") else -- Attente de récéption de fin de donnée pour produire le bon résultat de l'égalité/inégalitée
                 not cmp_equ_s when cfg_op_i = FU_OPCODE_NEQ else
                 '0';
    ncmp_o <= cmp_let_s when cfg_op_i = FU_OPCODE_GRE else
              cmp_neq_s when cfg_op_i = FU_OPCODE_EQU else
              (cmp_let_s or cmp_neq_s) when cfg_op_i = FU_OPCODE_GEQ else
              '0';

    process(clk_i, rst_i, end_of_data_i)
    begin
      if rst_i='1'then
        result_o <= (others => '0');
      elsif rising_edge(clk_i) then
        if en_i = '1' then
          if end_of_data_i = '1' then
              result_o <= std_logic_vector(sign_mult_a * sign_mult_b);  -- Permet un maintiens du résultat
          end if;
        end if;
      end if;
    end process;

end dsp_mult;

architecture online_mult of mult_comp is
component on_fly_conv is
    generic (
            FIFOSIZE : integer :=32
    );
    port (
        -- Entrées
        clk_i          : in std_logic;
        rst_i          : in std_logic;
        data_prv_i     : in std_logic_vector(size_bus-1 downto 0); --donnée sérialisée
        en_conv_i      : in std_logic;
        clear_conv_i   : in std_logic;

        -- Sorties
        data_nxt_o     : out std_logic_vector(size_bus_ext-1 downto 0)
    );
end component;

signal data_a1_s         : std_logic_vector(1 downto 0);
signal data_b1_s         : std_logic_vector(1 downto 0);
signal data_a_conv_s     : std_logic_vector(1 downto 0);
signal en_conv_a_s       : std_logic;
signal en_conv_b_s       : std_logic;
signal a_b_dec           : std_logic_vector(size_bus_ext+2 downto 0);   -- contiens (a+b)*2^-3
signal data_a_full_s     : std_logic_vector(size_bus_ext-1 downto 0);
signal data_b_full_s     : std_logic_vector(size_bus_ext-1 downto 0);
signal data_a_full_cmp_s : std_logic_vector(size_bus_ext-1 downto 0);
signal data_b_full_cmp_s : std_logic_vector(size_bus_ext-1 downto 0);
signal data_a_full_sel_s : std_logic_vector(size_bus_ext-1 downto 0);
signal data_b_full_sel_s : std_logic_vector(size_bus_ext-1 downto 0);
signal cpt_fut           : std_logic_vector(4 downto 0);            -- position du bit de poid faible de v̂ conpris entre 2 et 31
signal cpt_pres          : std_logic_vector(4 downto 0);
signal en_cpt_s          : std_logic;
signal v_s               : std_logic_vector(size_bus_ext+2 downto 0);
signal v_norm_s          : std_logic_vector(size_bus_ext+2 downto 0);
signal w_fut             : std_logic_vector(size_bus_ext+2 downto 0);
signal w_pres            : std_logic_vector(size_bus_ext+2 downto 0);
signal en_s              : std_logic;
signal result_s          : std_logic_vector(size_bus_ext - 1 downto 0);
signal carry_s           : unsigned(1 downto 0);   -- Carry for data_a and data_b !
signal cmp_grt_s     : std_logic;
signal cmp_equ_s     : std_logic;

begin

  convA : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_a_conv_s,
      en_conv_i      => en_conv_a_s,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_a_full_s
    );

  convB : on_fly_conv
    generic map (FIFOSIZE => size_bus_ext)
    port map (
      clk_i          => clk_i,
      rst_i          => rst_i,
      data_prv_i     => data_b_i,
      en_conv_i      => en_conv_b_s,
      clear_conv_i   => clear_i,
      data_nxt_o    => data_b_full_s
    );

  data_a_conv_s <= data_a1_s when cfg_op_i = FU_OPCODE_MUL else
                   data_a_i; -- comparaison
  data_a_full_cmp_s <= data_a_full_s when data_a_i="11" else
                       '0' & data_a_full_s(data_a_full_s'length-1 downto 1);
  data_b_full_cmp_s <= data_b_full_s when data_b_i="11" else
                      '0' & data_b_full_s(data_b_full_s'length-1 downto 1);
  cmp_grt_s <= '1' when to_integer(signed(data_a_full_cmp_s)) > to_integer(signed(data_b_full_cmp_s)) else '0';
  ncmp_o <= '1' when to_integer(signed(data_a_full_cmp_s)) < to_integer(signed(data_b_full_cmp_s)) else '0';
  cmp_equ_s <= '0' when data_a_i /= "11" and data_b_i /= "11" else  -- Attente de récéption de fin de donnée pour produire le bon résultat de l'égalité/inégalitée
               '1' when data_a_full_s = data_b_full_s else
               '0';

  cmp_res_o <= cmp_grt_s when cfg_op_i = FU_OPCODE_GRE else
               (cmp_grt_s  or cmp_equ_s) when cfg_op_i = FU_OPCODE_GEQ else
               cmp_equ_s when cfg_op_i = FU_OPCODE_EQU else
               not cmp_equ_s when data_a_i="11" and data_b_i = "11" else -- Attente de récéption de fin de donnée pour produire le bon résultat de l'égalité/inégalitée
               '0';

  en_conv_a_s <= '0' when cfg_op_i = FU_OPCODE_MUL and data_a1_s = "11" else
                 '0' when cfg_op_i /= FU_OPCODE_MUL and data_a_i = "11" else
                 en_i;
  en_conv_b_s <= '0' when data_b_i = "11" else
                 en_i;
  en_cpt_s <= en_conv_b_s and en_s;
  carry_s <= "10" when data_b1_s = "10" and data_a1_s = "10" else
             "01" when data_b1_s = "10" or data_a1_s = "10" else
             "00";
  data_a_full_sel_s <= (others => '0') when (data_b1_s = "00" or data_b1_s = "11") else
                       data_a_full_s(data_a_full_s'length-2 downto 0) & '0' when data_b1_s = "01" else
                       not(data_a_full_s(data_a_full_s'length-2 downto 0) & '0');

  data_b_full_sel_s <= (others => '0') when (data_a1_s = "00" or data_a1_s = "11") else
                       data_b_full_s when data_a1_s= "01" else
                      not data_b_full_s;
  a_b_dec <= "000" & std_logic_vector(unsigned(data_a_full_sel_s) + unsigned(data_b_full_sel_s) + carry_s);
  v_norm_s <=  std_logic_vector(unsigned(a_b_dec) + unsigned(w_pres(w_pres'length-2 downto 0) & '0'));
  v_s <= v_norm_s;-- when en_cpt_s = '0' else
        -- v_norm_s(v_s'length-2 downto 0) & '0'; -- Décalage à droite pour rester dans la même norme que data_b_full qui est décalé
  cpt_fut <= std_logic_vector(unsigned(cpt_pres)+1);

  process(v_s, en_cpt_s, cpt_pres)
  variable v_v : std_logic_vector(2 downto 0);  -- correspnd à v̂
  variable p_v : std_logic;
  variable w_v : std_logic_vector(size_bus_ext+2 downto 0);
  begin
    w_v := v_s;
    v_v := v_s(to_integer(unsigned(cpt_pres))+2 downto to_integer(unsigned(cpt_pres)));
    if v_v = "000" or v_v = "001" then    -- Simplifie SELM
      p_v := '0';
    else
      p_v := '1';
    end if;
    w_v(to_integer(unsigned(cpt_pres))+1) := v_v(1) xor p_v;
    if en_cpt_s = '1' then
      w_v := w_v(w_v'length-2 downto 0) & '0';
    end if;
    w_fut <= w_v;
  end process;

  process(clk_i, rst_i, clear_i)
  begin
    if rst_i = '1' or clear_i = '1' then
      en_s <= '0';
    elsif rising_edge(clk_i) then
      en_s <= en_i;
    end if;
  end process;

  process(clk_i, rst_i, clear_i)
  begin
    if rst_i = '1' or clear_i = '1' then
      data_a1_s <= (others => '0');
      data_b1_s <= (others => '0');
      cpt_pres  <= "00011";  -- position initial du bit de poid faible de v̂ est 3
      w_pres <= (others => '0');
      result_s <= (others => '0');
    elsif (rising_edge(clk_i)) then
      if en_s = '1' then
        data_a1_s <= data_a_i;
        data_b1_s <= data_b_i;
        w_pres <= w_fut;
        result_s <=  w_pres(w_pres'length-4 downto 0);
      end if;
      if en_cpt_s = '1' then
        cpt_pres <= cpt_fut;
      end if;
    end if;
  end process;

  result_o <= result_s;
end online_mult;
