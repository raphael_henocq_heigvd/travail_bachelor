-------------------------------------------------------------------------------
-- HEIG-VD, Haute Ecole d'Ingenierie et de Gestion du canton de Vaud
-- Institut REDS, Reconfigurable & Embedded Digital Systems
--
-- File         : constant.vhd
-- Description  : Effectue une addition entre les trois entrées a, b et c. Transmet
--                le carry de l'opération et le résultat en sortie
--
-- Author       : Raphael Henocq
-- Date         : 14.07.17
-- Version      : 0.0
--
-- Dependencies :
--
--| Modifications |------------------------------------------------------------
-- Version   Author Date               Description
-- 0.0       RHE    14.07.17           Creation
-------------------------------------------------------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.Numeric_Std.all;
    use work.overlay_pkg.all;
    use IEEE.math_real.all;

entity constants is
    Port (
        clk_i  : in std_logic;
        rst_i  : in std_logic;
        en_i   : in std_logic;
        dat_prv_i : in std_logic_vector(size_bus-1 downto 0);
        val_prv_i : in std_logic;
        acc_nxt_i : in std_logic;
        dat_nxt_o : out std_logic_vector(size_bus-1 downto 0);
        val_nxt_o : out std_logic;
        acc_prv_o : out std_logic
    );
end constants;

architecture data_flow of constants is
signal cpt_pres : unsigned(integer(ceil(log2(real(size_bus_ext)))) downto 0);
signal cpt_fut : unsigned(integer(ceil(log2(real(size_bus_ext)))) downto 0);
signal data_pres : std_logic_vector((size_bus_ext*2)-1 downto 0);
signal data_fut : std_logic_vector((size_bus_ext*2)-1 downto 0);
signal dat_nxt_s : std_logic_vector (size_bus-1 downto 0);

begin

  process (data_pres, dat_prv_i)
  begin
    data_fut <= data_pres;
    data_fut(to_integer(cpt_pres)) <= dat_prv_i(1);
    data_fut(to_integer(cpt_pres-1)) <= dat_prv_i(0);
  end process;

  acc_prv_o <= en_i;
  val_nxt_o <= not en_i;
  dat_nxt_s(1) <= data_pres(to_integer(cpt_pres));
  dat_nxt_s(0) <= data_pres(to_integer(cpt_pres-1));

  dat_nxt_o <= dat_nxt_s;
  cpt_fut <= to_unsigned(size_bus_ext*2-1,cpt_fut'length) when dat_prv_i = "11" and val_prv_i = '1' and en_i = '1' else
             cpt_pres-2 when en_i = '1' and val_prv_i = '1' else
             to_unsigned(size_bus_ext*2-1,cpt_fut'length) when dat_nxt_s = "11" and acc_nxt_i = '1' and en_i = '0' else
             cpt_pres-2 when acc_nxt_i = '1' and en_i = '0' else
             cpt_pres;


  process (clk_i, rst_i)
  begin
    if rst_i = '1' then
      data_pres <= (others => '0');
      cpt_pres <= to_unsigned(size_bus_ext*2-1,cpt_fut'length);
    elsif rising_edge(clk_i) then
      cpt_pres <= cpt_fut;
      if en_i='1' then
        data_pres <= data_fut;
      end if;
    end if;
  end process;

end data_flow;
