------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : cfg_data_patern_overlay.vhd
-- Author               : Convers Anthony
-- Date                 : 15.09.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--
------------------------------------------------------------------------------------------
-- Dependencies : overlay_pkg package
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.overlay_pkg.all;

entity cfg_data_patern_overlay is
    Port ( clk : in  STD_LOGIC;
           clk_usr : in  STD_LOGIC;
           rst_config_i : in  STD_LOGIC;
           rst_overlay_i : in  STD_LOGIC;
           clk_ram : in  STD_LOGIC;
           ram_we_i : in  STD_LOGIC_VECTOR (0 downto 0);
           ram_addr_i : in  STD_LOGIC_VECTOR (10 downto 0);
           ram_din_i : in  STD_LOGIC_VECTOR (31 downto 0);
           ram_dout_o : out  STD_LOGIC_VECTOR (31 downto 0);
           dat_prv_i : in  STD_LOGIC_VECTOR (size_bus_ext-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size_bus_ext+bit_cmd_mux_io-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_i : in  STD_LOGIC;
           data_size_i : in std_logic_vector (data_size_length -1 downto 0));
end cfg_data_patern_overlay;

architecture cfg_data_patern_overlay_arch of cfg_data_patern_overlay is

-- signal intern
--cfg mux
signal cfg_x_mux_s : cfg_mux_ovly_type;

--ram_32b config all
signal ram_web_s : std_logic_vector(0 downto 0);
signal ram_addrb_s : std_logic_vector(10 downto 0);
signal ram_dinb_s : std_logic_vector(31 downto 0);
signal ram_doutb_s : std_logic_vector(31 downto 0);

--ram_32b config patern
signal ram_pat_wea_s : std_logic_vector(0 downto 0);
signal ram_pat_addra_s : std_logic_vector(7 downto 0);
signal ram_pat_dina_s : std_logic_vector(31 downto 0);
signal ram_pat_douta_s : std_logic_vector(31 downto 0);

signal ram_pat_web_s : std_logic_vector(0 downto 0);
signal ram_pat_addrb_s : std_logic_vector(7 downto 0);
signal ram_pat_dinb_s : std_logic_vector(31 downto 0);
signal ram_pat_doutb_s : std_logic_vector(31 downto 0);

signal cfg_pat_available_s : std_logic;
signal cfg_pat_length_s : std_logic_vector(7 downto 0);

--Signal intern
signal dat_01_s : std_logic_vector(size_bus_ext-1 downto 0);
signal val_01_s : std_logic;
signal acc_01_s : std_logic;

signal dat_02_s : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
signal val_02_s : std_logic;
signal acc_02_s : std_logic;



begin

-------------------- configuration interfaces (overlay + patern)
U01: entity work.ram_32b port map (
          clka => clk_ram,
          --rsta => reset,
          wea => ram_we_i,
          addra => ram_addr_i,
          dina => ram_din_i,
          douta => ram_dout_o,
          clkb => clk,
          --rstb => reset,
          web => ram_web_s,
          addrb => ram_addrb_s,
          dinb => ram_dinb_s,
          doutb => ram_doutb_s);

U02: entity work.config_overlay_patern_in port map (clk,rst_config_i,cfg_x_mux_s,ram_web_s,ram_addrb_s,ram_dinb_s,ram_doutb_s,
                                                    cfg_pat_available_s,cfg_pat_length_s,ram_pat_wea_s,ram_pat_addra_s,ram_pat_dina_s,ram_pat_douta_s);

-------------------- configuration patern
U03: entity work.ram_32b_pat port map (
          clka => clk,
          --rsta => reset,
          wea => ram_pat_wea_s,
          addra => ram_pat_addra_s,
          dina => ram_pat_dina_s,
          douta => ram_pat_douta_s,
          clkb => clk_usr,
          --rstb => reset,
          web => ram_pat_web_s,
          addrb => ram_pat_addrb_s,
          dinb => ram_pat_dinb_s,
          doutb => ram_pat_doutb_s);


U04: entity work.d_fifo_ext generic map(size_bus_ext) port map (clk_usr,rst_overlay_i,dat_prv_i,val_prv_i,acc_prv_o,dat_01_s,val_01_s,acc_01_s);

U05: entity work.config_dat_input port map (
          clk => clk_usr,
          reset => rst_overlay_i,
          cfg_available_i => cfg_pat_available_s,
          cfg_length_i => cfg_pat_length_s,
          ram_we_o => ram_pat_web_s,
          ram_addr_o => ram_pat_addrb_s,
          ram_din_o => ram_pat_dinb_s,
          ram_dout_i => ram_pat_doutb_s,
          dat_prv_i => dat_01_s,
          val_prv_i => val_01_s,
          acc_prv_o => acc_01_s,
          dat_nxt_o => dat_02_s,
          val_nxt_o => val_02_s,
          acc_nxt_i => acc_02_s);


-------------------- overlay with data in and out interfaces
U06: entity work.data_in_out_overlay port map (clk_usr,clk,rst_overlay_i,cfg_x_mux_s,dat_02_s,val_02_s,acc_02_s,dat_nxt_o,val_nxt_o,acc_nxt_i,data_size_i);

end cfg_data_patern_overlay_arch;
