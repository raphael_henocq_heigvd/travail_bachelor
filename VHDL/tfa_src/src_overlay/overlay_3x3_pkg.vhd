------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : overlay_3x3_pkg.vhd
-- Author               : Convers Anthony
-- Date                 : 19.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package overlay_3x3_pkg is

constant size_bus_ext   : integer := 32;        -- Modif by RHE
constant size_bus       : integer := 3;         -- Modif by RHE
constant nbr_line_cell : integer := 3;
constant nbr_all_cell : integer := 9;

type dat_ovly_type is array (nbr_line_cell-1 downto 0) of std_logic_vector (size_bus-1 downto 0);
type bit_ovly_type is array (nbr_line_cell-1 downto 0) of std_logic;

type cfg_mux_ovly_type is array (nbr_all_cell-1 downto 0) of std_logic_vector (17 downto 0);
--type cfg_fs_ovly_type is array (nbr_all_cell-1 downto 0) of std_logic_vector (23 downto 0);

end overlay_3x3_pkg;

