------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : cfg_cell_mux2fs.vhd
-- Author               : Convers Anthony
-- Date                 : 15.02.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cfg_cell_mux2fs is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           cfg_mux_i : in STD_LOGIC_VECTOR (31 downto 0);
           cfg_mux_n_o : out STD_LOGIC_VECTOR (2 downto 0);
           cfg_mux_w_o : out STD_LOGIC_VECTOR (2 downto 0);
           cfg_mux_e_o : out STD_LOGIC_VECTOR (2 downto 0);
           cfg_mux_s_o : out STD_LOGIC_VECTOR (2 downto 0);
           cfg_mux_fu1_o : out STD_LOGIC_VECTOR (2 downto 0);
           cfg_mux_fu2_o : out STD_LOGIC_VECTOR (2 downto 0);
           cfg_mux_fsel_o : out STD_LOGIC_VECTOR (2 downto 0);
           cfg_fs_n_o : out STD_LOGIC_VECTOR (5 downto 0);
           cfg_fs_w_o : out STD_LOGIC_VECTOR (5 downto 0);
           cfg_fs_e_o : out STD_LOGIC_VECTOR (5 downto 0);
           cfg_fs_s_o : out STD_LOGIC_VECTOR (5 downto 0);
           cfg_fs_f_o : out STD_LOGIC_VECTOR (3 downto 0);
           cfg_fu_const_o : out STD_LOGIC_VECTOR (1 downto 0);
           cfg_fu_op_o : out STD_LOGIC_VECTOR (7 downto 0));
end cfg_cell_mux2fs;

architecture cfg_cell_mux2fs_arch of cfg_cell_mux2fs is

signal cfg_mux_reg1 : std_logic_vector(31 downto 0);
signal cfg_mux_reg2 : std_logic_vector(31 downto 0);

signal cfg_fs_n_s : std_logic_vector(5 downto 0);
signal cfg_fs_w_s : std_logic_vector(5 downto 0);
signal cfg_fs_e_s : std_logic_vector(5 downto 0);
signal cfg_fs_s_s : std_logic_vector(5 downto 0);
signal cfg_fs_f_s : std_logic_vector(3 downto 0);

signal en_north_s : std_logic;
signal en_west_s : std_logic;
signal en_east_s : std_logic;
signal en_south_s : std_logic;
signal en_fu1_s : std_logic;
signal en_fu2_s : std_logic;
signal en_fsel_s : std_logic;
signal mux_north_s : std_logic_vector(1 downto 0);
signal mux_west_s : std_logic_vector(1 downto 0);
signal mux_east_s : std_logic_vector(1 downto 0);
signal mux_south_s : std_logic_vector(1 downto 0);
signal mux_fu1_s : std_logic_vector(1 downto 0);
signal mux_fu2_s : std_logic_vector(1 downto 0);
signal mux_fsel_s : std_logic_vector(1 downto 0);

begin

mux_north_s <= cfg_mux_reg1(1 downto 0);
en_north_s <= cfg_mux_reg1(2);

mux_west_s <= cfg_mux_reg1(4 downto 3);
en_west_s <= cfg_mux_reg1(5);

mux_east_s <= cfg_mux_reg1(7 downto 6);
en_east_s <= cfg_mux_reg1(8);

mux_south_s <= cfg_mux_reg1(10 downto 9);
en_south_s <= cfg_mux_reg1(11);

mux_fu1_s <= cfg_mux_reg1(13 downto 12);
en_fu1_s <= cfg_mux_reg1(14);

mux_fu2_s <= cfg_mux_reg1(16 downto 15);
en_fu2_s <= cfg_mux_reg1(17);

mux_fsel_s <= cfg_mux_reg1(19 downto 18);
en_fsel_s <= cfg_mux_reg1(20);

process(clk,reset)
begin
    if reset='1' then
        cfg_mux_reg1 <= (others => '0');
        cfg_mux_reg2 <= (others => '0');
        cfg_fs_n_s <= (others => '1');
        cfg_fs_w_s <= (others => '1');
        cfg_fs_e_s <= (others => '1');
        cfg_fs_s_s <= (others => '1');
        cfg_fs_f_s <= (others => '1');
    elsif (clk'event and clk='1') then
        cfg_mux_reg1 <= cfg_mux_i;
        cfg_mux_reg2 <= cfg_mux_reg1;
        
        if cfg_mux_reg2/=cfg_mux_reg1 then      --detect a change in cfg_mux_i
            cfg_fs_n_s <= (others => '1');
            cfg_fs_w_s <= (others => '1');
            cfg_fs_e_s <= (others => '1');
            cfg_fs_s_s <= (others => '1');
            cfg_fs_f_s <= (others => '1');
            
            --Mux North out
            if en_north_s='1' then
                case mux_north_s is
                    when "00" => cfg_fs_w_s(0)<='0';
                    when "01" => cfg_fs_e_s(0)<='0';
                    when "10" => cfg_fs_s_s(0)<='0';
                    when others =>  cfg_fs_f_s(0)<='0';     --"11"
                end case;
            end if;
            
            --Mux West out
            if en_west_s='1' then
                case mux_west_s is
                    when "00" => cfg_fs_n_s(0)<='0';
                    when "01" => cfg_fs_e_s(1)<='0';
                    when "10" => cfg_fs_s_s(1)<='0';
                    when others =>  cfg_fs_f_s(1)<='0';     --"11"
                end case;
            end if;
            
            --Mux East out
            if en_east_s='1' then
                case mux_east_s is
                    when "00" => cfg_fs_n_s(1)<='0';
                    when "01" => cfg_fs_w_s(1)<='0';
                    when "10" => cfg_fs_s_s(2)<='0';
                    when others =>  cfg_fs_f_s(2)<='0';     --"11"
                end case;
            end if;
            
            --Mux South out
            if en_south_s='1' then
                case mux_south_s is
                    when "00" => cfg_fs_n_s(2)<='0';
                    when "01" => cfg_fs_w_s(2)<='0';
                    when "10" => cfg_fs_e_s(2)<='0';
                    when others =>  cfg_fs_f_s(3)<='0';     --"11"
                end case;
            end if;
            
            --Mux Fu1 in
            if en_fu1_s='1' then
                case mux_fu1_s is
                    when "00" => cfg_fs_n_s(3)<='0';
                    when "01" => cfg_fs_w_s(3)<='0';
                    when "10" => cfg_fs_e_s(3)<='0';
                    when others =>  cfg_fs_s_s(3)<='0';     --"11"
                end case;
            end if;
            
            --Mux Fu2 in
            if en_fu2_s='1' then
                case mux_fu2_s is
                    when "00" => cfg_fs_n_s(4)<='0';
                    when "01" => cfg_fs_w_s(4)<='0';
                    when "10" => cfg_fs_e_s(4)<='0';
                    when others =>  cfg_fs_s_s(4)<='0';     --"11"
                end case;
            end if;

            --Mux Fsel in
            if en_fsel_s='1' then
                case mux_fsel_s is
                    when "00" => cfg_fs_n_s(5)<='0';
                    when "01" => cfg_fs_w_s(5)<='0';
                    when "10" => cfg_fs_e_s(5)<='0';
                    when others =>  cfg_fs_s_s(5)<='0';     --"11"
                end case;
            end if;
            
        else
            cfg_fs_n_s <= cfg_fs_n_s;
            cfg_fs_w_s <= cfg_fs_w_s;
            cfg_fs_e_s <= cfg_fs_e_s;
            cfg_fs_s_s <= cfg_fs_s_s;
            cfg_fs_f_s <= cfg_fs_f_s;
        end if;
        
    end if;
end process;

--configuration mux (output)
cfg_mux_n_o <= cfg_mux_reg2(2 downto 0);
cfg_mux_w_o <= cfg_mux_reg2(5 downto 3);
cfg_mux_e_o <= cfg_mux_reg2(8 downto 6);
cfg_mux_s_o <= cfg_mux_reg2(11 downto 9);
cfg_mux_fu1_o <= cfg_mux_reg2(14 downto 12);
cfg_mux_fu2_o <= cfg_mux_reg2(17 downto 15);
cfg_mux_fsel_o <= cfg_mux_reg2(20 downto 18);

--configuration fs (output)
cfg_fs_n_o <= cfg_fs_n_s;
cfg_fs_w_o <= cfg_fs_w_s;
cfg_fs_e_o <= cfg_fs_e_s;
cfg_fs_s_o <= cfg_fs_s_s;
cfg_fs_f_o <= cfg_fs_f_s;

--configuration fu operation (output)
cfg_fu_const_o <= cfg_mux_reg2(22 downto 21);
cfg_fu_op_o <= cfg_mux_reg2(31 downto 24);

end cfg_cell_mux2fs_arch;
