------------------------------------------------------------------------------------------
-- HEIG-VD ///////////////////////////////////////////////////////////////////////////////
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute ////////////////////////////////////////////////////////////////////////
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : d_fs_6.vhd
-- Author               : Convers Anthony
-- Date                 : 13.01.2016
--
-- Context              : HPA
--
------------------------------------------------------------------------------------------
-- Description : Overlay design
--   
------------------------------------------------------------------------------------------
-- Dependencies : 
--   
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    See header  ACS           Initial version.
--
------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity d_fs_6 is
    generic (size: integer:= 8);
    Port ( mask_sel_i : in  STD_LOGIC_VECTOR (5 downto 0);
           dat_prv_i : in  STD_LOGIC_VECTOR (size-1 downto 0);
           val_prv_i : in  STD_LOGIC;
           acc_prv_o : out  STD_LOGIC;
           dat_nxt_o : out  STD_LOGIC_VECTOR (size-1 downto 0);
           val_nxt_o : out  STD_LOGIC;
           acc_nxt_0_i : in  STD_LOGIC;
           acc_nxt_1_i : in  STD_LOGIC;
           acc_nxt_2_i : in  STD_LOGIC;
           acc_nxt_3_i : in  STD_LOGIC;
           acc_nxt_4_i : in  STD_LOGIC;
           acc_nxt_5_i : in  STD_LOGIC);
end d_fs_6;

architecture d_fs_6_arch of d_fs_6 is

-- signal intern
signal acc_mask_0_s : std_logic;
signal acc_mask_1_s : std_logic;
signal acc_mask_2_s : std_logic;
signal acc_mask_3_s : std_logic;
signal acc_mask_4_s : std_logic;
signal acc_mask_5_s : std_logic;

signal acc_prv_s : std_logic;

begin

--data and valid signal
dat_nxt_o <= dat_prv_i;
val_nxt_o <= val_prv_i and acc_prv_s;

--------------------
--accept signal
--mask part
acc_mask_0_s <= acc_nxt_0_i or mask_sel_i(0);
acc_mask_1_s <= acc_nxt_1_i or mask_sel_i(1);
acc_mask_2_s <= acc_nxt_2_i or mask_sel_i(2);
acc_mask_3_s <= acc_nxt_3_i or mask_sel_i(3);
acc_mask_4_s <= acc_nxt_4_i or mask_sel_i(4);
acc_mask_5_s <= acc_nxt_5_i or mask_sel_i(5);

acc_prv_s <= acc_mask_0_s and acc_mask_1_s and acc_mask_2_s and acc_mask_3_s and acc_mask_4_s and acc_mask_5_s;
acc_prv_o <= acc_prv_s;


end d_fs_6_arch;

