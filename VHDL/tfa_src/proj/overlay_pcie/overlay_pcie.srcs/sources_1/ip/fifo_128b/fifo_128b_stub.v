// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (lin64) Build 1756540 Mon Jan 23 19:11:19 MST 2017
// Date        : Wed May  3 09:53:27 2017
// Host        : A12PC06 running 64-bit Ubuntu 16.04.2 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/redsuser/Documents/Travail_Bachelor/Repo/travail_bachelor/tfa_src/proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_128b/fifo_128b_stub.v
// Design      : fifo_128b
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7vx485tffg1761-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_1_3,Vivado 2016.4" *)
module fifo_128b(clk, rst, din, wr_en, rd_en, dout, full, empty, 
  data_count)
/* synthesis syn_black_box black_box_pad_pin="clk,rst,din[131:0],wr_en,rd_en,dout[131:0],full,empty,data_count[10:0]" */;
  input clk;
  input rst;
  input [131:0]din;
  input wr_en;
  input rd_en;
  output [131:0]dout;
  output full;
  output empty;
  output [10:0]data_count;
endmodule
