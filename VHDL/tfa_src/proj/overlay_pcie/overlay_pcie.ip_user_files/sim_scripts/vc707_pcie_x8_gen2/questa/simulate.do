onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib vc707_pcie_x8_gen2_opt

do {wave.do}

view wave
view structure
view signals

do {vc707_pcie_x8_gen2.udo}

run -all

quit -force
