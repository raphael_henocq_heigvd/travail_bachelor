vlib work
vlib riviera

vlib riviera/xil_defaultlib
vlib riviera/xpm

vmap xil_defaultlib riviera/xil_defaultlib
vmap xpm riviera/xpm

vlog -work xil_defaultlib  -sv2k12 \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"/opt/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xil_defaultlib -93 \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_core_top.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_clock.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_drp.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_eq.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_rate.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_reset.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_sync.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gtp_pipe_rate.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gtp_pipe_drp.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gtp_pipe_reset.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_user.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pipe_wrapper.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_qpll_drp.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_qpll_reset.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_qpll_wrapper.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_rxeq_scan.v" \

vcom -work xil_defaultlib -93 \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_axi_basic_rx_null_gen.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_axi_basic_rx_pipeline.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_axi_basic_rx.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_axi_basic_top.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_axi_basic_tx_pipeline.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_axi_basic_tx_thrtl_ctl.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_axi_basic_tx.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_7x.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_bram_7x.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_bram_top_7x.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_brams_7x.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_pipe_lane.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_pipe_misc.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_pipe_pipeline.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gt_rx_valid_filter_7x.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gt_top.vhd" \

vlog -work xil_defaultlib  -v2k5 \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gt_wrapper.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gt_common.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gtp_cpllpd_ovrd.v" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_gtx_cpllpd_ovrd.v" \

vcom -work xil_defaultlib -93 \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie_top.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/source/vc707_pcie_x8_gen2_pcie2_top.vhd" \
"../../../../../../src_pcie/ip/vc707_pcie_x8_gen2/sim/vc707_pcie_x8_gen2.vhd" \

vlog -work xil_defaultlib "glbl.v"

