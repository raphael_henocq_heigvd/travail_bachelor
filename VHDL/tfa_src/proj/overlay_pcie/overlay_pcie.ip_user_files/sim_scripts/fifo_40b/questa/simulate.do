onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fifo_40b_opt

do {wave.do}

view wave
view structure
view signals

do {fifo_40b.udo}

run -all

quit -force
