########################################################
# Simulation script
#######################################################


#create library work
vlib work

#Compile all the modules below it into the library work
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/overlay_pkg.vhd
#Online arithmetics add by RHE
# serializer_files compilation
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/get_in_MSB_order.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/signed_digit_serializer.vhd

# deserializer_files compilation
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/on_fly_conv.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/uc_signed_digit_deserializer.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/signed_digit_deserializer.vhd

# Fu_files compilation
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/uc_fu_all.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/online_adder.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/fa_adder.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/mult_comp.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/constant.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/uc_mult_comp.vhd
vcom -93 -check_synthesis -work work   ../src_overlay/online_arithm/fu_mult_comp.vhd

vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux_xto1.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux_xto1_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1tox.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1tox_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_flipflop.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_flipflop_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux2to1_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux2to1_bus.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux4to1_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux4to1_bus.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_reg.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_eb.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_buf.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_fs_4.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_fs_5.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_fs_6.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_mux.vhd
#vlog -64 -incr   -work work ./../proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_data/sim/fifo_data.v
vcom -93 -check_synthesis -quiet -work work ./../proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_data/synth/fifo_data.vhd
#vlog -64 -incr   -work work ./../proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_40b/sim/fifo_40b.v
vcom -93 -check_synthesis -quiet -work work ./../proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_40b/synth/fifo_40b.vhd
vcom -93 -check_synthesis -quiet -work work ./../proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_data_ext/synth/fifo_data_ext.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_fifo.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_fifo_ext.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/fu_all.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/cfg_cell_mux2fs.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/overlay_cell.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/overlay_NxM.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/priority_enc.vhd
#vlog -64 -incr   -work work ./../proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_38b_inout/sim/fifo_38b_inout.v
vcom -93 -check_synthesis -quiet -work work ./../proj/overlay_pcie/overlay_pcie.srcs/sources_1/ip/fifo_38b_inout/synth/fifo_38b_inout.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/d_fifo_38_io.vhd

vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1to4.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1to4_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1tocol.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1tocol_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1toline.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_1toline_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux_4to1_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux_colto1_bit.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux_lineto1_bit.vhd

vcom -93 -check_synthesis -quiet -work work ./../src_overlay/demux_data_in.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/mux_data_out.vhd
vcom -93 -check_synthesis -quiet -work work ./../src_overlay/data_in_out_overlay.vhd


#Compile the test-bench
vcom -93 -quiet tb_data_in_out_overlay.vhd

#Init simulation
vsim -t ps tb_data_in_out_overlay

# Add wave
source wave.do

#Start simulation
#run -all
run 5000ns
