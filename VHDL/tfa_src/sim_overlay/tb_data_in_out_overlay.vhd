--------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:   16:00:36 01/27/2016
-- Design Name:
-- Module Name:   D:/projet_hpa/test_overlay/ise_project/overlay_arch/sim/tb_data_in_out_overlay.vhd
-- Project Name:  overlay_arch
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: data_in_out_overlay
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

--include this library for file handling in VHDL.
library std;
use std.textio.all;
use ieee.std_logic_textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

use work.overlay_pkg.all;

ENTITY tb_data_in_out_overlay IS
END tb_data_in_out_overlay;

ARCHITECTURE behavior OF tb_data_in_out_overlay IS

    -- Component Declaration for the Unit Under Test (UUT)

    COMPONENT data_in_out_overlay
    PORT(
         clk_usr : IN  std_logic;
         clk_ovly : IN  std_logic;
         reset : IN  std_logic;
         cfg_x_mux_i : IN  cfg_mux_ovly_type;
         dat_prv_i : IN  std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
         val_prv_i : IN  std_logic;
         acc_prv_o : OUT  std_logic;
         dat_nxt_o : OUT  std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
         val_nxt_o : OUT  std_logic;
         acc_nxt_i : IN  std_logic;
         data_size_i : in std_logic_vector (data_size_length -1 downto 0)
        );
    END COMPONENT;


   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';
   signal cfg_x_mux_i : cfg_mux_ovly_type := (others => x"00000000");
   signal dat_prv_i : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0) := (others => '0');
   signal data_in : std_logic_vector(size_bus_ext-1 downto 0) := (others => '0');
   signal sel_in : std_logic_vector(bit_cmd_mux_io-1 downto 0) := (others => '0');
   signal val_prv_i : std_logic := '0';
   signal acc_nxt_i : std_logic := '0';

 	--Outputs
   signal acc_prv_o : std_logic;
   signal dat_nxt_o : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
   signal val_nxt_o : std_logic;
	signal data_out : std_logic_vector(size_bus_ext-1 downto 0);
	signal sel_out : std_logic_vector(bit_cmd_mux_io-1 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;

    --text files definitions
    file infile_d : text open read_mode is "input_data.txt";   --declare input file
    file outfile_d : text open write_mode is "output_data.txt";   --declare output file

    --
    type state_type is (S0, S1, S2, S3);
    signal state_rd_din     : state_type;
    signal state_wr_dout    : state_type;

    signal start_read_din : std_logic := '0';
    signal start_write_dout : std_logic := '0';
    signal data_input : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
    signal data_output : std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
    signal valid_input : std_logic;
    signal valid_output : std_logic;
    signal acc_input : std_logic;
    signal acc_output : std_logic;
    signal cpt_wr : std_logic_vector(7 downto 0);
    signal data_size_s : std_logic_vector(data_size_length -1 downto 0);

BEGIN

data_size_s <= std_logic_vector(to_unsigned(8, data_size_s'length));
sel_in <= data_input(size_bus_ext+bit_cmd_mux_io-1 downto size_bus_ext);	--only for display
data_in <= data_input(size_bus_ext-1 downto 0);	--only for display

dat_prv_i <= data_input;
val_prv_i <= valid_input;
acc_input <= acc_prv_o;

	-- Instantiate the Unit Under Test (UUT)
   U01: data_in_out_overlay PORT MAP (
          clk_usr => clk,
          clk_ovly => clk,
          reset => reset,
          cfg_x_mux_i => cfg_x_mux_i,
          dat_prv_i => dat_prv_i,
          val_prv_i => val_prv_i,
          acc_prv_o => acc_prv_o,
          dat_nxt_o => dat_nxt_o,
          val_nxt_o => val_nxt_o,
          acc_nxt_i => acc_nxt_i,
          data_size_i => data_size_s
        );

sel_out <= dat_nxt_o(size_bus_ext+bit_cmd_mux_io-1 downto size_bus_ext);
data_out <= dat_nxt_o(size_bus_ext-1 downto 0);

data_output <= dat_nxt_o;
valid_output <= val_nxt_o;
acc_nxt_i <= acc_output;

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '1';
		wait for clk_period/2;
		clk <= '0';
		wait for clk_period/2;
   end process;

	-- Read data from file
	read_din: process(clk,reset)
		variable line_in: line; -- Line buffers
		variable good: boolean;   -- Status of the read operations
		variable A_read: std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
	begin
		if reset= '1' then
			state_rd_din <= S0;
			A_read := (others => '0');
			data_input <= (others => '0');
			valid_input <= '0';
		elsif (clk'event and clk = '1') then

			case state_rd_din is
				when S0 =>
					A_read := (others => '0');
					data_input <= (others => '0');
					valid_input <= '0';
					if start_read_din='1' then
						state_rd_din <= S1;
					end if;

				when S1 =>
					if endfile(infile_d) then  -- Check EOF
						state_rd_din <= S2;
						valid_input <= '0';
					elsif acc_input='1' then
						readline(infile_d,line_in);     -- Read a line from the file
						while (line_in'length = 0) loop	-- Skip empty lines
							readline(infile_d,line_in);
						end loop;

						hread(line_in,A_read,good);     -- Read the A argument as hex value
						assert good
							 report "read_cfg: Text I/O read error"
							 severity ERROR;

						data_input <= A_read;
						valid_input <= '1';
					else
						valid_input <= '0';
					end if;

				when S2 =>

				when S3 =>

				when others =>
					state_rd_din <= S0;
			end case;
		end if;

	end process;

	-- Write data from file
	write_dout: process(clk,reset)
		variable line_out: line; -- Line buffers
		variable good: boolean;   -- Status of the write operations
		variable B_write: std_logic_vector(size_bus_ext+bit_cmd_mux_io-1 downto 0);
	begin
		if reset= '1' then
			state_wr_dout <= S0;
			B_write := (others => '0');
			acc_output <= '0';
			cpt_wr <= x"00";
		elsif (clk'event and clk = '1') then

			case state_wr_dout is
				when S0 =>
					B_write := (others => '0');
					acc_output <= '0';
					cpt_wr <= x"00";
					if start_write_dout='1' then
						state_wr_dout <= S1;
						acc_output <= '1';
					end if;

				when S1 =>
					acc_output <= '1';
					if valid_output='1' then
						B_write := data_output;
						hwrite(line_out,B_write,RIGHT,10);	-- write data in hexa.
						writeline(outfile_d,line_out);	-- write line to external file.
						cpt_wr <= cpt_wr + 1;
					end if;

					if cpt_wr=x"20" then  -- Check end of write
						state_wr_dout <= S2;
						file_close(outfile_d);	-- close the file
						acc_output <= '0';
					end if;


				when S2 =>

				when S3 =>

				when others =>
					state_wr_dout <= S0;
			end case;
		end if;

	end process;


   -- Stimulus process
   stim_proc: process
   begin
      -- hold reset state for 100 ns.
      reset <= '1';
		start_read_din <= '0';
		start_write_dout <= '0';
      wait for 108 ns;
      reset <= '0';
		wait for clk_period*20;
		cfg_x_mux_i(0)<= B"00000011_000_000_101_111_111_100_000_000";  -- col = 0 , ligne = 0
		cfg_x_mux_i(1)<= B"00000011_000_000_100_101_111_100_000_000";  -- col = 1 , ligne = 0
		cfg_x_mux_i(2)<= B"00000011_000_000_100_101_111_000_000_000";  -- col = 2 , ligne = 0
		cfg_x_mux_i(3)<= B"00000000_000_000_000_000_100_000_000_100";  -- col = 0 , ligne = 1
		cfg_x_mux_i(4)<= B"00000011_000_000_110_100_111_000_000_000";  -- col = 1 , ligne = 1
		cfg_x_mux_i(5)<= B"00000000_000_000_000_000_000_000_100_000";  -- col = 2 , ligne = 1
		cfg_x_mux_i(6)<= B"00000011_000_000_110_100_111_000_000_000";  -- col = 0 , ligne = 2
		cfg_x_mux_i(7)<= B"00000000_000_000_000_000_100_000_100_000";  -- col = 1 , ligne = 2
		cfg_x_mux_i(8)<= B"00000000_000_000_000_000_000_000_000_000";  -- col = 2 , ligne = 2

		start_read_din <= '1';
		wait for clk_period;
		start_read_din <= '0';

		wait for clk_period*10;
		start_write_dout <= '1';
		wait for clk_period;
		start_write_dout <= '0';


      wait;
   end process;

END;
