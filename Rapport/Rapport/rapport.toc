\select@language {french}
\contentsline {section}{\numberline {1}Cahier des charges}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}R\IeC {\'e}sum\IeC {\'e} du probl\IeC {\`e}me}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Cahier des charges}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Introduction}{5}{section.2}
\contentsline {subsection}{\numberline {2.1}Introduction au fonctionnement de TFA}{8}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Etat initial de l'overlay TFA}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Objectifs du travail de bachelor}{11}{subsection.2.3}
\contentsline {section}{\numberline {3}Online Arithmetics}{12}{section.3}
\contentsline {subsection}{\numberline {3.1}Principe g\IeC {\'e}n\IeC {\'e}ral}{12}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Le format signed digit}{13}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Conversion entier en SD}{14}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Conversion SD en entier}{14}{subsubsection.3.2.2}
\contentsline {subsection}{\numberline {3.3}Additionneur}{16}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Multiplicateur}{19}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Diviseur}{22}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Comparateurs}{23}{subsection.3.6}
\contentsline {section}{\numberline {4}R\IeC {\'e}alisation des convertisseurs et op\IeC {\'e}rateurs}{25}{section.4}
\contentsline {subsection}{\numberline {4.1}M\IeC {\'e}chansmes de synchronisation}{26}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Simulation des op\IeC {\'e}rateurs d'online arithmetics}{26}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Convertisseurs}{26}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}S\IeC {\'e}rialiseur : convertion entier en SD}{26}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}D\IeC {\'e}serialisateur : signed digit to integer}{28}{subsubsection.4.3.2}
\contentsline {subsection}{\numberline {4.4}Op\IeC {\'e}rateurs}{29}{subsection.4.4}
\contentsline {subsubsection}{\numberline {4.4.1}Additionneur/Soustracteur}{30}{subsubsection.4.4.1}
\contentsline {subsubsection}{\numberline {4.4.2}Multiplicateur}{30}{subsubsection.4.4.2}
\contentsline {paragraph}{\numberline {4.4.2.1}Multiplication MSB first}{31}{paragraph.4.4.2.1}
\contentsline {paragraph}{\numberline {4.4.2.2}Multiplication DSP}{31}{paragraph.4.4.2.2}
\contentsline {paragraph}{\numberline {4.4.2.3}Comparaison des deux m\IeC {\'e}thodes}{31}{paragraph.4.4.2.3}
\contentsline {subsubsection}{\numberline {4.4.3}Comparateurs}{32}{subsubsection.4.4.3}
\contentsline {subsubsection}{\numberline {4.4.4}S\IeC {\'e}lecteurs}{32}{subsubsection.4.4.4}
\contentsline {subsubsection}{\numberline {4.4.5}Constantes}{33}{subsubsection.4.4.5}
\contentsline {subsection}{\numberline {4.5}Verification du design}{33}{subsection.4.5}
\contentsline {section}{\numberline {5}Int\IeC {\'e}gration dans TFA}{35}{section.5}
\contentsline {subsection}{\numberline {5.1}Simulation}{36}{subsection.5.1}
\contentsline {section}{\numberline {6}Benchmarking}{36}{section.6}
\contentsline {section}{\numberline {7}Conclusion}{39}{section.7}
